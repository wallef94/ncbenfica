<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/img/{{ Auth::user()->avatar }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">LINKS</li>

            <li class="{{ Ekko::isActiveRoute('admin.dashboard') }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-fw fa-tachometer"></i> <span>Dashboard</span>
                </a>
            </li>

            <li class="{{ Ekko::areActiveRoutes(['admin.category.index', 'admin.category.create', 'admin.category.edit']) }}">
                <a href="{{ route('admin.category.index') }}">
                    <i class="fa fa-fw fa-files-o"></i> <span>Categorias de Conteúdo</span>
                </a>
            </li>

            <li class="{{ Ekko::areActiveRoutes(['admin.content.index', 'admin.content.create', 'admin.content.edit']) }}">
                <a href="{{ route('admin.content.index') }}">
                    <i class="fa fa-fw fa-file-text-o"></i> <span>Conteúdos</span>
                </a>
            </li>

            <li class="{{ Ekko::areActiveRoutes(['admin.team.index', 'admin.team.create', 'admin.team.edit']) }}">
                <a href="{{ route('admin.team.index') }}">
                    <i class="fa fa-fw fa-users"></i> <span>Equipas</span>
                </a>
            </li>

            <li class="{{ Ekko::areActiveRoutes(['admin.player.index', 'admin.player.create', 'admin.player.edit']) }}">
                <a href="{{ route('admin.player.index') }}">
                    <i class="fa fa-fw fa-user"></i> <span>Jogadores</span>
                </a>
            </li>

            <li class="{{ Ekko::areActiveRoutes(['admin.tournament.index', 'admin.tournament.create', 'admin.tournament.edit', 'admin.tournament.team']) }}">
                <a href="{{ route('admin.tournament.index') }}">
                    <i class="fa fa-fw fa-trophy"></i> <span>Campeonatos e Torneios</span>
                </a>
            </li>

            {{--<li class="treeview">--}}
                {{--<a href="#">--}}
                    {{--<span>Multilevel</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-fw fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}

                {{--<ul class="treeview-menu">--}}
                    {{--<li>--}}
                        {{--<a href="#">Link in level 2</a>--}}
                    {{--</li>--}}

                    {{--<li>--}}
                        {{--<a href="#">Link in level 2</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>