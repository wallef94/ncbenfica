<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ !empty($page_title) ? 'Admin | ' . $page_description : 'Admin' }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Compiled css -->
    <link href="{{ asset("/css/app.css") }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-red hold-transition sidebar-mini">
<div class="wrapper">

    <!-- Header -->
    @include('admin.layout._header')

    <!-- Sidebar -->
    @include('admin.layout._sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title or "Page Title" }}
                <small>{{ $page_description or null }}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                @if(!empty($breadcrumb))
                    @foreach($breadcrumb as $bread)
                        @if($bread['name'] == end($breadcrumb)['name'])
                        <li class="active">{{ $bread['name'] }}</li>
                        @else
                        <li><a href="{{ $bread['link'] }}">{{ $bread['name'] }}</a></li>
                        @endif
                    @endforeach
                @endif
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('admin.layout._footer')

</div><!-- ./wrapper -->

<!-- Compiled js -->
<script src="{{ asset ("/js/app.js") }}"></script>

<script>
    function inputMaskInit() {
        $('.only-date').inputmask("99/99/9999");
        $('.cellphone').inputmask("999 999 999");
        $('.phone').inputmask("999 999 999");
        $('.zipcode').inputmask("9999-999");
        $('.percent').inputmask("[9][9]9.[9][9] %");
    }

    function validateField(route, field, message) {
        $(field + ' + span').removeClass('glyphicon-ok glyphicon-remove').addClass('glyphicon-refresh fa-spin');

        $.get(route, function (data) {
            $(field + ' + span').removeClass('glyphicon-refresh fa-spin');

            if (data == 200) {
                $(field + ' + span + div').html('');
                $(field).parents('.has-feedback').removeClass('has-error').addClass('has-success');
                $(field + ' + span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            } else {
                $(field).parents('.has-feedback').removeClass('has-success').addClass('has-error');
                $(field + ' + span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                $(field + ' + span + div').html('<ul class="list-unstyled"><li>'+message+'</li></ul>');
            }
        });
    }

    function setFieldError(fields, message) {
        $.each(fields, function(index, field) {
            $(field).parents('.has-feedback').removeClass('has-success').addClass('has-error');
            $(field + ' + span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
            $(field + ' + span + div').html('<ul class="list-unstyled"><li>' + message + '</li></ul>');
        });
    }

    function setFieldSuccess(fields) {
        $.each(fields, function(index, field) {
            $(field + ' + span + div').html('');
            $(field).parents('.has-feedback').removeClass('has-error').addClass('has-success');
            $(field + ' + span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        });
    }

    function setFieldClear(fields) {
        $.each(fields, function(index, field) {
            $(field + ' + span + div').html('');
            $(field).parents('.has-feedback').removeClass('has-error has-success');
            $(field + ' + span').removeClass('glyphicon-remove glyphicon-ok');
        });
    }

    $(function() {
        $(document).ajaxStart(function() {
            Pace.restart();
        });

        inputMaskInit();
        $('select').select2();
    });

</script>
@stack('scripts')
<!-- Optionally, you can add Slimscroll and FastClick plugins. Both of these plugins are recommended to enhance the user experience -->
</body>
</html>