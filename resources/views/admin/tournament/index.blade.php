@extends('admin.layout.admin_template')

@section('content')
<div class='row'>
    <div class='col-xs-12'>
        <div class="box box-danger">
            <div class="box-header">
                <div class="col-xs-6 col-xs-offset-6 col-sm-4 col-sm-offset-8 col-md-3 col-md-offset-9 col-lg-2 col-lg-offset-10">
                    <a href="{{ route('admin.tournament.create') }}" class="btn btn-block btn-info">
                        <i class="fa fa-plus-circle"></i>
                        Criar novo
                    </a>
                </div>
            </div>

            <div class="box-body">
                <div class="table-list table-responsive">
                    <div class='col-xs-12'>
                        @if (count($tournaments) > 0)
                            <table class="table table-bordered table-striped table-hover" id="users-table">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Divisão</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($tournaments as $tournament)
                                    <tr>
                                        <td>{{ $tournament->name }}</td>
                                        <td>{{ $tournament->division->name or null }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn bg-navy dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                    Ações <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" style="min-width: initial;">
                                                    <li>
                                                        <a href="{{route('admin.tournament.team', ['id' => $tournament->id])}}">Equipas</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{route('admin.tournament.edit', ['id' => $tournament->id])}}">Editar</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{route('admin.tournament.destroy', ['id' => $tournament->id])}}">Apagar</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Nome</th>
                                    <th>Divisão</th>
                                    <th>Ações</th>
                                </tr>
                                </tfoot>
                            </table>

                            <div class="alert alert-info">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        A listar {{ $tournaments->count() }} de {{ $tournaments->total() }} equipas
                                    </div>

                                    <div class="col-xs-12  col-sm-6 text-right">
                                        Página {{ $tournaments->currentPage() }} de {{ $tournaments->lastPage() }}
                                    </div>
                                </div>
                            </div>

                            <div class="text-center">
                                {{ $tournaments->render() }}
                            </div>
                        @else
                            <div class="alert alert-danger">
                                <i class="fa fa-warning"></i>
                                Nenhuma equipa foi encontrada.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection