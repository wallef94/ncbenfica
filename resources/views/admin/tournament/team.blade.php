@extends('admin.layout.admin_template')

@section('content')
    <div class='row'>
        <div class='col-xs-12'>
            <div class="box box-danger">
                <div class="box-header with-border">
                    <div class="col-xs-6 col-xs-offset-6 col-sm-4 col-sm-offset-8 col-md-3 col-md-offset-9 col-lg-2 col-lg-offset-10">
                        <a href="{{ route('admin.tournament.index') }}" class="btn btn-block btn-primary">
                            <i class="fa fa-reply"></i>&nbsp;
                            Voltar
                        </a>
                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        {!! Form::open(['route' => ['admin.tournament.team.store', $tournament->id], 'data-toggle' => 'validator']) !!}
                        <div class="col-xs-12">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group has-feedback">
                                    {!! Form::label('team_id', 'Equipas') !!}
                                    <i class="fa fa-asterisk pull-right text-red" data-toggle="tooltip" title="Selecione para adicionar"></i>
                                    {!! Form::select('team_id', $teams, null, ['placeholder' => 'Selecione...','class' => 'form-control', 'required']) !!}
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                                <label class="empty">&nbsp;</label>
                                <button type="submit" class="btn btn-block bg-olive btn-mini">
                                    <i class="fa fa-plus"></i>&nbsp;
                                    Adicionar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-list table-responsive">
                                <div class="col-xs-12">
                                    @if (count($tournament->team) > 0)
                                        <table class="table table-bordered table-striped table-hover" id="users-table">
                                            <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Pontos</th>
                                                <th>Vitórias</th>
                                                <th>Derrotas</th>
                                                <th>Empates</th>
                                                <th>GM*</th>
                                                <th>GS*</th>
                                                <th>Ações</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($tournament->team as $team)
                                                <tr>
                                                    <td>{{ $team->name }}</td>
                                                    <td>{{ $team->pivot->points }}</td>
                                                    <td>{{ $team->pivot->victories }}</td>
                                                    <td>{{ $team->pivot->defeats }}</td>
                                                    <td>{{ $team->pivot->ties }}</td>
                                                    <td>{{ $team->pivot->goals_scored }}</td>
                                                    <td>{{ $team->pivot->goals_against }}</td>
                                                    <td>
                                                        <a href="{{ route('admin.tournament.team.destroy', ['id' => $tournament->id, 'team_id' => $team->id]) }}" class="btn btn-danger disabled">
                                                            Remover
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Pontos</th>
                                                <th>Vitórias</th>
                                                <th>Derrotas</th>
                                                <th>Empates</th>
                                                <th>GM*</th>
                                                <th>GS*</th>
                                                <th>Ações</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        <strong>GM = Golos Marcados / GS = Golos Sofridos</strong>
                                    @else
                                        <div class="alert alert-danger">
                                            <i class="fa fa-warning"></i>
                                            Nenhuma equipa foi encontrada.
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection