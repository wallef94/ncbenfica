@extends('admin.layout.admin_template')

@section('content')
    <div class='row'>
        <div class='col-xs-12'>
            <div class="box box-danger">
                <div class="box-header with-border">
                    <div class="col-xs-6 col-xs-offset-6 col-sm-4 col-sm-offset-8 col-md-3 col-md-offset-9 col-lg-2 col-lg-offset-10">
                        <a href="{{ route('admin.tournament.index') }}" class="btn btn-block btn-primary">
                            <i class="fa fa-reply"></i>&nbsp;
                            Voltar
                        </a>
                    </div>
                </div>

                {!! Form::open(['route' => 'admin.tournament.store', 'data-toggle' => 'validator']) !!}
                <div class="box-body">
                    @include('admin.tournament._form')
                </div>

                <div class="box-footer">
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 row-btn">
                        <a href="{{ route('admin.tournament.index') }}" class="btn btn-block btn-danger btn-lg">
                            <i class="fa fa-times"></i>&nbsp;
                            Cancelar
                        </a>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 row-btn col-xs-offset-0 col-sm-offset-0 col-md-offset-4 col-lg-offset-4">
                        <button type="submit" class="btn btn-block bg-olive btn-lg">
                            <i class="fa fa-floppy-o"></i>&nbsp;
                            Adicionar
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection