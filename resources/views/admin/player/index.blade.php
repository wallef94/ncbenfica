@extends('admin.layout.admin_template')

@section('content')
    <div class='row'>
        <div class='col-xs-12'>
            <div class="box box-danger">
                <div class="box-header">
                    <div class="col-xs-6 col-xs-offset-6 col-sm-4 col-sm-offset-8 col-md-3 col-md-offset-9 col-lg-2 col-lg-offset-10">
                        <a href="{{ route('admin.player.create') }}" class="btn btn-block btn-info">
                            <i class="fa fa-plus-circle"></i>
                            Criar novo
                        </a>
                    </div>
                </div>

                <div class="box-body">
                    <div class="table-list table-responsive">
                        <div class='col-xs-12'>
                            @if (count($players) > 0)
                                <table class="table table-bordered table-striped table-hover" id="users-table">
                                    <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Equipa</th>
                                        <th>Data de Nascimento</th>
                                        <th>Camisola</th>
                                        <th>Ações</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($players as $player)
                                        <tr>
                                            <td>{{ $player->name }}</td>
                                            <td>{{ $player->team->name or null }}</td>
                                            <td>{{ $player->date_of_birth }}</td>
                                            <td>{{ $player->shirt }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn bg-navy dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Ações <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" style="min-width: initial;">
                                                        <li>
                                                            <a href="{{route('admin.player.edit', ['id' => $player->id])}}">Editar</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{route('admin.player.destroy', ['id' => $player->id])}}">Apagar</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Equipa</th>
                                        <th>Data de Nascimento</th>
                                        <th>Camisola</th>
                                        <th>Ações</th>
                                    </tr>
                                    </tfoot>
                                </table>

                                <div class="alert alert-info">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            A listar {{ $players->count() }} de {{ $players->total() }} jogadores
                                        </div>

                                        <div class="col-xs-12  col-sm-6 text-right">
                                            Página {{ $players->currentPage() }} de {{ $players->lastPage() }}
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    {{ $players->render() }}
                                </div>
                            @else
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    Nenhum jogador foi encontrada.
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection