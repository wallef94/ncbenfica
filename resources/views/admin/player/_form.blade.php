<div class="col-xs-12 col-sm-12 col-md-6">
    <div class="form-group has-feedback">
        <i class="fa fa-asterisk pull-right text-red" data-toggle="tooltip" title="Campo obrigatório"></i>
        {!! Form::label('name', 'Nome do Jogador') !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6">
    <div class="form-group has-feedback">
        {!! Form::label('team_id', 'Equipa') !!}
        <i class="fa fa-asterisk pull-right text-red" data-toggle="tooltip" title="Campo obrigatório"></i>
        {!! Form::select('team_id', $teams, null, ['placeholder' => 'Selecione...','class' => 'form-control', 'required']) !!}
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-4">
    <div class="form-group has-feedback">
        <i class="fa fa-asterisk pull-right text-red" data-toggle="tooltip" title="Campo obrigatório"></i>
        {!! Form::label('date_of_birth', 'Data de Nascimento') !!}
        {!! Form::text('date_of_birth', null, ['class' => 'form-control only-date', 'required']) !!}
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-4">
    <div class="form-group has-feedback">
        {!! Form::label('first_year_korfball', 'Primeiro ano no Corfebol') !!}
        {!! Form::text('first_year_korfball', null, ['class' => 'form-control only-date']) !!}
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-4">
    <div class="form-group has-feedback">
        {!! Form::label('first_year_ncb', 'Primeiro ano no NCB') !!}
        {!! Form::text('first_year_ncb', null, ['class' => 'form-control only-date']) !!}
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6">
    <div class="form-group has-feedback">
        {!! Form::label('previous_clubs', 'Equipas Anteriores') !!}
        {!! Form::text('previous_clubs', null, ['class' => 'form-control']) !!}
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6">
    <div class="form-group has-feedback">
        {!! Form::label('national_teams', 'Seleções Nacionais') !!}
        {!! Form::text('national_teams', null, ['class' => 'form-control']) !!}
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6">
    <div class="form-group has-feedback">
        <i class="fa fa-asterisk pull-right text-red" data-toggle="tooltip" title="Campo obrigatório"></i>
        {!! Form::label('gender', 'Sexo') !!}
        {!! Form::select('gender', array('M' => 'Masculino', 'F' => 'Feminino'), null, ['class' => 'form-control', 'required'])  !!}
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6">
    <div class="form-group has-feedback">
        {!! Form::label('shirt', 'Camisola') !!}
        {!! Form::text('shirt', null, ['class' => 'form-control']) !!}
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>