<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    <div class="form-group has-feedback">
        <i class="fa fa-asterisk pull-right text-red" data-toggle="tooltip" title="Campo obrigatório"></i>
        {!! Form::label('name', 'Nome da categoria') !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    <div class="form-group has-feedback">
        {!! Form::label('category_id', 'Categoria Superior') !!}
        {!! Form::select('category_id', $categories, null, ['placeholder' => 'Selecione...','class' => 'form-control']) !!}
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>