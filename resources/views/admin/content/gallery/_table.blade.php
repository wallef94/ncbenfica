<div class="box-body">
    <div class="table-list table-responsive">
        <div class='col-xs-12'>
            @if (count($gallery) > 0)
                <table class="table table-bordered table-striped table-hover" id="gallery-table">
                    <thead>
                    <tr>
                        <th>Imagem</th>
                        <th>Label</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($gallery as $image)
                        <tr>
                            <td width="150">
                                <img class="view img-responsive img-thumbnail pointer" src="{{ asset("/uploads/cms/content/{$content_id}/gallery/thumbs/{$image->name}")}}" alt="Imagem da Galeria:" class="img-responsive">
                            </td>
                            <td data-label="{{$image->label}}" id="label{{ $image->id }}">{{$image->label}}</td>
                            <td width="130">
                                <div class="btn-group">
                                    <button type="button" class="btn bg-navy dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Ações <span class="caret"></span>
                                    </button>

                                    <ul class="dropdown-menu" role="menu" style="min-width: initial;">
                                        <li>
                                            <a href="#" class="edit" data-action="{{$image->id}}">Editar</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.content.gallery.destroy', [$content_id, $image->id]) }}">Apagar</a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Imagem</th>
                        <th>Label</th>
                        <th>Ações</th>
                    </tr>
                    </tfoot>
                </table>

                <div class="photoswipe hide" id="gallery">
                    @foreach ($gallery as $image)
                        <figure>
                            <a class="thumbnail" href="{{ asset($image->path) }}" data-size="{{ $image->dimensions }}">
                                <img src="{{ asset("/uploads/cms/content/{$content_id}/gallery/thumbs/{$image->name}")}}" alt="Imagem da Galeria:" class="img-responsive">
                            </a>
                        </figure>
                    @endforeach
                </div>

                <div class="alert alert-info">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            A listar {{ $gallery->count() }} de {{ $gallery->total() }} imagens
                        </div>

                        <div class="col-xs-12  col-sm-6 text-right">
                            Página {{ $gallery->currentPage() }} de {{ $gallery->lastPage() }}
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    {{ $gallery->render() }}
                </div>
            @else
                <div class="alert alert-danger">
                    <i class="fa fa-warning"></i>
                    Nenhuma imagem foi encontrada.
                </div>
            @endif
        </div>
    </div>
</div>
