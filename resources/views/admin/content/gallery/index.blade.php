@extends('admin.layout.admin_template')

@section('content')
    <div class='row'>
        <div class='col-xs-12'>
            <div class="box box-danger">
                <div class="box-header">
                    <div class="col-xs-6 col-xs-offset-6 col-sm-4 col-sm-offset-8 col-md-3 col-md-offset-9 col-lg-2 col-lg-offset-10">
                        <a href="{{  route('admin.content.index') }}" class="btn btn-block btn-primary">
                            <i class="fa fa-reply"></i>&nbsp;
                            Voltar
                        </a>
                    </div>
                </div>

                <div class="box-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li><a href="{{  route('admin.content.edit', $content_id) }}">Dados do Conteúdo</a></li>
                            <li><a href="{{ route('admin.content.banner.index', $content_id) }}">Banner do Conteúdo</a></li>
                            <li class="active"><a href="javascript:void(0);">Galeria de Imagens</a></li>
                            {{--<li><a href="{{  route('admin.content.files.index', $content_id) }}">Ficheiros</a></li>--}}
                        </ul>

                        <div class="tab-content">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="callout callout-info">
                                        <p>Formatos suportados: JPG, JPEG, PNG, GIF</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <form action="{{  route('admin.content.gallery.store', $content_id) }}" method="POST" class="dropzone" id="my-dropzone">
                                        <div class="dz-message" data-dz-message>
                                            <h3>Arraste as imagens ou clique para adicionar</h3>
                                        </div>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Galeria</h3>
                            </div>
                            @include('admin.content.gallery._table')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.content.gallery._photoswipe')
@endsection

@push('scripts')
<script src="{{ asset("js/photoswipe-template.js") }}" type="text/javascript"></script>
<script>
$(function() {
    $('.view').on('click', function() {
        $('#gallery figure:first-child a').click();
    });

    Dropzone.options.myDropzone = {
        autoProcessQueue: true,
        uploadMultiple: true,
        paramName: 'image',
        maxFilesize: 10,
        maxFiles: 10,
        parallelUploads: 10,
        acceptedFiles: ".jpg, .jpeg, .png, .gif",
        addRemoveLinks: true,
        init: function ()
        {
            var myDropzone = this;

//                this.on("complete", function (file) {
//                   myDropzone.removeFile(file);
//                });

            this.on("success", function() {
                window.location = window.location;
            });
        }
    };

    $(".gallery figure span").on("click", function () {
        var url = $(this).data("remove");

        $.get( url, function() {
            window.location = window.location;
        });

        return false;
    });

    $('.edit').on('click', function (e) {
        e.preventDefault();

        var id = $(this).data("action");
        var td = "#label"+id;
        var label = $(td).html();

        $('#gallery-table tbody tr td:nth-child(2)').each(function() {
            $(this).html($(this).data('label'));
        });

        $(td).html('<form><div class="input-group"><input class="change-label form-control" type="text" name="label" value="'+label+'"><span class="input-group-addon" id="basic-addon2"><a href="#"><i class="fa fa-check"></i></a></span></div><input type="hidden" name="_token" value="{{ csrf_token() }}"></form>');

        $('#gallery-table form').submit(function() {
            $('#gallery-table form a').click();
            return false;
        });

        $(td + " a").on('click', function() {
            var data = $(td + " form").serialize();
            var route = "{{  route('admin.content.gallery.update', [$content_id, 0]) }}";
            route = route.replace("0", id);

            $.post(route, data, function(){
                location.reload();
            });

            return false;
        });
    });
});
</script>
@endpush