@extends('admin.layout.admin_template')

@section('content')
    <div class='row'>
        <div class='col-xs-12'>
            <div class="box box-danger">
                <div class="box-header with-border">
                    <div class="col-xs-6 col-xs-offset-6 col-sm-4 col-sm-offset-8 col-md-3 col-md-offset-9 col-lg-2 col-lg-offset-10">
                        <a href="{{  route('admin.content.index') }}" class="btn btn-block btn-primary">
                            <i class="fa fa-reply"></i>&nbsp;
                            Voltar
                        </a>
                    </div>
                </div>

                <div class="box-body">
                    <div class="nav-tabs-custom clearfix">
                        <ul class="nav nav-tabs">
                            <li><a href="{{  route('admin.content.edit', $content->id) }}">Dados do Conteúdo</a></li>
                            <li class="active"><a href="javascript:void(0);">Banner do Conteúdo</a></li>
                            <li><a href="{{ route('admin.content.gallery.index', $content->id) }}">Galeria de Imagens</a></li>
                            {{--<li><a href="{{ route('admin.content.files.index', $content->id) }}">Ficheiros</a></li>--}}
                        </ul>

                        <div class="tab-content">
                            @if(empty($banner))
                                {!! Form::open(['route' => ['admin.content.banner.store', $content->id], 'files' => true, 'id' => 'form-upload']) !!}
                                <div class="form-group col-xs-4">
                                    <label>&nbsp;</label>
                                    <input type="file" id="banner" name="banner">
                                </div>
                                {!! Form::close() !!}
                            @else
                                <div class="callout callout-info">
                                    <p>Este conteúdo já tem um banner principal. Apague o mesmo para que possa inserir um novo</p>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    @if(!empty($banner))
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row-btn text-right">
                            <a href="{{  route('admin.content.banner.destroy', $content->id) }}" class="btn btn-danger btn-lg">
                                <i class="fa fa-trash"></i>&nbsp;
                                Apagar
                            </a>
                        </div>
                        <img class="img-responsive" src="{{ asset("/uploads/cms/content/{$content->id}/banner/{$banner}") }}" id="target">
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $(function () {
        $('#banner').on('change', function () {
            $('#form-upload').submit();
        });
    });
</script>
@endpush