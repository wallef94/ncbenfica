@extends('admin.layout.admin_template')

@section('content')
    <div class='row'>
        <div class='col-xs-12'>
            <div class="box box-danger">
                <div class="box-header">
                    <div class="col-xs-6 col-xs-offset-6 col-sm-4 col-sm-offset-8 col-md-3 col-md-offset-9 col-lg-2 col-lg-offset-10">
                        <a href="{{ route('admin.content.create') }}" class="btn btn-block btn-info">
                            <i class="fa fa-plus-circle"></i>
                            Criar novo
                        </a>
                    </div>
                </div>

                <div class="box-body">
                    <div class="table-list table-responsive">
                        <div class='col-xs-12'>
                            @if (count($contents) > 0)
                                <table class="table table-bordered table-striped table-hover" id="users-table">
                                    <thead>
                                    <tr>
                                        <th>Título</th>
                                        <th>Categoria</th>
                                        <th>Ações</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($contents as $content)
                                        <tr>
                                            <td>{{ $content->title }}</td>
                                            <td>{{ $content->category->name or null }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn bg-navy dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Ações <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" style="min-width: initial;">
                                                        <li>
                                                            <a href="{{route('admin.content.edit', ['id' => $content->id])}}">Editar</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{route('admin.content.destroy', ['id' => $content->id])}}">Apagar</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Título</th>
                                        <th>Categoria</th>
                                        <th>Ações</th>
                                    </tr>
                                    </tfoot>
                                </table>

                                <div class="alert alert-info">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            A listar {{ $contents->count() }} de {{ $contents->total() }} conteúdos
                                        </div>

                                        <div class="col-xs-12  col-sm-6 text-right">
                                            Página {{ $contents->currentPage() }} de {{ $contents->lastPage() }}
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    {{ $contents->render() }}
                                </div>
                            @else
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    Nenhum conteúdo foi encontrado.
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection