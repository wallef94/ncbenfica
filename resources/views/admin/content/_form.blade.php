<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group has-feedback">
        {!! Form::label('category_id', 'Categoria') !!}
        <i class="fa fa-asterisk pull-right text-red" data-toggle="tooltip" title="Campo obrigatório"></i>
        {!! Form::select('category_id', $categories, null, ['placeholder' => 'Selecione...','class' => 'form-control']) !!}
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6">
    <div class="form-group has-feedback">
        <i class="fa fa-asterisk pull-right text-red" data-toggle="tooltip" title="Campo obrigatório"></i>
        {!! Form::label('title', 'Título do Conteúdo') !!}
        {!! Form::text('title', null, ['class' => 'form-control', 'required']) !!}
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>

@if(!empty($content))
    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="form-group has-feedback">
            {!! Form::label('created_at', 'Data do Conteúdo') !!}
            {!! Form::text('created_at', Carbon\Carbon::parse($content->created_at)->format('d/m/Y H:i') , ['class' => 'form-control datepicker datetime', 'disabled']) !!}
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="form-group has-feedback">
            {!! Form::label('updated_at', 'Última Atualização') !!}
            {!! Form::text('updated_at', Carbon\Carbon::parse($content->updated_at)->format('d/m/Y H:i'), ['class' => 'form-control datepicker datetime', 'disabled']) !!}
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>
        </div>
    </div>
@endif

<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group has-feedback">
        {!! Form::label('summary', 'Resumo do Conteúdo') !!}
        {!! Form::textarea('summary', null, ['class' => 'form-control ckeditor', 'id' => 'summary']) !!}
        <div class="help-block with-errors"></div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group has-feedback">
        {!! Form::label('body', 'Texto do Conteúdo') !!}
        {!! Form::textarea('body', null, ['class' => 'form-control ckeditor', 'id' => 'description']) !!}
        <div class="help-block with-errors"></div>
    </div>
</div>

@push('scripts')
<script src="{{ asset('/js/ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace('summary',
        {
            customConfig : 'config-limited.js',
            toolbarGroups: [
                {"name":"basicstyles","groups":["basicstyles"]},
                {"name":"links","groups":["links"]},
                {"name":"paragraph","groups":["align"]},
            ],
            removeButtons: 'Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
        }
    );

    CKEDITOR.replace('description',
        {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',

            customConfig : 'config-default.js',
            toolbarGroups: [
                {"name":"document","groups":["mode"]},
                {"name":"basicstyles","groups":["basicstyles"]},
                {"name":"links","groups":["links"]},
                {"name":"paragraph","groups":["list","blocks","align"]},
                {"name":"insert","groups":["insert"]},
                {"name":"styles","groups":["styles"]},
            ],
            removeButtons: 'Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
        }
    );
</script>

@endpush