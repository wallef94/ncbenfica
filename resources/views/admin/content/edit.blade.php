@extends('admin.layout.admin_template')

@section('content')
    <div class='row'>
        <div class='col-xs-12'>
            <div class="box box-danger">
                <div class="box-header with-border">
                    <div class="col-xs-6 col-xs-offset-6 col-sm-4 col-sm-offset-8 col-md-3 col-md-offset-9 col-lg-2 col-lg-offset-10">
                        <a href="{{ route('admin.content.index') }}" class="btn btn-block btn-primary">
                            <i class="fa fa-reply"></i>&nbsp;
                            Voltar
                        </a>
                    </div>
                </div>

                {!! Form::model($content, ['route' => ['admin.content.update', $content->id], 'data-toggle' => 'validator', 'method' => 'put']) !!}
                <div class="box-body">
                    <div class="nav-tabs-custom clearfix">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="javascript:void(0);">Dados do Conteúdo</a></li>
                            <li><a href="{{ route('admin.content.banner.index', $content->id) }}">Banner do Conteúdo</a></li>
                            <li><a href="{{ route('admin.content.gallery.index', $content->id) }}">Galeria de Imagens</a></li>
                            {{--<li><a href="{{ route('admin.content.files.index', $content->id) }}">Ficheiros</a></li>--}}
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active clearfix">
                                @include('admin.content._form')
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 row-btn">
                        <a href="{{ route('admin.content.index') }}" class="btn btn-block btn-danger btn-lg">
                            <i class="fa fa-times"></i>&nbsp;
                            Cancelar
                        </a>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 row-btn col-xs-offset-0 col-sm-offset-0 col-md-offset-4 col-lg-offset-4">
                        <button type="submit" class="btn btn-block bg-olive btn-lg">
                            <i class="fa fa-floppy-o"></i>&nbsp;
                            Gravar
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection