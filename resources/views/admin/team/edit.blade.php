@extends('admin.layout.admin_template')

@section('content')
    <div class='row'>
        <div class='col-xs-12'>
            <div class="box box-danger">
                <div class="box-header with-border">
                    <div class="col-xs-6 col-xs-offset-6 col-sm-4 col-sm-offset-8 col-md-3 col-md-offset-9 col-lg-2 col-lg-offset-10">
                        <a href="{{ route('admin.team.index') }}" class="btn btn-block btn-primary">
                            <i class="fa fa-reply"></i>&nbsp;
                            Voltar
                        </a>
                    </div>
                </div>

                {!! Form::model($team, ['route' => ['admin.team.update', $team->id], 'data-toggle' => 'validator', 'method' => 'put']) !!}
                <div class="box-body">
                    @include('admin.team._form')
                </div>

                <div class="box-footer">
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 row-btn">
                        <a href="{{ route('admin.team.index') }}" class="btn btn-block btn-danger btn-lg">
                            <i class="fa fa-times"></i>&nbsp;
                            Cancelar
                        </a>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 row-btn col-xs-offset-0 col-sm-offset-0 col-md-offset-4 col-lg-offset-4">
                        <button type="submit" class="btn btn-block bg-olive btn-lg">
                            <i class="fa fa-floppy-o"></i>&nbsp;
                            Gravar
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

        @if(!empty($team->avatar))
        <div class="col-xs-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h4>Altere aqui a sua logo</h4>
                </div>
                <div class="box-body">
                    {!! Form::open(['route' => ['admin.team.updateAvatar', $team->id], 'data-toggle' => 'validator', 'files' => true, 'id' => 'upload-form']) !!}
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group has-feedback">
                            <img src="{{ ($team->avatar == 'default-team.png') ? "/img/{$team->avatar}" : asset("uploads/cms/team/{$team->id}/avatar/{$team->avatar}") }}" class="img-lg" alt="team logo">
                            <input type="file" id="avatar" name="avatar">
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            @endif
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $(function () {
        $('#avatar').on('change', function () {
            $('#upload-form').submit();
        });
    });
</script>
@endpush