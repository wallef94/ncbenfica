@extends('site.layout.site_template')

@section('content')
@include('site.europashield.standings._page_heading')

<!-- Content
    ================================================== -->
<div class="site-content">
    <div class="container">

        <div class="row">
            <!-- Posts -->
            <div class="col-md-9 col-md-push-3">
                @include('site.europashield._partials._tgoo_banner')
                <!-- Team Standings -->
                <div class="card card--has-table">
                    <div class="card__header">
                        <h4>Group A</h4>
                    </div>
                    <div class="card__content">
                        <div class="table-responsive">
                            <table class="table table-hover table-standings table-standings--full">
                                <thead>
                                <tr>
                                    <th class="team-standings__team">Teams</th>
                                    <th class="team-standings__pos">P</th>
                                    <th class="team-standings__win">W</th>
                                    <th class="team-standings__lose">L</th>
                                    <th class="team-standings__pct">T</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($group_a as $team)
                                <tr>
                                    <td class="team-standings__team">
                                        <div class="team-meta">
                                            <figure class="team-meta__logo">
                                                <img src="{{ ($team->avatar == 'default-team.png') ? "/img/{$team->avatar}" : asset("uploads/cms/team/{$team->id}/avatar/{$team->avatar}") }}" alt="Team Logo: {{ $team->name }}" />
                                            </figure>
                                            <div class="team-meta__info">
                                                <h6 class="team-meta__name">{{ $team->initials }}</h6>
                                                <span class="team-meta__place">{{ $team->name }}</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="team-standings__pos">{{ $team->points }}</td>
                                    <td class="team-standings__win">{{ $team->victories }}</td>
                                    <td class="team-standings__lose">{{ $team->defeats }}</td>
                                    <td class="team-standings__pct">{{ $team->ties }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Team Standings / End -->

                <!-- Team Standings -->
                <div class="card card--has-table">
                    <div class="card__header">
                        <h4>Group B</h4>
                    </div>
                    <div class="card__content">
                        <div class="table-responsive">
                            <table class="table table-hover table-standings table-standings--full">
                                <thead>
                                <tr>
                                    <th class="team-standings__team">Teams</th>
                                    <th class="team-standings__pos">P</th>
                                    <th class="team-standings__win">W</th>
                                    <th class="team-standings__lose">L</th>
                                    <th class="team-standings__pct">T</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($group_b as $team)
                                    <tr>
                                        <td class="team-standings__team">
                                            <div class="team-meta">
                                                <figure class="team-meta__logo">
                                                    <img src="{{ ($team->avatar == 'default-team.png') ? "/img/{$team->avatar}" : asset("uploads/cms/team/{$team->id}/avatar/{$team->avatar}") }}" alt="Team Logo: {{ $team->name }}" />
                                                </figure>
                                                <div class="team-meta__info">
                                                    <h6 class="team-meta__name">{{ $team->initials }}</h6>
                                                    <span class="team-meta__place">{{ $team->name }}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="team-standings__pos">{{ $team->points }}</td>
                                        <td class="team-standings__win">{{ $team->victories }}</td>
                                        <td class="team-standings__lose">{{ $team->defeats }}</td>
                                        <td class="team-standings__pct">{{ $team->ties }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Team Standings / End -->

                <a href="http://ncbenfica.pt/europashield2017/schedule#playoffs" class="btn btn-success btn-lg">GO TO PLAYOFFS</a>
            </div>
            <!-- Posts / End -->
            @include('site.europashield._partials._sidebar')
        </div>
    </div>
</div>
@endsection