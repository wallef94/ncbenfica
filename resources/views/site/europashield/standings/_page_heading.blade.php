<!-- Page Heading
    ================================================== -->
<div class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h1 class="page-heading__title">EUROPA SHIELD <span class="highlight">2017</span></h1>
                <ol class="page-heading__breadcrumb breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li><a href="{{ route('site.europashield.home') }}">Europa Shield</a></li>
                    <li class="active">Standings</li>
                </ol>
            </div>
        </div>
    </div>
</div>