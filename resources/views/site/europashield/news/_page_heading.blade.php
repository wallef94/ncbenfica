<!-- Page Heading
    ================================================== -->
<div class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h1 class="page-heading__title">EUROPA SHIELD <span class="highlight">2017</span></h1>
                <ol class="page-heading__breadcrumb breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li><a href="{{ route('site.europashield.home') }}">Europa Shield</a></li>
                    @if(\Request::route()->getName() == 'site.europashield.content.list')
                    <li class="active">{{ $category->name }}</li>
                    @elseif(\Request::route()->getName() == 'site.europashield.content.show')
                    <li>
                        <a title="{{ $category->name }}" href="{{ route('site.europashield.content.list', [$category->id, $category->slug]) }}">
                            {{ $category->name }}
                        </a>
                    </li>
                    <li class="active">{{ $news->title }}</li>
                    @endif
                </ol>
            </div>
        </div>
    </div>
</div>