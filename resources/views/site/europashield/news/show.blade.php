@extends('site.layout.site_template')

@section('content')
    @include('site.europashield.news._page_heading')

    <!-- Content
    ================================================== -->
    <div class="site-content">
        <div class="container">

            <div class="row">
                <div class="col-md-9 col-md-push-3">
                    <!-- Article -->
                    <article class="card card--lg post post--single">
                        @if(!empty($news->banner))
                        <figure class="post__thumbnail">
                            <img src="{{ asset("/uploads/cms/content/{$news->id}/banner/{$news->banner}") }}" alt="Banner: {{ $news->title }}" />
                        </figure>
                        @endif

                        <div class="card__content">
                            <div class="post__category">
                                <span class="label posts__cat-label">{{ $news->category->name or null }}</span>
                            </div>

                            <header class="post__header">
                                <h2 class="post__title">{{ $news->title }}</h2>
                                <ul class="post__meta meta">
                                    <li class="meta__item meta__item--date"><time datetime="{{ $news->created_at }}">{{ Carbon\Carbon::parse($news->created_at)->diffForHumans() }}</time></li>
                                </ul>
                            </header>

                            <div class="post__content">
                                {!! $news->body !!}
                            </div>

                            @if(!empty($gallery))
                                <div class="album container-fluid">
                                    <div class="row">
                                        @foreach($gallery as $image)
                                            <div class="album__item col-xs-6 col-sm-4">
                                                <div class="album__item-holder">
                                                    <a href="{{ asset($image->path)}}" class="album__item-link mp_gallery">
                                                        <figure class="album__thumb">
                                                            <img src="{{ asset("/uploads/cms/content/{$news->id}/gallery/thumbs/{$image->name}")}}" alt="{{ $image->label }}" />
                                                        </figure>
                                                        <div class="album__item-desc">
                                                            <h4 class="album__item-title">{{ $image->label }}</h4>
                                                            <span class="album__item-btn-fab btn-fab btn-fab--clean"></span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                    </article>
                    <!-- Article / End -->
                </div>
                @include('site.europashield._partials._sidebar')
            </div>
        </div>
    </div>
@endsection