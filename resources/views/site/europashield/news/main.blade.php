@extends('site.layout.site_template')

@section('content')
@include('site.europashield.news._page_heading')

<!-- Content
    ================================================== -->
<div class="site-content">
    <div class="container">

        <div class="row">
            <div class="col-md-9 col-md-push-3">
            @include('site.europashield._partials._tgoo_banner')
            @if(count($news) > 0)
                <div class="posts posts--cards post-grid post-grid--masonry row">
                    @foreach($news as $n)
                        <div class="post-grid__item col-sm-6">
                            <div class="posts__item posts__item--card posts__item--category-1 card">
                                <figure class="posts__thumb">
                                    <div class="posts__cat">
                                        <span class="label posts__cat-label">{{ $n->category->name or null }}</span>
                                    </div>
                                    @if(!empty($n->banner))
                                        <a href="{{ route('site.europashield.content.show', [$n->category_id, $n->category->slug, $n->slug]) }}">
                                            <img src="{{ asset("/uploads/cms/content/{$n->id}/banner/{$n->banner}") }}" alt="Banner" />
                                        </a>
                                    @endif
                                </figure>
                                <div class="posts__inner card__content">
                                    <a href="{{ route('site.europashield.content.show', [$n->category_id, $n->category->slug, $n->slug]) }}" class="posts__cta"></a>
                                    <time datetime="{{ $n->created_at }}" class="posts__date">{{ Carbon\Carbon::parse($n->created_at)->diffForHumans() }}</time>
                                    <h6 class="posts__title">
                                        <a href="{{ route('site.europashield.content.show', [$n->category_id, $n->category->slug, $n->slug]) }}">
                                            {{ $n->title }}
                                        </a>
                                    </h6>
                                    <div class="posts__excerpt">
                                        {{ str_limit(strip_tags($n->body), 200) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="align-center">
                    {{ $news->links() }}
                </div>
            @else
                <div class="alert alert-warning">Sorry! There are no content available for this category at the moment.</div>
            @endif
            </div>
            @include('site.europashield._partials._sidebar')
        </div>
    </div>
</div>
@endsection