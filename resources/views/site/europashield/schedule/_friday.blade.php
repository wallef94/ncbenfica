<div class="card card--has-table">
    <div class="card__header">
        <h4>Schedule Friday, Jan 27th</h4>
    </div>
    <div class="card__content">
        <div class="table-responsive">
            <table class="table table-hover team-schedule team-schedule--full">
                <thead>
                <tr>
                    <th class="team-schedule__date">Date</th>
                    <th class="team-schedule__versus">Team</th>
                    <th class="team-schedule__date">Score</th>
                    <th class="team-schedule__versus">Versus</th>
                    <th class="team-schedule__time">Time</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="team-schedule__date">Friday, Jan 27</td>
                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">Bec KC (ENG)</h6>
                                <span class="team-meta__place">Bec Korfball Club</span>
                            </div>
                        </div>
                    </td>

                    <td class="team-schedule__date">13 - 12</td>

                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">Schweriner KC (GER)</h6>
                                <span class="team-meta__place">Schweriner Korfball Club e.V '67</span>
                            </div>
                        </div>
                    </td>
                    <td class="team-schedule__time">17:00</td>
                </tr>

                <tr>
                    <td class="team-schedule__date">Friday, Jan 27</td>
                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">SG Pegasus (GER)</h6>
                                <span class="team-meta__place">SG Pegasus Rommerscheid 1991 e.V</span>
                            </div>
                        </div>
                    </td>

                    <td class="team-schedule__date">23 - 21</td>

                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">KCCS ČESKĚ BUDEJOVICE (CZE)</h6>
                                <span class="team-meta__place">KCC SOKOL ČESKĚ BUDEJOVICE</span>
                            </div>
                        </div>
                    </td>
                    <td class="team-schedule__time">18:05</td>
                </tr>

                <tr style="background-color: #ebebeb">
                    <td class="team-schedule__date">Friday, Jan 27</td>
                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">Opening Cerimony</h6>
                                <span class="team-meta__place"></span>
                            </div>
                        </div>
                    </td>

                    <th class="team-schedule__date"></th>

                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">Opening Cerimony</h6>
                                <span class="team-meta__place"></span>
                            </div>
                        </div>
                    </td>
                    <td class="team-schedule__time">19:00</td>
                </tr>

                <tr>
                    <td class="team-schedule__date">Friday, Jan 27</td>
                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">KC Barcelona (CAT)</h6>
                                <span class="team-meta__place">Korfbal Club Barcelona</span>
                            </div>
                        </div>
                    </td>

                    <td class="team-schedule__date">18 - 14</td>

                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">CCCD (POR)</h6>
                                <span class="team-meta__place">Clube de Carnaxide Cultura e Desporto</span>
                            </div>
                        </div>
                    </td>
                    <td class="team-schedule__time">19:40</td>
                </tr>

                <tr>
                    <td class="team-schedule__date">Friday, Jan 27</td>
                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">CRCQL (POR)</h6>
                                <span class="team-meta__place">CRC Quinta dos Lombos</span>
                            </div>
                        </div>
                    </td>

                    <td class="team-schedule__date">20 - 06</td>

                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">CK Montcada (CAT)</h6>
                                <span class="team-meta__place">CK Montcada</span>
                            </div>
                        </div>
                    </td>
                    <td class="team-schedule__time">20:45</td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>