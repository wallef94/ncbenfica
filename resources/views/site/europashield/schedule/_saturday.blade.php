<div class="card card--has-table">
    <div class="card__header">
        <h4>Schedule Saturday, Jan 28th</h4>
    </div>
    <div class="card__content">
        <div class="table-responsive">
            <table class="table table-hover team-schedule team-schedule--full">
                <thead>
                <tr>
                    <th class="team-schedule__date">Date</th>
                    <th class="team-schedule__versus">Team</th>
                    <th class="team-schedule__date">Score</th>
                    <th class="team-schedule__versus">Versus</th>
                    <th class="team-schedule__time">Time</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="team-schedule__date">Saturday, Jan 28</td>
                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">KC Barcelona (CAT)</h6>
                                <span class="team-meta__place">Korfbal Club Barcelona</span>
                            </div>
                        </div>
                    </td>

                    <td class="team-schedule__date">15 - 18</td>

                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">Schweriner KC (GER)</h6>
                                <span class="team-meta__place">Schweriner Korfball Club e.V '67</span>
                            </div>
                        </div>
                    </td>
                    <td class="team-schedule__time">09:45</td>
                </tr>

                <tr>
                    <td class="team-schedule__date">Saturday, Jan 28</td>
                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">Bec KC (ENG)</h6>
                                <span class="team-meta__place">Bec Korfball Club</span>
                            </div>
                        </div>
                    </td>

                    <td class="team-schedule__date">20 - 09</td>

                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">CCCD (POR)</h6>
                                <span class="team-meta__place">Clube de Carnaxide Cultura e Desporto</span>
                            </div>
                        </div>
                    </td>
                    <td class="team-schedule__time">10:50</td>
                </tr>

                <tr>
                    <td class="team-schedule__date">Saturday, Jan 28</td>
                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">CRCQL (POR)</h6>
                                <span class="team-meta__place">CRC Quinta dos Lombos</span>
                            </div>
                        </div>
                    </td>

                    <td class="team-schedule__date">15 - 13</td>

                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">KCCS ČESKĚ BUDEJOVICE (CZE)</h6>
                                <span class="team-meta__place">KCC SOKOL ČESKĚ BUDEJOVICE</span>
                            </div>
                        </div>
                    </td>
                    <td class="team-schedule__time">11:55</td>
                </tr>

                <tr>
                    <td class="team-schedule__date">Saturday, Jan 28</td>
                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">SG Pegasus (GER)</h6>
                                <span class="team-meta__place">SG Pegasus Rommerscheid 1991 e.V</span>
                            </div>
                        </div>
                    </td>

                    <td class="team-schedule__date">13 - 15</td>

                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">CK Montcada (CAT)</h6>
                                <span class="team-meta__place">CK Montcada</span>
                            </div>
                        </div>
                    </td>
                    <td class="team-schedule__time">13:00</td>
                </tr>

                <tr>
                    <td class="team-schedule__date">Saturday, Jan 28</td>
                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">KC Barcelona (CAT)</h6>
                                <span class="team-meta__place">Korfbal Club Barcelona</span>
                            </div>
                        </div>
                    </td>

                    <td class="team-schedule__date">17 - 19</td>

                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">Bec KC (ENG)</h6>
                                <span class="team-meta__place">Bec Korfball Club</span>
                            </div>
                        </div>
                    </td>
                    <td class="team-schedule__time">15:00</td>
                </tr>

                <tr>
                    <td class="team-schedule__date">Saturday, Jan 28</td>
                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">KCCS ČESKĚ BUDEJOVICE (CZE)</h6>
                                <span class="team-meta__place">KCC SOKOL ČESKĚ BUDEJOVICE</span>
                            </div>
                        </div>
                    </td>

                    <td class="team-schedule__date">12 - 15</td>

                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">CK Montcada (CAT)</h6>
                                <span class="team-meta__place">CK Montcada</span>
                            </div>
                        </div>
                    </td>
                    <td class="team-schedule__time">16:05</td>
                </tr>

                <tr>
                    <td class="team-schedule__date">Saturday, Jan 28</td>
                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">CCCD (POR)</h6>
                                <span class="team-meta__place">Clube de Carnaxide Cultura e Desporto</span>
                            </div>
                        </div>
                    </td>

                    <td class="team-schedule__date">11 - 13</td>

                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">Schweriner KC (GER)</h6>
                                <span class="team-meta__place">Schweriner Korfball Club e.V '67</span>
                            </div>
                        </div>
                    </td>
                    <td class="team-schedule__time">17:10</td>
                </tr>

                <tr>
                    <td class="team-schedule__date">Saturday, Jan 28</td>
                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">SG Pegasus (GER)</h6>
                                <span class="team-meta__place">SG Pegasus Rommerscheid 1991 e.V</span>
                            </div>
                        </div>
                    </td>

                    <td class="team-schedule__date">14 - 15</td>

                    <td class="team-schedule__versus">
                        <div class="team-meta">
                            <div class="team-meta__info">
                                <h6 class="team-meta__name">CRCQL (POR)</h6>
                                <span class="team-meta__place">CRC Quinta dos Lombos</span>
                            </div>
                        </div>
                    </td>
                    <td class="team-schedule__time">18:15</td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>