@extends('site.layout.site_template')

@section('content')
    @include('site.europashield.schedule._page_heading')

    <!-- Content
    ================================================== -->
    <div class="site-content">
        <div class="container">

            <div class="row">
                <div class="col-md-9 col-md-push-3">
                    @include('site.europashield._partials._tgoo_banner')
                    @include('site.europashield.schedule._friday')
                    @include('site.europashield.schedule._saturday')
                    @include('site.europashield.schedule._sunday')
                </div>
                @include('site.europashield._partials._sidebar')
            </div>
        </div>
    </div>
@endsection