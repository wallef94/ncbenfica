@extends('site.layout.site_template')

@section('content')
@include('site.europashield.home._page_heading')

<!-- Content
    ================================================== -->
<div class="site-content">
    <div class="container">
        <div class="row">
            <!-- Posts -->
            <div class="col-md-9 col-md-push-3">
                <!-- Featured News -->
                @include('site.europashield._partials._tgoo_banner')
                <div class="card card--clean">
                    <header class="card__header card__header--has-filter">
                        <h4>Recent News</h4>
                    </header>
                </div>
                <!-- Featured News / End -->
                <div class="posts posts--cards post-grid post-grid--masonry row">
                    @foreach($news as $n)
                        <div class="post-grid__item col-sm-6">
                            <div class="posts__item posts__item--card posts__item--category-1 card">
                                <figure class="posts__thumb">
                                    <div class="posts__cat">
                                        <span class="label posts__cat-label">{{ $n->category->name or null }}</span>
                                    </div>
                                    @if(!empty($n->banner))
                                    <a href="{{ route('site.europashield.content.show', [$n->category_id, $n->category->slug, $n->slug]) }}">
                                        <img src="{{ asset("/uploads/cms/content/{$n->id}/banner/{$n->banner}") }}" alt="Banner" />
                                    </a>
                                    @endif
                                </figure>
                                <div class="posts__inner card__content">
                                    <a href="{{ route('site.europashield.content.show', [$n->category_id, $n->category->slug, $n->slug]) }}" class="posts__cta"></a>
                                    <time datetime="2016-08-23" class="posts__date">{{ Carbon\Carbon::parse($n->created_at)->diffForHumans() }}</time>
                                    <h6 class="posts__title">
                                        <a href="{{ route('site.europashield.content.show', [$n->category_id, $n->category->slug, $n->slug]) }}">
                                            {{ $n->title }}
                                        </a>
                                    </h6>
                                    <div class="posts__excerpt">
                                        {{ str_limit(strip_tags($n->body), 200) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- Posts / End -->
            @include('site.europashield._partials._sidebar')
        </div>
    </div>
</div>
@endsection