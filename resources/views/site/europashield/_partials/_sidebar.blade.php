<!-- Sidebar -->
<div class="sidebar sidebar--shop col-md-3 col-md-pull-9">

    <!-- Widget: Categories -->
    <aside class="widget card widget--sidebar widget_categories">
        <div class="widget__title card__header">
            <h4>Menu</h4>
        </div>
        <div class="widget__content card__content">
            <ul class="widget__list">
                <li>
                    <a href="http://ncbenfica.pt/europashield2017">Home</a>
                </li>
                @foreach($categories as $category)
                    <li>
                        <a title="{{ $category->name }}" href="{{ route('site.europashield.content.list', [$category->id, $category->slug]) }}">{{ $category->name }}</a>
                    </li>
                @endforeach
                <li>
                    <a href="{{ route('site.europashield.schedule') }}">Schedule</a>
                </li>
                <li>
                    <a href="{{ route('site.europashield.standings') }}">Standings</a>
                </li>
            </ul>
        </div>
    </aside>
    <!-- Widget: Categories / End -->

    @if(!(\Request::route()->getName() == 'site.europashield.standings'))
    <!-- Widget: Standings -->
        <aside class="widget card widget--sidebar widget-standings">
            <div class="widget__title card__header card__header--has-btn">
                <h4>Group A</h4>
                <a href="{{ route('site.europashield.standings') }}" class="btn btn-default btn-outline btn-xs card-header__button">See more</a>
            </div>
            <div class="widget__content card__content">
                <div class="table-responsive">
                    <table class="table table-hover table-standings">
                        <thead>
                        <tr>
                            <th>Standings</th>
                            <th>P</th>
                            <th>W</th>
                            <th>L</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($group_a as $team)
                            <tr>
                                <td>
                                    <div class="team-meta">
                                        <div class="team-meta__info">
                                            <h6 class="team-meta__name">{{ $team->initials }}</h6>
                                            <span class="team-meta__place">{{ $team->name }}</span>
                                        </div>
                                    </div>
                                </td>
                                <td>{{ $team->points }}</td>
                                <td>{{ $team->victories }}</td>
                                <td>{{ $team->defeats }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </aside>
        <!-- Widget: Standings / End -->

        <!-- Widget: Standings -->
        <aside class="widget card widget--sidebar widget-standings">
            <div class="widget__title card__header card__header--has-btn">
                <h4>Group B</h4>
                <a href="{{ route('site.europashield.standings') }}" class="btn btn-default btn-outline btn-xs card-header__button">See more</a>
            </div>
            <div class="widget__content card__content">
                <div class="table-responsive">
                    <table class="table table-hover table-standings">
                        <thead>
                        <tr>
                            <th>Standings</th>
                            <th>P</th>
                            <th>W</th>
                            <th>L</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($group_b as $team)
                            <tr>
                                <td>
                                    <div class="team-meta">
                                        <div class="team-meta__info">
                                            <h6 class="team-meta__name">{{ $team->initials }}</h6>
                                            <span class="team-meta__place">{{ $team->name }}</span>
                                        </div>
                                    </div>
                                </td>
                                <td>{{ $team->points }}</td>
                                <td>{{ $team->victories }}</td>
                                <td>{{ $team->defeats }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </aside>
        <!-- Widget: Standings / End -->
    @endif
</div>
<!-- Sidebar / End -->