<!-- Header
    ================================================== -->

<!-- Header Mobile -->
<div class="header-mobile clearfix" id="header-mobile">
    <div class="header-mobile__logo">
        <a href="/"><img src="{{ asset('img/logo.png') }}" alt="NCB" class="header-mobile__logo-img" /></a>
    </div>
    <div class="header-mobile__inner">
        <a id="header-mobile__toggle" class="burger-menu-icon"><span class="burger-menu-icon__line"></span></a>
        <span class="header-mobile__search-icon" id="header-mobile__search-icon"></span>
    </div>
</div>

<!-- Header Desktop -->
<header class="header">

    <!-- Header Top Bar -->
    <div class="header__top-bar clearfix">
        <div class="container">
            <a href="{{ route('site.europashield.home') }}" class="btn btn-warning pull-right">Europa Shield <i class="fa fa-trophy"></i> </a>
            <!-- Social Links -->
            <ul class="social-links social-links--inline social-links--main-nav pull-right">
                <li class="social-links__item">
                    <a href="#" class="social-links__link" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>
                </li>
                <li class="social-links__item">
                    <a href="#" class="social-links__link" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>
                </li>
                <li class="social-links__item">
                    <a href="#" class="social-links__link" data-toggle="tooltip" data-placement="bottom" title="Google+"><i class="fa fa-google-plus"></i></a>
                </li>
            </ul>
            <!-- Social Links / End -->
        </div>
    </div>
    <!-- Header Top Bar / End -->

    <!-- Header Secondary -->
    <div class="header__secondary">
        <div class="container">
            <!-- Header Search Form -->
            <div class="header-search-form">
                <form action="#" id="mobile-search-form" class="search-form" />
                <input type="text" class="form-control header-mobile__search-control" value="" placeholder="Procure aqui..." />
                <button type="submit" class="header-mobile__search-submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <!-- Header Search Form / End -->
            <ul class="info-block info-block--header">
                <li class="info-block__item info-block__item--contact-primary">
                    <h6 class="info-block__heading">Faça parte da equipa!</h6>
                    <a class="info-block__link" href="mailto:tryouts@ncbenfica.pt">tryouts@ncbenfica.pt</a>
                </li>
                <li class="info-block__item info-block__item--contact-secondary">
                    <h6 class="info-block__heading">Contate-nos</h6>
                    <a class="info-block__link" href="mailto:info@ncbenfica.pt">info@ncbenfica.pt</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- Header Secondary / End -->

    <!-- Header Primary -->
    <div class="header__primary">
        <div class="container">
            <div class="header__primary-inner">
                <!-- Header Logo -->
                <div class="header-logo">
                    <a href="/"><img src="{{ asset('img/logo.png') }}" alt="NCB" class="header-logo__img" /></a>
                </div>
                <!-- Header Logo / End -->

                <!-- Main Navigation -->
                <nav class="main-nav clearfix">
                    <ul class="main-nav__list">
                        <li class="active">
                            <a href="/">Home</a>
                        </li>
                        <li class="">
                            <a href="javascript:void(0)">Clube</a>
                        </li>
                        <li class="">
                            <a href="javascript:void(0)">Equipas</a>
                            <ul class="main-nav__sub">
                                <li>
                                    <a href="javascript:void(0)">Equipa A</a>
                                    <ul class="main-nav__sub-2">
                                        <li class="">
                                            <a href="javascript:void(0)">Calendário</a>
                                        </li>
                                        <li class="">
                                            <a href="javascript:void(0)">Classificação</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Equipa B</a>
                                    <ul class="main-nav__sub-2">
                                        <li class="">
                                            <a href="javascript:void(0)">Calendário</a>
                                        </li>
                                        <li class="">
                                            <a href="javascript:void(0)">Classificação</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Equipa C</a>
                                    <ul class="main-nav__sub-2">
                                        <li class="">
                                            <a href="javascript:void(0)">Calendário</a>
                                        </li>
                                        <li class="">
                                            <a href="javascript:void(0)">Classificação</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Equipa D</a>
                                    <ul class="main-nav__sub-2">
                                        <li class="">
                                            <a href="javascript:void(0)">Calendário</a>
                                        </li>
                                        <li class="">
                                            <a href="javascript:void(0)">Classificação</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="javascript:void(0)">Corfebol</a>
                        </li>
                        <li class="">
                            <a href="javascript:void(0)">Media</a>
                        </li>
                        <li class="">
                            <a href="javascript:void(0)">Notícias</a>
                        </li>
                        <li class="">
                            <a href="javascript:void(0)">Contatos</a>
                        </li>
                    </ul>

                    <!-- Pushy Panel Toggle -->
                    <a href="#" class="pushy-panel__toggle">
                        <span class="pushy-panel__line"></span>
                    </a>
                    <!-- Pushy Panel Toggle / Eng -->
                </nav>
                <!-- Main Navigation / End -->
            </div>
        </div>
    </div>
    <!-- Header Primary / End -->

</header>
<!-- Header / End -->