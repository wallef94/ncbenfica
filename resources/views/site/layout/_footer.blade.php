<!-- Footer
    ================================================== -->
<footer id="footer" class="footer">

    <!-- Footer Widgets -->
    <div class="footer-widgets">
        <div class="footer-widgets__inner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-3">
                        <div class="footer-col-inner">

                            <!-- Footer Logo -->
                            <div class="footer-logo">
                                <a href="/"><img src="{{ asset('img/logo.png') }}" alt="NCB" class="footer-logo__img" /></a>
                            </div>
                            <!-- Footer Logo / End -->

                        </div>
                    </div>
                    <div class="col-sm-4 col-md-3">
                        <div class="footer-col-inner">
                            <!-- Widget: Contact Info -->
                            <div class="widget widget--footer widget-contact-info">
                                <h4 class="widget__title">Informações de Contato</h4>
                                <div class="widget__content">
                                    <div class="widget-contact-info__desc">
                                        <p>
                                            Rua Professor José Sebastião e Silva<br>
                                            1500-500 Lisboa
                                        </p>
                                    </div>
                                    <div class="widget-contact-info__body info-block">
                                        <div class="info-block__item">
                                            <h6 class="info-block__heading">Contate-nos</h6>
                                            <a class="info-block__link" href="mailto:info@ncbenfica.pt">info@ncbenfica.pt</a>
                                        </div>
                                        <div class="info-block__item">
                                            <h6 class="info-block__heading">Faça parte da equipa!</h6>
                                            <a class="info-block__link" href="mailto:tryouts@ncbenfica.pt">tryouts@ncbenfica.pt</a>
                                        </div>
                                        <div class="info-block__item info-block__item--nopadding">
                                            <ul class="social-links">
                                                <li class="social-links__item">
                                                    <a href="#" class="social-links__link"><i class="fa fa-facebook"></i> Facebook</a>
                                                </li>
                                                <li class="social-links__item">
                                                    <a href="#" class="social-links__link"><i class="fa fa-twitter"></i> Twitter</a>
                                                </li>
                                                <li class="social-links__item">
                                                    <a href="#" class="social-links__link"><i class="fa fa-google-plus"></i> Google+</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Widget: Contact Info / End -->
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-3">
                        <div class="footer-col-inner">
                            <!-- Widget: Popular Posts / End -->
                            <div class="widget widget--footer widget-popular-posts">
                                <h4 class="widget__title">Notícias Recentes</h4>
                                <div class="widget__content">
                                    <ul class="posts posts--simple-list">
                                        <li class="posts__item posts__item--category-2">
                                            <div class="posts__cat">
                                                <span class="label posts__cat-label">Injuries</span>
                                            </div>
                                            <h6 class="posts__title"><a href="#">Mark Johnson has a Tibia Fracture and is gonna be out</a></h6>
                                            <time datetime="2016-08-23" class="posts__date">August 23rd, 2016</time>
                                        </li>
                                        <li class="posts__item posts__item--category-1">
                                            <div class="posts__cat">
                                                <span class="label posts__cat-label">The Team</span>
                                            </div>
                                            <h6 class="posts__title"><a href="#">Jay Rorks is only 24 points away from breaking the record</a></h6>
                                            <time datetime="2016-08-22" class="posts__date">August 22nd, 2016</time>
                                        </li>
                                        <li class="posts__item posts__item--category-1">
                                            <div class="posts__cat">
                                                <span class="label posts__cat-label">The Team</span>
                                            </div>
                                            <h6 class="posts__title"><a href="#">The new eco friendly stadium won a Leafy Award in 2016</a></h6>
                                            <time datetime="2016-08-21" class="posts__date">August 21st, 2016</time>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Widget: Popular Posts / End -->
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-3">
                        <div class="footer-col-inner">

                            <!-- Widget: Instagram / End -->
                            <div class="widget widget--footer widget-instagram">
                                <h4 class="widget__title">Instagram</h4>
                                <div class="widget__content">
                                    <ul class="widget-instagram__list">
                                        <li class="widget-instagram__item">
                                            <a href="#" class="widget-instagram__link-wrapper">
                                                <span class="widget-instagram__plus-sign">
                                                  <img src="{{ asset('img/photo1.png') }}" class="widget-instagram__img" alt="" />
                                                </span>
                                            </a>
                                        </li>
                                        <li class="widget-instagram__item">
                                            <a href="#" class="widget-instagram__link-wrapper">
                                                <span class="widget-instagram__plus-sign">
                                                  <img src="{{ asset('img/photo1.png') }}" class="widget-instagram__img" alt="" />
                                                </span>
                                            </a>
                                        </li>
                                        <li class="widget-instagram__item">
                                            <a href="#" class="widget-instagram__link-wrapper">
                                                <span class="widget-instagram__plus-sign">
                                                  <img src="{{ asset('img/photo1.png') }}" class="widget-instagram__img" alt="" />
                                                </span>
                                            </a>
                                        </li>
                                        <li class="widget-instagram__item">
                                            <a href="#" class="widget-instagram__link-wrapper">
                                                <span class="widget-instagram__plus-sign">
                                                  <img src="{{ asset('img/photo1.png') }}" class="widget-instagram__img" alt="" />
                                                </span>
                                            </a>
                                        </li>
                                        <li class="widget-instagram__item">
                                            <a href="#" class="widget-instagram__link-wrapper">
                                                <span class="widget-instagram__plus-sign">
                                                  <img src="{{ asset('img/photo1.png') }}" class="widget-instagram__img" alt="" />
                                                </span>
                                            </a>
                                        </li>
                                        <li class="widget-instagram__item">
                                            <a href="#" class="widget-instagram__link-wrapper">
                                                <span class="widget-instagram__plus-sign">
                                                  <img src="{{ asset('img/photo1.png') }}" class="widget-instagram__img" alt="" />
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                    <a href="#" class="btn btn-sm btn-instagram btn-icon-right">Siga o nosso Instagram <i class="icon-arrow-right"></i></a>
                                </div>
                            </div>
                            <!-- Widget: Instagram / End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer Widgets / End -->

    <!-- Footer Secondary -->
    <div class="footer-secondary footer-secondary--has-decor">
        <div class="container">
            <div class="footer-secondary__inner">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <ul class="footer-nav">
                            <li class="footer-nav__item"><a href="javascript:void(0)">Home</a></li>
                            <li class="footer-nav__item"><a href="javascript:void(0)">Equipas</a></li>
                            <li class="footer-nav__item"><a href="javascript:void(0)">Notícias</a></li>
                            <li class="footer-nav__item"><a href="javascript:void(0)">Corfebol</a></li>
                            <li class="footer-nav__item"><a href="javascript:void(0)">Clube</a></li>
                            <li class="footer-nav__item"><a href="javascript:void(0)">Media</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer Secondary / End -->
</footer>
<!-- Footer / End -->