<!DOCTYPE html>
<html lang="zxx">
<head>
    <!-- Basic Page Needs
    ================================================== -->
    <title>NCB | Núcleo de Corfebol de Benfica</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="NCB | Núcleo de Corfebol de Benfica" />
    <meta name="author" content="Wallef Borges" />
    <meta name="keywords" content="Sports News Korfball Corfebol Benfica" />

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="{{ asset('img/favicons/favicon.ico') }}" />
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('img/favicons/favicon-120.png') }}" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('img/favicons/favicon-152.png') }}" />

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />

    <!-- Google Web Fonts
    ================================================== -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CSource+Sans+Pro:400,700" rel="stylesheet" />

    <!-- CSS
    ================================================== -->
    <!-- Compiled CSS -->
    <link href="{{ asset("/css/site.css") }}" rel="stylesheet" type="text/css" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body class="template-basketball">

<div class="site-wrapper clearfix">
    <div class="site-overlay"></div>

    @include('site.layout._header')

    @include('site.layout._pushy_panel')

    {{--@yield('hero_unit')--}}

    {{--@yield('header_news')--}}

    @yield('content')

    @include('site.layout._footer')

    @yield('modals')

</div>

<!-- Javascript Files
================================================== -->
<!-- Compiled JS -->
<script src="{{ asset ("/js/site.js") }}"></script>

</body>
</html>
  
