<div class="hero-unit">
    <div class="container hero-unit__container">
        <div class="hero-unit__content">
            <span class="hero-unit__decor">
                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
            </span>
            <h5 class="hero-unit__subtitle">Desde 1993</h5>
            <h1 class="hero-unit__title">NC <span class="text-primary">Benfica</span></h1>
            <div class="hero-unit__desc">O Núcleo de Corfebol de Benfica foi criado em 1993/1994, como um núcleo da Escola Secundária José Gomes Ferreira. Este projecto teve como mentor o Prof. Nuno Ferro, que é actualmente o Director Técnico.</div>
            <a href="#" class="btn btn-inverse btn-sm btn-outline btn-icon-right btn-condensed hero-unit__btn">Saiba Mais <i class="fa fa-plus text-primary"></i></a>
        </div>
    </div>
</div>