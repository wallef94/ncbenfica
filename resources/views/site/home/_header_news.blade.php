<div class="posts posts--carousel-featured featured-carousel">

    <div class="posts__item posts__item--category-1">
        <a href="#" class="posts__link-wrapper">
            <figure class="posts__thumb">
                <img src="{{ asset('img/photo3.jpg') }}" alt="" />
            </figure>
            <div class="posts__inner">
                <div class="posts__cat">
                    <span class="label posts__cat-label">The Team</span>
                </div>
                <h3 class="posts__title">Alchemists women team tryouts will start in January</h3>
                <time datetime="2017-08-23" class="posts__date">August 23rd, 2017</time>
            </div>
        </a>
    </div>

    <div class="posts__item posts__item--category-1">
        <a href="#" class="posts__link-wrapper">
            <figure class="posts__thumb">
                <img src="{{ asset('img/photo3.jpg') }}" alt="" />
            </figure>
            <div class="posts__inner">
                <div class="posts__cat">
                    <span class="label posts__cat-label">The Team</span>
                </div>
                <h3 class="posts__title">Checkout the new ride of our best player of the season</h3>
                <time datetime="2017-08-23" class="posts__date">August 23rd, 2017</time>
            </div>
        </a>
    </div>

    <div class="posts__item posts__item--category-1">
        <a href="#" class="posts__link-wrapper">
            <figure class="posts__thumb">
                <img src="{{ asset('img/photo3.jpg') }}" alt="" />
            </figure>
            <div class="posts__inner">
                <div class="posts__cat">
                    <span class="label posts__cat-label">The Team</span>
                </div>
                <h3 class="posts__title">All the players are taking a team trip this summer</h3>
                <time datetime="2017-08-23" class="posts__date">August 23rd, 2017</time>
            </div>
        </a>
    </div>

    <div class="posts__item posts__item--category-1">
        <a href="#" class="posts__link-wrapper">
            <figure class="posts__thumb">
                <img src="{{ asset('img/photo3.jpg') }}" alt="" />
            </figure>
            <div class="posts__inner">
                <div class="posts__cat">
                    <span class="label posts__cat-label">The Team</span>
                </div>
                <h3 class="posts__title">Alchemists women team tryouts will start in January</h3>
                <time datetime="2017-08-23" class="posts__date">August 23rd, 2017</time>
            </div>
        </a>
    </div>

    <div class="posts__item posts__item--category-1">
        <a href="#" class="posts__link-wrapper">
            <figure class="posts__thumb">
                <img src="{{ asset('img/photo3.jpg') }}" alt="" />
            </figure>
            <div class="posts__inner">
                <div class="posts__cat">
                    <span class="label posts__cat-label">The Team</span>
                </div>
                <h3 class="posts__title">All the players are taking a team trip this summer</h3>
                <time datetime="2017-08-23" class="posts__date">August 23rd, 2017</time>
            </div>
        </a>
    </div>

</div>