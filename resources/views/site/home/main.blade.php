@extends('site.layout.site_template')

@section('content')
    @include('site.home._hero_unit')

    @include('site.home._header_news')

    <div class="site-content">
        <div class="container">

            <div class="row">
                <!-- Content -->
                <div class="content col-md-8">

                    @include('site.home._last_game_results')
                    <!-- Featured News -->
                    <div class="card card--clean">
                        <header class="card__header card__header--has-filter">
                            <h4>Notícias Recentes</h4>
                        </header>
                    </div>
                    <!-- Featured News / End -->


                    <!-- Post Area -->
                    <div class="posts posts--cards post-grid row">

                        <div class="post-grid__item col-sm-6">
                            <div class="posts__item posts__item--card posts__item--category-1 card">
                                <figure class="posts__thumb">
                                    <div class="posts__cat">
                                        <span class="label posts__cat-label">The Team</span>
                                    </div>
                                    <a href="#"><img src="{{ asset('img/photo4.jpg') }}" alt="" /></a>
                                </figure>
                                <div class="posts__inner card__content">
                                    <a href="#" class="posts__cta"></a>
                                    <time datetime="2016-08-23" class="posts__date">August 23rd, 2016</time>
                                    <h6 class="posts__title"><a href="#">Michael Bryan was chosen again as best player with 45 points</a></h6>
                                    <div class="posts__excerpt">
                                        Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad mini veniam, quis nostrud en derum sum laborem.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="post-grid__item col-sm-6">
                            <div class="posts__item posts__item--card posts__item--category-1 card">
                                <figure class="posts__thumb">
                                    <div class="posts__cat">
                                        <span class="label posts__cat-label">The Team</span>
                                    </div>
                                    <a href="#"><img src="{{ asset('img/photo3.jpg') }}" alt="" /></a>
                                </figure>
                                <div class="posts__inner card__content">
                                    <a href="#" class="posts__cta"></a>
                                    <time datetime="2016-08-23" class="posts__date">August 23rd, 2016</time>
                                    <h6 class="posts__title"><a href="#">The new eco friendly stadium won a Leafy Award in 2016</a></h6>
                                    <div class="posts__excerpt">
                                        Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad mini veniam, quis nostrud en derum sum laborem.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="post-grid__item col-sm-6">
                            <div class="posts__item posts__item--card posts__item--category-1 card">
                                <figure class="posts__thumb">
                                    <div class="posts__cat">
                                        <span class="label posts__cat-label">The Team</span>
                                    </div>
                                    <a href="#"><img src="{{ asset('img/photo4.jpg') }}" alt="" /></a>
                                </figure>
                                <div class="posts__inner card__content">
                                    <a href="#" class="posts__cta"></a>
                                    <time datetime="2016-08-23" class="posts__date">August 23rd, 2016</time>
                                    <h6 class="posts__title"><a href="#">Michael Bryan was chosen again as best player with 45 points</a></h6>
                                    <div class="posts__excerpt">
                                        Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad mini veniam, quis nostrud en derum sum laborem.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="post-grid__item col-sm-6">
                            <div class="posts__item posts__item--card posts__item--category-1 card">
                                <figure class="posts__thumb">
                                    <div class="posts__cat">
                                        <span class="label posts__cat-label">The Team</span>
                                    </div>
                                    <a href="#"><img src="{{ asset('img/photo4.jpg') }}" alt="" /></a>
                                </figure>
                                <div class="posts__inner card__content">
                                    <a href="#" class="posts__cta"></a>
                                    <time datetime="2016-08-23" class="posts__date">August 23rd, 2016</time>
                                    <h6 class="posts__title"><a href="#">Michael Bryan was chosen again as best player with 45 points</a></h6>
                                    <div class="posts__excerpt">
                                        Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad mini veniam, quis nostrud en derum sum laborem.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Post Area / End -->
                </div>
                <!-- Content / End -->

                <!-- Sidebar -->
                <div id="sidebar" class="sidebar col-md-4">
                    <!-- Widget: Standings -->
                    <aside class="widget card widget--sidebar widget-standings">
                        <div class="widget__title card__header card__header--has-btn">
                            <h4>Classificação Corfliga</h4>
                            <a href="#" class="btn btn-default btn-outline btn-xs card-header__button">Ver mais</a>
                        </div>
                        <div class="widget__content card__content">
                            <div class="table-responsive">
                                <table class="table table-hover table-standings">
                                    <thead>
                                    <tr>
                                        <th>Posições</th>
                                        <th>P</th>
                                        <th>V</th>
                                        <th>D</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="team-meta">
                                                <div class="team-meta__info">
                                                    <h6 class="team-meta__name">L.A Pirates</h6>
                                                    <span class="team-meta__place">Bebop Institute</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>45</td>
                                        <td>5</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="team-meta">
                                                <div class="team-meta__info">
                                                    <h6 class="team-meta__name">Sharks</h6>
                                                    <span class="team-meta__place">Marine College</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>42</td>
                                        <td>8</td>
                                        <td>3</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="team-meta">
                                                <div class="team-meta__info">
                                                    <h6 class="team-meta__name">The Alchemists</h6>
                                                    <span class="team-meta__place">Eric Bros School</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>40</td>
                                        <td>10</td>
                                        <td>5</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="team-meta">
                                                <div class="team-meta__info">
                                                    <h6 class="team-meta__name">Ocean Kings</h6>
                                                    <span class="team-meta__place">Bay College</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>38</td>
                                        <td>12</td>
                                        <td>7</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="team-meta">
                                                <div class="team-meta__info">
                                                    <h6 class="team-meta__name">Red Wings</h6>
                                                    <span class="team-meta__place">Icarus College</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>37</td>
                                        <td>13</td>
                                        <td>8</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="team-meta">
                                                <div class="team-meta__info">
                                                    <h6 class="team-meta__name">Lucky Clovers</h6>
                                                    <span class="team-meta__place">St. Patrick’s Institute</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>34</td>
                                        <td>16</td>
                                        <td>11</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="team-meta">
                                                <div class="team-meta__info">
                                                    <h6 class="team-meta__name">Draconians</h6>
                                                    <span class="team-meta__place">Draconians</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>31</td>
                                        <td>19</td>
                                        <td>14</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="team-meta">
                                                <div class="team-meta__info">
                                                    <h6 class="team-meta__name">Bloody Wave</h6>
                                                    <span class="team-meta__place">Atlantic School</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>30</td>
                                        <td>20</td>
                                        <td>15</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </aside>
                    <!-- Widget: Standings / End -->


                    <!-- Widget: Social Buttons -->
                    <aside class="widget widget--sidebar widget-social">
                        <a href="#" class="btn-social-counter btn-social-counter--fb">
                            <div class="btn-social-counter__icon">
                                <i class="fa fa-facebook"></i>
                            </div>
                            <h6 class="btn-social-counter__title">Like Our Facebook Page</h6>
                            <span class="btn-social-counter__add-icon"></span>
                        </a>
                        <a href="#" class="btn-social-counter btn-social-counter--twitter">
                            <div class="btn-social-counter__icon">
                                <i class="fa fa-twitter"></i>
                            </div>
                            <h6 class="btn-social-counter__title">Follow Us on Twitter</h6>
                            <span class="btn-social-counter__add-icon"></span>
                        </a>
                        <a href="#" class="btn-social-counter btn-social-counter--gplus">
                            <div class="btn-social-counter__icon">
                                <i class="fa fa-google-plus"></i>
                            </div>
                            <h6 class="btn-social-counter__title">Friend Us on Google+</h6>
                            <span class="btn-social-counter__add-icon"></span>
                        </a>
                    </aside>
                    <!-- Widget: Social Buttons / End -->
                </div>
                <!-- Sidebar / End -->
            </div>
        </div>
    </div>

    <!-- Content / End -->
@endsection