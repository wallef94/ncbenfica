<!-- Last Game Results -->
<div class="card">
    <header class="card__header card__header--has-btn">
        <h4>Resultado do Último Jogo</h4>
        <a href="#" class="btn btn-default btn-outline btn-xs card-header__button">Ver todos os Resultados</a>
    </header>
    <div class="card__content">

        <!-- Game Result -->
        <div class="game-result">
            <section class="game-result__section">
                <header class="game-result__header">
                    <h3 class="game-result__title">CorfLiga</h3>
                    <time class="game-result__date" datetime="2017-03-17">Sábado, 17 de Junho de 2017</time>
                </header>
                <div class="game-result__content">

                    <!-- 1st Team -->
                    <div class="game-result__team game-result__team--first">
                        <div class="game-result__team-info">
                            <h5 class="game-result__team-name">NCB</h5>
                            <div class="game-result__team-desc">Núcleo de Corfebol de Benfica</div>
                        </div>
                    </div>
                    <!-- 1st Team / End -->
                    <div class="game-result__score-wrap">
                        <div class="game-result__score">
                                                <span class="game-result__score-result game-result__score-result--winner">
                                                    25
                                                </span>

                            <span class="game-result__score-dash">
                                                    -
                                                </span>

                            <span class="game-result__score-result game-result__score-result--loser">
                                                    20
                                                </span>
                        </div>
                        <div class="game-result__score-label">Resultado Final</div>
                    </div>

                    <!-- 2nd Team -->
                    <div class="game-result__team game-result__team--second">
                        <div class="game-result__team-info">
                            <h5 class="game-result__team-name">CCCD</h5>
                            <div class="game-result__team-desc">Clube de Carnaxide Cultura e Desporto</div>
                        </div>
                    </div>
                    <!-- 2nd Team / End -->
                </div>
            </section>
        </div>
        <!-- Game Result / End -->

    </div>
</div>
<!-- Last Game Results / End -->