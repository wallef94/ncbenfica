<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('avatar')->default('default.jpg');
            $table->date('date_of_birth');
            $table->date('first_year_korfball')->nullable();
            $table->date('first_year_ncb')->nullable();
            $table->string('previous_clubs')->nullable();
            $table->string('national_teams')->nullable();
            $table->integer('shirt')->nullable();
            $table->enum('gender', ['M', 'F']);
            $table->integer('team_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('team_id')
                ->references('id')
                ->on('teams')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('players');
    }
}
