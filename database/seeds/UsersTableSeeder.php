<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use NCBenfica\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::create([
        	'name' => 'Wallef Borges',
        	'email' => 'borgeswallef@icloud.com',
            'password' => bcrypt('Isabella10'),
            'remember_token' => str_random(10),
    	]);
    }
}
