<?php

use NCBenfica\Models\Division;
use Illuminate\Database\Seeder;

class DivisionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Division::create([
        	'name' => '1ª Divisão'
    	]);

	    Division::create([
	    	'name' => '2ª Divisão'
		]);

	    Division::create([
	    	'name' => '3ª Divisão'
		]);

        Division::create([
            'name' => 'Internacional'
        ]);
    }
}
