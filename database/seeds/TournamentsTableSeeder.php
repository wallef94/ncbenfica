<?php

use Illuminate\Database\Seeder;
use NCBenfica\Models\Tournament;

class TournamentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tournament::create([
        	'name' => 'CorfLiga',
        	'division_id' => 1
    	]);

	    Tournament::create([
	    	'name' => 'Segunda Divisão',
	    	'division_id' => 2
		]);

	    Tournament::create([
	    	'name' => 'Terceira Divisão',
	    	'division_id' => 3
		]);
    }
}
