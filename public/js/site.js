/*!
 * jQuery JavaScript Library v2.2.4
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2016-05-20T17:23Z
 */

(function( global, factory ) {

	if ( typeof module === "object" && typeof module.exports === "object" ) {
		// For CommonJS and CommonJS-like environments where a proper `window`
		// is present, execute the factory and get jQuery.
		// For environments that do not have a `window` with a `document`
		// (such as Node.js), expose a factory as module.exports.
		// This accentuates the need for the creation of a real `window`.
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info.
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
}(typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Support: Firefox 18+
// Can't be in strict mode, several libs including ASP.NET trace
// the stack via arguments.caller.callee and Firefox dies if
// you try to trace through "use strict" call chains. (#13335)
//"use strict";
var arr = [];

var document = window.document;

var slice = arr.slice;

var concat = arr.concat;

var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var support = {};



var
	version = "2.2.4",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {

		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	},

	// Support: Android<4.1
	// Make sure we trim BOM and NBSP
	rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,

	// Matches dashed string for camelizing
	rmsPrefix = /^-ms-/,
	rdashAlpha = /-([\da-z])/gi,

	// Used by jQuery.camelCase as callback to replace()
	fcamelCase = function( all, letter ) {
		return letter.toUpperCase();
	};

jQuery.fn = jQuery.prototype = {

	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// Start with an empty selector
	selector: "",

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {
		return num != null ?

			// Return just the one element from the set
			( num < 0 ? this[ num + this.length ] : this[ num ] ) :

			// Return all the elements in a clean array
			slice.call( this );
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;
		ret.context = this.context;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	each: function( callback ) {
		return jQuery.each( this, callback );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map( this, function( elem, i ) {
			return callback.call( elem, i, elem );
		} ) );
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[ j ] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor();
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: arr.sort,
	splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[ 0 ] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// Skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !jQuery.isFunction( target ) ) {
		target = {};
	}

	// Extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {

		// Only deal with non-null/undefined values
		if ( ( options = arguments[ i ] ) != null ) {

			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject( copy ) ||
					( copyIsArray = jQuery.isArray( copy ) ) ) ) {

					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && jQuery.isArray( src ) ? src : [];

					} else {
						clone = src && jQuery.isPlainObject( src ) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend( {

	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	isFunction: function( obj ) {
		return jQuery.type( obj ) === "function";
	},

	isArray: Array.isArray,

	isWindow: function( obj ) {
		return obj != null && obj === obj.window;
	},

	isNumeric: function( obj ) {

		// parseFloat NaNs numeric-cast false positives (null|true|false|"")
		// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
		// subtraction forces infinities to NaN
		// adding 1 corrects loss of precision from parseFloat (#15100)
		var realStringObj = obj && obj.toString();
		return !jQuery.isArray( obj ) && ( realStringObj - parseFloat( realStringObj ) + 1 ) >= 0;
	},

	isPlainObject: function( obj ) {
		var key;

		// Not plain objects:
		// - Any object or value whose internal [[Class]] property is not "[object Object]"
		// - DOM nodes
		// - window
		if ( jQuery.type( obj ) !== "object" || obj.nodeType || jQuery.isWindow( obj ) ) {
			return false;
		}

		// Not own constructor property must be Object
		if ( obj.constructor &&
				!hasOwn.call( obj, "constructor" ) &&
				!hasOwn.call( obj.constructor.prototype || {}, "isPrototypeOf" ) ) {
			return false;
		}

		// Own properties are enumerated firstly, so to speed up,
		// if last one is own, then all properties are own
		for ( key in obj ) {}

		return key === undefined || hasOwn.call( obj, key );
	},

	isEmptyObject: function( obj ) {
		var name;
		for ( name in obj ) {
			return false;
		}
		return true;
	},

	type: function( obj ) {
		if ( obj == null ) {
			return obj + "";
		}

		// Support: Android<4.0, iOS<6 (functionish RegExp)
		return typeof obj === "object" || typeof obj === "function" ?
			class2type[ toString.call( obj ) ] || "object" :
			typeof obj;
	},

	// Evaluates a script in a global context
	globalEval: function( code ) {
		var script,
			indirect = eval;

		code = jQuery.trim( code );

		if ( code ) {

			// If the code includes a valid, prologue position
			// strict mode pragma, execute code by injecting a
			// script tag into the document.
			if ( code.indexOf( "use strict" ) === 1 ) {
				script = document.createElement( "script" );
				script.text = code;
				document.head.appendChild( script ).parentNode.removeChild( script );
			} else {

				// Otherwise, avoid the DOM node creation, insertion
				// and removal by using an indirect global eval

				indirect( code );
			}
		}
	},

	// Convert dashed to camelCase; used by the css and data modules
	// Support: IE9-11+
	// Microsoft forgot to hump their vendor prefix (#9572)
	camelCase: function( string ) {
		return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
	},

	nodeName: function( elem, name ) {
		return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
	},

	each: function( obj, callback ) {
		var length, i = 0;

		if ( isArrayLike( obj ) ) {
			length = obj.length;
			for ( ; i < length; i++ ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		} else {
			for ( i in obj ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		}

		return obj;
	},

	// Support: Android<4.1
	trim: function( text ) {
		return text == null ?
			"" :
			( text + "" ).replace( rtrim, "" );
	},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArrayLike( Object( arr ) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		return arr == null ? -1 : indexOf.call( arr, elem, i );
	},

	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		for ( ; j < len; j++ ) {
			first[ i++ ] = second[ j ];
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var length, value,
			i = 0,
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArrayLike( elems ) ) {
			length = elems.length;
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// Bind a function to a context, optionally partially applying any
	// arguments.
	proxy: function( fn, context ) {
		var tmp, args, proxy;

		if ( typeof context === "string" ) {
			tmp = fn[ context ];
			context = fn;
			fn = tmp;
		}

		// Quick check to determine if target is callable, in the spec
		// this throws a TypeError, but we will just return undefined.
		if ( !jQuery.isFunction( fn ) ) {
			return undefined;
		}

		// Simulated bind
		args = slice.call( arguments, 2 );
		proxy = function() {
			return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
		};

		// Set the guid of unique handler to the same of original handler, so it can be removed
		proxy.guid = fn.guid = fn.guid || jQuery.guid++;

		return proxy;
	},

	now: Date.now,

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
} );

// JSHint would error on this code due to the Symbol not being defined in ES5.
// Defining this global in .jshintrc would create a danger of using the global
// unguarded in another place, it seems safer to just disable JSHint for these
// three lines.
/* jshint ignore: start */
if ( typeof Symbol === "function" ) {
	jQuery.fn[ Symbol.iterator ] = arr[ Symbol.iterator ];
}
/* jshint ignore: end */

// Populate the class2type map
jQuery.each( "Boolean Number String Function Array Date RegExp Object Error Symbol".split( " " ),
function( i, name ) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
} );

function isArrayLike( obj ) {

	// Support: iOS 8.2 (not reproducible in simulator)
	// `in` check used to prevent JIT error (gh-2145)
	// hasOwn isn't used here due to false negatives
	// regarding Nodelist length in IE
	var length = !!obj && "length" in obj && obj.length,
		type = jQuery.type( obj );

	if ( type === "function" || jQuery.isWindow( obj ) ) {
		return false;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v2.2.1
 * http://sizzlejs.com/
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2015-10-17
 */
(function( window ) {

var i,
	support,
	Expr,
	getText,
	isXML,
	tokenize,
	compile,
	select,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + 1 * new Date(),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// General-purpose constants
	MAX_NEGATIVE = 1 << 31,

	// Instance methods
	hasOwn = ({}).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	push_native = arr.push,
	push = arr.push,
	slice = arr.slice,
	// Use a stripped-down indexOf as it's faster than native
	// http://jsperf.com/thor-indexof-vs-for/5
	indexOf = function( list, elem ) {
		var i = 0,
			len = list.length;
		for ( ; i < len; i++ ) {
			if ( list[i] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",

	// http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
	identifier = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",

	// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +
		// Operator (capture 2)
		"*([*^$|!~]?=)" + whitespace +
		// "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
		"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
		"*\\]",

	pseudos = ":(" + identifier + ")(?:\\((" +
		// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
		// 1. quoted (capture 3; capture 4 or capture 5)
		"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
		// 2. simple (capture 6)
		"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
		// 3. anything else (capture 2)
		".*" +
		")\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rwhitespace = new RegExp( whitespace + "+", "g" ),
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

	rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + identifier + ")" ),
		"CLASS": new RegExp( "^\\.(" + identifier + ")" ),
		"TAG": new RegExp( "^(" + identifier + "|[*])" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
			"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
			"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
			whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,
	rescape = /'|\\/g,

	// CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
	funescape = function( _, escaped, escapedWhitespace ) {
		var high = "0x" + escaped - 0x10000;
		// NaN means non-codepoint
		// Support: Firefox<24
		// Workaround erroneous numeric interpretation of +"0x"
		return high !== high || escapedWhitespace ?
			escaped :
			high < 0 ?
				// BMP codepoint
				String.fromCharCode( high + 0x10000 ) :
				// Supplemental Plane codepoint (surrogate pair)
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	},

	// Used for iframes
	// See setDocument()
	// Removing the function wrapper causes a "Permission Denied"
	// error in IE
	unloadHandler = function() {
		setDocument();
	};

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		(arr = slice.call( preferredDoc.childNodes )),
		preferredDoc.childNodes
	);
	// Support: Android<4.0
	// Detect silently failing push.apply
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			push_native.apply( target, slice.call(els) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;
			// Can't trust NodeList.length
			while ( (target[j++] = els[i++]) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var m, i, elem, nid, nidselect, match, groups, newSelector,
		newContext = context && context.ownerDocument,

		// nodeType defaults to 9, since context defaults to document
		nodeType = context ? context.nodeType : 9;

	results = results || [];

	// Return early from calls with invalid selector or context
	if ( typeof selector !== "string" || !selector ||
		nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

		return results;
	}

	// Try to shortcut find operations (as opposed to filters) in HTML documents
	if ( !seed ) {

		if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
			setDocument( context );
		}
		context = context || document;

		if ( documentIsHTML ) {

			// If the selector is sufficiently simple, try using a "get*By*" DOM method
			// (excepting DocumentFragment context, where the methods don't exist)
			if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {

				// ID selector
				if ( (m = match[1]) ) {

					// Document context
					if ( nodeType === 9 ) {
						if ( (elem = context.getElementById( m )) ) {

							// Support: IE, Opera, Webkit
							// TODO: identify versions
							// getElementById can match elements by name instead of ID
							if ( elem.id === m ) {
								results.push( elem );
								return results;
							}
						} else {
							return results;
						}

					// Element context
					} else {

						// Support: IE, Opera, Webkit
						// TODO: identify versions
						// getElementById can match elements by name instead of ID
						if ( newContext && (elem = newContext.getElementById( m )) &&
							contains( context, elem ) &&
							elem.id === m ) {

							results.push( elem );
							return results;
						}
					}

				// Type selector
				} else if ( match[2] ) {
					push.apply( results, context.getElementsByTagName( selector ) );
					return results;

				// Class selector
				} else if ( (m = match[3]) && support.getElementsByClassName &&
					context.getElementsByClassName ) {

					push.apply( results, context.getElementsByClassName( m ) );
					return results;
				}
			}

			// Take advantage of querySelectorAll
			if ( support.qsa &&
				!compilerCache[ selector + " " ] &&
				(!rbuggyQSA || !rbuggyQSA.test( selector )) ) {

				if ( nodeType !== 1 ) {
					newContext = context;
					newSelector = selector;

				// qSA looks outside Element context, which is not what we want
				// Thanks to Andrew Dupont for this workaround technique
				// Support: IE <=8
				// Exclude object elements
				} else if ( context.nodeName.toLowerCase() !== "object" ) {

					// Capture the context ID, setting it first if necessary
					if ( (nid = context.getAttribute( "id" )) ) {
						nid = nid.replace( rescape, "\\$&" );
					} else {
						context.setAttribute( "id", (nid = expando) );
					}

					// Prefix every selector in the list
					groups = tokenize( selector );
					i = groups.length;
					nidselect = ridentifier.test( nid ) ? "#" + nid : "[id='" + nid + "']";
					while ( i-- ) {
						groups[i] = nidselect + " " + toSelector( groups[i] );
					}
					newSelector = groups.join( "," );

					// Expand context for sibling selectors
					newContext = rsibling.test( selector ) && testContext( context.parentNode ) ||
						context;
				}

				if ( newSelector ) {
					try {
						push.apply( results,
							newContext.querySelectorAll( newSelector )
						);
						return results;
					} catch ( qsaError ) {
					} finally {
						if ( nid === expando ) {
							context.removeAttribute( "id" );
						}
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {function(string, object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {
		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {
			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return (cache[ key + " " ] = value);
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created div and expects a boolean result
 */
function assert( fn ) {
	var div = document.createElement("div");

	try {
		return !!fn( div );
	} catch (e) {
		return false;
	} finally {
		// Remove from its parent by default
		if ( div.parentNode ) {
			div.parentNode.removeChild( div );
		}
		// release memory in IE
		div = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split("|"),
		i = arr.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[i] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			( ~b.sourceIndex || MAX_NEGATIVE ) -
			( ~a.sourceIndex || MAX_NEGATIVE );

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( (cur = cur.nextSibling) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return (name === "input" || name === "button") && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction(function( argument ) {
		argument = +argument;
		return markFunction(function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ (j = matchIndexes[i]) ] ) {
					seed[j] = !(matches[j] = seed[j]);
				}
			}
		});
	});
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== "undefined" && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	// documentElement is verified for cases where it doesn't yet exist
	// (such as loading iframes in IE - #4833)
	var documentElement = elem && (elem.ownerDocument || elem).documentElement;
	return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare, parent,
		doc = node ? node.ownerDocument || node : preferredDoc;

	// Return early if doc is invalid or already selected
	if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Update global variables
	document = doc;
	docElem = document.documentElement;
	documentIsHTML = !isXML( document );

	// Support: IE 9-11, Edge
	// Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
	if ( (parent = document.defaultView) && parent.top !== parent ) {
		// Support: IE 11
		if ( parent.addEventListener ) {
			parent.addEventListener( "unload", unloadHandler, false );

		// Support: IE 9 - 10 only
		} else if ( parent.attachEvent ) {
			parent.attachEvent( "onunload", unloadHandler );
		}
	}

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties
	// (excepting IE8 booleans)
	support.attributes = assert(function( div ) {
		div.className = "i";
		return !div.getAttribute("className");
	});

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert(function( div ) {
		div.appendChild( document.createComment("") );
		return !div.getElementsByTagName("*").length;
	});

	// Support: IE<9
	support.getElementsByClassName = rnative.test( document.getElementsByClassName );

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert(function( div ) {
		docElem.appendChild( div ).id = expando;
		return !document.getElementsByName || !document.getElementsByName( expando ).length;
	});

	// ID find and filter
	if ( support.getById ) {
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var m = context.getElementById( id );
				return m ? [ m ] : [];
			}
		};
		Expr.filter["ID"] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute("id") === attrId;
			};
		};
	} else {
		// Support: IE6/7
		// getElementById is not reliable as a find shortcut
		delete Expr.find["ID"];

		Expr.filter["ID"] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== "undefined" &&
					elem.getAttributeNode("id");
				return node && node.value === attrId;
			};
		};
	}

	// Tag
	Expr.find["TAG"] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== "undefined" ) {
				return context.getElementsByTagName( tag );

			// DocumentFragment nodes don't have gEBTN
			} else if ( support.qsa ) {
				return context.querySelectorAll( tag );
			}
		} :

		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,
				// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( (elem = results[i++]) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
		if ( typeof context.getElementsByClassName !== "undefined" && documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See http://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( (support.qsa = rnative.test( document.querySelectorAll )) ) {
		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert(function( div ) {
			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// http://bugs.jquery.com/ticket/12359
			docElem.appendChild( div ).innerHTML = "<a id='" + expando + "'></a>" +
				"<select id='" + expando + "-\r\\' msallowcapture=''>" +
				"<option selected=''></option></select>";

			// Support: IE8, Opera 11-12.16
			// Nothing should be selected when empty strings follow ^= or $= or *=
			// The test attribute must be unknown in Opera but "safe" for WinRT
			// http://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
			if ( div.querySelectorAll("[msallowcapture^='']").length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !div.querySelectorAll("[selected]").length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+
			if ( !div.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
				rbuggyQSA.push("~=");
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":checked").length ) {
				rbuggyQSA.push(":checked");
			}

			// Support: Safari 8+, iOS 8+
			// https://bugs.webkit.org/show_bug.cgi?id=136851
			// In-page `selector#id sibing-combinator selector` fails
			if ( !div.querySelectorAll( "a#" + expando + "+*" ).length ) {
				rbuggyQSA.push(".#.+[+~]");
			}
		});

		assert(function( div ) {
			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = document.createElement("input");
			input.setAttribute( "type", "hidden" );
			div.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( div.querySelectorAll("[name=d]").length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":enabled").length ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Opera 10-11 does not throw on post-comma invalid pseudos
			div.querySelectorAll("*,:x");
			rbuggyQSA.push(",.*:");
		});
	}

	if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
		docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector) )) ) {

		assert(function( div ) {
			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( div, "div" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( div, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		});
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully self-exclusive
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			));
		} :
		function( a, b ) {
			if ( b ) {
				while ( (b = b.parentNode) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

			// Choose the first element that is related to our preferred document
			if ( a === document || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
				return -1;
			}
			if ( b === document || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {
		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {
			return a === document ? -1 :
				b === document ? 1 :
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( (cur = cur.parentNode) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( (cur = cur.parentNode) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[i] === bp[i] ) {
			i++;
		}

		return i ?
			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[i], bp[i] ) :

			// Otherwise nodes in our document sort first
			ap[i] === preferredDoc ? -1 :
			bp[i] === preferredDoc ? 1 :
			0;
	};

	return document;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	// Make sure that attribute selectors are quoted
	expr = expr.replace( rattributeQuotes, "='$1']" );

	if ( support.matchesSelector && documentIsHTML &&
		!compilerCache[ expr + " " ] &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||
					// As well, disconnected nodes are said to be in a document
					// fragment in IE 9
					elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch (e) {}
	}

	return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
	// Set document vars if needed
	if ( ( context.ownerDocument || context ) !== document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],
		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			(val = elem.getAttributeNode(name)) && val.specified ?
				val.value :
				null;
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( (elem = results[i++]) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {
		// If no nodeType, this is expected to be an array
		while ( (node = elem[i++]) ) {
			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {
			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}
	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[1] = match[1].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );

			if ( match[2] === "~=" ) {
				match[3] = " " + match[3] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {
			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[1] = match[1].toLowerCase();

			if ( match[1].slice( 0, 3 ) === "nth" ) {
				// nth-* requires argument
				if ( !match[3] ) {
					Sizzle.error( match[0] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
				match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

			// other types prohibit arguments
			} else if ( match[3] ) {
				Sizzle.error( match[0] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[6] && match[2];

			if ( matchExpr["CHILD"].test( match[0] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[3] ) {
				match[2] = match[4] || match[5] || "";

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&
				// Get excess from tokenize (recursively)
				(excess = tokenize( unquoted, true )) &&
				// advance to the next closing parenthesis
				(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

				// excess is a negative index
				match[0] = match[0].slice( 0, excess );
				match[2] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() { return true; } :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
				classCache( className, function( elem ) {
					return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
				});
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
			};
		},

		"CHILD": function( type, what, argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, context, xml ) {
					var cache, uniqueCache, outerCache, node, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType,
						diff = false;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( (node = node[ dir ]) ) {
									if ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) {

										return false;
									}
								}
								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {

							// Seek `elem` from a previously-cached index

							// ...in a gzip-friendly way
							node = parent;
							outerCache = node[ expando ] || (node[ expando ] = {});

							// Support: IE <9 only
							// Defend against cloned attroperties (jQuery gh-1709)
							uniqueCache = outerCache[ node.uniqueID ] ||
								(outerCache[ node.uniqueID ] = {});

							cache = uniqueCache[ type ] || [];
							nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
							diff = nodeIndex && cache[ 2 ];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( (node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								(diff = nodeIndex = 0) || start.pop()) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									uniqueCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						} else {
							// Use previously-cached element index if available
							if ( useCache ) {
								// ...in a gzip-friendly way
								node = elem;
								outerCache = node[ expando ] || (node[ expando ] = {});

								// Support: IE <9 only
								// Defend against cloned attroperties (jQuery gh-1709)
								uniqueCache = outerCache[ node.uniqueID ] ||
									(outerCache[ node.uniqueID ] = {});

								cache = uniqueCache[ type ] || [];
								nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
								diff = nodeIndex;
							}

							// xml :nth-child(...)
							// or :nth-last-child(...) or :nth(-last)?-of-type(...)
							if ( diff === false ) {
								// Use the same loop as above to seek `elem` from the start
								while ( (node = ++nodeIndex && node && node[ dir ] ||
									(diff = nodeIndex = 0) || start.pop()) ) {

									if ( ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) &&
										++diff ) {

										// Cache the index of each encountered element
										if ( useCache ) {
											outerCache = node[ expando ] || (node[ expando ] = {});

											// Support: IE <9 only
											// Defend against cloned attroperties (jQuery gh-1709)
											uniqueCache = outerCache[ node.uniqueID ] ||
												(outerCache[ node.uniqueID ] = {});

											uniqueCache[ type ] = [ dirruns, diff ];
										}

										if ( node === elem ) {
											break;
										}
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {
			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction(function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf( seed, matched[i] );
							seed[ idx ] = !( matches[ idx ] = matched[i] );
						}
					}) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {
		// Potentially complex pseudos
		"not": markFunction(function( selector ) {
			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction(function( seed, matches, context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( (elem = unmatched[i]) ) {
							seed[i] = !(matches[i] = elem);
						}
					}
				}) :
				function( elem, context, xml ) {
					input[0] = elem;
					matcher( input, null, xml, results );
					// Don't keep the element (issue #299)
					input[0] = null;
					return !results.pop();
				};
		}),

		"has": markFunction(function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		}),

		"contains": markFunction(function( text ) {
			text = text.replace( runescape, funescape );
			return function( elem ) {
				return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
			};
		}),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {
			// lang value must be a valid identifier
			if ( !ridentifier.test(lang || "") ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( (elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
				return false;
			};
		}),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
		},

		// Boolean properties
		"enabled": function( elem ) {
			return elem.disabled === false;
		},

		"disabled": function( elem ) {
			return elem.disabled === true;
		},

		"checked": function( elem ) {
			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
		},

		"selected": function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {
			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos["empty"]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo(function() {
			return [ 0 ];
		}),

		"last": createPositionalPseudo(function( matchIndexes, length ) {
			return [ length - 1 ];
		}),

		"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		}),

		"even": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"odd": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		})
	}
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || (match = rcomma.exec( soFar )) ) {
			if ( match ) {
				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[0].length ) || soFar;
			}
			groups.push( (tokens = []) );
		}

		matched = false;

		// Combinators
		if ( (match = rcombinators.exec( soFar )) ) {
			matched = match.shift();
			tokens.push({
				value: matched,
				// Cast descendant combinators to space
				type: match[0].replace( rtrim, " " )
			});
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
				(match = preFilters[ type ]( match ))) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					type: type,
					matches: match
				});
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :
			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
};

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[i].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		checkNonElements = base && dir === "parentNode",
		doneName = done++;

	return combinator.first ?
		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( (elem = elem[ dir ]) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, uniqueCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching
			if ( xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || (elem[ expando ] = {});

						// Support: IE <9 only
						// Defend against cloned attroperties (jQuery gh-1709)
						uniqueCache = outerCache[ elem.uniqueID ] || (outerCache[ elem.uniqueID ] = {});

						if ( (oldCache = uniqueCache[ dir ]) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return (newCache[ 2 ] = oldCache[ 2 ]);
						} else {
							// Reuse newcache so results back-propagate to previous elements
							uniqueCache[ dir ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
								return true;
							}
						}
					}
				}
			}
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[i]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[0];
}

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[i], results );
	}
	return results;
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( (elem = unmatched[i]) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction(function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?
				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( (elem = temp[i]) ) {
					matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {
					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) ) {
							// Restore matcherIn since elem is not yet a final match
							temp.push( (matcherIn[i] = elem) );
						}
					}
					postFinder( null, (matcherOut = []), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( (elem = matcherOut[i]) &&
						(temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {

						seed[temp] = !(results[temp] = elem);
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	});
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[0].type ],
		implicitRelative = leadingRelative || Expr.relative[" "],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				(checkContext = context).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );
			// Avoid hanging onto element (issue #299)
			checkContext = null;
			return ret;
		} ];

	for ( ; i < len; i++ ) {
		if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
			matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
		} else {
			matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {
				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[j].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(
						// If the preceding token was a descendant combinator, insert an implicit any-element `*`
						tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,
				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
				len = elems.length;

			if ( outermost ) {
				outermostContext = context === document || context || outermost;
			}

			// Add elements passing elementMatchers directly to results
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;
					if ( !context && elem.ownerDocument !== document ) {
						setDocument( elem );
						xml = !documentIsHTML;
					}
					while ( (matcher = elementMatchers[j++]) ) {
						if ( matcher( elem, context || document, xml) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {
					// They will have gone through all possible matchers
					if ( (elem = !matcher && elem) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// `i` is now the count of elements visited above, and adding it to `matchedCount`
			// makes the latter nonnegative.
			matchedCount += i;

			// Apply set filters to unmatched elements
			// NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
			// equals `i`), unless we didn't visit _any_ elements in the above loop because we have
			// no element matchers and no seed.
			// Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
			// case, which will result in a "00" `matchedCount` that differs from `i` but is also
			// numerically zero.
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( (matcher = setMatchers[j++]) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {
					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !(unmatched[i] || setMatched[i]) ) {
								setMatched[i] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {
		// Generate a function of recursive functions that can be used to check each element
		if ( !match ) {
			match = tokenize( selector );
		}
		i = match.length;
		while ( i-- ) {
			cached = matcherFromTokens( match[i] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );

		// Save selector and tokenization
		cached.selector = selector;
	}
	return cached;
};

/**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
select = Sizzle.select = function( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		compiled = typeof selector === "function" && selector,
		match = !seed && tokenize( (selector = compiled.selector || selector) );

	results = results || [];

	// Try to minimize operations if there is only one selector in the list and no seed
	// (the latter of which guarantees us context)
	if ( match.length === 1 ) {

		// Reduce context if the leading compound selector is an ID
		tokens = match[0] = match[0].slice( 0 );
		if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
				support.getById && context.nodeType === 9 && documentIsHTML &&
				Expr.relative[ tokens[1].type ] ) {

			context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
			if ( !context ) {
				return results;

			// Precompiled matchers will still verify ancestry, so step up a level
			} else if ( compiled ) {
				context = context.parentNode;
			}

			selector = selector.slice( tokens.shift().value.length );
		}

		// Fetch a seed set for right-to-left matching
		i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
		while ( i-- ) {
			token = tokens[i];

			// Abort if we hit a combinator
			if ( Expr.relative[ (type = token.type) ] ) {
				break;
			}
			if ( (find = Expr.find[ type ]) ) {
				// Search, expanding context for leading sibling combinators
				if ( (seed = find(
					token.matches[0].replace( runescape, funescape ),
					rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
				)) ) {

					// If seed is empty or no tokens remain, we can return early
					tokens.splice( i, 1 );
					selector = seed.length && toSelector( tokens );
					if ( !selector ) {
						push.apply( results, seed );
						return results;
					}

					break;
				}
			}
		}
	}

	// Compile and execute a filtering function if one is not provided
	// Provide `match` to avoid retokenization if we modified the selector above
	( compiled || compile( selector, match ) )(
		seed,
		context,
		!documentIsHTML,
		results,
		!context || rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( div1 ) {
	// Should return 1, but returns 4 (following)
	return div1.compareDocumentPosition( document.createElement("div") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( div ) {
	div.innerHTML = "<a href='#'></a>";
	return div.firstChild.getAttribute("href") === "#" ;
}) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	});
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( div ) {
	div.innerHTML = "<input/>";
	div.firstChild.setAttribute( "value", "" );
	return div.firstChild.getAttribute( "value" ) === "";
}) ) {
	addHandle( "value", function( elem, name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	});
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( div ) {
	return div.getAttribute("disabled") == null;
}) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
					(val = elem.getAttributeNode( name )) && val.specified ?
					val.value :
				null;
		}
	});
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;
jQuery.expr[ ":" ] = jQuery.expr.pseudos;
jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;



var dir = function( elem, dir, until ) {
	var matched = [],
		truncate = until !== undefined;

	while ( ( elem = elem[ dir ] ) && elem.nodeType !== 9 ) {
		if ( elem.nodeType === 1 ) {
			if ( truncate && jQuery( elem ).is( until ) ) {
				break;
			}
			matched.push( elem );
		}
	}
	return matched;
};


var siblings = function( n, elem ) {
	var matched = [];

	for ( ; n; n = n.nextSibling ) {
		if ( n.nodeType === 1 && n !== elem ) {
			matched.push( n );
		}
	}

	return matched;
};


var rneedsContext = jQuery.expr.match.needsContext;

var rsingleTag = ( /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/ );



var risSimple = /^.[^:#\[\.,]*$/;

// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( jQuery.isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			/* jshint -W018 */
			return !!qualifier.call( elem, i, elem ) !== not;
		} );

	}

	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		} );

	}

	if ( typeof qualifier === "string" ) {
		if ( risSimple.test( qualifier ) ) {
			return jQuery.filter( qualifier, elements, not );
		}

		qualifier = jQuery.filter( qualifier, elements );
	}

	return jQuery.grep( elements, function( elem ) {
		return ( indexOf.call( qualifier, elem ) > -1 ) !== not;
	} );
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	return elems.length === 1 && elem.nodeType === 1 ?
		jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [] :
		jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
			return elem.nodeType === 1;
		} ) );
};

jQuery.fn.extend( {
	find: function( selector ) {
		var i,
			len = this.length,
			ret = [],
			self = this;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter( function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			} ) );
		}

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		// Needed because $( selector, context ) becomes $( context ).find( selector )
		ret = this.pushStack( len > 1 ? jQuery.unique( ret ) : ret );
		ret.selector = this.selector ? this.selector + " " + selector : selector;
		return ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow( this, selector || [], false ) );
	},
	not: function( selector ) {
		return this.pushStack( winnow( this, selector || [], true ) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
} );


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,

	init = jQuery.fn.init = function( selector, context, root ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Method init() accepts an alternate rootjQuery
		// so migrate can support jQuery.sub (gh-2101)
		root = root || rootjQuery;

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector[ 0 ] === "<" &&
				selector[ selector.length - 1 ] === ">" &&
				selector.length >= 3 ) {

				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && ( match[ 1 ] || !context ) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[ 1 ] ) {
					context = context instanceof jQuery ? context[ 0 ] : context;

					// Option to run scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[ 1 ],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[ 1 ] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {

							// Properties of context are called as methods if possible
							if ( jQuery.isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[ 2 ] );

					// Support: Blackberry 4.6
					// gEBID returns nodes no longer in the document (#6963)
					if ( elem && elem.parentNode ) {

						// Inject the element directly into the jQuery object
						this.length = 1;
						this[ 0 ] = elem;
					}

					this.context = document;
					this.selector = selector;
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || root ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this.context = this[ 0 ] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( jQuery.isFunction( selector ) ) {
			return root.ready !== undefined ?
				root.ready( selector ) :

				// Execute immediately if ready is not present
				selector( jQuery );
		}

		if ( selector.selector !== undefined ) {
			this.selector = selector.selector;
			this.context = selector.context;
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,

	// Methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.fn.extend( {
	has: function( target ) {
		var targets = jQuery( target, this ),
			l = targets.length;

		return this.filter( function() {
			var i = 0;
			for ( ; i < l; i++ ) {
				if ( jQuery.contains( this, targets[ i ] ) ) {
					return true;
				}
			}
		} );
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			pos = rneedsContext.test( selectors ) || typeof selectors !== "string" ?
				jQuery( selectors, context || this.context ) :
				0;

		for ( ; i < l; i++ ) {
			for ( cur = this[ i ]; cur && cur !== context; cur = cur.parentNode ) {

				// Always skip document fragments
				if ( cur.nodeType < 11 && ( pos ?
					pos.index( cur ) > -1 :

					// Don't pass non-elements to Sizzle
					cur.nodeType === 1 &&
						jQuery.find.matchesSelector( cur, selectors ) ) ) {

					matched.push( cur );
					break;
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.uniqueSort( matched ) : matched );
	},

	// Determine the position of an element within the set
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
		}

		// Index in selector
		if ( typeof elem === "string" ) {
			return indexOf.call( jQuery( elem ), this[ 0 ] );
		}

		// Locate the position of the desired element
		return indexOf.call( this,

			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[ 0 ] : elem
		);
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.uniqueSort(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter( selector )
		);
	}
} );

function sibling( cur, dir ) {
	while ( ( cur = cur[ dir ] ) && cur.nodeType !== 1 ) {}
	return cur;
}

jQuery.each( {
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return siblings( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return siblings( elem.firstChild );
	},
	contents: function( elem ) {
		return elem.contentDocument || jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var matched = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			matched = jQuery.filter( selector, matched );
		}

		if ( this.length > 1 ) {

			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				jQuery.uniqueSort( matched );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				matched.reverse();
			}
		}

		return this.pushStack( matched );
	};
} );
var rnotwhite = ( /\S+/g );



// Convert String-formatted options into Object-formatted ones
function createOptions( options ) {
	var object = {};
	jQuery.each( options.match( rnotwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	} );
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		createOptions( options ) :
		jQuery.extend( {}, options );

	var // Flag to know if list is currently firing
		firing,

		// Last fire value for non-forgettable lists
		memory,

		// Flag to know if list was already fired
		fired,

		// Flag to prevent firing
		locked,

		// Actual callback list
		list = [],

		// Queue of execution data for repeatable lists
		queue = [],

		// Index of currently firing callback (modified by add/remove as needed)
		firingIndex = -1,

		// Fire callbacks
		fire = function() {

			// Enforce single-firing
			locked = options.once;

			// Execute callbacks for all pending executions,
			// respecting firingIndex overrides and runtime changes
			fired = firing = true;
			for ( ; queue.length; firingIndex = -1 ) {
				memory = queue.shift();
				while ( ++firingIndex < list.length ) {

					// Run callback and check for early termination
					if ( list[ firingIndex ].apply( memory[ 0 ], memory[ 1 ] ) === false &&
						options.stopOnFalse ) {

						// Jump to end and forget the data so .add doesn't re-fire
						firingIndex = list.length;
						memory = false;
					}
				}
			}

			// Forget the data if we're done with it
			if ( !options.memory ) {
				memory = false;
			}

			firing = false;

			// Clean up if we're done firing for good
			if ( locked ) {

				// Keep an empty list if we have data for future add calls
				if ( memory ) {
					list = [];

				// Otherwise, this object is spent
				} else {
					list = "";
				}
			}
		},

		// Actual Callbacks object
		self = {

			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {

					// If we have memory from a past run, we should fire after adding
					if ( memory && !firing ) {
						firingIndex = list.length - 1;
						queue.push( memory );
					}

					( function add( args ) {
						jQuery.each( args, function( _, arg ) {
							if ( jQuery.isFunction( arg ) ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && jQuery.type( arg ) !== "string" ) {

								// Inspect recursively
								add( arg );
							}
						} );
					} )( arguments );

					if ( memory && !firing ) {
						fire();
					}
				}
				return this;
			},

			// Remove a callback from the list
			remove: function() {
				jQuery.each( arguments, function( _, arg ) {
					var index;
					while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
						list.splice( index, 1 );

						// Handle firing indexes
						if ( index <= firingIndex ) {
							firingIndex--;
						}
					}
				} );
				return this;
			},

			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ?
					jQuery.inArray( fn, list ) > -1 :
					list.length > 0;
			},

			// Remove all callbacks from the list
			empty: function() {
				if ( list ) {
					list = [];
				}
				return this;
			},

			// Disable .fire and .add
			// Abort any current/pending executions
			// Clear all callbacks and values
			disable: function() {
				locked = queue = [];
				list = memory = "";
				return this;
			},
			disabled: function() {
				return !list;
			},

			// Disable .fire
			// Also disable .add unless we have memory (since it would have no effect)
			// Abort any pending executions
			lock: function() {
				locked = queue = [];
				if ( !memory ) {
					list = memory = "";
				}
				return this;
			},
			locked: function() {
				return !!locked;
			},

			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( !locked ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					queue.push( args );
					if ( !firing ) {
						fire();
					}
				}
				return this;
			},

			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},

			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


jQuery.extend( {

	Deferred: function( func ) {
		var tuples = [

				// action, add listener, listener list, final state
				[ "resolve", "done", jQuery.Callbacks( "once memory" ), "resolved" ],
				[ "reject", "fail", jQuery.Callbacks( "once memory" ), "rejected" ],
				[ "notify", "progress", jQuery.Callbacks( "memory" ) ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				then: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;
					return jQuery.Deferred( function( newDefer ) {
						jQuery.each( tuples, function( i, tuple ) {
							var fn = jQuery.isFunction( fns[ i ] ) && fns[ i ];

							// deferred[ done | fail | progress ] for forwarding actions to newDefer
							deferred[ tuple[ 1 ] ]( function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && jQuery.isFunction( returned.promise ) ) {
									returned.promise()
										.progress( newDefer.notify )
										.done( newDefer.resolve )
										.fail( newDefer.reject );
								} else {
									newDefer[ tuple[ 0 ] + "With" ](
										this === promise ? newDefer.promise() : this,
										fn ? [ returned ] : arguments
									);
								}
							} );
						} );
						fns = null;
					} ).promise();
				},

				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Keep pipe for back-compat
		promise.pipe = promise.then;

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 3 ];

			// promise[ done | fail | progress ] = list.add
			promise[ tuple[ 1 ] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add( function() {

					// state = [ resolved | rejected ]
					state = stateString;

				// [ reject_list | resolve_list ].disable; progress_list.lock
				}, tuples[ i ^ 1 ][ 2 ].disable, tuples[ 2 ][ 2 ].lock );
			}

			// deferred[ resolve | reject | notify ]
			deferred[ tuple[ 0 ] ] = function() {
				deferred[ tuple[ 0 ] + "With" ]( this === deferred ? promise : this, arguments );
				return this;
			};
			deferred[ tuple[ 0 ] + "With" ] = list.fireWith;
		} );

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( subordinate /* , ..., subordinateN */ ) {
		var i = 0,
			resolveValues = slice.call( arguments ),
			length = resolveValues.length,

			// the count of uncompleted subordinates
			remaining = length !== 1 ||
				( subordinate && jQuery.isFunction( subordinate.promise ) ) ? length : 0,

			// the master Deferred.
			// If resolveValues consist of only a single Deferred, just use that.
			deferred = remaining === 1 ? subordinate : jQuery.Deferred(),

			// Update function for both resolve and progress values
			updateFunc = function( i, contexts, values ) {
				return function( value ) {
					contexts[ i ] = this;
					values[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( values === progressValues ) {
						deferred.notifyWith( contexts, values );
					} else if ( !( --remaining ) ) {
						deferred.resolveWith( contexts, values );
					}
				};
			},

			progressValues, progressContexts, resolveContexts;

		// Add listeners to Deferred subordinates; treat others as resolved
		if ( length > 1 ) {
			progressValues = new Array( length );
			progressContexts = new Array( length );
			resolveContexts = new Array( length );
			for ( ; i < length; i++ ) {
				if ( resolveValues[ i ] && jQuery.isFunction( resolveValues[ i ].promise ) ) {
					resolveValues[ i ].promise()
						.progress( updateFunc( i, progressContexts, progressValues ) )
						.done( updateFunc( i, resolveContexts, resolveValues ) )
						.fail( deferred.reject );
				} else {
					--remaining;
				}
			}
		}

		// If we're not waiting on anything, resolve the master
		if ( !remaining ) {
			deferred.resolveWith( resolveContexts, resolveValues );
		}

		return deferred.promise();
	}
} );


// The deferred used on DOM ready
var readyList;

jQuery.fn.ready = function( fn ) {

	// Add the callback
	jQuery.ready.promise().done( fn );

	return this;
};

jQuery.extend( {

	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Hold (or release) the ready event
	holdReady: function( hold ) {
		if ( hold ) {
			jQuery.readyWait++;
		} else {
			jQuery.ready( true );
		}
	},

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );

		// Trigger any bound ready events
		if ( jQuery.fn.triggerHandler ) {
			jQuery( document ).triggerHandler( "ready" );
			jQuery( document ).off( "ready" );
		}
	}
} );

/**
 * The ready event handler and self cleanup method
 */
function completed() {
	document.removeEventListener( "DOMContentLoaded", completed );
	window.removeEventListener( "load", completed );
	jQuery.ready();
}

jQuery.ready.promise = function( obj ) {
	if ( !readyList ) {

		readyList = jQuery.Deferred();

		// Catch cases where $(document).ready() is called
		// after the browser event has already occurred.
		// Support: IE9-10 only
		// Older IE sometimes signals "interactive" too soon
		if ( document.readyState === "complete" ||
			( document.readyState !== "loading" && !document.documentElement.doScroll ) ) {

			// Handle it asynchronously to allow scripts the opportunity to delay ready
			window.setTimeout( jQuery.ready );

		} else {

			// Use the handy event callback
			document.addEventListener( "DOMContentLoaded", completed );

			// A fallback to window.onload, that will always work
			window.addEventListener( "load", completed );
		}
	}
	return readyList.promise( obj );
};

// Kick off the DOM ready check even if the user does not
jQuery.ready.promise();




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		len = elems.length,
		bulk = key == null;

	// Sets many values
	if ( jQuery.type( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			access( elems, fn, i, key[ i ], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !jQuery.isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {

			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < len; i++ ) {
				fn(
					elems[ i ], key, raw ?
					value :
					value.call( elems[ i ], i, fn( elems[ i ], key ) )
				);
			}
		}
	}

	return chainable ?
		elems :

		// Gets
		bulk ?
			fn.call( elems ) :
			len ? fn( elems[ 0 ], key ) : emptyGet;
};
var acceptData = function( owner ) {

	// Accepts only:
	//  - Node
	//    - Node.ELEMENT_NODE
	//    - Node.DOCUMENT_NODE
	//  - Object
	//    - Any
	/* jshint -W018 */
	return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};




function Data() {
	this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;

Data.prototype = {

	register: function( owner, initial ) {
		var value = initial || {};

		// If it is a node unlikely to be stringify-ed or looped over
		// use plain assignment
		if ( owner.nodeType ) {
			owner[ this.expando ] = value;

		// Otherwise secure it in a non-enumerable, non-writable property
		// configurability must be true to allow the property to be
		// deleted with the delete operator
		} else {
			Object.defineProperty( owner, this.expando, {
				value: value,
				writable: true,
				configurable: true
			} );
		}
		return owner[ this.expando ];
	},
	cache: function( owner ) {

		// We can accept data for non-element nodes in modern browsers,
		// but we should not, see #8335.
		// Always return an empty object.
		if ( !acceptData( owner ) ) {
			return {};
		}

		// Check if the owner object already has a cache
		var value = owner[ this.expando ];

		// If not, create one
		if ( !value ) {
			value = {};

			// We can accept data for non-element nodes in modern browsers,
			// but we should not, see #8335.
			// Always return an empty object.
			if ( acceptData( owner ) ) {

				// If it is a node unlikely to be stringify-ed or looped over
				// use plain assignment
				if ( owner.nodeType ) {
					owner[ this.expando ] = value;

				// Otherwise secure it in a non-enumerable property
				// configurable must be true to allow the property to be
				// deleted when data is removed
				} else {
					Object.defineProperty( owner, this.expando, {
						value: value,
						configurable: true
					} );
				}
			}
		}

		return value;
	},
	set: function( owner, data, value ) {
		var prop,
			cache = this.cache( owner );

		// Handle: [ owner, key, value ] args
		if ( typeof data === "string" ) {
			cache[ data ] = value;

		// Handle: [ owner, { properties } ] args
		} else {

			// Copy the properties one-by-one to the cache object
			for ( prop in data ) {
				cache[ prop ] = data[ prop ];
			}
		}
		return cache;
	},
	get: function( owner, key ) {
		return key === undefined ?
			this.cache( owner ) :
			owner[ this.expando ] && owner[ this.expando ][ key ];
	},
	access: function( owner, key, value ) {
		var stored;

		// In cases where either:
		//
		//   1. No key was specified
		//   2. A string key was specified, but no value provided
		//
		// Take the "read" path and allow the get method to determine
		// which value to return, respectively either:
		//
		//   1. The entire cache object
		//   2. The data stored at the key
		//
		if ( key === undefined ||
				( ( key && typeof key === "string" ) && value === undefined ) ) {

			stored = this.get( owner, key );

			return stored !== undefined ?
				stored : this.get( owner, jQuery.camelCase( key ) );
		}

		// When the key is not a string, or both a key and value
		// are specified, set or extend (existing objects) with either:
		//
		//   1. An object of properties
		//   2. A key and value
		//
		this.set( owner, key, value );

		// Since the "set" path can have two possible entry points
		// return the expected data based on which path was taken[*]
		return value !== undefined ? value : key;
	},
	remove: function( owner, key ) {
		var i, name, camel,
			cache = owner[ this.expando ];

		if ( cache === undefined ) {
			return;
		}

		if ( key === undefined ) {
			this.register( owner );

		} else {

			// Support array or space separated string of keys
			if ( jQuery.isArray( key ) ) {

				// If "name" is an array of keys...
				// When data is initially created, via ("key", "val") signature,
				// keys will be converted to camelCase.
				// Since there is no way to tell _how_ a key was added, remove
				// both plain key and camelCase key. #12786
				// This will only penalize the array argument path.
				name = key.concat( key.map( jQuery.camelCase ) );
			} else {
				camel = jQuery.camelCase( key );

				// Try the string as a key before any manipulation
				if ( key in cache ) {
					name = [ key, camel ];
				} else {

					// If a key with the spaces exists, use it.
					// Otherwise, create an array by matching non-whitespace
					name = camel;
					name = name in cache ?
						[ name ] : ( name.match( rnotwhite ) || [] );
				}
			}

			i = name.length;

			while ( i-- ) {
				delete cache[ name[ i ] ];
			}
		}

		// Remove the expando if there's no more data
		if ( key === undefined || jQuery.isEmptyObject( cache ) ) {

			// Support: Chrome <= 35-45+
			// Webkit & Blink performance suffers when deleting properties
			// from DOM nodes, so set to undefined instead
			// https://code.google.com/p/chromium/issues/detail?id=378607
			if ( owner.nodeType ) {
				owner[ this.expando ] = undefined;
			} else {
				delete owner[ this.expando ];
			}
		}
	},
	hasData: function( owner ) {
		var cache = owner[ this.expando ];
		return cache !== undefined && !jQuery.isEmptyObject( cache );
	}
};
var dataPriv = new Data();

var dataUser = new Data();



//	Implementation Summary
//
//	1. Enforce API surface and semantic compatibility with 1.9.x branch
//	2. Improve the module's maintainability by reducing the storage
//		paths to a single mechanism.
//	3. Use the same single mechanism to support "private" and "user" data.
//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//	5. Avoid exposing implementation details on user objects (eg. expando properties)
//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /[A-Z]/g;

function dataAttr( elem, key, data ) {
	var name;

	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {
		name = "data-" + key.replace( rmultiDash, "-$&" ).toLowerCase();
		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = data === "true" ? true :
					data === "false" ? false :
					data === "null" ? null :

					// Only convert to a number if it doesn't change the string
					+data + "" === data ? +data :
					rbrace.test( data ) ? jQuery.parseJSON( data ) :
					data;
			} catch ( e ) {}

			// Make sure we set the data so it isn't changed later
			dataUser.set( elem, key, data );
		} else {
			data = undefined;
		}
	}
	return data;
}

jQuery.extend( {
	hasData: function( elem ) {
		return dataUser.hasData( elem ) || dataPriv.hasData( elem );
	},

	data: function( elem, name, data ) {
		return dataUser.access( elem, name, data );
	},

	removeData: function( elem, name ) {
		dataUser.remove( elem, name );
	},

	// TODO: Now that all calls to _data and _removeData have been replaced
	// with direct calls to dataPriv methods, these can be deprecated.
	_data: function( elem, name, data ) {
		return dataPriv.access( elem, name, data );
	},

	_removeData: function( elem, name ) {
		dataPriv.remove( elem, name );
	}
} );

jQuery.fn.extend( {
	data: function( key, value ) {
		var i, name, data,
			elem = this[ 0 ],
			attrs = elem && elem.attributes;

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = dataUser.get( elem );

				if ( elem.nodeType === 1 && !dataPriv.get( elem, "hasDataAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {

						// Support: IE11+
						// The attrs elements can be null (#14894)
						if ( attrs[ i ] ) {
							name = attrs[ i ].name;
							if ( name.indexOf( "data-" ) === 0 ) {
								name = jQuery.camelCase( name.slice( 5 ) );
								dataAttr( elem, name, data[ name ] );
							}
						}
					}
					dataPriv.set( elem, "hasDataAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each( function() {
				dataUser.set( this, key );
			} );
		}

		return access( this, function( value ) {
			var data, camelKey;

			// The calling jQuery object (element matches) is not empty
			// (and therefore has an element appears at this[ 0 ]) and the
			// `value` parameter was not undefined. An empty jQuery object
			// will result in `undefined` for elem = this[ 0 ] which will
			// throw an exception if an attempt to read a data cache is made.
			if ( elem && value === undefined ) {

				// Attempt to get data from the cache
				// with the key as-is
				data = dataUser.get( elem, key ) ||

					// Try to find dashed key if it exists (gh-2779)
					// This is for 2.2.x only
					dataUser.get( elem, key.replace( rmultiDash, "-$&" ).toLowerCase() );

				if ( data !== undefined ) {
					return data;
				}

				camelKey = jQuery.camelCase( key );

				// Attempt to get data from the cache
				// with the key camelized
				data = dataUser.get( elem, camelKey );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to "discover" the data in
				// HTML5 custom data-* attrs
				data = dataAttr( elem, camelKey, undefined );
				if ( data !== undefined ) {
					return data;
				}

				// We tried really hard, but the data doesn't exist.
				return;
			}

			// Set the data...
			camelKey = jQuery.camelCase( key );
			this.each( function() {

				// First, attempt to store a copy or reference of any
				// data that might've been store with a camelCased key.
				var data = dataUser.get( this, camelKey );

				// For HTML5 data-* attribute interop, we have to
				// store property names with dashes in a camelCase form.
				// This might not apply to all properties...*
				dataUser.set( this, camelKey, value );

				// *... In the case of properties that might _actually_
				// have dashes, we need to also store a copy of that
				// unchanged property.
				if ( key.indexOf( "-" ) > -1 && data !== undefined ) {
					dataUser.set( this, key, value );
				}
			} );
		}, null, value, arguments.length > 1, null, true );
	},

	removeData: function( key ) {
		return this.each( function() {
			dataUser.remove( this, key );
		} );
	}
} );


jQuery.extend( {
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = dataPriv.get( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || jQuery.isArray( data ) ) {
					queue = dataPriv.access( elem, type, jQuery.makeArray( data ) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// Clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// Not public - generate a queueHooks object, or return the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return dataPriv.get( elem, key ) || dataPriv.access( elem, key, {
			empty: jQuery.Callbacks( "once memory" ).add( function() {
				dataPriv.remove( elem, [ type + "queue", key ] );
			} )
		} );
	}
} );

jQuery.fn.extend( {
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[ 0 ], type );
		}

		return data === undefined ?
			this :
			this.each( function() {
				var queue = jQuery.queue( this, type, data );

				// Ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[ 0 ] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			} );
	},
	dequeue: function( type ) {
		return this.each( function() {
			jQuery.dequeue( this, type );
		} );
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},

	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = dataPriv.get( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
} );
var pnum = ( /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/ ).source;

var rcssNum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" );


var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var isHidden = function( elem, el ) {

		// isHidden might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;
		return jQuery.css( elem, "display" ) === "none" ||
			!jQuery.contains( elem.ownerDocument, elem );
	};



function adjustCSS( elem, prop, valueParts, tween ) {
	var adjusted,
		scale = 1,
		maxIterations = 20,
		currentValue = tween ?
			function() { return tween.cur(); } :
			function() { return jQuery.css( elem, prop, "" ); },
		initial = currentValue(),
		unit = valueParts && valueParts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

		// Starting value computation is required for potential unit mismatches
		initialInUnit = ( jQuery.cssNumber[ prop ] || unit !== "px" && +initial ) &&
			rcssNum.exec( jQuery.css( elem, prop ) );

	if ( initialInUnit && initialInUnit[ 3 ] !== unit ) {

		// Trust units reported by jQuery.css
		unit = unit || initialInUnit[ 3 ];

		// Make sure we update the tween properties later on
		valueParts = valueParts || [];

		// Iteratively approximate from a nonzero starting point
		initialInUnit = +initial || 1;

		do {

			// If previous iteration zeroed out, double until we get *something*.
			// Use string for doubling so we don't accidentally see scale as unchanged below
			scale = scale || ".5";

			// Adjust and apply
			initialInUnit = initialInUnit / scale;
			jQuery.style( elem, prop, initialInUnit + unit );

		// Update scale, tolerating zero or NaN from tween.cur()
		// Break the loop if scale is unchanged or perfect, or if we've just had enough.
		} while (
			scale !== ( scale = currentValue() / initial ) && scale !== 1 && --maxIterations
		);
	}

	if ( valueParts ) {
		initialInUnit = +initialInUnit || +initial || 0;

		// Apply relative offset (+=/-=) if specified
		adjusted = valueParts[ 1 ] ?
			initialInUnit + ( valueParts[ 1 ] + 1 ) * valueParts[ 2 ] :
			+valueParts[ 2 ];
		if ( tween ) {
			tween.unit = unit;
			tween.start = initialInUnit;
			tween.end = adjusted;
		}
	}
	return adjusted;
}
var rcheckableType = ( /^(?:checkbox|radio)$/i );

var rtagName = ( /<([\w:-]+)/ );

var rscriptType = ( /^$|\/(?:java|ecma)script/i );



// We have to close these tags to support XHTML (#13200)
var wrapMap = {

	// Support: IE9
	option: [ 1, "<select multiple='multiple'>", "</select>" ],

	// XHTML parsers do not magically insert elements in the
	// same way that tag soup parsers do. So we cannot shorten
	// this by omitting <tbody> or other required elements.
	thead: [ 1, "<table>", "</table>" ],
	col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
	tr: [ 2, "<table><tbody>", "</tbody></table>" ],
	td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

	_default: [ 0, "", "" ]
};

// Support: IE9
wrapMap.optgroup = wrapMap.option;

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;


function getAll( context, tag ) {

	// Support: IE9-11+
	// Use typeof to avoid zero-argument method invocation on host objects (#15151)
	var ret = typeof context.getElementsByTagName !== "undefined" ?
			context.getElementsByTagName( tag || "*" ) :
			typeof context.querySelectorAll !== "undefined" ?
				context.querySelectorAll( tag || "*" ) :
			[];

	return tag === undefined || tag && jQuery.nodeName( context, tag ) ?
		jQuery.merge( [ context ], ret ) :
		ret;
}


// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		dataPriv.set(
			elems[ i ],
			"globalEval",
			!refElements || dataPriv.get( refElements[ i ], "globalEval" )
		);
	}
}


var rhtml = /<|&#?\w+;/;

function buildFragment( elems, context, scripts, selection, ignored ) {
	var elem, tmp, tag, wrap, contains, j,
		fragment = context.createDocumentFragment(),
		nodes = [],
		i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		elem = elems[ i ];

		if ( elem || elem === 0 ) {

			// Add nodes directly
			if ( jQuery.type( elem ) === "object" ) {

				// Support: Android<4.1, PhantomJS<2
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

			// Convert non-html into a text node
			} else if ( !rhtml.test( elem ) ) {
				nodes.push( context.createTextNode( elem ) );

			// Convert html into DOM nodes
			} else {
				tmp = tmp || fragment.appendChild( context.createElement( "div" ) );

				// Deserialize a standard representation
				tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
				wrap = wrapMap[ tag ] || wrapMap._default;
				tmp.innerHTML = wrap[ 1 ] + jQuery.htmlPrefilter( elem ) + wrap[ 2 ];

				// Descend through wrappers to the right content
				j = wrap[ 0 ];
				while ( j-- ) {
					tmp = tmp.lastChild;
				}

				// Support: Android<4.1, PhantomJS<2
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, tmp.childNodes );

				// Remember the top-level container
				tmp = fragment.firstChild;

				// Ensure the created nodes are orphaned (#12392)
				tmp.textContent = "";
			}
		}
	}

	// Remove wrapper from fragment
	fragment.textContent = "";

	i = 0;
	while ( ( elem = nodes[ i++ ] ) ) {

		// Skip elements already in the context collection (trac-4087)
		if ( selection && jQuery.inArray( elem, selection ) > -1 ) {
			if ( ignored ) {
				ignored.push( elem );
			}
			continue;
		}

		contains = jQuery.contains( elem.ownerDocument, elem );

		// Append to fragment
		tmp = getAll( fragment.appendChild( elem ), "script" );

		// Preserve script evaluation history
		if ( contains ) {
			setGlobalEval( tmp );
		}

		// Capture executables
		if ( scripts ) {
			j = 0;
			while ( ( elem = tmp[ j++ ] ) ) {
				if ( rscriptType.test( elem.type || "" ) ) {
					scripts.push( elem );
				}
			}
		}
	}

	return fragment;
}


( function() {
	var fragment = document.createDocumentFragment(),
		div = fragment.appendChild( document.createElement( "div" ) ),
		input = document.createElement( "input" );

	// Support: Android 4.0-4.3, Safari<=5.1
	// Check state lost if the name is set (#11217)
	// Support: Windows Web Apps (WWA)
	// `name` and `type` must use .setAttribute for WWA (#14901)
	input.setAttribute( "type", "radio" );
	input.setAttribute( "checked", "checked" );
	input.setAttribute( "name", "t" );

	div.appendChild( input );

	// Support: Safari<=5.1, Android<4.2
	// Older WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE<=11+
	// Make sure textarea (and checkbox) defaultValue is properly cloned
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
} )();


var
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

// Support: IE9
// See #13393 for more info
function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

function on( elem, types, selector, data, fn, one ) {
	var origFn, type;

	// Types can be a map of types/handlers
	if ( typeof types === "object" ) {

		// ( types-Object, selector, data )
		if ( typeof selector !== "string" ) {

			// ( types-Object, data )
			data = data || selector;
			selector = undefined;
		}
		for ( type in types ) {
			on( elem, type, selector, data, types[ type ], one );
		}
		return elem;
	}

	if ( data == null && fn == null ) {

		// ( types, fn )
		fn = selector;
		data = selector = undefined;
	} else if ( fn == null ) {
		if ( typeof selector === "string" ) {

			// ( types, selector, fn )
			fn = data;
			data = undefined;
		} else {

			// ( types, data, fn )
			fn = data;
			data = selector;
			selector = undefined;
		}
	}
	if ( fn === false ) {
		fn = returnFalse;
	} else if ( !fn ) {
		return elem;
	}

	if ( one === 1 ) {
		origFn = fn;
		fn = function( event ) {

			// Can use an empty set, since event contains the info
			jQuery().off( event );
			return origFn.apply( this, arguments );
		};

		// Use same guid so caller can remove using origFn
		fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
	}
	return elem.each( function() {
		jQuery.event.add( this, types, fn, data, selector );
	} );
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {

		var handleObjIn, eventHandle, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.get( elem );

		// Don't attach events to noData or text/comment nodes (but allow plain objects)
		if ( !elemData ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !( events = elemData.events ) ) {
			events = elemData.events = {};
		}
		if ( !( eventHandle = elemData.handle ) ) {
			eventHandle = elemData.handle = function( e ) {

				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ?
					jQuery.event.dispatch.apply( elem, arguments ) : undefined;
			};
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend( {
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join( "." )
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !( handlers = events[ type ] ) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener if the special events handler returns false
				if ( !special.setup ||
					special.setup.call( elem, data, namespaces, eventHandle ) === false ) {

					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {

		var j, origCount, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.hasData( elem ) && dataPriv.get( elem );

		if ( !elemData || !( events = elemData.events ) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[ 2 ] &&
				new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector ||
						selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown ||
					special.teardown.call( elem, namespaces, elemData.handle ) === false ) {

					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove data and the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			dataPriv.remove( elem, "handle events" );
		}
	},

	dispatch: function( event ) {

		// Make a writable jQuery.Event from the native event object
		event = jQuery.event.fix( event );

		var i, j, ret, matched, handleObj,
			handlerQueue = [],
			args = slice.call( arguments ),
			handlers = ( dataPriv.get( this, "events" ) || {} )[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[ 0 ] = event;
		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( ( matched = handlerQueue[ i++ ] ) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( ( handleObj = matched.handlers[ j++ ] ) &&
				!event.isImmediatePropagationStopped() ) {

				// Triggered event must either 1) have no namespace, or 2) have namespace(s)
				// a subset or equal to those in the bound event (both can have no namespace).
				if ( !event.rnamespace || event.rnamespace.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( ( jQuery.event.special[ handleObj.origType ] || {} ).handle ||
						handleObj.handler ).apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( ( event.result = ret ) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var i, matches, sel, handleObj,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Support (at least): Chrome, IE9
		// Find delegate handlers
		// Black-hole SVG <use> instance trees (#13180)
		//
		// Support: Firefox<=42+
		// Avoid non-left-click in FF but don't block IE radio events (#3861, gh-2343)
		if ( delegateCount && cur.nodeType &&
			( event.type !== "click" || isNaN( event.button ) || event.button < 1 ) ) {

			for ( ; cur !== this; cur = cur.parentNode || this ) {

				// Don't check non-elements (#13208)
				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.nodeType === 1 && ( cur.disabled !== true || event.type !== "click" ) ) {
					matches = [];
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matches[ sel ] === undefined ) {
							matches[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) > -1 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matches[ sel ] ) {
							matches.push( handleObj );
						}
					}
					if ( matches.length ) {
						handlerQueue.push( { elem: cur, handlers: matches } );
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		if ( delegateCount < handlers.length ) {
			handlerQueue.push( { elem: this, handlers: handlers.slice( delegateCount ) } );
		}

		return handlerQueue;
	},

	// Includes some event props shared by KeyEvent and MouseEvent
	props: ( "altKey bubbles cancelable ctrlKey currentTarget detail eventPhase " +
		"metaKey relatedTarget shiftKey target timeStamp view which" ).split( " " ),

	fixHooks: {},

	keyHooks: {
		props: "char charCode key keyCode".split( " " ),
		filter: function( event, original ) {

			// Add which for key events
			if ( event.which == null ) {
				event.which = original.charCode != null ? original.charCode : original.keyCode;
			}

			return event;
		}
	},

	mouseHooks: {
		props: ( "button buttons clientX clientY offsetX offsetY pageX pageY " +
			"screenX screenY toElement" ).split( " " ),
		filter: function( event, original ) {
			var eventDoc, doc, body,
				button = original.button;

			// Calculate pageX/Y if missing and clientX/Y available
			if ( event.pageX == null && original.clientX != null ) {
				eventDoc = event.target.ownerDocument || document;
				doc = eventDoc.documentElement;
				body = eventDoc.body;

				event.pageX = original.clientX +
					( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) -
					( doc && doc.clientLeft || body && body.clientLeft || 0 );
				event.pageY = original.clientY +
					( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) -
					( doc && doc.clientTop  || body && body.clientTop  || 0 );
			}

			// Add which for click: 1 === left; 2 === middle; 3 === right
			// Note: button is not normalized, so don't use it
			if ( !event.which && button !== undefined ) {
				event.which = ( button & 1 ? 1 : ( button & 2 ? 3 : ( button & 4 ? 2 : 0 ) ) );
			}

			return event;
		}
	},

	fix: function( event ) {
		if ( event[ jQuery.expando ] ) {
			return event;
		}

		// Create a writable copy of the event object and normalize some properties
		var i, prop, copy,
			type = event.type,
			originalEvent = event,
			fixHook = this.fixHooks[ type ];

		if ( !fixHook ) {
			this.fixHooks[ type ] = fixHook =
				rmouseEvent.test( type ) ? this.mouseHooks :
				rkeyEvent.test( type ) ? this.keyHooks :
				{};
		}
		copy = fixHook.props ? this.props.concat( fixHook.props ) : this.props;

		event = new jQuery.Event( originalEvent );

		i = copy.length;
		while ( i-- ) {
			prop = copy[ i ];
			event[ prop ] = originalEvent[ prop ];
		}

		// Support: Cordova 2.5 (WebKit) (#13255)
		// All events should have a target; Cordova deviceready doesn't
		if ( !event.target ) {
			event.target = document;
		}

		// Support: Safari 6.0+, Chrome<28
		// Target should not be a text node (#504, #13143)
		if ( event.target.nodeType === 3 ) {
			event.target = event.target.parentNode;
		}

		return fixHook.filter ? fixHook.filter( event, originalEvent ) : event;
	},

	special: {
		load: {

			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		focus: {

			// Fire native event if possible so blur/focus sequence is correct
			trigger: function() {
				if ( this !== safeActiveElement() && this.focus ) {
					this.focus();
					return false;
				}
			},
			delegateType: "focusin"
		},
		blur: {
			trigger: function() {
				if ( this === safeActiveElement() && this.blur ) {
					this.blur();
					return false;
				}
			},
			delegateType: "focusout"
		},
		click: {

			// For checkbox, fire native event so checked state will be right
			trigger: function() {
				if ( this.type === "checkbox" && this.click && jQuery.nodeName( this, "input" ) ) {
					this.click();
					return false;
				}
			},

			// For cross-browser consistency, don't fire native .click() on links
			_default: function( event ) {
				return jQuery.nodeName( event.target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Support: Firefox 20+
				// Firefox doesn't alert if the returnValue field is not set.
				if ( event.result !== undefined && event.originalEvent ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	}
};

jQuery.removeEvent = function( elem, type, handle ) {

	// This "if" is needed for plain objects
	if ( elem.removeEventListener ) {
		elem.removeEventListener( type, handle );
	}
};

jQuery.Event = function( src, props ) {

	// Allow instantiation without the 'new' keyword
	if ( !( this instanceof jQuery.Event ) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined &&

				// Support: Android<4.0
				src.returnValue === false ?
			returnTrue :
			returnFalse;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || jQuery.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	constructor: jQuery.Event,
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,
	isSimulated: false,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;

		if ( e && !this.isSimulated ) {
			e.preventDefault();
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopPropagation();
		}
	},
	stopImmediatePropagation: function() {
		var e = this.originalEvent;

		this.isImmediatePropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopImmediatePropagation();
		}

		this.stopPropagation();
	}
};

// Create mouseenter/leave events using mouseover/out and event-time checks
// so that event delegation works in jQuery.
// Do the same for pointerenter/pointerleave and pointerover/pointerout
//
// Support: Safari 7 only
// Safari sends mouseenter too often; see:
// https://code.google.com/p/chromium/issues/detail?id=470258
// for the description of the bug (it existed in older Chrome versions as well).
jQuery.each( {
	mouseenter: "mouseover",
	mouseleave: "mouseout",
	pointerenter: "pointerover",
	pointerleave: "pointerout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mouseenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || ( related !== target && !jQuery.contains( target, related ) ) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
} );

jQuery.fn.extend( {
	on: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn );
	},
	one: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {

			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ?
					handleObj.origType + "." + handleObj.namespace :
					handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {

			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {

			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each( function() {
			jQuery.event.remove( this, types, fn, selector );
		} );
	}
} );


var
	rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,

	// Support: IE 10-11, Edge 10240+
	// In IE/Edge using regex groups here causes severe slowdowns.
	// See https://connect.microsoft.com/IE/feedback/details/1736512/
	rnoInnerhtml = /<script|<style|<link/i,

	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rscriptTypeMasked = /^true\/(.*)/,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

// Manipulating tables requires a tbody
function manipulationTarget( elem, content ) {
	return jQuery.nodeName( elem, "table" ) &&
		jQuery.nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ?

		elem.getElementsByTagName( "tbody" )[ 0 ] ||
			elem.appendChild( elem.ownerDocument.createElement( "tbody" ) ) :
		elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = ( elem.getAttribute( "type" ) !== null ) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	var match = rscriptTypeMasked.exec( elem.type );

	if ( match ) {
		elem.type = match[ 1 ];
	} else {
		elem.removeAttribute( "type" );
	}

	return elem;
}

function cloneCopyEvent( src, dest ) {
	var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;

	if ( dest.nodeType !== 1 ) {
		return;
	}

	// 1. Copy private data: events, handlers, etc.
	if ( dataPriv.hasData( src ) ) {
		pdataOld = dataPriv.access( src );
		pdataCur = dataPriv.set( dest, pdataOld );
		events = pdataOld.events;

		if ( events ) {
			delete pdataCur.handle;
			pdataCur.events = {};

			for ( type in events ) {
				for ( i = 0, l = events[ type ].length; i < l; i++ ) {
					jQuery.event.add( dest, type, events[ type ][ i ] );
				}
			}
		}
	}

	// 2. Copy user data
	if ( dataUser.hasData( src ) ) {
		udataOld = dataUser.access( src );
		udataCur = jQuery.extend( {}, udataOld );

		dataUser.set( dest, udataCur );
	}
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
	var nodeName = dest.nodeName.toLowerCase();

	// Fails to persist the checked state of a cloned checkbox or radio button.
	if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		dest.checked = src.checked;

	// Fails to return the selected option to the default selected state when cloning options
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

function domManip( collection, args, callback, ignored ) {

	// Flatten any nested arrays
	args = concat.apply( [], args );

	var fragment, first, scripts, hasScripts, node, doc,
		i = 0,
		l = collection.length,
		iNoClone = l - 1,
		value = args[ 0 ],
		isFunction = jQuery.isFunction( value );

	// We can't cloneNode fragments that contain checked, in WebKit
	if ( isFunction ||
			( l > 1 && typeof value === "string" &&
				!support.checkClone && rchecked.test( value ) ) ) {
		return collection.each( function( index ) {
			var self = collection.eq( index );
			if ( isFunction ) {
				args[ 0 ] = value.call( this, index, self.html() );
			}
			domManip( self, args, callback, ignored );
		} );
	}

	if ( l ) {
		fragment = buildFragment( args, collection[ 0 ].ownerDocument, false, collection, ignored );
		first = fragment.firstChild;

		if ( fragment.childNodes.length === 1 ) {
			fragment = first;
		}

		// Require either new content or an interest in ignored elements to invoke the callback
		if ( first || ignored ) {
			scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
			hasScripts = scripts.length;

			// Use the original fragment for the last item
			// instead of the first because it can end up
			// being emptied incorrectly in certain situations (#8070).
			for ( ; i < l; i++ ) {
				node = fragment;

				if ( i !== iNoClone ) {
					node = jQuery.clone( node, true, true );

					// Keep references to cloned scripts for later restoration
					if ( hasScripts ) {

						// Support: Android<4.1, PhantomJS<2
						// push.apply(_, arraylike) throws on ancient WebKit
						jQuery.merge( scripts, getAll( node, "script" ) );
					}
				}

				callback.call( collection[ i ], node, i );
			}

			if ( hasScripts ) {
				doc = scripts[ scripts.length - 1 ].ownerDocument;

				// Reenable scripts
				jQuery.map( scripts, restoreScript );

				// Evaluate executable scripts on first document insertion
				for ( i = 0; i < hasScripts; i++ ) {
					node = scripts[ i ];
					if ( rscriptType.test( node.type || "" ) &&
						!dataPriv.access( node, "globalEval" ) &&
						jQuery.contains( doc, node ) ) {

						if ( node.src ) {

							// Optional AJAX dependency, but won't run scripts if not present
							if ( jQuery._evalUrl ) {
								jQuery._evalUrl( node.src );
							}
						} else {
							jQuery.globalEval( node.textContent.replace( rcleanScript, "" ) );
						}
					}
				}
			}
		}
	}

	return collection;
}

function remove( elem, selector, keepData ) {
	var node,
		nodes = selector ? jQuery.filter( selector, elem ) : elem,
		i = 0;

	for ( ; ( node = nodes[ i ] ) != null; i++ ) {
		if ( !keepData && node.nodeType === 1 ) {
			jQuery.cleanData( getAll( node ) );
		}

		if ( node.parentNode ) {
			if ( keepData && jQuery.contains( node.ownerDocument, node ) ) {
				setGlobalEval( getAll( node, "script" ) );
			}
			node.parentNode.removeChild( node );
		}
	}

	return elem;
}

jQuery.extend( {
	htmlPrefilter: function( html ) {
		return html.replace( rxhtmlTag, "<$1></$2>" );
	},

	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var i, l, srcElements, destElements,
			clone = elem.cloneNode( true ),
			inPage = jQuery.contains( elem.ownerDocument, elem );

		// Fix IE cloning issues
		if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
				!jQuery.isXMLDoc( elem ) ) {

			// We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			for ( i = 0, l = srcElements.length; i < l; i++ ) {
				fixInput( srcElements[ i ], destElements[ i ] );
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					cloneCopyEvent( srcElements[ i ], destElements[ i ] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		// Return the cloned set
		return clone;
	},

	cleanData: function( elems ) {
		var data, elem, type,
			special = jQuery.event.special,
			i = 0;

		for ( ; ( elem = elems[ i ] ) !== undefined; i++ ) {
			if ( acceptData( elem ) ) {
				if ( ( data = elem[ dataPriv.expando ] ) ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}

					// Support: Chrome <= 35-45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataPriv.expando ] = undefined;
				}
				if ( elem[ dataUser.expando ] ) {

					// Support: Chrome <= 35-45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataUser.expando ] = undefined;
				}
			}
		}
	}
} );

jQuery.fn.extend( {

	// Keep domManip exposed until 3.0 (gh-2225)
	domManip: domManip,

	detach: function( selector ) {
		return remove( this, selector, true );
	},

	remove: function( selector ) {
		return remove( this, selector );
	},

	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().each( function() {
					if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
						this.textContent = value;
					}
				} );
		}, null, value, arguments.length );
	},

	append: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		} );
	},

	prepend: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		} );
	},

	before: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		} );
	},

	after: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		} );
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; ( elem = this[ i ] ) != null; i++ ) {
			if ( elem.nodeType === 1 ) {

				// Prevent memory leaks
				jQuery.cleanData( getAll( elem, false ) );

				// Remove any remaining nodes
				elem.textContent = "";
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map( function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		} );
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined && elem.nodeType === 1 ) {
				return elem.innerHTML;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

				value = jQuery.htmlPrefilter( value );

				try {
					for ( ; i < l; i++ ) {
						elem = this[ i ] || {};

						// Remove element nodes and prevent memory leaks
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch ( e ) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var ignored = [];

		// Make the changes, replacing each non-ignored context element with the new content
		return domManip( this, arguments, function( elem ) {
			var parent = this.parentNode;

			if ( jQuery.inArray( this, ignored ) < 0 ) {
				jQuery.cleanData( getAll( this ) );
				if ( parent ) {
					parent.replaceChild( elem, this );
				}
			}

		// Force callback invocation
		}, ignored );
	}
} );

jQuery.each( {
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1,
			i = 0;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone( true );
			jQuery( insert[ i ] )[ original ]( elems );

			// Support: QtWebKit
			// .get() because push.apply(_, arraylike) throws
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
} );


var iframe,
	elemdisplay = {

		// Support: Firefox
		// We have to pre-define these values for FF (#10227)
		HTML: "block",
		BODY: "block"
	};

/**
 * Retrieve the actual display of a element
 * @param {String} name nodeName of the element
 * @param {Object} doc Document object
 */

// Called only from within defaultDisplay
function actualDisplay( name, doc ) {
	var elem = jQuery( doc.createElement( name ) ).appendTo( doc.body ),

		display = jQuery.css( elem[ 0 ], "display" );

	// We don't have any data stored on the element,
	// so use "detach" method as fast way to get rid of the element
	elem.detach();

	return display;
}

/**
 * Try to determine the default display value of an element
 * @param {String} nodeName
 */
function defaultDisplay( nodeName ) {
	var doc = document,
		display = elemdisplay[ nodeName ];

	if ( !display ) {
		display = actualDisplay( nodeName, doc );

		// If the simple way fails, read from inside an iframe
		if ( display === "none" || !display ) {

			// Use the already-created iframe if possible
			iframe = ( iframe || jQuery( "<iframe frameborder='0' width='0' height='0'/>" ) )
				.appendTo( doc.documentElement );

			// Always write a new HTML skeleton so Webkit and Firefox don't choke on reuse
			doc = iframe[ 0 ].contentDocument;

			// Support: IE
			doc.write();
			doc.close();

			display = actualDisplay( nodeName, doc );
			iframe.detach();
		}

		// Store the correct default display
		elemdisplay[ nodeName ] = display;
	}

	return display;
}
var rmargin = ( /^margin/ );

var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var getStyles = function( elem ) {

		// Support: IE<=11+, Firefox<=30+ (#15098, #14150)
		// IE throws on elements created in popups
		// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
		var view = elem.ownerDocument.defaultView;

		if ( !view || !view.opener ) {
			view = window;
		}

		return view.getComputedStyle( elem );
	};

var swap = function( elem, options, callback, args ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};


var documentElement = document.documentElement;



( function() {
	var pixelPositionVal, boxSizingReliableVal, pixelMarginRightVal, reliableMarginLeftVal,
		container = document.createElement( "div" ),
		div = document.createElement( "div" );

	// Finish early in limited (non-browser) environments
	if ( !div.style ) {
		return;
	}

	// Support: IE9-11+
	// Style of cloned element affects source element cloned (#8908)
	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	container.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;" +
		"padding:0;margin-top:1px;position:absolute";
	container.appendChild( div );

	// Executing both pixelPosition & boxSizingReliable tests require only one layout
	// so they're executed at the same time to save the second computation.
	function computeStyleTests() {
		div.style.cssText =

			// Support: Firefox<29, Android 2.3
			// Vendor-prefix box-sizing
			"-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;" +
			"position:relative;display:block;" +
			"margin:auto;border:1px;padding:1px;" +
			"top:1%;width:50%";
		div.innerHTML = "";
		documentElement.appendChild( container );

		var divStyle = window.getComputedStyle( div );
		pixelPositionVal = divStyle.top !== "1%";
		reliableMarginLeftVal = divStyle.marginLeft === "2px";
		boxSizingReliableVal = divStyle.width === "4px";

		// Support: Android 4.0 - 4.3 only
		// Some styles come back with percentage values, even though they shouldn't
		div.style.marginRight = "50%";
		pixelMarginRightVal = divStyle.marginRight === "4px";

		documentElement.removeChild( container );
	}

	jQuery.extend( support, {
		pixelPosition: function() {

			// This test is executed only once but we still do memoizing
			// since we can use the boxSizingReliable pre-computing.
			// No need to check if the test was already performed, though.
			computeStyleTests();
			return pixelPositionVal;
		},
		boxSizingReliable: function() {
			if ( boxSizingReliableVal == null ) {
				computeStyleTests();
			}
			return boxSizingReliableVal;
		},
		pixelMarginRight: function() {

			// Support: Android 4.0-4.3
			// We're checking for boxSizingReliableVal here instead of pixelMarginRightVal
			// since that compresses better and they're computed together anyway.
			if ( boxSizingReliableVal == null ) {
				computeStyleTests();
			}
			return pixelMarginRightVal;
		},
		reliableMarginLeft: function() {

			// Support: IE <=8 only, Android 4.0 - 4.3 only, Firefox <=3 - 37
			if ( boxSizingReliableVal == null ) {
				computeStyleTests();
			}
			return reliableMarginLeftVal;
		},
		reliableMarginRight: function() {

			// Support: Android 2.3
			// Check if div with explicit width and no margin-right incorrectly
			// gets computed margin-right based on width of container. (#3333)
			// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
			// This support function is only executed once so no memoizing is needed.
			var ret,
				marginDiv = div.appendChild( document.createElement( "div" ) );

			// Reset CSS: box-sizing; display; margin; border; padding
			marginDiv.style.cssText = div.style.cssText =

				// Support: Android 2.3
				// Vendor-prefix box-sizing
				"-webkit-box-sizing:content-box;box-sizing:content-box;" +
				"display:block;margin:0;border:0;padding:0";
			marginDiv.style.marginRight = marginDiv.style.width = "0";
			div.style.width = "1px";
			documentElement.appendChild( container );

			ret = !parseFloat( window.getComputedStyle( marginDiv ).marginRight );

			documentElement.removeChild( container );
			div.removeChild( marginDiv );

			return ret;
		}
	} );
} )();


function curCSS( elem, name, computed ) {
	var width, minWidth, maxWidth, ret,
		style = elem.style;

	computed = computed || getStyles( elem );
	ret = computed ? computed.getPropertyValue( name ) || computed[ name ] : undefined;

	// Support: Opera 12.1x only
	// Fall back to style even without computed
	// computed is undefined for elems on document fragments
	if ( ( ret === "" || ret === undefined ) && !jQuery.contains( elem.ownerDocument, elem ) ) {
		ret = jQuery.style( elem, name );
	}

	// Support: IE9
	// getPropertyValue is only needed for .css('filter') (#12537)
	if ( computed ) {

		// A tribute to the "awesome hack by Dean Edwards"
		// Android Browser returns percentage for some values,
		// but width seems to be reliably pixels.
		// This is against the CSSOM draft spec:
		// http://dev.w3.org/csswg/cssom/#resolved-values
		if ( !support.pixelMarginRight() && rnumnonpx.test( ret ) && rmargin.test( name ) ) {

			// Remember the original values
			width = style.width;
			minWidth = style.minWidth;
			maxWidth = style.maxWidth;

			// Put in the new values to get a computed value out
			style.minWidth = style.maxWidth = style.width = ret;
			ret = computed.width;

			// Revert the changed values
			style.width = width;
			style.minWidth = minWidth;
			style.maxWidth = maxWidth;
		}
	}

	return ret !== undefined ?

		// Support: IE9-11+
		// IE returns zIndex value as an integer.
		ret + "" :
		ret;
}


function addGetHookIf( conditionFn, hookFn ) {

	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			if ( conditionFn() ) {

				// Hook not needed (or it's not possible to use it due
				// to missing dependency), remove it.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.
			return ( this.get = hookFn ).apply( this, arguments );
		}
	};
}


var

	// Swappable if display is none or starts with table
	// except "table", "table-cell", or "table-caption"
	// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,

	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: "0",
		fontWeight: "400"
	},

	cssPrefixes = [ "Webkit", "O", "Moz", "ms" ],
	emptyStyle = document.createElement( "div" ).style;

// Return a css property mapped to a potentially vendor prefixed property
function vendorPropName( name ) {

	// Shortcut for names that are not vendor prefixed
	if ( name in emptyStyle ) {
		return name;
	}

	// Check for vendor prefixed names
	var capName = name[ 0 ].toUpperCase() + name.slice( 1 ),
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in emptyStyle ) {
			return name;
		}
	}
}

function setPositiveNumber( elem, value, subtract ) {

	// Any relative (+/-) values have already been
	// normalized at this point
	var matches = rcssNum.exec( value );
	return matches ?

		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 2 ] - ( subtract || 0 ) ) + ( matches[ 3 ] || "px" ) :
		value;
}

function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
	var i = extra === ( isBorderBox ? "border" : "content" ) ?

		// If we already have the right measurement, avoid augmentation
		4 :

		// Otherwise initialize for horizontal or vertical properties
		name === "width" ? 1 : 0,

		val = 0;

	for ( ; i < 4; i += 2 ) {

		// Both box models exclude margin, so add it if we want it
		if ( extra === "margin" ) {
			val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
		}

		if ( isBorderBox ) {

			// border-box includes padding, so remove it if we want content
			if ( extra === "content" ) {
				val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// At this point, extra isn't border nor margin, so remove border
			if ( extra !== "margin" ) {
				val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		} else {

			// At this point, extra isn't content, so add padding
			val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// At this point, extra isn't content nor padding, so add border
			if ( extra !== "padding" ) {
				val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	return val;
}

function getWidthOrHeight( elem, name, extra ) {

	// Start with offset property, which is equivalent to the border-box value
	var valueIsBorderBox = true,
		val = name === "width" ? elem.offsetWidth : elem.offsetHeight,
		styles = getStyles( elem ),
		isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

	// Some non-html elements return undefined for offsetWidth, so check for null/undefined
	// svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
	// MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
	if ( val <= 0 || val == null ) {

		// Fall back to computed then uncomputed css if necessary
		val = curCSS( elem, name, styles );
		if ( val < 0 || val == null ) {
			val = elem.style[ name ];
		}

		// Computed unit is not pixels. Stop here and return.
		if ( rnumnonpx.test( val ) ) {
			return val;
		}

		// Check for style in case a browser which returns unreliable values
		// for getComputedStyle silently falls back to the reliable elem.style
		valueIsBorderBox = isBorderBox &&
			( support.boxSizingReliable() || val === elem.style[ name ] );

		// Normalize "", auto, and prepare for extra
		val = parseFloat( val ) || 0;
	}

	// Use the active box-sizing model to add/subtract irrelevant styles
	return ( val +
		augmentWidthOrHeight(
			elem,
			name,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles
		)
	) + "px";
}

function showHide( elements, show ) {
	var display, elem, hidden,
		values = [],
		index = 0,
		length = elements.length;

	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		values[ index ] = dataPriv.get( elem, "olddisplay" );
		display = elem.style.display;
		if ( show ) {

			// Reset the inline display of this element to learn if it is
			// being hidden by cascaded rules or not
			if ( !values[ index ] && display === "none" ) {
				elem.style.display = "";
			}

			// Set elements which have been overridden with display: none
			// in a stylesheet to whatever the default browser style is
			// for such an element
			if ( elem.style.display === "" && isHidden( elem ) ) {
				values[ index ] = dataPriv.access(
					elem,
					"olddisplay",
					defaultDisplay( elem.nodeName )
				);
			}
		} else {
			hidden = isHidden( elem );

			if ( display !== "none" || !hidden ) {
				dataPriv.set(
					elem,
					"olddisplay",
					hidden ? display : jQuery.css( elem, "display" )
				);
			}
		}
	}

	// Set the display of most of the elements in a second loop
	// to avoid the constant reflow
	for ( index = 0; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}
		if ( !show || elem.style.display === "none" || elem.style.display === "" ) {
			elem.style.display = show ? values[ index ] || "" : "none";
		}
	}

	return elements;
}

jQuery.extend( {

	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {

					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"animationIterationCount": true,
		"columnCount": true,
		"fillOpacity": true,
		"flexGrow": true,
		"flexShrink": true,
		"fontWeight": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {
		"float": "cssFloat"
	},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {

		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = jQuery.camelCase( name ),
			style = elem.style;

		name = jQuery.cssProps[ origName ] ||
			( jQuery.cssProps[ origName ] = vendorPropName( origName ) || origName );

		// Gets hook for the prefixed version, then unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// Convert "+=" or "-=" to relative numbers (#7345)
			if ( type === "string" && ( ret = rcssNum.exec( value ) ) && ret[ 1 ] ) {
				value = adjustCSS( elem, name, ret );

				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set (#7116)
			if ( value == null || value !== value ) {
				return;
			}

			// If a number was passed in, add the unit (except for certain CSS properties)
			if ( type === "number" ) {
				value += ret && ret[ 3 ] || ( jQuery.cssNumber[ origName ] ? "" : "px" );
			}

			// Support: IE9-11+
			// background-* props affect original clone's values
			if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !( "set" in hooks ) ||
				( value = hooks.set( elem, value, extra ) ) !== undefined ) {

				style[ name ] = value;
			}

		} else {

			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks &&
				( ret = hooks.get( elem, false, extra ) ) !== undefined ) {

				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var val, num, hooks,
			origName = jQuery.camelCase( name );

		// Make sure that we're working with the right name
		name = jQuery.cssProps[ origName ] ||
			( jQuery.cssProps[ origName ] = vendorPropName( origName ) || origName );

		// Try prefixed name followed by the unprefixed name
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		// Convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Make numeric if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || isFinite( num ) ? num || 0 : val;
		}
		return val;
	}
} );

jQuery.each( [ "height", "width" ], function( i, name ) {
	jQuery.cssHooks[ name ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {

				// Certain elements can have dimension info if we invisibly show them
				// but it must have a current display style that would benefit
				return rdisplayswap.test( jQuery.css( elem, "display" ) ) &&
					elem.offsetWidth === 0 ?
						swap( elem, cssShow, function() {
							return getWidthOrHeight( elem, name, extra );
						} ) :
						getWidthOrHeight( elem, name, extra );
			}
		},

		set: function( elem, value, extra ) {
			var matches,
				styles = extra && getStyles( elem ),
				subtract = extra && augmentWidthOrHeight(
					elem,
					name,
					extra,
					jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
					styles
				);

			// Convert to pixels if value adjustment is needed
			if ( subtract && ( matches = rcssNum.exec( value ) ) &&
				( matches[ 3 ] || "px" ) !== "px" ) {

				elem.style[ name ] = value;
				value = jQuery.css( elem, name );
			}

			return setPositiveNumber( elem, value, subtract );
		}
	};
} );

jQuery.cssHooks.marginLeft = addGetHookIf( support.reliableMarginLeft,
	function( elem, computed ) {
		if ( computed ) {
			return ( parseFloat( curCSS( elem, "marginLeft" ) ) ||
				elem.getBoundingClientRect().left -
					swap( elem, { marginLeft: 0 }, function() {
						return elem.getBoundingClientRect().left;
					} )
				) + "px";
		}
	}
);

// Support: Android 2.3
jQuery.cssHooks.marginRight = addGetHookIf( support.reliableMarginRight,
	function( elem, computed ) {
		if ( computed ) {
			return swap( elem, { "display": "inline-block" },
				curCSS, [ elem, "marginRight" ] );
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each( {
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// Assumes a single number if not a string
				parts = typeof value === "string" ? value.split( " " ) : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( !rmargin.test( prefix ) ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
} );

jQuery.fn.extend( {
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( jQuery.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	},
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each( function() {
			if ( isHidden( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		} );
	}
} );


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || jQuery.easing._default;
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			// Use a property on the element directly when it is not a DOM element,
			// or when there is no matching style property that exists.
			if ( tween.elem.nodeType !== 1 ||
				tween.elem[ tween.prop ] != null && tween.elem.style[ tween.prop ] == null ) {
				return tween.elem[ tween.prop ];
			}

			// Passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails.
			// Simple values such as "10px" are parsed to Float;
			// complex values such as "rotate(1rad)" are returned as-is.
			result = jQuery.css( tween.elem, tween.prop, "" );

			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {

			// Use step hook for back compat.
			// Use cssHook if its there.
			// Use .style if available and use plain properties where available.
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.nodeType === 1 &&
				( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null ||
					jQuery.cssHooks[ tween.prop ] ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE9
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	},
	_default: "swing"
};

jQuery.fx = Tween.prototype.init;

// Back Compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, timerId,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rrun = /queueHooks$/;

// Animations created synchronously will run synchronously
function createFxNow() {
	window.setTimeout( function() {
		fxNow = undefined;
	} );
	return ( fxNow = jQuery.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		i = 0,
		attrs = { height: type };

	// If we include width, step value is 1 to do all cssExpand values,
	// otherwise step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4 ; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( Animation.tweeners[ prop ] || [] ).concat( Animation.tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( ( tween = collection[ index ].call( animation, prop, value ) ) ) {

			// We're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	/* jshint validthis: true */
	var prop, value, toggle, tween, hooks, oldfire, display, checkDisplay,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHidden( elem ),
		dataShow = dataPriv.get( elem, "fxshow" );

	// Handle queue: false promises
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always( function() {

			// Ensure the complete handler is called before this completes
			anim.always( function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			} );
		} );
	}

	// Height/width overflow pass
	if ( elem.nodeType === 1 && ( "height" in props || "width" in props ) ) {

		// Make sure that nothing sneaks out
		// Record all 3 overflow attributes because IE9-10 do not
		// change the overflow attribute when overflowX and
		// overflowY are set to the same value
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Set display property to inline-block for height/width
		// animations on inline elements that are having width/height animated
		display = jQuery.css( elem, "display" );

		// Test default display if display is currently "none"
		checkDisplay = display === "none" ?
			dataPriv.get( elem, "olddisplay" ) || defaultDisplay( elem.nodeName ) : display;

		if ( checkDisplay === "inline" && jQuery.css( elem, "float" ) === "none" ) {
			style.display = "inline-block";
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		anim.always( function() {
			style.overflow = opts.overflow[ 0 ];
			style.overflowX = opts.overflow[ 1 ];
			style.overflowY = opts.overflow[ 2 ];
		} );
	}

	// show/hide pass
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.exec( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// If there is dataShow left over from a stopped hide or show
				// and we are going to proceed with show, we should pretend to be hidden
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );

		// Any non-fx value stops us from restoring the original display value
		} else {
			display = undefined;
		}
	}

	if ( !jQuery.isEmptyObject( orig ) ) {
		if ( dataShow ) {
			if ( "hidden" in dataShow ) {
				hidden = dataShow.hidden;
			}
		} else {
			dataShow = dataPriv.access( elem, "fxshow", {} );
		}

		// Store state if its toggle - enables .stop().toggle() to "reverse"
		if ( toggle ) {
			dataShow.hidden = !hidden;
		}
		if ( hidden ) {
			jQuery( elem ).show();
		} else {
			anim.done( function() {
				jQuery( elem ).hide();
			} );
		}
		anim.done( function() {
			var prop;

			dataPriv.remove( elem, "fxshow" );
			for ( prop in orig ) {
				jQuery.style( elem, prop, orig[ prop ] );
			}
		} );
		for ( prop in orig ) {
			tween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );

			if ( !( prop in dataShow ) ) {
				dataShow[ prop ] = tween.start;
				if ( hidden ) {
					tween.end = tween.start;
					tween.start = prop === "width" || prop === "height" ? 1 : 0;
				}
			}
		}

	// If this is a noop like .hide().hide(), restore an overwritten display value
	} else if ( ( display === "none" ? defaultDisplay( elem.nodeName ) : display ) === "inline" ) {
		style.display = display;
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = jQuery.camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( jQuery.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// Not quite $.extend, this won't overwrite existing keys.
			// Reusing 'index' because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = Animation.prefilters.length,
		deferred = jQuery.Deferred().always( function() {

			// Don't match elem in the :animated selector
			delete tick.elem;
		} ),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),

				// Support: Android 2.3
				// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length ; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ] );

			if ( percent < 1 && length ) {
				return remaining;
			} else {
				deferred.resolveWith( elem, [ animation ] );
				return false;
			}
		},
		animation = deferred.promise( {
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, {
				specialEasing: {},
				easing: jQuery.easing._default
			}, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,

					// If we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length ; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// Resolve when we played the last frame; otherwise, reject
				if ( gotoEnd ) {
					deferred.notifyWith( elem, [ animation, 1, 0 ] );
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		} ),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length ; index++ ) {
		result = Animation.prefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			if ( jQuery.isFunction( result.stop ) ) {
				jQuery._queueHooks( animation.elem, animation.opts.queue ).stop =
					jQuery.proxy( result.stop, result );
			}
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( jQuery.isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		} )
	);

	// attach callbacks from options
	return animation.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );
}

jQuery.Animation = jQuery.extend( Animation, {
	tweeners: {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value );
			adjustCSS( tween.elem, prop, rcssNum.exec( value ), tween );
			return tween;
		} ]
	},

	tweener: function( props, callback ) {
		if ( jQuery.isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.match( rnotwhite );
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length ; index++ ) {
			prop = props[ index ];
			Animation.tweeners[ prop ] = Animation.tweeners[ prop ] || [];
			Animation.tweeners[ prop ].unshift( callback );
		}
	},

	prefilters: [ defaultPrefilter ],

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			Animation.prefilters.unshift( callback );
		} else {
			Animation.prefilters.push( callback );
		}
	}
} );

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			jQuery.isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
	};

	opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ?
		opt.duration : opt.duration in jQuery.fx.speeds ?
			jQuery.fx.speeds[ opt.duration ] : jQuery.fx.speeds._default;

	// Normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( jQuery.isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend( {
	fadeTo: function( speed, to, easing, callback ) {

		// Show any hidden elements after setting opacity to 0
		return this.filter( isHidden ).css( "opacity", 0 ).show()

			// Animate to the value specified
			.end().animate( { opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {

				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || dataPriv.get( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue && type !== false ) {
			this.queue( type || "fx", [] );
		}

		return this.each( function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = dataPriv.get( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this &&
					( type == null || timers[ index ].queue === type ) ) {

					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// Start the next in the queue if the last step wasn't forced.
			// Timers currently will call their complete callbacks, which
			// will dequeue but only if they were gotoEnd.
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		} );
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each( function() {
			var index,
				data = dataPriv.get( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// Enable finishing flag on private data
			data.finish = true;

			// Empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// Look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// Look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// Turn off finishing flag
			delete data.finish;
		} );
	}
} );

jQuery.each( [ "toggle", "show", "hide" ], function( i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
} );

// Generate shortcuts for custom animations
jQuery.each( {
	slideDown: genFx( "show" ),
	slideUp: genFx( "hide" ),
	slideToggle: genFx( "toggle" ),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
} );

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		i = 0,
		timers = jQuery.timers;

	fxNow = jQuery.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];

		// Checks the timer has not already been removed
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	if ( timer() ) {
		jQuery.fx.start();
	} else {
		jQuery.timers.pop();
	}
};

jQuery.fx.interval = 13;
jQuery.fx.start = function() {
	if ( !timerId ) {
		timerId = window.setInterval( jQuery.fx.tick, jQuery.fx.interval );
	}
};

jQuery.fx.stop = function() {
	window.clearInterval( timerId );

	timerId = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,

	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// http://web.archive.org/web/20100324014747/http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = window.setTimeout( next, time );
		hooks.stop = function() {
			window.clearTimeout( timeout );
		};
	} );
};


( function() {
	var input = document.createElement( "input" ),
		select = document.createElement( "select" ),
		opt = select.appendChild( document.createElement( "option" ) );

	input.type = "checkbox";

	// Support: iOS<=5.1, Android<=4.2+
	// Default value for a checkbox should be "on"
	support.checkOn = input.value !== "";

	// Support: IE<=11+
	// Must access selectedIndex to make default options select
	support.optSelected = opt.selected;

	// Support: Android<=2.3
	// Options inside disabled selects are incorrectly marked as disabled
	select.disabled = true;
	support.optDisabled = !opt.disabled;

	// Support: IE<=11+
	// An input loses its value after becoming a radio
	input = document.createElement( "input" );
	input.value = "t";
	input.type = "radio";
	support.radioValue = input.value === "t";
} )();


var boolHook,
	attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend( {
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each( function() {
			jQuery.removeAttr( this, name );
		} );
	}
} );

jQuery.extend( {
	attr: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set attributes on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === "undefined" ) {
			return jQuery.prop( elem, name, value );
		}

		// All attributes are lowercase
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			name = name.toLowerCase();
			hooks = jQuery.attrHooks[ name ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : undefined );
		}

		if ( value !== undefined ) {
			if ( value === null ) {
				jQuery.removeAttr( elem, name );
				return;
			}

			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			elem.setAttribute( name, value + "" );
			return value;
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		ret = jQuery.find.attr( elem, name );

		// Non-existent attributes return null, we normalize to undefined
		return ret == null ? undefined : ret;
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" &&
					jQuery.nodeName( elem, "input" ) ) {
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	},

	removeAttr: function( elem, value ) {
		var name, propName,
			i = 0,
			attrNames = value && value.match( rnotwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( ( name = attrNames[ i++ ] ) ) {
				propName = jQuery.propFix[ name ] || name;

				// Boolean attributes get special treatment (#10870)
				if ( jQuery.expr.match.bool.test( name ) ) {

					// Set corresponding property to false
					elem[ propName ] = false;
				}

				elem.removeAttribute( name );
			}
		}
	}
} );

// Hooks for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {

			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else {
			elem.setAttribute( name, name );
		}
		return name;
	}
};
jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = function( elem, name, isXML ) {
		var ret, handle;
		if ( !isXML ) {

			// Avoid an infinite loop by temporarily removing this function from the getter
			handle = attrHandle[ name ];
			attrHandle[ name ] = ret;
			ret = getter( elem, name, isXML ) != null ?
				name.toLowerCase() :
				null;
			attrHandle[ name ] = handle;
		}
		return ret;
	};
} );




var rfocusable = /^(?:input|select|textarea|button)$/i,
	rclickable = /^(?:a|area)$/i;

jQuery.fn.extend( {
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		return this.each( function() {
			delete this[ jQuery.propFix[ name ] || name ];
		} );
	}
} );

jQuery.extend( {
	prop: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set properties on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {

			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			return ( elem[ name ] = value );
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		return elem[ name ];
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {

				// elem.tabIndex doesn't always return the
				// correct value when it hasn't been explicitly set
				// http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
				// Use proper attribute retrieval(#12072)
				var tabindex = jQuery.find.attr( elem, "tabindex" );

				return tabindex ?
					parseInt( tabindex, 10 ) :
					rfocusable.test( elem.nodeName ) ||
						rclickable.test( elem.nodeName ) && elem.href ?
							0 :
							-1;
			}
		}
	},

	propFix: {
		"for": "htmlFor",
		"class": "className"
	}
} );

// Support: IE <=11 only
// Accessing the selectedIndex property
// forces the browser to respect setting selected
// on the option
// The getter ensures a default option is selected
// when in an optgroup
if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {
			var parent = elem.parentNode;
			if ( parent && parent.parentNode ) {
				parent.parentNode.selectedIndex;
			}
			return null;
		},
		set: function( elem ) {
			var parent = elem.parentNode;
			if ( parent ) {
				parent.selectedIndex;

				if ( parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
			}
		}
	};
}

jQuery.each( [
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
} );




var rclass = /[\t\r\n\f]/g;

function getClass( elem ) {
	return elem.getAttribute && elem.getAttribute( "class" ) || "";
}

jQuery.fn.extend( {
	addClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( jQuery.isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).addClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		if ( typeof value === "string" && value ) {
			classes = value.match( rnotwhite ) || [];

			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );
				cur = elem.nodeType === 1 &&
					( " " + curValue + " " ).replace( rclass, " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = jQuery.trim( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( jQuery.isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).removeClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		if ( !arguments.length ) {
			return this.attr( "class", "" );
		}

		if ( typeof value === "string" && value ) {
			classes = value.match( rnotwhite ) || [];

			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );

				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 &&
					( " " + curValue + " " ).replace( rclass, " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {

						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) > -1 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = jQuery.trim( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value;

		if ( typeof stateVal === "boolean" && type === "string" ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( jQuery.isFunction( value ) ) {
			return this.each( function( i ) {
				jQuery( this ).toggleClass(
					value.call( this, i, getClass( this ), stateVal ),
					stateVal
				);
			} );
		}

		return this.each( function() {
			var className, i, self, classNames;

			if ( type === "string" ) {

				// Toggle individual class names
				i = 0;
				self = jQuery( this );
				classNames = value.match( rnotwhite ) || [];

				while ( ( className = classNames[ i++ ] ) ) {

					// Check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( value === undefined || type === "boolean" ) {
				className = getClass( this );
				if ( className ) {

					// Store className if set
					dataPriv.set( this, "__className__", className );
				}

				// If the element has a class name or if we're passed `false`,
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				if ( this.setAttribute ) {
					this.setAttribute( "class",
						className || value === false ?
						"" :
						dataPriv.get( this, "__className__" ) || ""
					);
				}
			}
		} );
	},

	hasClass: function( selector ) {
		var className, elem,
			i = 0;

		className = " " + selector + " ";
		while ( ( elem = this[ i++ ] ) ) {
			if ( elem.nodeType === 1 &&
				( " " + getClass( elem ) + " " ).replace( rclass, " " )
					.indexOf( className ) > -1
			) {
				return true;
			}
		}

		return false;
	}
} );




var rreturn = /\r/g,
	rspaces = /[\x20\t\r\n\f]+/g;

jQuery.fn.extend( {
	val: function( value ) {
		var hooks, ret, isFunction,
			elem = this[ 0 ];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] ||
					jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks &&
					"get" in hooks &&
					( ret = hooks.get( elem, "value" ) ) !== undefined
				) {
					return ret;
				}

				ret = elem.value;

				return typeof ret === "string" ?

					// Handle most common string cases
					ret.replace( rreturn, "" ) :

					// Handle cases where value is null/undef or number
					ret == null ? "" : ret;
			}

			return;
		}

		isFunction = jQuery.isFunction( value );

		return this.each( function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( isFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";

			} else if ( typeof val === "number" ) {
				val += "";

			} else if ( jQuery.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				} );
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !( "set" in hooks ) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		} );
	}
} );

jQuery.extend( {
	valHooks: {
		option: {
			get: function( elem ) {

				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :

					// Support: IE10-11+
					// option.text throws exceptions (#14686, #14858)
					// Strip and collapse whitespace
					// https://html.spec.whatwg.org/#strip-and-collapse-whitespace
					jQuery.trim( jQuery.text( elem ) ).replace( rspaces, " " );
			}
		},
		select: {
			get: function( elem ) {
				var value, option,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one" || index < 0,
					values = one ? null : [],
					max = one ? index + 1 : options.length,
					i = index < 0 ?
						max :
						one ? index : 0;

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// IE8-9 doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&

							// Don't return options that are disabled or in a disabled optgroup
							( support.optDisabled ?
								!option.disabled : option.getAttribute( "disabled" ) === null ) &&
							( !option.parentNode.disabled ||
								!jQuery.nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];
					if ( option.selected =
						jQuery.inArray( jQuery.valHooks.option.get( option ), values ) > -1
					) {
						optionSet = true;
					}
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}
				return values;
			}
		}
	}
} );

// Radios and checkboxes getter/setter
jQuery.each( [ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( jQuery.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery( elem ).val(), value ) > -1 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			return elem.getAttribute( "value" ) === null ? "on" : elem.value;
		};
	}
} );




// Return jQuery for attributes-only inclusion


var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/;

jQuery.extend( jQuery.event, {

	trigger: function( event, data, elem, onlyHandlers ) {

		var i, cur, tmp, bubbleType, ontype, handle, special,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split( "." ) : [];

		cur = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf( "." ) > -1 ) {

			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split( "." );
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf( ":" ) < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join( "." );
		event.rnamespace = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === ( elem.ownerDocument || document ) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( ( cur = eventPath[ i++ ] ) && !event.isPropagationStopped() ) {

			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = ( dataPriv.get( cur, "events" ) || {} )[ event.type ] &&
				dataPriv.get( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( ( !special._default ||
				special._default.apply( eventPath.pop(), data ) === false ) &&
				acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name name as the event.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && jQuery.isFunction( elem[ type ] ) && !jQuery.isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;
					elem[ type ]();
					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	// Piggyback on a donor event to simulate a different one
	// Used only for `focus(in | out)` events
	simulate: function( type, elem, event ) {
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true
			}
		);

		jQuery.event.trigger( e, null, elem );
	}

} );

jQuery.fn.extend( {

	trigger: function( type, data ) {
		return this.each( function() {
			jQuery.event.trigger( type, data, this );
		} );
	},
	triggerHandler: function( type, data ) {
		var elem = this[ 0 ];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
} );


jQuery.each( ( "blur focus focusin focusout load resize scroll unload click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup error contextmenu" ).split( " " ),
	function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		return arguments.length > 0 ?
			this.on( name, null, data, fn ) :
			this.trigger( name );
	};
} );

jQuery.fn.extend( {
	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	}
} );




support.focusin = "onfocusin" in window;


// Support: Firefox
// Firefox doesn't have focus(in | out) events
// Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
//
// Support: Chrome, Safari
// focus(in | out) events fire after focus & blur events,
// which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
// Related ticket - https://code.google.com/p/chromium/issues/detail?id=449857
if ( !support.focusin ) {
	jQuery.each( { focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
			jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ) );
		};

		jQuery.event.special[ fix ] = {
			setup: function() {
				var doc = this.ownerDocument || this,
					attaches = dataPriv.access( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				dataPriv.access( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this,
					attaches = dataPriv.access( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					dataPriv.remove( doc, fix );

				} else {
					dataPriv.access( doc, fix, attaches );
				}
			}
		};
	} );
}
var location = window.location;

var nonce = jQuery.now();

var rquery = ( /\?/ );



// Support: Android 2.3
// Workaround failure to string-cast null input
jQuery.parseJSON = function( data ) {
	return JSON.parse( data + "" );
};


// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml;
	if ( !data || typeof data !== "string" ) {
		return null;
	}

	// Support: IE9
	try {
		xml = ( new window.DOMParser() ).parseFromString( data, "text/xml" );
	} catch ( e ) {
		xml = undefined;
	}

	if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	rhash = /#.*$/,
	rts = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,

	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat( "*" ),

	// Anchor tag for parsing the document origin
	originAnchor = document.createElement( "a" );
	originAnchor.href = location.href;

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnotwhite ) || [];

		if ( jQuery.isFunction( func ) ) {

			// For each dataType in the dataTypeExpression
			while ( ( dataType = dataTypes[ i++ ] ) ) {

				// Prepend if requested
				if ( dataType[ 0 ] === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					( structure[ dataType ] = structure[ dataType ] || [] ).unshift( func );

				// Otherwise append
				} else {
					( structure[ dataType ] = structure[ dataType ] || [] ).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" &&
				!seekingTransport && !inspected[ dataTypeOrTransport ] ) {

				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		} );
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var key, deep,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || ( deep = {} ) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {

	var ct, type, finalDataType, firstDataType,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader( "Content-Type" );
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {

		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[ 0 ] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}

		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},

		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

		// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {

								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s.throws ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return {
								state: "parsererror",
								error: conv ? e : "No conversion from " + prev + " to " + current
							};
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend( {

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: location.href,
		type: "GET",
		isLocal: rlocalProtocol.test( location.protocol ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /\bxml\b/,
			html: /\bhtml/,
			json: /\bjson\b/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": jQuery.parseJSON,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var transport,

			// URL without anti-cache param
			cacheURL,

			// Response headers
			responseHeadersString,
			responseHeaders,

			// timeout handle
			timeoutTimer,

			// Url cleanup var
			urlAnchor,

			// To know if global events are to be dispatched
			fireGlobals,

			// Loop variable
			i,

			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),

			// Callbacks context
			callbackContext = s.context || s,

			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context &&
				( callbackContext.nodeType || callbackContext.jquery ) ?
					jQuery( callbackContext ) :
					jQuery.event,

			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks( "once memory" ),

			// Status-dependent callbacks
			statusCode = s.statusCode || {},

			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},

			// The jqXHR state
			state = 0,

			// Default abort message
			strAbort = "canceled",

			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( state === 2 ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( ( match = rheaders.exec( responseHeadersString ) ) ) {
								responseHeaders[ match[ 1 ].toLowerCase() ] = match[ 2 ];
							}
						}
						match = responseHeaders[ key.toLowerCase() ];
					}
					return match == null ? null : match;
				},

				// Raw string
				getAllResponseHeaders: function() {
					return state === 2 ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					var lname = name.toLowerCase();
					if ( !state ) {
						name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( !state ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( state < 2 ) {
							for ( code in map ) {

								// Lazy-add the new callback in a way that preserves old ones
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						} else {

							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR ).complete = completeDeferred.add;
		jqXHR.success = jqXHR.done;
		jqXHR.error = jqXHR.fail;

		// Remove hash character (#7531: and string promotion)
		// Add protocol if not provided (prefilters might expect it)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || location.href ) + "" ).replace( rhash, "" )
			.replace( rprotocol, location.protocol + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = jQuery.trim( s.dataType || "*" ).toLowerCase().match( rnotwhite ) || [ "" ];

		// A cross-domain request is in order when the origin doesn't match the current origin.
		if ( s.crossDomain == null ) {
			urlAnchor = document.createElement( "a" );

			// Support: IE8-11+
			// IE throws exception if url is malformed, e.g. http://example.com:80x/
			try {
				urlAnchor.href = s.url;

				// Support: IE8-11+
				// Anchor's host property isn't correctly set when s.url is relative
				urlAnchor.href = urlAnchor.href;
				s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !==
					urlAnchor.protocol + "//" + urlAnchor.host;
			} catch ( e ) {

				// If there is an error parsing the URL, assume it is crossDomain,
				// it can be rejected by the transport if it is invalid
				s.crossDomain = true;
			}
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( state === 2 ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
		fireGlobals = jQuery.event && s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger( "ajaxStart" );
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		cacheURL = s.url;

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// If data is available, append data to url
			if ( s.data ) {
				cacheURL = ( s.url += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data );

				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add anti-cache in url if needed
			if ( s.cache === false ) {
				s.url = rts.test( cacheURL ) ?

					// If there is already a '_' parameter, set its value
					cacheURL.replace( rts, "$1_=" + nonce++ ) :

					// Otherwise add one to the end
					cacheURL + ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + nonce++;
			}
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[ 0 ] ] ?
				s.accepts[ s.dataTypes[ 0 ] ] +
					( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend &&
			( s.beforeSend.call( callbackContext, jqXHR, s ) === false || state === 2 ) ) {

			// Abort if not done already and return
			return jqXHR.abort();
		}

		// Aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		for ( i in { success: 1, error: 1, complete: 1 } ) {
			jqXHR[ i ]( s[ i ] );
		}

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}

			// If request was aborted inside ajaxSend, stop there
			if ( state === 2 ) {
				return jqXHR;
			}

			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = window.setTimeout( function() {
					jqXHR.abort( "timeout" );
				}, s.timeout );
			}

			try {
				state = 1;
				transport.send( requestHeaders, done );
			} catch ( e ) {

				// Propagate exception as error if not done
				if ( state < 2 ) {
					done( -1, e );

				// Simply rethrow otherwise
				} else {
					throw e;
				}
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Called once
			if ( state === 2 ) {
				return;
			}

			// State is "done" now
			state = 2;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				window.clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader( "Last-Modified" );
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader( "etag" );
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {

				// Extract error from statusText and normalize for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );

				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger( "ajaxStop" );
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
} );

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {

		// Shift arguments if data argument was omitted
		if ( jQuery.isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		// The url can be an options object (which then must have .url)
		return jQuery.ajax( jQuery.extend( {
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		}, jQuery.isPlainObject( url ) && url ) );
	};
} );


jQuery._evalUrl = function( url ) {
	return jQuery.ajax( {
		url: url,

		// Make this explicit, since user can override this through ajaxSetup (#11264)
		type: "GET",
		dataType: "script",
		async: false,
		global: false,
		"throws": true
	} );
};


jQuery.fn.extend( {
	wrapAll: function( html ) {
		var wrap;

		if ( jQuery.isFunction( html ) ) {
			return this.each( function( i ) {
				jQuery( this ).wrapAll( html.call( this, i ) );
			} );
		}

		if ( this[ 0 ] ) {

			// The elements to wrap the target around
			wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

			if ( this[ 0 ].parentNode ) {
				wrap.insertBefore( this[ 0 ] );
			}

			wrap.map( function() {
				var elem = this;

				while ( elem.firstElementChild ) {
					elem = elem.firstElementChild;
				}

				return elem;
			} ).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each( function( i ) {
				jQuery( this ).wrapInner( html.call( this, i ) );
			} );
		}

		return this.each( function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		} );
	},

	wrap: function( html ) {
		var isFunction = jQuery.isFunction( html );

		return this.each( function( i ) {
			jQuery( this ).wrapAll( isFunction ? html.call( this, i ) : html );
		} );
	},

	unwrap: function() {
		return this.parent().each( function() {
			if ( !jQuery.nodeName( this, "body" ) ) {
				jQuery( this ).replaceWith( this.childNodes );
			}
		} ).end();
	}
} );


jQuery.expr.filters.hidden = function( elem ) {
	return !jQuery.expr.filters.visible( elem );
};
jQuery.expr.filters.visible = function( elem ) {

	// Support: Opera <= 12.12
	// Opera reports offsetWidths and offsetHeights less than zero on some elements
	// Use OR instead of AND as the element is not visible if either is true
	// See tickets #10406 and #13132
	return elem.offsetWidth > 0 || elem.offsetHeight > 0 || elem.getClientRects().length > 0;
};




var r20 = /%20/g,
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( jQuery.isArray( obj ) ) {

		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {

				// Treat each array item as a scalar.
				add( prefix, v );

			} else {

				// Item is non-scalar (array or object), encode its numeric index.
				buildParams(
					prefix + "[" + ( typeof v === "object" && v != null ? i : "" ) + "]",
					v,
					traditional,
					add
				);
			}
		} );

	} else if ( !traditional && jQuery.type( obj ) === "object" ) {

		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {

		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, value ) {

			// If value is a function, invoke it and return its value
			value = jQuery.isFunction( value ) ? value() : ( value == null ? "" : value );
			s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
		};

	// Set traditional to true for jQuery <= 1.3.2 behavior.
	if ( traditional === undefined ) {
		traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
	}

	// If an array was passed in, assume that it is an array of form elements.
	if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {

		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		} );

	} else {

		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" ).replace( r20, "+" );
};

jQuery.fn.extend( {
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map( function() {

			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		} )
		.filter( function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		} )
		.map( function( i, elem ) {
			var val = jQuery( this ).val();

			return val == null ?
				null :
				jQuery.isArray( val ) ?
					jQuery.map( val, function( val ) {
						return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
					} ) :
					{ name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		} ).get();
	}
} );


jQuery.ajaxSettings.xhr = function() {
	try {
		return new window.XMLHttpRequest();
	} catch ( e ) {}
};

var xhrSuccessStatus = {

		// File protocol always yields status code 0, assume 200
		0: 200,

		// Support: IE9
		// #1450: sometimes IE returns 1223 when it should be 204
		1223: 204
	},
	xhrSupported = jQuery.ajaxSettings.xhr();

support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
support.ajax = xhrSupported = !!xhrSupported;

jQuery.ajaxTransport( function( options ) {
	var callback, errorCallback;

	// Cross domain only allowed if supported through XMLHttpRequest
	if ( support.cors || xhrSupported && !options.crossDomain ) {
		return {
			send: function( headers, complete ) {
				var i,
					xhr = options.xhr();

				xhr.open(
					options.type,
					options.url,
					options.async,
					options.username,
					options.password
				);

				// Apply custom fields if provided
				if ( options.xhrFields ) {
					for ( i in options.xhrFields ) {
						xhr[ i ] = options.xhrFields[ i ];
					}
				}

				// Override mime type if needed
				if ( options.mimeType && xhr.overrideMimeType ) {
					xhr.overrideMimeType( options.mimeType );
				}

				// X-Requested-With header
				// For cross-domain requests, seeing as conditions for a preflight are
				// akin to a jigsaw puzzle, we simply never set it to be sure.
				// (it can always be set on a per-request basis or even using ajaxSetup)
				// For same-domain requests, won't change header if already provided.
				if ( !options.crossDomain && !headers[ "X-Requested-With" ] ) {
					headers[ "X-Requested-With" ] = "XMLHttpRequest";
				}

				// Set headers
				for ( i in headers ) {
					xhr.setRequestHeader( i, headers[ i ] );
				}

				// Callback
				callback = function( type ) {
					return function() {
						if ( callback ) {
							callback = errorCallback = xhr.onload =
								xhr.onerror = xhr.onabort = xhr.onreadystatechange = null;

							if ( type === "abort" ) {
								xhr.abort();
							} else if ( type === "error" ) {

								// Support: IE9
								// On a manual native abort, IE9 throws
								// errors on any property access that is not readyState
								if ( typeof xhr.status !== "number" ) {
									complete( 0, "error" );
								} else {
									complete(

										// File: protocol always yields status 0; see #8605, #14207
										xhr.status,
										xhr.statusText
									);
								}
							} else {
								complete(
									xhrSuccessStatus[ xhr.status ] || xhr.status,
									xhr.statusText,

									// Support: IE9 only
									// IE9 has no XHR2 but throws on binary (trac-11426)
									// For XHR2 non-text, let the caller handle it (gh-2498)
									( xhr.responseType || "text" ) !== "text"  ||
									typeof xhr.responseText !== "string" ?
										{ binary: xhr.response } :
										{ text: xhr.responseText },
									xhr.getAllResponseHeaders()
								);
							}
						}
					};
				};

				// Listen to events
				xhr.onload = callback();
				errorCallback = xhr.onerror = callback( "error" );

				// Support: IE9
				// Use onreadystatechange to replace onabort
				// to handle uncaught aborts
				if ( xhr.onabort !== undefined ) {
					xhr.onabort = errorCallback;
				} else {
					xhr.onreadystatechange = function() {

						// Check readyState before timeout as it changes
						if ( xhr.readyState === 4 ) {

							// Allow onerror to be called first,
							// but that will not handle a native abort
							// Also, save errorCallback to a variable
							// as xhr.onerror cannot be accessed
							window.setTimeout( function() {
								if ( callback ) {
									errorCallback();
								}
							} );
						}
					};
				}

				// Create the abort callback
				callback = callback( "abort" );

				try {

					// Do send the request (this may raise an exception)
					xhr.send( options.hasContent && options.data || null );
				} catch ( e ) {

					// #14683: Only rethrow if this hasn't been notified as an error yet
					if ( callback ) {
						throw e;
					}
				}
			},

			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




// Install script dataType
jQuery.ajaxSetup( {
	accepts: {
		script: "text/javascript, application/javascript, " +
			"application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /\b(?:java|ecma)script\b/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
} );

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
	}
} );

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {

	// This transport only deals with cross domain requests
	if ( s.crossDomain ) {
		var script, callback;
		return {
			send: function( _, complete ) {
				script = jQuery( "<script>" ).prop( {
					charset: s.scriptCharset,
					src: s.url
				} ).on(
					"load error",
					callback = function( evt ) {
						script.remove();
						callback = null;
						if ( evt ) {
							complete( evt.type === "error" ? 404 : 200, evt.type );
						}
					}
				);

				// Use native DOM manipulation to avoid our domManip AJAX trickery
				document.head.appendChild( script[ 0 ] );
			},
			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup( {
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
		this[ callback ] = true;
		return callback;
	}
} );

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" &&
				( s.contentType || "" )
					.indexOf( "application/x-www-form-urlencoded" ) === 0 &&
				rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters[ "script json" ] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// Force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always( function() {

			// If previous value didn't exist - remove it
			if ( overwritten === undefined ) {
				jQuery( window ).removeProp( callbackName );

			// Otherwise restore preexisting value
			} else {
				window[ callbackName ] = overwritten;
			}

			// Save back as free
			if ( s[ callbackName ] ) {

				// Make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// Save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && jQuery.isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		} );

		// Delegate to script
		return "script";
	}
} );




// Argument "data" should be string of html
// context (optional): If specified, the fragment will be created in this context,
// defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( !data || typeof data !== "string" ) {
		return null;
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}
	context = context || document;

	var parsed = rsingleTag.exec( data ),
		scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[ 1 ] ) ];
	}

	parsed = buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


// Keep a copy of the old load method
var _load = jQuery.fn.load;

/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	if ( typeof url !== "string" && _load ) {
		return _load.apply( this, arguments );
	}

	var selector, type, response,
		self = this,
		off = url.indexOf( " " );

	if ( off > -1 ) {
		selector = jQuery.trim( url.slice( off ) );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( jQuery.isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax( {
			url: url,

			// If "type" variable is undefined, then "GET" method will be used.
			// Make value of this field explicit since
			// user can override it through ajaxSetup method
			type: type || "GET",
			dataType: "html",
			data: params
		} ).done( function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery( "<div>" ).append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		// If the request succeeds, this function gets "data", "status", "jqXHR"
		// but they are ignored because response was set above.
		// If it fails, this function gets "jqXHR", "status", "error"
		} ).always( callback && function( jqXHR, status ) {
			self.each( function() {
				callback.apply( this, response || [ jqXHR.responseText, status, jqXHR ] );
			} );
		} );
	}

	return this;
};




// Attach a bunch of functions for handling common AJAX events
jQuery.each( [
	"ajaxStart",
	"ajaxStop",
	"ajaxComplete",
	"ajaxError",
	"ajaxSuccess",
	"ajaxSend"
], function( i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
} );




jQuery.expr.filters.animated = function( elem ) {
	return jQuery.grep( jQuery.timers, function( fn ) {
		return elem === fn.elem;
	} ).length;
};




/**
 * Gets a window from an element
 */
function getWindow( elem ) {
	return jQuery.isWindow( elem ) ? elem : elem.nodeType === 9 && elem.defaultView;
}

jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// Set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			( curCSSTop + curCSSLeft ).indexOf( "auto" ) > -1;

		// Need to be able to calculate position if either
		// top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;

		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( jQuery.isFunction( options ) ) {

			// Use jQuery.extend here to allow modification of coordinates argument (gh-1848)
			options = options.call( elem, i, jQuery.extend( {}, curOffset ) );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );

		} else {
			curElem.css( props );
		}
	}
};

jQuery.fn.extend( {
	offset: function( options ) {
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each( function( i ) {
					jQuery.offset.setOffset( this, options, i );
				} );
		}

		var docElem, win,
			elem = this[ 0 ],
			box = { top: 0, left: 0 },
			doc = elem && elem.ownerDocument;

		if ( !doc ) {
			return;
		}

		docElem = doc.documentElement;

		// Make sure it's not a disconnected DOM node
		if ( !jQuery.contains( docElem, elem ) ) {
			return box;
		}

		box = elem.getBoundingClientRect();
		win = getWindow( doc );
		return {
			top: box.top + win.pageYOffset - docElem.clientTop,
			left: box.left + win.pageXOffset - docElem.clientLeft
		};
	},

	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset,
			elem = this[ 0 ],
			parentOffset = { top: 0, left: 0 };

		// Fixed elements are offset from window (parentOffset = {top:0, left: 0},
		// because it is its only offset parent
		if ( jQuery.css( elem, "position" ) === "fixed" ) {

			// Assume getBoundingClientRect is there when computed position is fixed
			offset = elem.getBoundingClientRect();

		} else {

			// Get *real* offsetParent
			offsetParent = this.offsetParent();

			// Get correct offsets
			offset = this.offset();
			if ( !jQuery.nodeName( offsetParent[ 0 ], "html" ) ) {
				parentOffset = offsetParent.offset();
			}

			// Add offsetParent borders
			parentOffset.top += jQuery.css( offsetParent[ 0 ], "borderTopWidth", true );
			parentOffset.left += jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true );
		}

		// Subtract parent offsets and element margins
		return {
			top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
		};
	},

	// This method will return documentElement in the following cases:
	// 1) For the element inside the iframe without offsetParent, this method will return
	//    documentElement of the parent window
	// 2) For the hidden or detached element
	// 3) For body or html element, i.e. in case of the html node - it will return itself
	//
	// but those exceptions were never presented as a real life use-cases
	// and might be considered as more preferable results.
	//
	// This logic, however, is not guaranteed and can change at any point in the future
	offsetParent: function() {
		return this.map( function() {
			var offsetParent = this.offsetParent;

			while ( offsetParent && jQuery.css( offsetParent, "position" ) === "static" ) {
				offsetParent = offsetParent.offsetParent;
			}

			return offsetParent || documentElement;
		} );
	}
} );

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = "pageYOffset" === prop;

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {
			var win = getWindow( elem );

			if ( val === undefined ) {
				return win ? win[ prop ] : elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : win.pageXOffset,
					top ? val : win.pageYOffset
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length );
	};
} );

// Support: Safari<7-8+, Chrome<37-44+
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://code.google.com/p/chromium/issues/detail?id=229280
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );

				// If curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
} );


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name },
		function( defaultExtra, funcName ) {

		// Margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( jQuery.isWindow( elem ) ) {

					// As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there
					// isn't a whole lot we can do. See pull request at this URL for discussion:
					// https://github.com/jquery/jquery/pull/764
					return elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
					// whichever is greatest
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?

					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable, null );
		};
	} );
} );


jQuery.fn.extend( {

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {

		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ?
			this.off( selector, "**" ) :
			this.off( types, selector || "**", fn );
	},
	size: function() {
		return this.length;
	}
} );

jQuery.fn.andSelf = jQuery.fn.addBack;




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( typeof define === "function" && define.amd ) {
	define( "jquery", [], function() {
		return jQuery;
	} );
}



var

	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in AMD
// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( !noGlobal ) {
	window.jQuery = window.$ = jQuery;
}

return jQuery;
}));

if(function($){var t=new Array,e=new Array,i=function(){},n=0,o={splashVPos:"35%",loaderVPos:"75%",splashID:"#jpreContent",showSplash:!0,showPercentage:!0,autoClose:!0,closeBtnText:"Start!",onetimeLoad:!1,debugMode:!1,splashFunction:function(){}},r=function(){if(o.onetimeLoad){for(var t=document.cookie.split("; "),e=0,i;i=t[e]&&t[e].split("=");e++)if("jpreLoader"===i.shift())return i.join("=");return!1}return!1},a=function(t){if(o.onetimeLoad){var e=new Date;e.setDate(e.getDate()+t);var i=null==t?"":"expires="+e.toUTCString();document.cookie="jpreLoader=loaded; "+i}},s=function(){if(jOverlay=$("<div></div>").attr("id","jpreOverlay").css({position:"fixed",top:0,left:0,width:"100%",height:"100%",zIndex:9999999}).appendTo("body"),o.showSplash){jContent=$("<div></div>").attr("id","jpreSlide").appendTo(jOverlay);var t=$(window).width()-$(jContent).width();$(jContent).css({position:"absolute",top:o.splashVPos,left:Math.round(50/$(window).width()*t)+"%"}),$(jContent).html($(o.splashID).wrap("<div/>").parent().html()),$(o.splashID).remove(),o.splashFunction()}jLoader=$("<div></div>").attr("id","jpreLoader").appendTo(jOverlay);var e=$(window).width()-$(jLoader).width();$(jLoader).css({position:"absolute",top:o.loaderVPos,left:Math.round(50/$(window).width()*e)+"%"}),jBar=$("<div></div>").attr("id","jpreBar").css({width:"0%",height:"100%"}).appendTo(jLoader),o.showPercentage&&(jPer=$("<div></div>").attr("id","jprePercentage").css({position:"relative",height:"100%"}).appendTo(jLoader).html("0%")),o.autoclose||(jButton=$("<div></div>").attr("id","jpreButton").on("click",function(){h()}).css({position:"relative",height:"100%"}).appendTo(jLoader).text(o.closeBtnText).hide())},l=function(e){$(e).find("*:not(script)").each(function(){var e="";if($(this).css("background-image").indexOf("none")==-1&&$(this).css("background-image").indexOf("-gradient")==-1&&$(this).css("background-image").indexOf("svg")==-1){if(e=$(this).css("background-image"),e.indexOf("url")!=-1){var i=e.match(/url\((.*?)\)/);e=i[1].replace(/\"/g,"")}}else"img"==$(this).get(0).nodeName.toLowerCase()&&"undefined"!=typeof $(this).attr("src")&&(e=$(this).attr("src"));e.length>0&&t.push(e)})},d=function(){for(var e=0;e<t.length;e++)c(t[e])},c=function(t){var i=new Image;$(i).load(function(){u()}).error(function(){e.push($(this).attr("src")),u()}).attr("src",t)},u=function(){n++;var e=Math.round(n/t.length*100);if($(jBar).stop().animate({width:e+"%"},500,"linear"),o.showPercentage&&$(jPer).text(e+"%"),n>=t.length){if(n=t.length,a(),o.showPercentage&&$(jPer).text("100%"),o.debugMode)var i=p();$(jBar).stop().animate({width:"100%"},500,"linear",function(){o.autoClose?h():$(jButton).fadeIn(1e3)})}},h=function(){$(jOverlay).fadeOut(800,function(){$(jOverlay).remove(),i()})},p=function(){if(e.length>0){var t="ERROR - IMAGE FILES MISSING!!!\n\r";t+=e.length+" image files cound not be found. \n\r",t+="Please check your image paths and filenames:\n\r";for(var i=0;i<e.length;i++)t+="- "+e[i]+"\n\r";return!0}return!1};$.fn.jpreLoader=function(t,e){return t&&$.extend(o,t),"function"==typeof e&&(i=e),$("body").css({display:"block"}),this.each(function(){r()?($(o.splashID).remove(),i()):(s(),l(this),d())})}}(jQuery),"undefined"==typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");+function($){"use strict";var t=$.fn.jquery.split(" ")[0].split(".");if(t[0]<2&&t[1]<9||1==t[0]&&9==t[1]&&t[2]<1||t[0]>3)throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4")}(jQuery),+function($){"use strict";function t(){var t=document.createElement("bootstrap"),e={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var i in e)if(void 0!==t.style[i])return{end:e[i]};return!1}$.fn.emulateTransitionEnd=function(t){var e=!1,i=this;$(this).one("bsTransitionEnd",function(){e=!0});var n=function(){e||$(i).trigger($.support.transition.end)};return setTimeout(n,t),this},$(function(){$.support.transition=t(),$.support.transition&&($.event.special.bsTransitionEnd={bindType:$.support.transition.end,delegateType:$.support.transition.end,handle:function(t){if($(t.target).is(this))return t.handleObj.handler.apply(this,arguments)}})})}(jQuery),+function($){"use strict";function t(t){return this.each(function(){var e=$(this),n=e.data("bs.alert");n||e.data("bs.alert",n=new i(this)),"string"==typeof t&&n[t].call(e)})}var e='[data-dismiss="alert"]',i=function(t){$(t).on("click",e,this.close)};i.VERSION="3.3.7",i.TRANSITION_DURATION=150,i.prototype.close=function(t){function e(){r.detach().trigger("closed.bs.alert").remove()}var n=$(this),o=n.attr("data-target");o||(o=n.attr("href"),o=o&&o.replace(/.*(?=#[^\s]*$)/,""));var r=$("#"===o?[]:o);t&&t.preventDefault(),r.length||(r=n.closest(".alert")),r.trigger(t=$.Event("close.bs.alert")),t.isDefaultPrevented()||(r.removeClass("in"),$.support.transition&&r.hasClass("fade")?r.one("bsTransitionEnd",e).emulateTransitionEnd(i.TRANSITION_DURATION):e())};var n=$.fn.alert;$.fn.alert=t,$.fn.alert.Constructor=i,$.fn.alert.noConflict=function(){return $.fn.alert=n,this},$(document).on("click.bs.alert.data-api",e,i.prototype.close)}(jQuery),+function($){"use strict";function t(t){return this.each(function(){var i=$(this),n=i.data("bs.button"),o="object"==typeof t&&t;n||i.data("bs.button",n=new e(this,o)),"toggle"==t?n.toggle():t&&n.setState(t)})}var e=function(t,i){this.$element=$(t),this.options=$.extend({},e.DEFAULTS,i),this.isLoading=!1};e.VERSION="3.3.7",e.DEFAULTS={loadingText:"loading..."},e.prototype.setState=function(t){var e="disabled",i=this.$element,n=i.is("input")?"val":"html",o=i.data();t+="Text",null==o.resetText&&i.data("resetText",i[n]()),setTimeout($.proxy(function(){i[n](null==o[t]?this.options[t]:o[t]),"loadingText"==t?(this.isLoading=!0,i.addClass(e).attr(e,e).prop(e,!0)):this.isLoading&&(this.isLoading=!1,i.removeClass(e).removeAttr(e).prop(e,!1))},this),0)},e.prototype.toggle=function(){var t=!0,e=this.$element.closest('[data-toggle="buttons"]');if(e.length){var i=this.$element.find("input");"radio"==i.prop("type")?(i.prop("checked")&&(t=!1),e.find(".active").removeClass("active"),this.$element.addClass("active")):"checkbox"==i.prop("type")&&(i.prop("checked")!==this.$element.hasClass("active")&&(t=!1),this.$element.toggleClass("active")),i.prop("checked",this.$element.hasClass("active")),t&&i.trigger("change")}else this.$element.attr("aria-pressed",!this.$element.hasClass("active")),this.$element.toggleClass("active")};var i=$.fn.button;$.fn.button=t,$.fn.button.Constructor=e,$.fn.button.noConflict=function(){return $.fn.button=i,this},$(document).on("click.bs.button.data-api",'[data-toggle^="button"]',function(e){var i=$(e.target).closest(".btn");t.call(i,"toggle"),$(e.target).is('input[type="radio"], input[type="checkbox"]')||(e.preventDefault(),i.is("input,button")?i.trigger("focus"):i.find("input:visible,button:visible").first().trigger("focus"))}).on("focus.bs.button.data-api blur.bs.button.data-api",'[data-toggle^="button"]',function(t){$(t.target).closest(".btn").toggleClass("focus",/^focus(in)?$/.test(t.type))})}(jQuery),+function($){"use strict";function t(t){return this.each(function(){var i=$(this),n=i.data("bs.carousel"),o=$.extend({},e.DEFAULTS,i.data(),"object"==typeof t&&t),r="string"==typeof t?t:o.slide;n||i.data("bs.carousel",n=new e(this,o)),"number"==typeof t?n.to(t):r?n[r]():o.interval&&n.pause().cycle()})}var e=function(t,e){this.$element=$(t),this.$indicators=this.$element.find(".carousel-indicators"),this.options=e,this.paused=null,this.sliding=null,this.interval=null,this.$active=null,this.$items=null,this.options.keyboard&&this.$element.on("keydown.bs.carousel",$.proxy(this.keydown,this)),"hover"==this.options.pause&&!("ontouchstart"in document.documentElement)&&this.$element.on("mouseenter.bs.carousel",$.proxy(this.pause,this)).on("mouseleave.bs.carousel",$.proxy(this.cycle,this))};e.VERSION="3.3.7",e.TRANSITION_DURATION=600,e.DEFAULTS={interval:5e3,pause:"hover",wrap:!0,keyboard:!0},e.prototype.keydown=function(t){if(!/input|textarea/i.test(t.target.tagName)){switch(t.which){case 37:this.prev();break;case 39:this.next();break;default:return}t.preventDefault()}},e.prototype.cycle=function(t){return t||(this.paused=!1),this.interval&&clearInterval(this.interval),this.options.interval&&!this.paused&&(this.interval=setInterval($.proxy(this.next,this),this.options.interval)),this},e.prototype.getItemIndex=function(t){return this.$items=t.parent().children(".item"),this.$items.index(t||this.$active)},e.prototype.getItemForDirection=function(t,e){var i=this.getItemIndex(e),n="prev"==t&&0===i||"next"==t&&i==this.$items.length-1;if(n&&!this.options.wrap)return e;var o="prev"==t?-1:1,r=(i+o)%this.$items.length;return this.$items.eq(r)},e.prototype.to=function(t){var e=this,i=this.getItemIndex(this.$active=this.$element.find(".item.active"));if(!(t>this.$items.length-1||t<0))return this.sliding?this.$element.one("slid.bs.carousel",function(){e.to(t)}):i==t?this.pause().cycle():this.slide(t>i?"next":"prev",this.$items.eq(t))},e.prototype.pause=function(t){return t||(this.paused=!0),this.$element.find(".next, .prev").length&&$.support.transition&&(this.$element.trigger($.support.transition.end),this.cycle(!0)),this.interval=clearInterval(this.interval),this},e.prototype.next=function(){if(!this.sliding)return this.slide("next")},e.prototype.prev=function(){if(!this.sliding)return this.slide("prev")},e.prototype.slide=function(t,i){var n=this.$element.find(".item.active"),o=i||this.getItemForDirection(t,n),r=this.interval,a="next"==t?"left":"right",s=this;if(o.hasClass("active"))return this.sliding=!1;var l=o[0],d=$.Event("slide.bs.carousel",{relatedTarget:l,direction:a});if(this.$element.trigger(d),!d.isDefaultPrevented()){if(this.sliding=!0,r&&this.pause(),this.$indicators.length){this.$indicators.find(".active").removeClass("active");var c=$(this.$indicators.children()[this.getItemIndex(o)]);c&&c.addClass("active")}var u=$.Event("slid.bs.carousel",{relatedTarget:l,direction:a});return $.support.transition&&this.$element.hasClass("slide")?(o.addClass(t),o[0].offsetWidth,n.addClass(a),o.addClass(a),n.one("bsTransitionEnd",function(){o.removeClass([t,a].join(" ")).addClass("active"),n.removeClass(["active",a].join(" ")),s.sliding=!1,setTimeout(function(){s.$element.trigger(u)},0)}).emulateTransitionEnd(e.TRANSITION_DURATION)):(n.removeClass("active"),o.addClass("active"),this.sliding=!1,this.$element.trigger(u)),r&&this.cycle(),this}};var i=$.fn.carousel;$.fn.carousel=t,$.fn.carousel.Constructor=e,$.fn.carousel.noConflict=function(){return $.fn.carousel=i,this};var n=function(e){var i,n=$(this),o=$(n.attr("data-target")||(i=n.attr("href"))&&i.replace(/.*(?=#[^\s]+$)/,""));if(o.hasClass("carousel")){var r=$.extend({},o.data(),n.data()),a=n.attr("data-slide-to");a&&(r.interval=!1),t.call(o,r),a&&o.data("bs.carousel").to(a),e.preventDefault()}};$(document).on("click.bs.carousel.data-api","[data-slide]",n).on("click.bs.carousel.data-api","[data-slide-to]",n),$(window).on("load",function(){$('[data-ride="carousel"]').each(function(){var e=$(this);t.call(e,e.data())})})}(jQuery),+function($){"use strict";function t(t){var e,i=t.attr("data-target")||(e=t.attr("href"))&&e.replace(/.*(?=#[^\s]+$)/,"");return $(i)}function e(t){return this.each(function(){var e=$(this),n=e.data("bs.collapse"),o=$.extend({},i.DEFAULTS,e.data(),"object"==typeof t&&t);!n&&o.toggle&&/show|hide/.test(t)&&(o.toggle=!1),n||e.data("bs.collapse",n=new i(this,o)),"string"==typeof t&&n[t]()})}var i=function(t,e){this.$element=$(t),this.options=$.extend({},i.DEFAULTS,e),this.$trigger=$('[data-toggle="collapse"][href="#'+t.id+'"],[data-toggle="collapse"][data-target="#'+t.id+'"]'),this.transitioning=null,this.options.parent?this.$parent=this.getParent():this.addAriaAndCollapsedClass(this.$element,this.$trigger),this.options.toggle&&this.toggle()};i.VERSION="3.3.7",i.TRANSITION_DURATION=350,i.DEFAULTS={toggle:!0},i.prototype.dimension=function(){var t=this.$element.hasClass("width");return t?"width":"height"},i.prototype.show=function(){if(!this.transitioning&&!this.$element.hasClass("in")){var t,n=this.$parent&&this.$parent.children(".panel").children(".in, .collapsing");if(!(n&&n.length&&(t=n.data("bs.collapse"),t&&t.transitioning))){var o=$.Event("show.bs.collapse");if(this.$element.trigger(o),!o.isDefaultPrevented()){n&&n.length&&(e.call(n,"hide"),t||n.data("bs.collapse",null));var r=this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[r](0).attr("aria-expanded",!0),this.$trigger.removeClass("collapsed").attr("aria-expanded",!0),this.transitioning=1;var a=function(){this.$element.removeClass("collapsing").addClass("collapse in")[r](""),this.transitioning=0,this.$element.trigger("shown.bs.collapse")};if(!$.support.transition)return a.call(this);var s=$.camelCase(["scroll",r].join("-"));this.$element.one("bsTransitionEnd",$.proxy(a,this)).emulateTransitionEnd(i.TRANSITION_DURATION)[r](this.$element[0][s])}}}},i.prototype.hide=function(){if(!this.transitioning&&this.$element.hasClass("in")){var t=$.Event("hide.bs.collapse");if(this.$element.trigger(t),!t.isDefaultPrevented()){var e=this.dimension();this.$element[e](this.$element[e]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded",!1),this.$trigger.addClass("collapsed").attr("aria-expanded",!1),this.transitioning=1;var n=function(){this.transitioning=0,this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")};return $.support.transition?void this.$element[e](0).one("bsTransitionEnd",$.proxy(n,this)).emulateTransitionEnd(i.TRANSITION_DURATION):n.call(this)}}},i.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()},i.prototype.getParent=function(){return $(this.options.parent).find('[data-toggle="collapse"][data-parent="'+this.options.parent+'"]').each($.proxy(function(e,i){var n=$(i);this.addAriaAndCollapsedClass(t(n),n)},this)).end()},i.prototype.addAriaAndCollapsedClass=function(t,e){var i=t.hasClass("in");t.attr("aria-expanded",i),e.toggleClass("collapsed",!i).attr("aria-expanded",i)};var n=$.fn.collapse;$.fn.collapse=e,$.fn.collapse.Constructor=i,$.fn.collapse.noConflict=function(){return $.fn.collapse=n,this},$(document).on("click.bs.collapse.data-api",'[data-toggle="collapse"]',function(i){var n=$(this);n.attr("data-target")||i.preventDefault();var o=t(n),r=o.data("bs.collapse"),a=r?"toggle":n.data();e.call(o,a)})}(jQuery),+function($){"use strict";function t(t){var e=t.attr("data-target");e||(e=t.attr("href"),e=e&&/#[A-Za-z]/.test(e)&&e.replace(/.*(?=#[^\s]*$)/,""));var i=e&&$(e);return i&&i.length?i:t.parent()}function e(e){e&&3===e.which||($(n).remove(),$(o).each(function(){var i=$(this),n=t(i),o={relatedTarget:this};n.hasClass("open")&&(e&&"click"==e.type&&/input|textarea/i.test(e.target.tagName)&&$.contains(n[0],e.target)||(n.trigger(e=$.Event("hide.bs.dropdown",o)),e.isDefaultPrevented()||(i.attr("aria-expanded","false"),n.removeClass("open").trigger($.Event("hidden.bs.dropdown",o)))))}))}function i(t){return this.each(function(){var e=$(this),i=e.data("bs.dropdown");i||e.data("bs.dropdown",i=new r(this)),"string"==typeof t&&i[t].call(e)})}var n=".dropdown-backdrop",o='[data-toggle="dropdown"]',r=function(t){$(t).on("click.bs.dropdown",this.toggle)};r.VERSION="3.3.7",r.prototype.toggle=function(i){var n=$(this);if(!n.is(".disabled, :disabled")){var o=t(n),r=o.hasClass("open");if(e(),!r){"ontouchstart"in document.documentElement&&!o.closest(".navbar-nav").length&&$(document.createElement("div")).addClass("dropdown-backdrop").insertAfter($(this)).on("click",e);var a={relatedTarget:this};if(o.trigger(i=$.Event("show.bs.dropdown",a)),i.isDefaultPrevented())return;n.trigger("focus").attr("aria-expanded","true"),o.toggleClass("open").trigger($.Event("shown.bs.dropdown",a))}return!1}},r.prototype.keydown=function(e){if(/(38|40|27|32)/.test(e.which)&&!/input|textarea/i.test(e.target.tagName)){var i=$(this);if(e.preventDefault(),e.stopPropagation(),!i.is(".disabled, :disabled")){var n=t(i),r=n.hasClass("open");if(!r&&27!=e.which||r&&27==e.which)return 27==e.which&&n.find(o).trigger("focus"),i.trigger("click");var a=" li:not(.disabled):visible a",s=n.find(".dropdown-menu"+a);if(s.length){var l=s.index(e.target);38==e.which&&l>0&&l--,40==e.which&&l<s.length-1&&l++,~l||(l=0),s.eq(l).trigger("focus")}}}};var a=$.fn.dropdown;$.fn.dropdown=i,$.fn.dropdown.Constructor=r,$.fn.dropdown.noConflict=function(){return $.fn.dropdown=a,this},$(document).on("click.bs.dropdown.data-api",e).on("click.bs.dropdown.data-api",".dropdown form",function(t){t.stopPropagation()}).on("click.bs.dropdown.data-api",o,r.prototype.toggle).on("keydown.bs.dropdown.data-api",o,r.prototype.keydown).on("keydown.bs.dropdown.data-api",".dropdown-menu",r.prototype.keydown)}(jQuery),+function($){"use strict";function t(t,i){return this.each(function(){var n=$(this),o=n.data("bs.modal"),r=$.extend({},e.DEFAULTS,n.data(),"object"==typeof t&&t);o||n.data("bs.modal",o=new e(this,r)),"string"==typeof t?o[t](i):r.show&&o.show(i)})}var e=function(t,e){this.options=e,this.$body=$(document.body),this.$element=$(t),this.$dialog=this.$element.find(".modal-dialog"),this.$backdrop=null,this.isShown=null,this.originalBodyPad=null,this.scrollbarWidth=0,this.ignoreBackdropClick=!1,this.options.remote&&this.$element.find(".modal-content").load(this.options.remote,$.proxy(function(){this.$element.trigger("loaded.bs.modal")},this))};e.VERSION="3.3.7",e.TRANSITION_DURATION=300,e.BACKDROP_TRANSITION_DURATION=150,e.DEFAULTS={backdrop:!0,keyboard:!0,show:!0},e.prototype.toggle=function(t){return this.isShown?this.hide():this.show(t)},e.prototype.show=function(t){var i=this,n=$.Event("show.bs.modal",{relatedTarget:t});this.$element.trigger(n),this.isShown||n.isDefaultPrevented()||(this.isShown=!0,this.checkScrollbar(),this.setScrollbar(),this.$body.addClass("modal-open"),this.escape(),this.resize(),this.$element.on("click.dismiss.bs.modal",'[data-dismiss="modal"]',$.proxy(this.hide,this)),this.$dialog.on("mousedown.dismiss.bs.modal",function(){i.$element.one("mouseup.dismiss.bs.modal",function(t){$(t.target).is(i.$element)&&(i.ignoreBackdropClick=!0)})}),this.backdrop(function(){var n=$.support.transition&&i.$element.hasClass("fade");i.$element.parent().length||i.$element.appendTo(i.$body),i.$element.show().scrollTop(0),i.adjustDialog(),n&&i.$element[0].offsetWidth,i.$element.addClass("in"),i.enforceFocus();var o=$.Event("shown.bs.modal",{relatedTarget:t});n?i.$dialog.one("bsTransitionEnd",function(){i.$element.trigger("focus").trigger(o)}).emulateTransitionEnd(e.TRANSITION_DURATION):i.$element.trigger("focus").trigger(o)}))},e.prototype.hide=function(t){t&&t.preventDefault(),t=$.Event("hide.bs.modal"),this.$element.trigger(t),this.isShown&&!t.isDefaultPrevented()&&(this.isShown=!1,this.escape(),this.resize(),$(document).off("focusin.bs.modal"),this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"),this.$dialog.off("mousedown.dismiss.bs.modal"),$.support.transition&&this.$element.hasClass("fade")?this.$element.one("bsTransitionEnd",$.proxy(this.hideModal,this)).emulateTransitionEnd(e.TRANSITION_DURATION):this.hideModal())},e.prototype.enforceFocus=function(){$(document).off("focusin.bs.modal").on("focusin.bs.modal",$.proxy(function(t){document===t.target||this.$element[0]===t.target||this.$element.has(t.target).length||this.$element.trigger("focus")},this))},e.prototype.escape=function(){this.isShown&&this.options.keyboard?this.$element.on("keydown.dismiss.bs.modal",$.proxy(function(t){27==t.which&&this.hide()},this)):this.isShown||this.$element.off("keydown.dismiss.bs.modal")},e.prototype.resize=function(){this.isShown?$(window).on("resize.bs.modal",$.proxy(this.handleUpdate,this)):$(window).off("resize.bs.modal")},e.prototype.hideModal=function(){var t=this;this.$element.hide(),this.backdrop(function(){t.$body.removeClass("modal-open"),t.resetAdjustments(),t.resetScrollbar(),t.$element.trigger("hidden.bs.modal")})},e.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null},e.prototype.backdrop=function(t){var i=this,n=this.$element.hasClass("fade")?"fade":"";if(this.isShown&&this.options.backdrop){var o=$.support.transition&&n;if(this.$backdrop=$(document.createElement("div")).addClass("modal-backdrop "+n).appendTo(this.$body),this.$element.on("click.dismiss.bs.modal",$.proxy(function(t){return this.ignoreBackdropClick?void(this.ignoreBackdropClick=!1):void(t.target===t.currentTarget&&("static"==this.options.backdrop?this.$element[0].focus():this.hide()))},this)),o&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in"),!t)return;o?this.$backdrop.one("bsTransitionEnd",t).emulateTransitionEnd(e.BACKDROP_TRANSITION_DURATION):t()}else if(!this.isShown&&this.$backdrop){this.$backdrop.removeClass("in");var r=function(){i.removeBackdrop(),t&&t()};$.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one("bsTransitionEnd",r).emulateTransitionEnd(e.BACKDROP_TRANSITION_DURATION):r()}else t&&t()},e.prototype.handleUpdate=function(){this.adjustDialog()},e.prototype.adjustDialog=function(){var t=this.$element[0].scrollHeight>document.documentElement.clientHeight;this.$element.css({paddingLeft:!this.bodyIsOverflowing&&t?this.scrollbarWidth:"",paddingRight:this.bodyIsOverflowing&&!t?this.scrollbarWidth:""})},e.prototype.resetAdjustments=function(){this.$element.css({paddingLeft:"",paddingRight:""})},e.prototype.checkScrollbar=function(){var t=window.innerWidth;if(!t){var e=document.documentElement.getBoundingClientRect();t=e.right-Math.abs(e.left)}this.bodyIsOverflowing=document.body.clientWidth<t,this.scrollbarWidth=this.measureScrollbar()},e.prototype.setScrollbar=function(){var t=parseInt(this.$body.css("padding-right")||0,10);this.originalBodyPad=document.body.style.paddingRight||"",this.bodyIsOverflowing&&this.$body.css("padding-right",t+this.scrollbarWidth)},e.prototype.resetScrollbar=function(){this.$body.css("padding-right",this.originalBodyPad)},e.prototype.measureScrollbar=function(){var t=document.createElement("div");t.className="modal-scrollbar-measure",this.$body.append(t);var e=t.offsetWidth-t.clientWidth;return this.$body[0].removeChild(t),e};var i=$.fn.modal;$.fn.modal=t,$.fn.modal.Constructor=e,$.fn.modal.noConflict=function(){return $.fn.modal=i,this},$(document).on("click.bs.modal.data-api",'[data-toggle="modal"]',function(e){var i=$(this),n=i.attr("href"),o=$(i.attr("data-target")||n&&n.replace(/.*(?=#[^\s]+$)/,"")),r=o.data("bs.modal")?"toggle":$.extend({remote:!/#/.test(n)&&n},o.data(),i.data());i.is("a")&&e.preventDefault(),o.one("show.bs.modal",function(t){t.isDefaultPrevented()||o.one("hidden.bs.modal",function(){i.is(":visible")&&i.trigger("focus")})}),t.call(o,r,this)})}(jQuery),+function($){"use strict";function t(t){return this.each(function(){var i=$(this),n=i.data("bs.tooltip"),o="object"==typeof t&&t;!n&&/destroy|hide/.test(t)||(n||i.data("bs.tooltip",n=new e(this,o)),"string"==typeof t&&n[t]())})}var e=function(t,e){this.type=null,this.options=null,this.enabled=null,this.timeout=null,this.hoverState=null,this.$element=null,this.inState=null,this.init("tooltip",t,e)};e.VERSION="3.3.7",e.TRANSITION_DURATION=150,e.DEFAULTS={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1,viewport:{selector:"body",padding:0}},e.prototype.init=function(t,e,i){if(this.enabled=!0,this.type=t,this.$element=$(e),this.options=this.getOptions(i),this.$viewport=this.options.viewport&&$($.isFunction(this.options.viewport)?this.options.viewport.call(this,this.$element):this.options.viewport.selector||this.options.viewport),this.inState={click:!1,hover:!1,focus:!1},this.$element[0]instanceof document.constructor&&!this.options.selector)throw new Error("`selector` option must be specified when initializing "+this.type+" on the window.document object!");for(var n=this.options.trigger.split(" "),o=n.length;o--;){var r=n[o];if("click"==r)this.$element.on("click."+this.type,this.options.selector,$.proxy(this.toggle,this));else if("manual"!=r){var a="hover"==r?"mouseenter":"focusin",s="hover"==r?"mouseleave":"focusout";this.$element.on(a+"."+this.type,this.options.selector,$.proxy(this.enter,this)),this.$element.on(s+"."+this.type,this.options.selector,$.proxy(this.leave,this))}}this.options.selector?this._options=$.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()},e.prototype.getDefaults=function(){return e.DEFAULTS},e.prototype.getOptions=function(t){return t=$.extend({},this.getDefaults(),this.$element.data(),t),t.delay&&"number"==typeof t.delay&&(t.delay={show:t.delay,hide:t.delay}),t},e.prototype.getDelegateOptions=function(){var t={},e=this.getDefaults();return this._options&&$.each(this._options,function(i,n){e[i]!=n&&(t[i]=n)}),t},e.prototype.enter=function(t){var e=t instanceof this.constructor?t:$(t.currentTarget).data("bs."+this.type);return e||(e=new this.constructor(t.currentTarget,this.getDelegateOptions()),$(t.currentTarget).data("bs."+this.type,e)),t instanceof $.Event&&(e.inState["focusin"==t.type?"focus":"hover"]=!0),e.tip().hasClass("in")||"in"==e.hoverState?void(e.hoverState="in"):(clearTimeout(e.timeout),e.hoverState="in",e.options.delay&&e.options.delay.show?void(e.timeout=setTimeout(function(){"in"==e.hoverState&&e.show()},e.options.delay.show)):e.show())},e.prototype.isInStateTrue=function(){for(var t in this.inState)if(this.inState[t])return!0;return!1},e.prototype.leave=function(t){var e=t instanceof this.constructor?t:$(t.currentTarget).data("bs."+this.type);if(e||(e=new this.constructor(t.currentTarget,this.getDelegateOptions()),$(t.currentTarget).data("bs."+this.type,e)),t instanceof $.Event&&(e.inState["focusout"==t.type?"focus":"hover"]=!1),!e.isInStateTrue())return clearTimeout(e.timeout),e.hoverState="out",e.options.delay&&e.options.delay.hide?void(e.timeout=setTimeout(function(){"out"==e.hoverState&&e.hide()},e.options.delay.hide)):e.hide()},e.prototype.show=function(){var t=$.Event("show.bs."+this.type);if(this.hasContent()&&this.enabled){this.$element.trigger(t);var i=$.contains(this.$element[0].ownerDocument.documentElement,this.$element[0]);if(t.isDefaultPrevented()||!i)return;var n=this,o=this.tip(),r=this.getUID(this.type);this.setContent(),o.attr("id",r),this.$element.attr("aria-describedby",r),this.options.animation&&o.addClass("fade");var a="function"==typeof this.options.placement?this.options.placement.call(this,o[0],this.$element[0]):this.options.placement,s=/\s?auto?\s?/i,l=s.test(a);l&&(a=a.replace(s,"")||"top"),o.detach().css({top:0,left:0,display:"block"}).addClass(a).data("bs."+this.type,this),this.options.container?o.appendTo(this.options.container):o.insertAfter(this.$element),this.$element.trigger("inserted.bs."+this.type);var d=this.getPosition(),c=o[0].offsetWidth,u=o[0].offsetHeight;if(l){var h=a,p=this.getPosition(this.$viewport);a="bottom"==a&&d.bottom+u>p.bottom?"top":"top"==a&&d.top-u<p.top?"bottom":"right"==a&&d.right+c>p.width?"left":"left"==a&&d.left-c<p.left?"right":a,o.removeClass(h).addClass(a)}var f=this.getCalculatedOffset(a,d,c,u);this.applyPlacement(f,a);var g=function(){var t=n.hoverState;n.$element.trigger("shown.bs."+n.type),n.hoverState=null,"out"==t&&n.leave(n)};$.support.transition&&this.$tip.hasClass("fade")?o.one("bsTransitionEnd",g).emulateTransitionEnd(e.TRANSITION_DURATION):g()}},e.prototype.applyPlacement=function(t,e){var i=this.tip(),n=i[0].offsetWidth,o=i[0].offsetHeight,r=parseInt(i.css("margin-top"),10),a=parseInt(i.css("margin-left"),10);isNaN(r)&&(r=0),isNaN(a)&&(a=0),t.top+=r,t.left+=a,$.offset.setOffset(i[0],$.extend({using:function(t){i.css({top:Math.round(t.top),left:Math.round(t.left)})}},t),0),i.addClass("in");var s=i[0].offsetWidth,l=i[0].offsetHeight;"top"==e&&l!=o&&(t.top=t.top+o-l);var d=this.getViewportAdjustedDelta(e,t,s,l);d.left?t.left+=d.left:t.top+=d.top;var c=/top|bottom/.test(e),u=c?2*d.left-n+s:2*d.top-o+l,h=c?"offsetWidth":"offsetHeight";i.offset(t),this.replaceArrow(u,i[0][h],c)},e.prototype.replaceArrow=function(t,e,i){this.arrow().css(i?"left":"top",50*(1-t/e)+"%").css(i?"top":"left","")},e.prototype.setContent=function(){var t=this.tip(),e=this.getTitle();t.find(".tooltip-inner")[this.options.html?"html":"text"](e),t.removeClass("fade in top bottom left right")},e.prototype.hide=function(t){function i(){"in"!=n.hoverState&&o.detach(),n.$element&&n.$element.removeAttr("aria-describedby").trigger("hidden.bs."+n.type),t&&t()}var n=this,o=$(this.$tip),r=$.Event("hide.bs."+this.type);if(this.$element.trigger(r),!r.isDefaultPrevented())return o.removeClass("in"),$.support.transition&&o.hasClass("fade")?o.one("bsTransitionEnd",i).emulateTransitionEnd(e.TRANSITION_DURATION):i(),this.hoverState=null,this},e.prototype.fixTitle=function(){var t=this.$element;(t.attr("title")||"string"!=typeof t.attr("data-original-title"))&&t.attr("data-original-title",t.attr("title")||"").attr("title","")},e.prototype.hasContent=function(){return this.getTitle()},e.prototype.getPosition=function(t){t=t||this.$element;var e=t[0],i="BODY"==e.tagName,n=e.getBoundingClientRect();null==n.width&&(n=$.extend({},n,{width:n.right-n.left,height:n.bottom-n.top}));var o=window.SVGElement&&e instanceof window.SVGElement,r=i?{top:0,left:0}:o?null:t.offset(),a={scroll:i?document.documentElement.scrollTop||document.body.scrollTop:t.scrollTop()},s=i?{width:$(window).width(),height:$(window).height()}:null;return $.extend({},n,a,s,r)},e.prototype.getCalculatedOffset=function(t,e,i,n){return"bottom"==t?{top:e.top+e.height,left:e.left+e.width/2-i/2}:"top"==t?{top:e.top-n,left:e.left+e.width/2-i/2}:"left"==t?{top:e.top+e.height/2-n/2,left:e.left-i}:{top:e.top+e.height/2-n/2,left:e.left+e.width}},e.prototype.getViewportAdjustedDelta=function(t,e,i,n){var o={top:0,left:0};if(!this.$viewport)return o;var r=this.options.viewport&&this.options.viewport.padding||0,a=this.getPosition(this.$viewport);if(/right|left/.test(t)){var s=e.top-r-a.scroll,l=e.top+r-a.scroll+n;s<a.top?o.top=a.top-s:l>a.top+a.height&&(o.top=a.top+a.height-l)}else{var d=e.left-r,c=e.left+r+i;d<a.left?o.left=a.left-d:c>a.right&&(o.left=a.left+a.width-c)}return o},e.prototype.getTitle=function(){var t,e=this.$element,i=this.options;return t=e.attr("data-original-title")||("function"==typeof i.title?i.title.call(e[0]):i.title)},e.prototype.getUID=function(t){do t+=~~(1e6*Math.random());while(document.getElementById(t));return t},e.prototype.tip=function(){if(!this.$tip&&(this.$tip=$(this.options.template),1!=this.$tip.length))throw new Error(this.type+" `template` option must consist of exactly 1 top-level element!");return this.$tip},e.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")},e.prototype.enable=function(){this.enabled=!0},e.prototype.disable=function(){this.enabled=!1},e.prototype.toggleEnabled=function(){this.enabled=!this.enabled},e.prototype.toggle=function(t){var e=this;t&&(e=$(t.currentTarget).data("bs."+this.type),e||(e=new this.constructor(t.currentTarget,this.getDelegateOptions()),$(t.currentTarget).data("bs."+this.type,e))),t?(e.inState.click=!e.inState.click,e.isInStateTrue()?e.enter(e):e.leave(e)):e.tip().hasClass("in")?e.leave(e):e.enter(e)},e.prototype.destroy=function(){var t=this;clearTimeout(this.timeout),this.hide(function(){t.$element.off("."+t.type).removeData("bs."+t.type),t.$tip&&t.$tip.detach(),t.$tip=null,t.$arrow=null,t.$viewport=null,t.$element=null})};var i=$.fn.tooltip;$.fn.tooltip=t,$.fn.tooltip.Constructor=e,$.fn.tooltip.noConflict=function(){return $.fn.tooltip=i,this}}(jQuery),+function($){"use strict";function t(t){return this.each(function(){var i=$(this),n=i.data("bs.popover"),o="object"==typeof t&&t;
!n&&/destroy|hide/.test(t)||(n||i.data("bs.popover",n=new e(this,o)),"string"==typeof t&&n[t]())})}var e=function(t,e){this.init("popover",t,e)};if(!$.fn.tooltip)throw new Error("Popover requires tooltip.js");e.VERSION="3.3.7",e.DEFAULTS=$.extend({},$.fn.tooltip.Constructor.DEFAULTS,{placement:"right",trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'}),e.prototype=$.extend({},$.fn.tooltip.Constructor.prototype),e.prototype.constructor=e,e.prototype.getDefaults=function(){return e.DEFAULTS},e.prototype.setContent=function(){var t=this.tip(),e=this.getTitle(),i=this.getContent();t.find(".popover-title")[this.options.html?"html":"text"](e),t.find(".popover-content").children().detach().end()[this.options.html?"string"==typeof i?"html":"append":"text"](i),t.removeClass("fade top bottom left right in"),t.find(".popover-title").html()||t.find(".popover-title").hide()},e.prototype.hasContent=function(){return this.getTitle()||this.getContent()},e.prototype.getContent=function(){var t=this.$element,e=this.options;return t.attr("data-content")||("function"==typeof e.content?e.content.call(t[0]):e.content)},e.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".arrow")};var i=$.fn.popover;$.fn.popover=t,$.fn.popover.Constructor=e,$.fn.popover.noConflict=function(){return $.fn.popover=i,this}}(jQuery),+function($){"use strict";function t(e,i){this.$body=$(document.body),this.$scrollElement=$($(e).is(document.body)?window:e),this.options=$.extend({},t.DEFAULTS,i),this.selector=(this.options.target||"")+" .nav li > a",this.offsets=[],this.targets=[],this.activeTarget=null,this.scrollHeight=0,this.$scrollElement.on("scroll.bs.scrollspy",$.proxy(this.process,this)),this.refresh(),this.process()}function e(e){return this.each(function(){var i=$(this),n=i.data("bs.scrollspy"),o="object"==typeof e&&e;n||i.data("bs.scrollspy",n=new t(this,o)),"string"==typeof e&&n[e]()})}t.VERSION="3.3.7",t.DEFAULTS={offset:10},t.prototype.getScrollHeight=function(){return this.$scrollElement[0].scrollHeight||Math.max(this.$body[0].scrollHeight,document.documentElement.scrollHeight)},t.prototype.refresh=function(){var t=this,e="offset",i=0;this.offsets=[],this.targets=[],this.scrollHeight=this.getScrollHeight(),$.isWindow(this.$scrollElement[0])||(e="position",i=this.$scrollElement.scrollTop()),this.$body.find(this.selector).map(function(){var t=$(this),n=t.data("target")||t.attr("href"),o=/^#./.test(n)&&$(n);return o&&o.length&&o.is(":visible")&&[[o[e]().top+i,n]]||null}).sort(function(t,e){return t[0]-e[0]}).each(function(){t.offsets.push(this[0]),t.targets.push(this[1])})},t.prototype.process=function(){var t=this.$scrollElement.scrollTop()+this.options.offset,e=this.getScrollHeight(),i=this.options.offset+e-this.$scrollElement.height(),n=this.offsets,o=this.targets,r=this.activeTarget,a;if(this.scrollHeight!=e&&this.refresh(),t>=i)return r!=(a=o[o.length-1])&&this.activate(a);if(r&&t<n[0])return this.activeTarget=null,this.clear();for(a=n.length;a--;)r!=o[a]&&t>=n[a]&&(void 0===n[a+1]||t<n[a+1])&&this.activate(o[a])},t.prototype.activate=function(t){this.activeTarget=t,this.clear();var e=this.selector+'[data-target="'+t+'"],'+this.selector+'[href="'+t+'"]',i=$(e).parents("li").addClass("active");i.parent(".dropdown-menu").length&&(i=i.closest("li.dropdown").addClass("active")),i.trigger("activate.bs.scrollspy")},t.prototype.clear=function(){$(this.selector).parentsUntil(this.options.target,".active").removeClass("active")};var i=$.fn.scrollspy;$.fn.scrollspy=e,$.fn.scrollspy.Constructor=t,$.fn.scrollspy.noConflict=function(){return $.fn.scrollspy=i,this},$(window).on("load.bs.scrollspy.data-api",function(){$('[data-spy="scroll"]').each(function(){var t=$(this);e.call(t,t.data())})})}(jQuery),+function($){"use strict";function t(t){return this.each(function(){var i=$(this),n=i.data("bs.tab");n||i.data("bs.tab",n=new e(this)),"string"==typeof t&&n[t]()})}var e=function(t){this.element=$(t)};e.VERSION="3.3.7",e.TRANSITION_DURATION=150,e.prototype.show=function(){var t=this.element,e=t.closest("ul:not(.dropdown-menu)"),i=t.data("target");if(i||(i=t.attr("href"),i=i&&i.replace(/.*(?=#[^\s]*$)/,"")),!t.parent("li").hasClass("active")){var n=e.find(".active:last a"),o=$.Event("hide.bs.tab",{relatedTarget:t[0]}),r=$.Event("show.bs.tab",{relatedTarget:n[0]});if(n.trigger(o),t.trigger(r),!r.isDefaultPrevented()&&!o.isDefaultPrevented()){var a=$(i);this.activate(t.closest("li"),e),this.activate(a,a.parent(),function(){n.trigger({type:"hidden.bs.tab",relatedTarget:t[0]}),t.trigger({type:"shown.bs.tab",relatedTarget:n[0]})})}}},e.prototype.activate=function(t,i,n){function o(){r.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!1),t.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded",!0),a?(t[0].offsetWidth,t.addClass("in")):t.removeClass("fade"),t.parent(".dropdown-menu").length&&t.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!0),n&&n()}var r=i.find("> .active"),a=n&&$.support.transition&&(r.length&&r.hasClass("fade")||!!i.find("> .fade").length);r.length&&a?r.one("bsTransitionEnd",o).emulateTransitionEnd(e.TRANSITION_DURATION):o(),r.removeClass("in")};var i=$.fn.tab;$.fn.tab=t,$.fn.tab.Constructor=e,$.fn.tab.noConflict=function(){return $.fn.tab=i,this};var n=function(e){e.preventDefault(),t.call($(this),"show")};$(document).on("click.bs.tab.data-api",'[data-toggle="tab"]',n).on("click.bs.tab.data-api",'[data-toggle="pill"]',n)}(jQuery),+function($){"use strict";function t(t){return this.each(function(){var i=$(this),n=i.data("bs.affix"),o="object"==typeof t&&t;n||i.data("bs.affix",n=new e(this,o)),"string"==typeof t&&n[t]()})}var e=function(t,i){this.options=$.extend({},e.DEFAULTS,i),this.$target=$(this.options.target).on("scroll.bs.affix.data-api",$.proxy(this.checkPosition,this)).on("click.bs.affix.data-api",$.proxy(this.checkPositionWithEventLoop,this)),this.$element=$(t),this.affixed=null,this.unpin=null,this.pinnedOffset=null,this.checkPosition()};e.VERSION="3.3.7",e.RESET="affix affix-top affix-bottom",e.DEFAULTS={offset:0,target:window},e.prototype.getState=function(t,e,i,n){var o=this.$target.scrollTop(),r=this.$element.offset(),a=this.$target.height();if(null!=i&&"top"==this.affixed)return o<i&&"top";if("bottom"==this.affixed)return null!=i?!(o+this.unpin<=r.top)&&"bottom":!(o+a<=t-n)&&"bottom";var s=null==this.affixed,l=s?o:r.top,d=s?a:e;return null!=i&&o<=i?"top":null!=n&&l+d>=t-n&&"bottom"},e.prototype.getPinnedOffset=function(){if(this.pinnedOffset)return this.pinnedOffset;this.$element.removeClass(e.RESET).addClass("affix");var t=this.$target.scrollTop(),i=this.$element.offset();return this.pinnedOffset=i.top-t},e.prototype.checkPositionWithEventLoop=function(){setTimeout($.proxy(this.checkPosition,this),1)},e.prototype.checkPosition=function(){if(this.$element.is(":visible")){var t=this.$element.height(),i=this.options.offset,n=i.top,o=i.bottom,r=Math.max($(document).height(),$(document.body).height());"object"!=typeof i&&(o=n=i),"function"==typeof n&&(n=i.top(this.$element)),"function"==typeof o&&(o=i.bottom(this.$element));var a=this.getState(r,t,n,o);if(this.affixed!=a){null!=this.unpin&&this.$element.css("top","");var s="affix"+(a?"-"+a:""),l=$.Event(s+".bs.affix");if(this.$element.trigger(l),l.isDefaultPrevented())return;this.affixed=a,this.unpin="bottom"==a?this.getPinnedOffset():null,this.$element.removeClass(e.RESET).addClass(s).trigger(s.replace("affix","affixed")+".bs.affix")}"bottom"==a&&this.$element.offset({top:r-t-o})}};var i=$.fn.affix;$.fn.affix=t,$.fn.affix.Constructor=e,$.fn.affix.noConflict=function(){return $.fn.affix=i,this},$(window).on("load",function(){$('[data-spy="affix"]').each(function(){var e=$(this),i=e.data();i.offset=i.offset||{},null!=i.offsetBottom&&(i.offset.bottom=i.offsetBottom),null!=i.offsetTop&&(i.offset.top=i.offsetTop),t.call(e,i)})})}(jQuery),!function(t){"use strict";"function"==typeof define&&define.amd?define(["jquery"],t):"undefined"!=typeof exports?module.exports=t(require("jquery")):t(jQuery)}(function(t){"use strict";var e=window.Slick||{};e=function(){function e(e,n){var o,r=this;r.defaults={accessibility:!0,adaptiveHeight:!1,appendArrows:t(e),appendDots:t(e),arrows:!0,asNavFor:null,prevArrow:'<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',nextArrow:'<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',autoplay:!1,autoplaySpeed:3e3,centerMode:!1,centerPadding:"50px",cssEase:"ease",customPaging:function(e,i){return t('<button type="button" data-role="none" role="button" tabindex="0" />').text(i+1)},dots:!1,dotsClass:"slick-dots",draggable:!0,easing:"linear",edgeFriction:.35,fade:!1,focusOnSelect:!1,infinite:!0,initialSlide:0,lazyLoad:"ondemand",mobileFirst:!1,pauseOnHover:!0,pauseOnFocus:!0,pauseOnDotsHover:!1,respondTo:"window",responsive:null,rows:1,rtl:!1,slide:"",slidesPerRow:1,slidesToShow:1,slidesToScroll:1,speed:500,swipe:!0,swipeToSlide:!1,touchMove:!0,touchThreshold:5,useCSS:!0,useTransform:!0,variableWidth:!1,vertical:!1,verticalSwiping:!1,waitForAnimate:!0,zIndex:1e3},r.initials={animating:!1,dragging:!1,autoPlayTimer:null,currentDirection:0,currentLeft:null,currentSlide:0,direction:1,$dots:null,listWidth:null,listHeight:null,loadIndex:0,$nextArrow:null,$prevArrow:null,slideCount:null,slideWidth:null,$slideTrack:null,$slides:null,sliding:!1,slideOffset:0,swipeLeft:null,$list:null,touchObject:{},transformsEnabled:!1,unslicked:!1},t.extend(r,r.initials),r.activeBreakpoint=null,r.animType=null,r.animProp=null,r.breakpoints=[],r.breakpointSettings=[],r.cssTransitions=!1,r.focussed=!1,r.interrupted=!1,r.hidden="hidden",r.paused=!0,r.positionProp=null,r.respondTo=null,r.rowCount=1,r.shouldClick=!0,r.$slider=t(e),r.$slidesCache=null,r.transformType=null,r.transitionType=null,r.visibilityChange="visibilitychange",r.windowWidth=0,r.windowTimer=null,o=t(e).data("slick")||{},r.options=t.extend({},r.defaults,n,o),r.currentSlide=r.options.initialSlide,r.originalSettings=r.options,"undefined"!=typeof document.mozHidden?(r.hidden="mozHidden",r.visibilityChange="mozvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(r.hidden="webkitHidden",r.visibilityChange="webkitvisibilitychange"),r.autoPlay=t.proxy(r.autoPlay,r),r.autoPlayClear=t.proxy(r.autoPlayClear,r),r.autoPlayIterator=t.proxy(r.autoPlayIterator,r),r.changeSlide=t.proxy(r.changeSlide,r),r.clickHandler=t.proxy(r.clickHandler,r),r.selectHandler=t.proxy(r.selectHandler,r),r.setPosition=t.proxy(r.setPosition,r),r.swipeHandler=t.proxy(r.swipeHandler,r),r.dragHandler=t.proxy(r.dragHandler,r),r.keyHandler=t.proxy(r.keyHandler,r),r.instanceUid=i++,r.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/,r.registerBreakpoints(),r.init(!0)}var i=0;return e}(),e.prototype.activateADA=function(){var t=this;t.$slideTrack.find(".slick-active").attr({"aria-hidden":"false"}).find("a, input, button, select").attr({tabindex:"0"})},e.prototype.addSlide=e.prototype.slickAdd=function(e,i,n){var o=this;if("boolean"==typeof i)n=i,i=null;else if(0>i||i>=o.slideCount)return!1;o.unload(),"number"==typeof i?0===i&&0===o.$slides.length?t(e).appendTo(o.$slideTrack):n?t(e).insertBefore(o.$slides.eq(i)):t(e).insertAfter(o.$slides.eq(i)):n===!0?t(e).prependTo(o.$slideTrack):t(e).appendTo(o.$slideTrack),o.$slides=o.$slideTrack.children(this.options.slide),o.$slideTrack.children(this.options.slide).detach(),o.$slideTrack.append(o.$slides),o.$slides.each(function(e,i){t(i).attr("data-slick-index",e)}),o.$slidesCache=o.$slides,o.reinit()},e.prototype.animateHeight=function(){var t=this;if(1===t.options.slidesToShow&&t.options.adaptiveHeight===!0&&t.options.vertical===!1){var e=t.$slides.eq(t.currentSlide).outerHeight(!0);t.$list.animate({height:e},t.options.speed)}},e.prototype.animateSlide=function(e,i){var n={},o=this;o.animateHeight(),o.options.rtl===!0&&o.options.vertical===!1&&(e=-e),o.transformsEnabled===!1?o.options.vertical===!1?o.$slideTrack.animate({left:e},o.options.speed,o.options.easing,i):o.$slideTrack.animate({top:e},o.options.speed,o.options.easing,i):o.cssTransitions===!1?(o.options.rtl===!0&&(o.currentLeft=-o.currentLeft),t({animStart:o.currentLeft}).animate({animStart:e},{duration:o.options.speed,easing:o.options.easing,step:function(t){t=Math.ceil(t),o.options.vertical===!1?(n[o.animType]="translate("+t+"px, 0px)",o.$slideTrack.css(n)):(n[o.animType]="translate(0px,"+t+"px)",o.$slideTrack.css(n))},complete:function(){i&&i.call()}})):(o.applyTransition(),e=Math.ceil(e),o.options.vertical===!1?n[o.animType]="translate3d("+e+"px, 0px, 0px)":n[o.animType]="translate3d(0px,"+e+"px, 0px)",o.$slideTrack.css(n),i&&setTimeout(function(){o.disableTransition(),i.call()},o.options.speed))},e.prototype.getNavTarget=function(){var e=this,i=e.options.asNavFor;return i&&null!==i&&(i=t(i).not(e.$slider)),i},e.prototype.asNavFor=function(e){var i=this,n=i.getNavTarget();null!==n&&"object"==typeof n&&n.each(function(){var i=t(this).slick("getSlick");i.unslicked||i.slideHandler(e,!0)})},e.prototype.applyTransition=function(t){var e=this,i={};e.options.fade===!1?i[e.transitionType]=e.transformType+" "+e.options.speed+"ms "+e.options.cssEase:i[e.transitionType]="opacity "+e.options.speed+"ms "+e.options.cssEase,e.options.fade===!1?e.$slideTrack.css(i):e.$slides.eq(t).css(i)},e.prototype.autoPlay=function(){var t=this;t.autoPlayClear(),t.slideCount>t.options.slidesToShow&&(t.autoPlayTimer=setInterval(t.autoPlayIterator,t.options.autoplaySpeed))},e.prototype.autoPlayClear=function(){var t=this;t.autoPlayTimer&&clearInterval(t.autoPlayTimer)},e.prototype.autoPlayIterator=function(){var t=this,e=t.currentSlide+t.options.slidesToScroll;t.paused||t.interrupted||t.focussed||(t.options.infinite===!1&&(1===t.direction&&t.currentSlide+1===t.slideCount-1?t.direction=0:0===t.direction&&(e=t.currentSlide-t.options.slidesToScroll,t.currentSlide-1===0&&(t.direction=1))),t.slideHandler(e))},e.prototype.buildArrows=function(){var e=this;e.options.arrows===!0&&(e.$prevArrow=t(e.options.prevArrow).addClass("slick-arrow"),e.$nextArrow=t(e.options.nextArrow).addClass("slick-arrow"),e.slideCount>e.options.slidesToShow?(e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),e.htmlExpr.test(e.options.prevArrow)&&e.$prevArrow.prependTo(e.options.appendArrows),e.htmlExpr.test(e.options.nextArrow)&&e.$nextArrow.appendTo(e.options.appendArrows),e.options.infinite!==!0&&e.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true")):e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({"aria-disabled":"true",tabindex:"-1"}))},e.prototype.buildDots=function(){var e,i,n=this;if(n.options.dots===!0&&n.slideCount>n.options.slidesToShow){for(n.$slider.addClass("slick-dotted"),i=t("<ul />").addClass(n.options.dotsClass),e=0;e<=n.getDotCount();e+=1)i.append(t("<li />").append(n.options.customPaging.call(this,n,e)));n.$dots=i.appendTo(n.options.appendDots),n.$dots.find("li").first().addClass("slick-active").attr("aria-hidden","false")}},e.prototype.buildOut=function(){var e=this;e.$slides=e.$slider.children(e.options.slide+":not(.slick-cloned)").addClass("slick-slide"),e.slideCount=e.$slides.length,e.$slides.each(function(e,i){t(i).attr("data-slick-index",e).data("originalStyling",t(i).attr("style")||"")}),e.$slider.addClass("slick-slider"),e.$slideTrack=0===e.slideCount?t('<div class="slick-track"/>').appendTo(e.$slider):e.$slides.wrapAll('<div class="slick-track"/>').parent(),e.$list=e.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(),e.$slideTrack.css("opacity",0),(e.options.centerMode===!0||e.options.swipeToSlide===!0)&&(e.options.slidesToScroll=1),t("img[data-lazy]",e.$slider).not("[src]").addClass("slick-loading"),e.setupInfinite(),e.buildArrows(),e.buildDots(),e.updateDots(),e.setSlideClasses("number"==typeof e.currentSlide?e.currentSlide:0),e.options.draggable===!0&&e.$list.addClass("draggable")},e.prototype.buildRows=function(){var t,e,i,n,o,r,a,s=this;if(n=document.createDocumentFragment(),r=s.$slider.children(),s.options.rows>1){for(a=s.options.slidesPerRow*s.options.rows,o=Math.ceil(r.length/a),t=0;o>t;t++){var l=document.createElement("div");for(e=0;e<s.options.rows;e++){var d=document.createElement("div");for(i=0;i<s.options.slidesPerRow;i++){var c=t*a+(e*s.options.slidesPerRow+i);r.get(c)&&d.appendChild(r.get(c))}l.appendChild(d)}n.appendChild(l)}s.$slider.empty().append(n),s.$slider.children().children().children().css({width:100/s.options.slidesPerRow+"%",display:"inline-block"})}},e.prototype.checkResponsive=function(e,i){var n,o,r,a=this,s=!1,l=a.$slider.width(),d=window.innerWidth||t(window).width();if("window"===a.respondTo?r=d:"slider"===a.respondTo?r=l:"min"===a.respondTo&&(r=Math.min(d,l)),a.options.responsive&&a.options.responsive.length&&null!==a.options.responsive){o=null;for(n in a.breakpoints)a.breakpoints.hasOwnProperty(n)&&(a.originalSettings.mobileFirst===!1?r<a.breakpoints[n]&&(o=a.breakpoints[n]):r>a.breakpoints[n]&&(o=a.breakpoints[n]));null!==o?null!==a.activeBreakpoint?(o!==a.activeBreakpoint||i)&&(a.activeBreakpoint=o,"unslick"===a.breakpointSettings[o]?a.unslick(o):(a.options=t.extend({},a.originalSettings,a.breakpointSettings[o]),e===!0&&(a.currentSlide=a.options.initialSlide),a.refresh(e)),s=o):(a.activeBreakpoint=o,"unslick"===a.breakpointSettings[o]?a.unslick(o):(a.options=t.extend({},a.originalSettings,a.breakpointSettings[o]),e===!0&&(a.currentSlide=a.options.initialSlide),a.refresh(e)),s=o):null!==a.activeBreakpoint&&(a.activeBreakpoint=null,a.options=a.originalSettings,e===!0&&(a.currentSlide=a.options.initialSlide),a.refresh(e),s=o),e||s===!1||a.$slider.trigger("breakpoint",[a,s])}},e.prototype.changeSlide=function(e,i){var n,o,r,a=this,s=t(e.currentTarget);switch(s.is("a")&&e.preventDefault(),s.is("li")||(s=s.closest("li")),r=a.slideCount%a.options.slidesToScroll!==0,n=r?0:(a.slideCount-a.currentSlide)%a.options.slidesToScroll,e.data.message){case"previous":o=0===n?a.options.slidesToScroll:a.options.slidesToShow-n,a.slideCount>a.options.slidesToShow&&a.slideHandler(a.currentSlide-o,!1,i);break;case"next":o=0===n?a.options.slidesToScroll:n,a.slideCount>a.options.slidesToShow&&a.slideHandler(a.currentSlide+o,!1,i);break;case"index":var l=0===e.data.index?0:e.data.index||s.index()*a.options.slidesToScroll;a.slideHandler(a.checkNavigable(l),!1,i),s.children().trigger("focus");break;default:return}},e.prototype.checkNavigable=function(t){var e,i,n=this;if(e=n.getNavigableIndexes(),i=0,t>e[e.length-1])t=e[e.length-1];else for(var o in e){if(t<e[o]){t=i;break}i=e[o]}return t},e.prototype.cleanUpEvents=function(){var e=this;e.options.dots&&null!==e.$dots&&t("li",e.$dots).off("click.slick",e.changeSlide).off("mouseenter.slick",t.proxy(e.interrupt,e,!0)).off("mouseleave.slick",t.proxy(e.interrupt,e,!1)),e.$slider.off("focus.slick blur.slick"),e.options.arrows===!0&&e.slideCount>e.options.slidesToShow&&(e.$prevArrow&&e.$prevArrow.off("click.slick",e.changeSlide),e.$nextArrow&&e.$nextArrow.off("click.slick",e.changeSlide)),e.$list.off("touchstart.slick mousedown.slick",e.swipeHandler),e.$list.off("touchmove.slick mousemove.slick",e.swipeHandler),e.$list.off("touchend.slick mouseup.slick",e.swipeHandler),e.$list.off("touchcancel.slick mouseleave.slick",e.swipeHandler),e.$list.off("click.slick",e.clickHandler),t(document).off(e.visibilityChange,e.visibility),e.cleanUpSlideEvents(),e.options.accessibility===!0&&e.$list.off("keydown.slick",e.keyHandler),e.options.focusOnSelect===!0&&t(e.$slideTrack).children().off("click.slick",e.selectHandler),t(window).off("orientationchange.slick.slick-"+e.instanceUid,e.orientationChange),t(window).off("resize.slick.slick-"+e.instanceUid,e.resize),t("[draggable!=true]",e.$slideTrack).off("dragstart",e.preventDefault),t(window).off("load.slick.slick-"+e.instanceUid,e.setPosition),t(document).off("ready.slick.slick-"+e.instanceUid,e.setPosition)},e.prototype.cleanUpSlideEvents=function(){var e=this;e.$list.off("mouseenter.slick",t.proxy(e.interrupt,e,!0)),e.$list.off("mouseleave.slick",t.proxy(e.interrupt,e,!1))},e.prototype.cleanUpRows=function(){var t,e=this;e.options.rows>1&&(t=e.$slides.children().children(),t.removeAttr("style"),e.$slider.empty().append(t))},e.prototype.clickHandler=function(t){var e=this;e.shouldClick===!1&&(t.stopImmediatePropagation(),t.stopPropagation(),t.preventDefault())},e.prototype.destroy=function(e){var i=this;i.autoPlayClear(),i.touchObject={},i.cleanUpEvents(),t(".slick-cloned",i.$slider).detach(),i.$dots&&i.$dots.remove(),i.$prevArrow&&i.$prevArrow.length&&(i.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),i.htmlExpr.test(i.options.prevArrow)&&i.$prevArrow.remove()),i.$nextArrow&&i.$nextArrow.length&&(i.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),i.htmlExpr.test(i.options.nextArrow)&&i.$nextArrow.remove()),i.$slides&&(i.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function(){t(this).attr("style",t(this).data("originalStyling"))}),i.$slideTrack.children(this.options.slide).detach(),i.$slideTrack.detach(),i.$list.detach(),i.$slider.append(i.$slides)),i.cleanUpRows(),i.$slider.removeClass("slick-slider"),i.$slider.removeClass("slick-initialized"),i.$slider.removeClass("slick-dotted"),i.unslicked=!0,e||i.$slider.trigger("destroy",[i])},e.prototype.disableTransition=function(t){var e=this,i={};i[e.transitionType]="",e.options.fade===!1?e.$slideTrack.css(i):e.$slides.eq(t).css(i)},e.prototype.fadeSlide=function(t,e){var i=this;i.cssTransitions===!1?(i.$slides.eq(t).css({zIndex:i.options.zIndex}),i.$slides.eq(t).animate({opacity:1},i.options.speed,i.options.easing,e)):(i.applyTransition(t),i.$slides.eq(t).css({opacity:1,zIndex:i.options.zIndex}),e&&setTimeout(function(){i.disableTransition(t),e.call()},i.options.speed))},e.prototype.fadeSlideOut=function(t){var e=this;e.cssTransitions===!1?e.$slides.eq(t).animate({opacity:0,zIndex:e.options.zIndex-2},e.options.speed,e.options.easing):(e.applyTransition(t),e.$slides.eq(t).css({opacity:0,zIndex:e.options.zIndex-2}))},e.prototype.filterSlides=e.prototype.slickFilter=function(t){var e=this;null!==t&&(e.$slidesCache=e.$slides,e.unload(),e.$slideTrack.children(this.options.slide).detach(),e.$slidesCache.filter(t).appendTo(e.$slideTrack),e.reinit())},e.prototype.focusHandler=function(){var e=this;e.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick","*:not(.slick-arrow)",function(i){i.stopImmediatePropagation();var n=t(this);setTimeout(function(){e.options.pauseOnFocus&&(e.focussed=n.is(":focus"),e.autoPlay())},0)})},e.prototype.getCurrent=e.prototype.slickCurrentSlide=function(){var t=this;return t.currentSlide},e.prototype.getDotCount=function(){var t=this,e=0,i=0,n=0;if(t.options.infinite===!0)for(;e<t.slideCount;)++n,e=i+t.options.slidesToScroll,i+=t.options.slidesToScroll<=t.options.slidesToShow?t.options.slidesToScroll:t.options.slidesToShow;else if(t.options.centerMode===!0)n=t.slideCount;else if(t.options.asNavFor)for(;e<t.slideCount;)++n,e=i+t.options.slidesToScroll,i+=t.options.slidesToScroll<=t.options.slidesToShow?t.options.slidesToScroll:t.options.slidesToShow;else n=1+Math.ceil((t.slideCount-t.options.slidesToShow)/t.options.slidesToScroll);return n-1},e.prototype.getLeft=function(t){var e,i,n,o=this,r=0;return o.slideOffset=0,i=o.$slides.first().outerHeight(!0),o.options.infinite===!0?(o.slideCount>o.options.slidesToShow&&(o.slideOffset=o.slideWidth*o.options.slidesToShow*-1,r=i*o.options.slidesToShow*-1),o.slideCount%o.options.slidesToScroll!==0&&t+o.options.slidesToScroll>o.slideCount&&o.slideCount>o.options.slidesToShow&&(t>o.slideCount?(o.slideOffset=(o.options.slidesToShow-(t-o.slideCount))*o.slideWidth*-1,r=(o.options.slidesToShow-(t-o.slideCount))*i*-1):(o.slideOffset=o.slideCount%o.options.slidesToScroll*o.slideWidth*-1,r=o.slideCount%o.options.slidesToScroll*i*-1))):t+o.options.slidesToShow>o.slideCount&&(o.slideOffset=(t+o.options.slidesToShow-o.slideCount)*o.slideWidth,r=(t+o.options.slidesToShow-o.slideCount)*i),o.slideCount<=o.options.slidesToShow&&(o.slideOffset=0,r=0),o.options.centerMode===!0&&o.options.infinite===!0?o.slideOffset+=o.slideWidth*Math.floor(o.options.slidesToShow/2)-o.slideWidth:o.options.centerMode===!0&&(o.slideOffset=0,o.slideOffset+=o.slideWidth*Math.floor(o.options.slidesToShow/2)),e=o.options.vertical===!1?t*o.slideWidth*-1+o.slideOffset:t*i*-1+r,o.options.variableWidth===!0&&(n=o.slideCount<=o.options.slidesToShow||o.options.infinite===!1?o.$slideTrack.children(".slick-slide").eq(t):o.$slideTrack.children(".slick-slide").eq(t+o.options.slidesToShow),e=o.options.rtl===!0?n[0]?-1*(o.$slideTrack.width()-n[0].offsetLeft-n.width()):0:n[0]?-1*n[0].offsetLeft:0,o.options.centerMode===!0&&(n=o.slideCount<=o.options.slidesToShow||o.options.infinite===!1?o.$slideTrack.children(".slick-slide").eq(t):o.$slideTrack.children(".slick-slide").eq(t+o.options.slidesToShow+1),e=o.options.rtl===!0?n[0]?-1*(o.$slideTrack.width()-n[0].offsetLeft-n.width()):0:n[0]?-1*n[0].offsetLeft:0,e+=(o.$list.width()-n.outerWidth())/2)),e},e.prototype.getOption=e.prototype.slickGetOption=function(t){var e=this;return e.options[t]},e.prototype.getNavigableIndexes=function(){var t,e=this,i=0,n=0,o=[];for(e.options.infinite===!1?t=e.slideCount:(i=-1*e.options.slidesToScroll,n=-1*e.options.slidesToScroll,t=2*e.slideCount);t>i;)o.push(i),i=n+e.options.slidesToScroll,n+=e.options.slidesToScroll<=e.options.slidesToShow?e.options.slidesToScroll:e.options.slidesToShow;return o},e.prototype.getSlick=function(){return this},e.prototype.getSlideCount=function(){var e,i,n,o=this;return n=o.options.centerMode===!0?o.slideWidth*Math.floor(o.options.slidesToShow/2):0,o.options.swipeToSlide===!0?(o.$slideTrack.find(".slick-slide").each(function(e,r){return r.offsetLeft-n+t(r).outerWidth()/2>-1*o.swipeLeft?(i=r,!1):void 0}),e=Math.abs(t(i).attr("data-slick-index")-o.currentSlide)||1):o.options.slidesToScroll},e.prototype.goTo=e.prototype.slickGoTo=function(t,e){var i=this;i.changeSlide({data:{message:"index",index:parseInt(t)}},e)},e.prototype.init=function(e){var i=this;t(i.$slider).hasClass("slick-initialized")||(t(i.$slider).addClass("slick-initialized"),i.buildRows(),i.buildOut(),i.setProps(),i.startLoad(),i.loadSlider(),i.initializeEvents(),i.updateArrows(),i.updateDots(),i.checkResponsive(!0),i.focusHandler()),e&&i.$slider.trigger("init",[i]),i.options.accessibility===!0&&i.initADA(),i.options.autoplay&&(i.paused=!1,i.autoPlay())},e.prototype.initADA=function(){var e=this;e.$slides.add(e.$slideTrack.find(".slick-cloned")).attr({"aria-hidden":"true",tabindex:"-1"}).find("a, input, button, select").attr({tabindex:"-1"}),e.$slideTrack.attr("role","listbox"),e.$slides.not(e.$slideTrack.find(".slick-cloned")).each(function(i){t(this).attr({role:"option","aria-describedby":"slick-slide"+e.instanceUid+i})}),null!==e.$dots&&e.$dots.attr("role","tablist").find("li").each(function(i){t(this).attr({role:"presentation","aria-selected":"false","aria-controls":"navigation"+e.instanceUid+i,id:"slick-slide"+e.instanceUid+i})}).first().attr("aria-selected","true").end().find("button").attr("role","button").end().closest("div").attr("role","toolbar"),e.activateADA()},e.prototype.initArrowEvents=function(){var t=this;t.options.arrows===!0&&t.slideCount>t.options.slidesToShow&&(t.$prevArrow.off("click.slick").on("click.slick",{message:"previous"},t.changeSlide),t.$nextArrow.off("click.slick").on("click.slick",{message:"next"},t.changeSlide))},e.prototype.initDotEvents=function(){var e=this;e.options.dots===!0&&e.slideCount>e.options.slidesToShow&&t("li",e.$dots).on("click.slick",{message:"index"},e.changeSlide),e.options.dots===!0&&e.options.pauseOnDotsHover===!0&&t("li",e.$dots).on("mouseenter.slick",t.proxy(e.interrupt,e,!0)).on("mouseleave.slick",t.proxy(e.interrupt,e,!1))},e.prototype.initSlideEvents=function(){var e=this;e.options.pauseOnHover&&(e.$list.on("mouseenter.slick",t.proxy(e.interrupt,e,!0)),e.$list.on("mouseleave.slick",t.proxy(e.interrupt,e,!1)))},e.prototype.initializeEvents=function(){var e=this;e.initArrowEvents(),e.initDotEvents(),e.initSlideEvents(),e.$list.on("touchstart.slick mousedown.slick",{action:"start"},e.swipeHandler),e.$list.on("touchmove.slick mousemove.slick",{action:"move"},e.swipeHandler),e.$list.on("touchend.slick mouseup.slick",{action:"end"},e.swipeHandler),e.$list.on("touchcancel.slick mouseleave.slick",{action:"end"},e.swipeHandler),e.$list.on("click.slick",e.clickHandler),t(document).on(e.visibilityChange,t.proxy(e.visibility,e)),e.options.accessibility===!0&&e.$list.on("keydown.slick",e.keyHandler),e.options.focusOnSelect===!0&&t(e.$slideTrack).children().on("click.slick",e.selectHandler),t(window).on("orientationchange.slick.slick-"+e.instanceUid,t.proxy(e.orientationChange,e)),t(window).on("resize.slick.slick-"+e.instanceUid,t.proxy(e.resize,e)),t("[draggable!=true]",e.$slideTrack).on("dragstart",e.preventDefault),t(window).on("load.slick.slick-"+e.instanceUid,e.setPosition),t(document).on("ready.slick.slick-"+e.instanceUid,e.setPosition)},e.prototype.initUI=function(){var t=this;t.options.arrows===!0&&t.slideCount>t.options.slidesToShow&&(t.$prevArrow.show(),t.$nextArrow.show()),t.options.dots===!0&&t.slideCount>t.options.slidesToShow&&t.$dots.show()},e.prototype.keyHandler=function(t){var e=this;t.target.tagName.match("TEXTAREA|INPUT|SELECT")||(37===t.keyCode&&e.options.accessibility===!0?e.changeSlide({data:{message:e.options.rtl===!0?"next":"previous"}}):39===t.keyCode&&e.options.accessibility===!0&&e.changeSlide({data:{message:e.options.rtl===!0?"previous":"next"}}))},e.prototype.lazyLoad=function(){function e(e){t("img[data-lazy]",e).each(function(){var e=t(this),i=t(this).attr("data-lazy"),n=document.createElement("img");n.onload=function(){e.animate({opacity:0},100,function(){e.attr("src",i).animate({opacity:1},200,function(){e.removeAttr("data-lazy").removeClass("slick-loading")}),a.$slider.trigger("lazyLoaded",[a,e,i])})},n.onerror=function(){e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),a.$slider.trigger("lazyLoadError",[a,e,i])},n.src=i})}var i,n,o,r,a=this;a.options.centerMode===!0?a.options.infinite===!0?(o=a.currentSlide+(a.options.slidesToShow/2+1),r=o+a.options.slidesToShow+2):(o=Math.max(0,a.currentSlide-(a.options.slidesToShow/2+1)),r=2+(a.options.slidesToShow/2+1)+a.currentSlide):(o=a.options.infinite?a.options.slidesToShow+a.currentSlide:a.currentSlide,r=Math.ceil(o+a.options.slidesToShow),a.options.fade===!0&&(o>0&&o--,r<=a.slideCount&&r++)),i=a.$slider.find(".slick-slide").slice(o,r),e(i),a.slideCount<=a.options.slidesToShow?(n=a.$slider.find(".slick-slide"),e(n)):a.currentSlide>=a.slideCount-a.options.slidesToShow?(n=a.$slider.find(".slick-cloned").slice(0,a.options.slidesToShow),e(n)):0===a.currentSlide&&(n=a.$slider.find(".slick-cloned").slice(-1*a.options.slidesToShow),e(n))},e.prototype.loadSlider=function(){var t=this;t.setPosition(),t.$slideTrack.css({opacity:1}),t.$slider.removeClass("slick-loading"),t.initUI(),"progressive"===t.options.lazyLoad&&t.progressiveLazyLoad()},e.prototype.next=e.prototype.slickNext=function(){var t=this;t.changeSlide({data:{message:"next"}})},e.prototype.orientationChange=function(){var t=this;t.checkResponsive(),t.setPosition()},e.prototype.pause=e.prototype.slickPause=function(){var t=this;t.autoPlayClear(),t.paused=!0},e.prototype.play=e.prototype.slickPlay=function(){
var t=this;t.autoPlay(),t.options.autoplay=!0,t.paused=!1,t.focussed=!1,t.interrupted=!1},e.prototype.postSlide=function(t){var e=this;e.unslicked||(e.$slider.trigger("afterChange",[e,t]),e.animating=!1,e.setPosition(),e.swipeLeft=null,e.options.autoplay&&e.autoPlay(),e.options.accessibility===!0&&e.initADA())},e.prototype.prev=e.prototype.slickPrev=function(){var t=this;t.changeSlide({data:{message:"previous"}})},e.prototype.preventDefault=function(t){t.preventDefault()},e.prototype.progressiveLazyLoad=function(e){e=e||1;var i,n,o,r=this,a=t("img[data-lazy]",r.$slider);a.length?(i=a.first(),n=i.attr("data-lazy"),o=document.createElement("img"),o.onload=function(){i.attr("src",n).removeAttr("data-lazy").removeClass("slick-loading"),r.options.adaptiveHeight===!0&&r.setPosition(),r.$slider.trigger("lazyLoaded",[r,i,n]),r.progressiveLazyLoad()},o.onerror=function(){3>e?setTimeout(function(){r.progressiveLazyLoad(e+1)},500):(i.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),r.$slider.trigger("lazyLoadError",[r,i,n]),r.progressiveLazyLoad())},o.src=n):r.$slider.trigger("allImagesLoaded",[r])},e.prototype.refresh=function(e){var i,n,o=this;n=o.slideCount-o.options.slidesToShow,!o.options.infinite&&o.currentSlide>n&&(o.currentSlide=n),o.slideCount<=o.options.slidesToShow&&(o.currentSlide=0),i=o.currentSlide,o.destroy(!0),t.extend(o,o.initials,{currentSlide:i}),o.init(),e||o.changeSlide({data:{message:"index",index:i}},!1)},e.prototype.registerBreakpoints=function(){var e,i,n,o=this,r=o.options.responsive||null;if("array"===t.type(r)&&r.length){o.respondTo=o.options.respondTo||"window";for(e in r)if(n=o.breakpoints.length-1,i=r[e].breakpoint,r.hasOwnProperty(e)){for(;n>=0;)o.breakpoints[n]&&o.breakpoints[n]===i&&o.breakpoints.splice(n,1),n--;o.breakpoints.push(i),o.breakpointSettings[i]=r[e].settings}o.breakpoints.sort(function(t,e){return o.options.mobileFirst?t-e:e-t})}},e.prototype.reinit=function(){var e=this;e.$slides=e.$slideTrack.children(e.options.slide).addClass("slick-slide"),e.slideCount=e.$slides.length,e.currentSlide>=e.slideCount&&0!==e.currentSlide&&(e.currentSlide=e.currentSlide-e.options.slidesToScroll),e.slideCount<=e.options.slidesToShow&&(e.currentSlide=0),e.registerBreakpoints(),e.setProps(),e.setupInfinite(),e.buildArrows(),e.updateArrows(),e.initArrowEvents(),e.buildDots(),e.updateDots(),e.initDotEvents(),e.cleanUpSlideEvents(),e.initSlideEvents(),e.checkResponsive(!1,!0),e.options.focusOnSelect===!0&&t(e.$slideTrack).children().on("click.slick",e.selectHandler),e.setSlideClasses("number"==typeof e.currentSlide?e.currentSlide:0),e.setPosition(),e.focusHandler(),e.paused=!e.options.autoplay,e.autoPlay(),e.$slider.trigger("reInit",[e])},e.prototype.resize=function(){var e=this;t(window).width()!==e.windowWidth&&(clearTimeout(e.windowDelay),e.windowDelay=window.setTimeout(function(){e.windowWidth=t(window).width(),e.checkResponsive(),e.unslicked||e.setPosition()},50))},e.prototype.removeSlide=e.prototype.slickRemove=function(t,e,i){var n=this;return"boolean"==typeof t?(e=t,t=e===!0?0:n.slideCount-1):t=e===!0?--t:t,!(n.slideCount<1||0>t||t>n.slideCount-1)&&(n.unload(),i===!0?n.$slideTrack.children().remove():n.$slideTrack.children(this.options.slide).eq(t).remove(),n.$slides=n.$slideTrack.children(this.options.slide),n.$slideTrack.children(this.options.slide).detach(),n.$slideTrack.append(n.$slides),n.$slidesCache=n.$slides,void n.reinit())},e.prototype.setCSS=function(t){var e,i,n=this,o={};n.options.rtl===!0&&(t=-t),e="left"==n.positionProp?Math.ceil(t)+"px":"0px",i="top"==n.positionProp?Math.ceil(t)+"px":"0px",o[n.positionProp]=t,n.transformsEnabled===!1?n.$slideTrack.css(o):(o={},n.cssTransitions===!1?(o[n.animType]="translate("+e+", "+i+")",n.$slideTrack.css(o)):(o[n.animType]="translate3d("+e+", "+i+", 0px)",n.$slideTrack.css(o)))},e.prototype.setDimensions=function(){var t=this;t.options.vertical===!1?t.options.centerMode===!0&&t.$list.css({padding:"0px "+t.options.centerPadding}):(t.$list.height(t.$slides.first().outerHeight(!0)*t.options.slidesToShow),t.options.centerMode===!0&&t.$list.css({padding:t.options.centerPadding+" 0px"})),t.listWidth=t.$list.width(),t.listHeight=t.$list.height(),t.options.vertical===!1&&t.options.variableWidth===!1?(t.slideWidth=Math.ceil(t.listWidth/t.options.slidesToShow),t.$slideTrack.width(Math.ceil(t.slideWidth*t.$slideTrack.children(".slick-slide").length))):t.options.variableWidth===!0?t.$slideTrack.width(5e3*t.slideCount):(t.slideWidth=Math.ceil(t.listWidth),t.$slideTrack.height(Math.ceil(t.$slides.first().outerHeight(!0)*t.$slideTrack.children(".slick-slide").length)));var e=t.$slides.first().outerWidth(!0)-t.$slides.first().width();t.options.variableWidth===!1&&t.$slideTrack.children(".slick-slide").width(t.slideWidth-e)},e.prototype.setFade=function(){var e,i=this;i.$slides.each(function(n,o){e=i.slideWidth*n*-1,i.options.rtl===!0?t(o).css({position:"relative",right:e,top:0,zIndex:i.options.zIndex-2,opacity:0}):t(o).css({position:"relative",left:e,top:0,zIndex:i.options.zIndex-2,opacity:0})}),i.$slides.eq(i.currentSlide).css({zIndex:i.options.zIndex-1,opacity:1})},e.prototype.setHeight=function(){var t=this;if(1===t.options.slidesToShow&&t.options.adaptiveHeight===!0&&t.options.vertical===!1){var e=t.$slides.eq(t.currentSlide).outerHeight(!0);t.$list.css("height",e)}},e.prototype.setOption=e.prototype.slickSetOption=function(){var e,i,n,o,r,a=this,s=!1;if("object"===t.type(arguments[0])?(n=arguments[0],s=arguments[1],r="multiple"):"string"===t.type(arguments[0])&&(n=arguments[0],o=arguments[1],s=arguments[2],"responsive"===arguments[0]&&"array"===t.type(arguments[1])?r="responsive":"undefined"!=typeof arguments[1]&&(r="single")),"single"===r)a.options[n]=o;else if("multiple"===r)t.each(n,function(t,e){a.options[t]=e});else if("responsive"===r)for(i in o)if("array"!==t.type(a.options.responsive))a.options.responsive=[o[i]];else{for(e=a.options.responsive.length-1;e>=0;)a.options.responsive[e].breakpoint===o[i].breakpoint&&a.options.responsive.splice(e,1),e--;a.options.responsive.push(o[i])}s&&(a.unload(),a.reinit())},e.prototype.setPosition=function(){var t=this;t.setDimensions(),t.setHeight(),t.options.fade===!1?t.setCSS(t.getLeft(t.currentSlide)):t.setFade(),t.$slider.trigger("setPosition",[t])},e.prototype.setProps=function(){var t=this,e=document.body.style;t.positionProp=t.options.vertical===!0?"top":"left","top"===t.positionProp?t.$slider.addClass("slick-vertical"):t.$slider.removeClass("slick-vertical"),(void 0!==e.WebkitTransition||void 0!==e.MozTransition||void 0!==e.msTransition)&&t.options.useCSS===!0&&(t.cssTransitions=!0),t.options.fade&&("number"==typeof t.options.zIndex?t.options.zIndex<3&&(t.options.zIndex=3):t.options.zIndex=t.defaults.zIndex),void 0!==e.OTransform&&(t.animType="OTransform",t.transformType="-o-transform",t.transitionType="OTransition",void 0===e.perspectiveProperty&&void 0===e.webkitPerspective&&(t.animType=!1)),void 0!==e.MozTransform&&(t.animType="MozTransform",t.transformType="-moz-transform",t.transitionType="MozTransition",void 0===e.perspectiveProperty&&void 0===e.MozPerspective&&(t.animType=!1)),void 0!==e.webkitTransform&&(t.animType="webkitTransform",t.transformType="-webkit-transform",t.transitionType="webkitTransition",void 0===e.perspectiveProperty&&void 0===e.webkitPerspective&&(t.animType=!1)),void 0!==e.msTransform&&(t.animType="msTransform",t.transformType="-ms-transform",t.transitionType="msTransition",void 0===e.msTransform&&(t.animType=!1)),void 0!==e.transform&&t.animType!==!1&&(t.animType="transform",t.transformType="transform",t.transitionType="transition"),t.transformsEnabled=t.options.useTransform&&null!==t.animType&&t.animType!==!1},e.prototype.setSlideClasses=function(t){var e,i,n,o,r=this;i=r.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden","true"),r.$slides.eq(t).addClass("slick-current"),r.options.centerMode===!0?(e=Math.floor(r.options.slidesToShow/2),r.options.infinite===!0&&(t>=e&&t<=r.slideCount-1-e?r.$slides.slice(t-e,t+e+1).addClass("slick-active").attr("aria-hidden","false"):(n=r.options.slidesToShow+t,i.slice(n-e+1,n+e+2).addClass("slick-active").attr("aria-hidden","false")),0===t?i.eq(i.length-1-r.options.slidesToShow).addClass("slick-center"):t===r.slideCount-1&&i.eq(r.options.slidesToShow).addClass("slick-center")),r.$slides.eq(t).addClass("slick-center")):t>=0&&t<=r.slideCount-r.options.slidesToShow?r.$slides.slice(t,t+r.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false"):i.length<=r.options.slidesToShow?i.addClass("slick-active").attr("aria-hidden","false"):(o=r.slideCount%r.options.slidesToShow,n=r.options.infinite===!0?r.options.slidesToShow+t:t,r.options.slidesToShow==r.options.slidesToScroll&&r.slideCount-t<r.options.slidesToShow?i.slice(n-(r.options.slidesToShow-o),n+o).addClass("slick-active").attr("aria-hidden","false"):i.slice(n,n+r.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false")),"ondemand"===r.options.lazyLoad&&r.lazyLoad()},e.prototype.setupInfinite=function(){var e,i,n,o=this;if(o.options.fade===!0&&(o.options.centerMode=!1),o.options.infinite===!0&&o.options.fade===!1&&(i=null,o.slideCount>o.options.slidesToShow)){for(n=o.options.centerMode===!0?o.options.slidesToShow+1:o.options.slidesToShow,e=o.slideCount;e>o.slideCount-n;e-=1)i=e-1,t(o.$slides[i]).clone(!0).attr("id","").attr("data-slick-index",i-o.slideCount).prependTo(o.$slideTrack).addClass("slick-cloned");for(e=0;n>e;e+=1)i=e,t(o.$slides[i]).clone(!0).attr("id","").attr("data-slick-index",i+o.slideCount).appendTo(o.$slideTrack).addClass("slick-cloned");o.$slideTrack.find(".slick-cloned").find("[id]").each(function(){t(this).attr("id","")})}},e.prototype.interrupt=function(t){var e=this;t||e.autoPlay(),e.interrupted=t},e.prototype.selectHandler=function(e){var i=this,n=t(e.target).is(".slick-slide")?t(e.target):t(e.target).parents(".slick-slide"),o=parseInt(n.attr("data-slick-index"));return o||(o=0),i.slideCount<=i.options.slidesToShow?(i.setSlideClasses(o),void i.asNavFor(o)):void i.slideHandler(o)},e.prototype.slideHandler=function(t,e,i){var n,o,r,a,s,l=null,d=this;return e=e||!1,d.animating===!0&&d.options.waitForAnimate===!0||d.options.fade===!0&&d.currentSlide===t||d.slideCount<=d.options.slidesToShow?void 0:(e===!1&&d.asNavFor(t),n=t,l=d.getLeft(n),a=d.getLeft(d.currentSlide),d.currentLeft=null===d.swipeLeft?a:d.swipeLeft,d.options.infinite===!1&&d.options.centerMode===!1&&(0>t||t>d.getDotCount()*d.options.slidesToScroll)?void(d.options.fade===!1&&(n=d.currentSlide,i!==!0?d.animateSlide(a,function(){d.postSlide(n)}):d.postSlide(n))):d.options.infinite===!1&&d.options.centerMode===!0&&(0>t||t>d.slideCount-d.options.slidesToScroll)?void(d.options.fade===!1&&(n=d.currentSlide,i!==!0?d.animateSlide(a,function(){d.postSlide(n)}):d.postSlide(n))):(d.options.autoplay&&clearInterval(d.autoPlayTimer),o=0>n?d.slideCount%d.options.slidesToScroll!==0?d.slideCount-d.slideCount%d.options.slidesToScroll:d.slideCount+n:n>=d.slideCount?d.slideCount%d.options.slidesToScroll!==0?0:n-d.slideCount:n,d.animating=!0,d.$slider.trigger("beforeChange",[d,d.currentSlide,o]),r=d.currentSlide,d.currentSlide=o,d.setSlideClasses(d.currentSlide),d.options.asNavFor&&(s=d.getNavTarget(),s=s.slick("getSlick"),s.slideCount<=s.options.slidesToShow&&s.setSlideClasses(d.currentSlide)),d.updateDots(),d.updateArrows(),d.options.fade===!0?(i!==!0?(d.fadeSlideOut(r),d.fadeSlide(o,function(){d.postSlide(o)})):d.postSlide(o),void d.animateHeight()):void(i!==!0?d.animateSlide(l,function(){d.postSlide(o)}):d.postSlide(o))))},e.prototype.startLoad=function(){var t=this;t.options.arrows===!0&&t.slideCount>t.options.slidesToShow&&(t.$prevArrow.hide(),t.$nextArrow.hide()),t.options.dots===!0&&t.slideCount>t.options.slidesToShow&&t.$dots.hide(),t.$slider.addClass("slick-loading")},e.prototype.swipeDirection=function(){var t,e,i,n,o=this;return t=o.touchObject.startX-o.touchObject.curX,e=o.touchObject.startY-o.touchObject.curY,i=Math.atan2(e,t),n=Math.round(180*i/Math.PI),0>n&&(n=360-Math.abs(n)),45>=n&&n>=0?o.options.rtl===!1?"left":"right":360>=n&&n>=315?o.options.rtl===!1?"left":"right":n>=135&&225>=n?o.options.rtl===!1?"right":"left":o.options.verticalSwiping===!0?n>=35&&135>=n?"down":"up":"vertical"},e.prototype.swipeEnd=function(t){var e,i,n=this;if(n.dragging=!1,n.interrupted=!1,n.shouldClick=!(n.touchObject.swipeLength>10),void 0===n.touchObject.curX)return!1;if(n.touchObject.edgeHit===!0&&n.$slider.trigger("edge",[n,n.swipeDirection()]),n.touchObject.swipeLength>=n.touchObject.minSwipe){switch(i=n.swipeDirection()){case"left":case"down":e=n.options.swipeToSlide?n.checkNavigable(n.currentSlide+n.getSlideCount()):n.currentSlide+n.getSlideCount(),n.currentDirection=0;break;case"right":case"up":e=n.options.swipeToSlide?n.checkNavigable(n.currentSlide-n.getSlideCount()):n.currentSlide-n.getSlideCount(),n.currentDirection=1}"vertical"!=i&&(n.slideHandler(e),n.touchObject={},n.$slider.trigger("swipe",[n,i]))}else n.touchObject.startX!==n.touchObject.curX&&(n.slideHandler(n.currentSlide),n.touchObject={})},e.prototype.swipeHandler=function(t){var e=this;if(!(e.options.swipe===!1||"ontouchend"in document&&e.options.swipe===!1||e.options.draggable===!1&&-1!==t.type.indexOf("mouse")))switch(e.touchObject.fingerCount=t.originalEvent&&void 0!==t.originalEvent.touches?t.originalEvent.touches.length:1,e.touchObject.minSwipe=e.listWidth/e.options.touchThreshold,e.options.verticalSwiping===!0&&(e.touchObject.minSwipe=e.listHeight/e.options.touchThreshold),t.data.action){case"start":e.swipeStart(t);break;case"move":e.swipeMove(t);break;case"end":e.swipeEnd(t)}},e.prototype.swipeMove=function(t){var e,i,n,o,r,a=this;return r=void 0!==t.originalEvent?t.originalEvent.touches:null,!(!a.dragging||r&&1!==r.length)&&(e=a.getLeft(a.currentSlide),a.touchObject.curX=void 0!==r?r[0].pageX:t.clientX,a.touchObject.curY=void 0!==r?r[0].pageY:t.clientY,a.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(a.touchObject.curX-a.touchObject.startX,2))),a.options.verticalSwiping===!0&&(a.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(a.touchObject.curY-a.touchObject.startY,2)))),i=a.swipeDirection(),"vertical"!==i?(void 0!==t.originalEvent&&a.touchObject.swipeLength>4&&t.preventDefault(),o=(a.options.rtl===!1?1:-1)*(a.touchObject.curX>a.touchObject.startX?1:-1),a.options.verticalSwiping===!0&&(o=a.touchObject.curY>a.touchObject.startY?1:-1),n=a.touchObject.swipeLength,a.touchObject.edgeHit=!1,a.options.infinite===!1&&(0===a.currentSlide&&"right"===i||a.currentSlide>=a.getDotCount()&&"left"===i)&&(n=a.touchObject.swipeLength*a.options.edgeFriction,a.touchObject.edgeHit=!0),a.options.vertical===!1?a.swipeLeft=e+n*o:a.swipeLeft=e+n*(a.$list.height()/a.listWidth)*o,a.options.verticalSwiping===!0&&(a.swipeLeft=e+n*o),a.options.fade!==!0&&a.options.touchMove!==!1&&(a.animating===!0?(a.swipeLeft=null,!1):void a.setCSS(a.swipeLeft))):void 0)},e.prototype.swipeStart=function(t){var e,i=this;return i.interrupted=!0,1!==i.touchObject.fingerCount||i.slideCount<=i.options.slidesToShow?(i.touchObject={},!1):(void 0!==t.originalEvent&&void 0!==t.originalEvent.touches&&(e=t.originalEvent.touches[0]),i.touchObject.startX=i.touchObject.curX=void 0!==e?e.pageX:t.clientX,i.touchObject.startY=i.touchObject.curY=void 0!==e?e.pageY:t.clientY,void(i.dragging=!0))},e.prototype.unfilterSlides=e.prototype.slickUnfilter=function(){var t=this;null!==t.$slidesCache&&(t.unload(),t.$slideTrack.children(this.options.slide).detach(),t.$slidesCache.appendTo(t.$slideTrack),t.reinit())},e.prototype.unload=function(){var e=this;t(".slick-cloned",e.$slider).remove(),e.$dots&&e.$dots.remove(),e.$prevArrow&&e.htmlExpr.test(e.options.prevArrow)&&e.$prevArrow.remove(),e.$nextArrow&&e.htmlExpr.test(e.options.nextArrow)&&e.$nextArrow.remove(),e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden","true").css("width","")},e.prototype.unslick=function(t){var e=this;e.$slider.trigger("unslick",[e,t]),e.destroy()},e.prototype.updateArrows=function(){var t,e=this;t=Math.floor(e.options.slidesToShow/2),e.options.arrows===!0&&e.slideCount>e.options.slidesToShow&&!e.options.infinite&&(e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false"),e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false"),0===e.currentSlide?(e.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true"),e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false")):e.currentSlide>=e.slideCount-e.options.slidesToShow&&e.options.centerMode===!1?(e.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")):e.currentSlide>=e.slideCount-1&&e.options.centerMode===!0&&(e.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")))},e.prototype.updateDots=function(){var t=this;null!==t.$dots&&(t.$dots.find("li").removeClass("slick-active").attr("aria-hidden","true"),t.$dots.find("li").eq(Math.floor(t.currentSlide/t.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden","false"))},e.prototype.visibility=function(){var t=this;t.options.autoplay&&(document[t.hidden]?t.interrupted=!0:t.interrupted=!1)},t.fn.slick=function(){var t,i,n=this,o=arguments[0],r=Array.prototype.slice.call(arguments,1),a=n.length;for(t=0;a>t;t++)if("object"==typeof o||"undefined"==typeof o?n[t].slick=new e(n[t],o):i=n[t].slick[o].apply(n[t].slick,r),"undefined"!=typeof i)return i;return n}}),!function(t,e){"function"==typeof define&&define.amd?define([],function(){return t.svg4everybody=e()}):"object"==typeof exports?module.exports=e():t.svg4everybody=e()}(this,function(){function t(t,e){if(e){var i=document.createDocumentFragment(),n=!t.getAttribute("viewBox")&&e.getAttribute("viewBox");n&&t.setAttribute("viewBox",n);for(var o=e.cloneNode(!0);o.childNodes.length;)i.appendChild(o.firstChild);t.appendChild(i)}}function e(e){e.onreadystatechange=function(){if(4===e.readyState){var i=e._cachedDocument;i||(i=e._cachedDocument=document.implementation.createHTMLDocument(""),i.body.innerHTML=e.responseText,e._cachedTarget={}),e._embeds.splice(0).map(function(n){var o=e._cachedTarget[n.id];o||(o=e._cachedTarget[n.id]=i.getElementById(n.id)),t(n.svg,o)})}},e.onreadystatechange()}function i(i){function n(){for(var i=0;i<u.length;){var a=u[i],s=a.parentNode;if(s&&/svg/i.test(s.nodeName)){var l=a.getAttribute("xlink:href");if(o&&(!r.validate||r.validate(l,s,a))){s.removeChild(a);var h=l.split("#"),p=h.shift(),f=h.join("#");if(p.length){var g=d[p];g||(g=d[p]=new XMLHttpRequest,g.open("GET",p),g.send(),g._embeds=[]),g._embeds.push({svg:s,id:f}),e(g)}else t(s,document.getElementById(f))}}else++i}c(n,67)}var o,r=Object(i),a=/\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/,s=/\bAppleWebKit\/(\d+)\b/,l=/\bEdge\/12\.(\d+)\b/;o="polyfill"in r?r.polyfill:a.test(navigator.userAgent)||(navigator.userAgent.match(l)||[])[1]<10547||(navigator.userAgent.match(s)||[])[1]<537;var d={},c=window.requestAnimationFrame||setTimeout,u=document.getElementsByTagName("use");o&&n()}return i}),!function(t){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=t();else if("function"==typeof define&&define.amd)define([],t);else{var e;"undefined"!=typeof window?e=window:"undefined"!=typeof global?e=global:"undefined"!=typeof self&&(e=self),e.Countdown=t()}}(function(){var t,e,i;return function t(e,i,n){function o(a,s){if(!i[a]){if(!e[a]){var l="function"==typeof require&&require;if(!s&&l)return l(a,!0);if(r)return r(a,!0);var d=new Error("Cannot find module '"+a+"'");throw d.code="MODULE_NOT_FOUND",d}var c=i[a]={exports:{}};e[a][0].call(c.exports,function(t){var i=e[a][1][t];return o(i?i:t)},c,c.exports,t,e,i,n)}return i[a].exports}for(var r="function"==typeof require&&require,a=0;a<n.length;a++)o(n[a]);return o}({1:[function(t,e,i){var n={date:"June 7, 2087 15:03:25",refresh:1e3,offset:0,onEnd:function(){},render:function(t){this.el.innerHTML=t.years+" years, "+t.days+" days, "+this.leadingZeros(t.hours)+" hours, "+this.leadingZeros(t.min)+" min and "+this.leadingZeros(t.sec)+" sec"}},o=function(t,e){this.el=t,this.options={},this.interval=!1,this.mergeOptions=function(t){for(var e in n)n.hasOwnProperty(e)&&(this.options[e]="undefined"!=typeof t[e]?t[e]:n[e],"date"===e&&"object"!=typeof this.options.date&&(this.options.date=new Date(this.options.date)),"function"==typeof this.options[e]&&(this.options[e]=this.options[e].bind(this)));"object"!=typeof this.options.date&&(this.options.date=new Date(this.options.date))}.bind(this),this.mergeOptions(e),this.getDiffDate=function(){var t=(this.options.date.getTime()-Date.now()+this.options.offset)/1e3,e={years:0,days:0,hours:0,min:0,sec:0,millisec:0};return t<=0?(this.interval&&(this.stop(),this.options.onEnd()),e):(t>=31557600&&(e.years=Math.floor(t/31557600),t-=365.25*e.years*86400),t>=86400&&(e.days=Math.floor(t/86400),t-=86400*e.days),t>=3600&&(e.hours=Math.floor(t/3600),t-=3600*e.hours),t>=60&&(e.min=Math.floor(t/60),t-=60*e.min),e.sec=Math.round(t),e.millisec=t%1*1e3,e)}.bind(this),this.leadingZeros=function(t,e){return e=e||2,t=String(t),t.length>e?t:(Array(e+1).join("0")+t).substr(-e)},this.update=function(t){return"object"!=typeof t&&(t=new Date(t)),this.options.date=t,this.render(),this}.bind(this),this.stop=function(){return this.interval&&(clearInterval(this.interval),this.interval=!1),this}.bind(this),this.render=function(){return this.options.render(this.getDiffDate()),this}.bind(this),this.start=function(){if(!this.interval)return this.render(),this.options.refresh&&(this.interval=setInterval(this.render,this.options.refresh)),this}.bind(this),this.updateOffset=function(t){return this.options.offset=t,this}.bind(this),this.restart=function(t){return this.mergeOptions(t),this.interval=!1,this.start(),this}.bind(this),this.start()};e.exports=o},{}],2:[function(t,e,i){var n=t("./countdown.js"),o="countdown",r="date";jQuery.fn.countdown=function(t){return $.each(this,function(e,i){var a=$(i);a.data(o)||(a.data(r)&&(t.date=a.data(r)),a.data(o,new n(i,t)))})},e.exports=n},{"./countdown.js":1}]},{},[2])(2)}),/**!
 * easy-pie-chart
 * Lightweight plugin to render simple, animated and retina optimized pie charts
 *
 * @license 
 * @author Robert Fleischmann <rendro87@gmail.com> (http://robert-fleischmann.de)
 * @version 2.1.7
 **/
function(t,e){"function"==typeof define&&define.amd?define(["jquery"],function(t){return e(t)}):"object"==typeof exports?module.exports=e(require("jquery")):e(jQuery)}(this,function($){var t=function(t,e){var i,n=document.createElement("canvas");t.appendChild(n),"object"==typeof G_vmlCanvasManager&&G_vmlCanvasManager.initElement(n);var o=n.getContext("2d");n.width=n.height=e.size;var r=1;window.devicePixelRatio>1&&(r=window.devicePixelRatio,n.style.width=n.style.height=[e.size,"px"].join(""),n.width=n.height=e.size*r,o.scale(r,r)),o.translate(e.size/2,e.size/2),o.rotate((-.5+e.rotate/180)*Math.PI);var a=(e.size-e.lineWidth)/2;e.scaleColor&&e.scaleLength&&(a-=e.scaleLength+2),Date.now=Date.now||function(){return+new Date};var s=function(t,e,i){i=Math.min(Math.max(-1,i||0),1);var n=i<=0;o.beginPath(),o.arc(0,0,a,0,2*Math.PI*i,n),o.strokeStyle=t,o.lineWidth=e,o.stroke()},l=function(){var t,i;o.lineWidth=1,o.fillStyle=e.scaleColor,o.save();for(var n=24;n>0;--n)n%6===0?(i=e.scaleLength,t=0):(i=.6*e.scaleLength,t=e.scaleLength-i),o.fillRect(-e.size/2+t,0,i,1),o.rotate(Math.PI/12);o.restore()},d=function(){return window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||function(t){window.setTimeout(t,1e3/60)}}(),c=function(){e.scaleColor&&l(),e.trackColor&&s(e.trackColor,e.trackWidth||e.lineWidth,1)};this.getCanvas=function(){return n},this.getCtx=function(){return o},this.clear=function(){o.clearRect(e.size/-2,e.size/-2,e.size,e.size)},this.draw=function(t){e.scaleColor||e.trackColor?o.getImageData&&o.putImageData?i?o.putImageData(i,0,0):(c(),i=o.getImageData(0,0,e.size*r,e.size*r)):(this.clear(),c()):this.clear(),o.lineCap=e.lineCap;var n;n="function"==typeof e.barColor?e.barColor(t):e.barColor,s(n,e.lineWidth,t/100)}.bind(this),this.animate=function(t,i){var n=Date.now();e.onStart(t,i);var o=function(){var r=Math.min(Date.now()-n,e.animate.duration),a=e.easing(this,r,t,i-t,e.animate.duration);this.draw(a),e.onStep(t,i,a),r>=e.animate.duration?e.onStop(t,i):d(o)}.bind(this);d(o)}.bind(this)},e=function(e,i){var n={barColor:"#ef1e25",trackColor:"#f9f9f9",scaleColor:"#dfe0e0",scaleLength:5,lineCap:"round",lineWidth:3,trackWidth:void 0,size:110,rotate:0,animate:{duration:1e3,enabled:!0},easing:function(t,e,i,n,o){return e/=o/2,e<1?n/2*e*e+i:-n/2*(--e*(e-2)-1)+i},onStart:function(t,e){},onStep:function(t,e,i){},onStop:function(t,e){}};if("undefined"!=typeof t)n.renderer=t;else{if("undefined"==typeof SVGRenderer)throw new Error("Please load either the SVG- or the CanvasRenderer");n.renderer=SVGRenderer}var o={},r=0,a=function(){this.el=e,this.options=o;for(var t in n)n.hasOwnProperty(t)&&(o[t]=i&&"undefined"!=typeof i[t]?i[t]:n[t],"function"==typeof o[t]&&(o[t]=o[t].bind(this)));"string"==typeof o.easing&&"undefined"!=typeof jQuery&&jQuery.isFunction(jQuery.easing[o.easing])?o.easing=jQuery.easing[o.easing]:o.easing=n.easing,"number"==typeof o.animate&&(o.animate={duration:o.animate,enabled:!0}),"boolean"!=typeof o.animate||o.animate||(o.animate={duration:1e3,enabled:o.animate}),this.renderer=new o.renderer(e,o),this.renderer.draw(r),e.dataset&&e.dataset.percent?this.update(parseFloat(e.dataset.percent)):e.getAttribute&&e.getAttribute("data-percent")&&this.update(parseFloat(e.getAttribute("data-percent")))}.bind(this);this.update=function(t){return t=parseFloat(t),o.animate.enabled?this.renderer.animate(r,t):this.renderer.draw(t),r=t,this}.bind(this),this.disableAnimation=function(){return o.animate.enabled=!1,this},this.enableAnimation=function(){return o.animate.enabled=!0,this},a()};$.fn.easyPieChart=function(t){return this.each(function(){var i;$.data(this,"easyPieChart")||(i=$.extend({},t,$(this).data()),$.data(this,"easyPieChart",new e(this,i)))})}}),function(t){"function"==typeof define&&define.amd?define(["jquery"],t):t("object"==typeof exports?require("jquery"):window.jQuery||window.Zepto)}(function($){var t="Close",e="BeforeClose",i="AfterClose",n="BeforeAppend",o="MarkupParse",r="Open",a="Change",s="mfp",l="."+s,d="mfp-ready",c="mfp-removing",u="mfp-prevent-close",h,p=function(){},f=!!window.jQuery,g,m=$(window),v,y,b,x,w=function(t,e){h.ev.on(s+t+l,e)},k=function(t,e,i,n){var o=document.createElement("div");return o.className="mfp-"+t,i&&(o.innerHTML=i),n?e&&e.appendChild(o):(o=$(o),e&&o.appendTo(e)),o},S=function(t,e){h.ev.triggerHandler(s+t,e),h.st.callbacks&&(t=t.charAt(0).toLowerCase()+t.slice(1),h.st.callbacks[t]&&h.st.callbacks[t].apply(h,$.isArray(e)?e:[e]))},C=function(t){return t===x&&h.currTemplate.closeBtn||(h.currTemplate.closeBtn=$(h.st.closeMarkup.replace("%title%",h.st.tClose)),x=t),h.currTemplate.closeBtn},T=function(){$.magnificPopup.instance||(h=new p,h.init(),$.magnificPopup.instance=h)},I=function(){var t=document.createElement("p").style,e=["ms","O","Moz","Webkit"];if(void 0!==t.transition)return!0;for(;e.length;)if(e.pop()+"Transition"in t)return!0;return!1};p.prototype={constructor:p,init:function(){var t=navigator.appVersion;h.isLowIE=h.isIE8=document.all&&!document.addEventListener,h.isAndroid=/android/gi.test(t),h.isIOS=/iphone|ipad|ipod/gi.test(t),h.supportsTransition=I(),h.probablyMobile=h.isAndroid||h.isIOS||/(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent),v=$(document),h.popupsCache={}},open:function(t){var e;if(t.isObj===!1){h.items=t.items.toArray(),h.index=0;var i=t.items,n;for(e=0;e<i.length;e++)if(n=i[e],n.parsed&&(n=n.el[0]),n===t.el[0]){h.index=e;break}}else h.items=$.isArray(t.items)?t.items:[t.items],h.index=t.index||0;if(h.isOpen)return void h.updateItemHTML();h.types=[],b="",t.mainEl&&t.mainEl.length?h.ev=t.mainEl.eq(0):h.ev=v,t.key?(h.popupsCache[t.key]||(h.popupsCache[t.key]={}),h.currTemplate=h.popupsCache[t.key]):h.currTemplate={},h.st=$.extend(!0,{},$.magnificPopup.defaults,t),h.fixedContentPos="auto"===h.st.fixedContentPos?!h.probablyMobile:h.st.fixedContentPos,h.st.modal&&(h.st.closeOnContentClick=!1,h.st.closeOnBgClick=!1,h.st.showCloseBtn=!1,h.st.enableEscapeKey=!1),h.bgOverlay||(h.bgOverlay=k("bg").on("click"+l,function(){h.close()}),h.wrap=k("wrap").attr("tabindex",-1).on("click"+l,function(t){h._checkIfClose(t.target)&&h.close()}),h.container=k("container",h.wrap)),h.contentContainer=k("content"),h.st.preloader&&(h.preloader=k("preloader",h.container,h.st.tLoading));var a=$.magnificPopup.modules;for(e=0;e<a.length;e++){var s=a[e];s=s.charAt(0).toUpperCase()+s.slice(1),h["init"+s].call(h)}S("BeforeOpen"),h.st.showCloseBtn&&(h.st.closeBtnInside?(w(o,function(t,e,i,n){i.close_replaceWith=C(n.type)}),b+=" mfp-close-btn-in"):h.wrap.append(C())),h.st.alignTop&&(b+=" mfp-align-top"),h.fixedContentPos?h.wrap.css({overflow:h.st.overflowY,overflowX:"hidden",overflowY:h.st.overflowY}):h.wrap.css({top:m.scrollTop(),position:"absolute"}),(h.st.fixedBgPos===!1||"auto"===h.st.fixedBgPos&&!h.fixedContentPos)&&h.bgOverlay.css({height:v.height(),position:"absolute"}),h.st.enableEscapeKey&&v.on("keyup"+l,function(t){27===t.keyCode&&h.close()}),m.on("resize"+l,function(){h.updateSize()}),h.st.closeOnContentClick||(b+=" mfp-auto-cursor"),b&&h.wrap.addClass(b);var c=h.wH=m.height(),u={};if(h.fixedContentPos&&h._hasScrollBar(c)){var p=h._getScrollbarSize();p&&(u.marginRight=p)}h.fixedContentPos&&(h.isIE7?$("body, html").css("overflow","hidden"):u.overflow="hidden");var f=h.st.mainClass;return h.isIE7&&(f+=" mfp-ie7"),f&&h._addClassToMFP(f),h.updateItemHTML(),S("BuildControls"),$("html").css(u),h.bgOverlay.add(h.wrap).prependTo(h.st.prependTo||$(document.body)),h._lastFocusedEl=document.activeElement,setTimeout(function(){h.content?(h._addClassToMFP(d),h._setFocus()):h.bgOverlay.addClass(d),v.on("focusin"+l,h._onFocusIn)},16),h.isOpen=!0,h.updateSize(c),S(r),t},close:function(){h.isOpen&&(S(e),h.isOpen=!1,h.st.removalDelay&&!h.isLowIE&&h.supportsTransition?(h._addClassToMFP(c),setTimeout(function(){h._close()},h.st.removalDelay)):h._close())},_close:function(){S(t);var e=c+" "+d+" ";if(h.bgOverlay.detach(),h.wrap.detach(),h.container.empty(),h.st.mainClass&&(e+=h.st.mainClass+" "),h._removeClassFromMFP(e),h.fixedContentPos){var n={marginRight:""};h.isIE7?$("body, html").css("overflow",""):n.overflow="",$("html").css(n)}v.off("keyup"+l+" focusin"+l),h.ev.off(l),h.wrap.attr("class","mfp-wrap").removeAttr("style"),h.bgOverlay.attr("class","mfp-bg"),h.container.attr("class","mfp-container"),!h.st.showCloseBtn||h.st.closeBtnInside&&h.currTemplate[h.currItem.type]!==!0||h.currTemplate.closeBtn&&h.currTemplate.closeBtn.detach(),h.st.autoFocusLast&&h._lastFocusedEl&&$(h._lastFocusedEl).focus(),h.currItem=null,h.content=null,h.currTemplate=null,h.prevHeight=0,S(i)},updateSize:function(t){if(h.isIOS){var e=document.documentElement.clientWidth/window.innerWidth,i=window.innerHeight*e;h.wrap.css("height",i),h.wH=i}else h.wH=t||m.height();h.fixedContentPos||h.wrap.css("height",h.wH),S("Resize")},updateItemHTML:function(){var t=h.items[h.index];h.contentContainer.detach(),h.content&&h.content.detach(),t.parsed||(t=h.parseEl(h.index));var e=t.type;if(S("BeforeChange",[h.currItem?h.currItem.type:"",e]),h.currItem=t,!h.currTemplate[e]){var i=!!h.st[e]&&h.st[e].markup;S("FirstMarkupParse",i),i?h.currTemplate[e]=$(i):h.currTemplate[e]=!0}y&&y!==t.type&&h.container.removeClass("mfp-"+y+"-holder");var n=h["get"+e.charAt(0).toUpperCase()+e.slice(1)](t,h.currTemplate[e]);h.appendContent(n,e),t.preloaded=!0,S(a,t),y=t.type,h.container.prepend(h.contentContainer),S("AfterChange")},appendContent:function(t,e){h.content=t,t?h.st.showCloseBtn&&h.st.closeBtnInside&&h.currTemplate[e]===!0?h.content.find(".mfp-close").length||h.content.append(C()):h.content=t:h.content="",S(n),h.container.addClass("mfp-"+e+"-holder"),h.contentContainer.append(h.content)},parseEl:function(t){var e=h.items[t],i;if(e.tagName?e={el:$(e)}:(i=e.type,e={data:e,src:e.src}),e.el){for(var n=h.types,o=0;o<n.length;o++)if(e.el.hasClass("mfp-"+n[o])){i=n[o];break}e.src=e.el.attr("data-mfp-src"),e.src||(e.src=e.el.attr("href"))}return e.type=i||h.st.type||"inline",e.index=t,e.parsed=!0,h.items[t]=e,S("ElementParse",e),h.items[t]},addGroup:function(t,e){var i=function(i){i.mfpEl=this,h._openClick(i,t,e)};e||(e={});var n="click.magnificPopup";e.mainEl=t,e.items?(e.isObj=!0,t.off(n).on(n,i)):(e.isObj=!1,e.delegate?t.off(n).on(n,e.delegate,i):(e.items=t,t.off(n).on(n,i)))},_openClick:function(t,e,i){var n=void 0!==i.midClick?i.midClick:$.magnificPopup.defaults.midClick;if(n||!(2===t.which||t.ctrlKey||t.metaKey||t.altKey||t.shiftKey)){var o=void 0!==i.disableOn?i.disableOn:$.magnificPopup.defaults.disableOn;if(o)if($.isFunction(o)){if(!o.call(h))return!0}else if(m.width()<o)return!0;t.type&&(t.preventDefault(),h.isOpen&&t.stopPropagation()),i.el=$(t.mfpEl),i.delegate&&(i.items=e.find(i.delegate)),h.open(i)}},updateStatus:function(t,e){if(h.preloader){g!==t&&h.container.removeClass("mfp-s-"+g),e||"loading"!==t||(e=h.st.tLoading);var i={status:t,text:e};S("UpdateStatus",i),t=i.status,e=i.text,h.preloader.html(e),h.preloader.find("a").on("click",function(t){t.stopImmediatePropagation()}),h.container.addClass("mfp-s-"+t),g=t}},_checkIfClose:function(t){if(!$(t).hasClass(u)){var e=h.st.closeOnContentClick,i=h.st.closeOnBgClick;if(e&&i)return!0;if(!h.content||$(t).hasClass("mfp-close")||h.preloader&&t===h.preloader[0])return!0;if(t===h.content[0]||$.contains(h.content[0],t)){if(e)return!0}else if(i&&$.contains(document,t))return!0;return!1}},_addClassToMFP:function(t){h.bgOverlay.addClass(t),h.wrap.addClass(t)},_removeClassFromMFP:function(t){this.bgOverlay.removeClass(t),h.wrap.removeClass(t)},_hasScrollBar:function(t){return(h.isIE7?v.height():document.body.scrollHeight)>(t||m.height())},_setFocus:function(){(h.st.focus?h.content.find(h.st.focus).eq(0):h.wrap).focus()},_onFocusIn:function(t){if(t.target!==h.wrap[0]&&!$.contains(h.wrap[0],t.target))return h._setFocus(),!1},_parseMarkup:function(t,e,i){var n;i.data&&(e=$.extend(i.data,e)),S(o,[t,e,i]),$.each(e,function(e,i){if(void 0===i||i===!1)return!0;if(n=e.split("_"),n.length>1){var o=t.find(l+"-"+n[0]);if(o.length>0){var r=n[1];"replaceWith"===r?o[0]!==i[0]&&o.replaceWith(i):"img"===r?o.is("img")?o.attr("src",i):o.replaceWith($("<img>").attr("src",i).attr("class",o.attr("class"))):o.attr(n[1],i)}}else t.find(l+"-"+e).html(i)})},_getScrollbarSize:function(){if(void 0===h.scrollbarSize){var t=document.createElement("div");t.style.cssText="width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;",document.body.appendChild(t),h.scrollbarSize=t.offsetWidth-t.clientWidth,document.body.removeChild(t)}return h.scrollbarSize}},$.magnificPopup={instance:null,proto:p.prototype,modules:[],open:function(t,e){return T(),t=t?$.extend(!0,{},t):{},t.isObj=!0,t.index=e||0,this.instance.open(t)},close:function(){return $.magnificPopup.instance&&$.magnificPopup.instance.close()},registerModule:function(t,e){e.options&&($.magnificPopup.defaults[t]=e.options),$.extend(this.proto,e.proto),this.modules.push(t)},defaults:{disableOn:0,key:null,midClick:!1,mainClass:"",preloader:!0,focus:"",closeOnContentClick:!1,closeOnBgClick:!0,closeBtnInside:!0,showCloseBtn:!0,enableEscapeKey:!0,modal:!1,alignTop:!1,removalDelay:0,prependTo:null,fixedContentPos:"auto",fixedBgPos:"auto",overflowY:"auto",closeMarkup:'<button title="%title%" type="button" class="mfp-close">&#215;</button>',tClose:"Close (Esc)",tLoading:"Loading...",autoFocusLast:!0}},$.fn.magnificPopup=function(t){T();var e=$(this);if("string"==typeof t)if("open"===t){var i,n=f?e.data("magnificPopup"):e[0].magnificPopup,o=parseInt(arguments[1],10)||0;n.items?i=n.items[o]:(i=e,n.delegate&&(i=i.find(n.delegate)),i=i.eq(o)),h._openClick({mfpEl:i},e,n)}else h.isOpen&&h[t].apply(h,Array.prototype.slice.call(arguments,1));else t=$.extend(!0,{},t),f?e.data("magnificPopup",t):e[0].magnificPopup=t,h.addGroup(e,t);return e};var M="inline",A,_,D,P=function(){D&&(_.after(D.addClass(A)).detach(),D=null)};$.magnificPopup.registerModule(M,{options:{hiddenClass:"hide",markup:"",tNotFound:"Content not found"},proto:{initInline:function(){h.types.push(M),w(t+"."+M,function(){P()})},getInline:function(t,e){if(P(),t.src){var i=h.st.inline,n=$(t.src);if(n.length){var o=n[0].parentNode;o&&o.tagName&&(_||(A=i.hiddenClass,_=k(A),A="mfp-"+A),D=n.after(_).detach().removeClass(A)),h.updateStatus("ready")}else h.updateStatus("error",i.tNotFound),n=$("<div>");return t.inlineElement=n,n}return h.updateStatus("ready"),h._parseMarkup(e,{},t),e}}});var E="ajax",O,L=function(){O&&$(document.body).removeClass(O)},z=function(){L(),h.req&&h.req.abort()};$.magnificPopup.registerModule(E,{options:{settings:null,cursor:"mfp-ajax-cur",tError:'<a href="%url%">The content</a> could not be loaded.'},proto:{initAjax:function(){h.types.push(E),O=h.st.ajax.cursor,w(t+"."+E,z),w("BeforeChange."+E,z)},getAjax:function(t){O&&$(document.body).addClass(O),h.updateStatus("loading");var e=$.extend({url:t.src,success:function(e,i,n){var o={data:e,xhr:n};S("ParseAjax",o),h.appendContent($(o.data),E),t.finished=!0,L(),h._setFocus(),setTimeout(function(){h.wrap.addClass(d)},16),h.updateStatus("ready"),S("AjaxContentAdded")},error:function(){L(),t.finished=t.loadError=!0,h.updateStatus("error",h.st.ajax.tError.replace("%url%",t.src))}},h.st.ajax.settings);return h.req=$.ajax(e),""}}});var F,R=function(t){if(t.data&&void 0!==t.data.title)return t.data.title;var e=h.st.image.titleSrc;if(e){if($.isFunction(e))return e.call(h,t);if(t.el)return t.el.attr(e)||""}return""};$.magnificPopup.registerModule("image",{options:{markup:'<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',cursor:"mfp-zoom-out-cur",titleSrc:"title",verticalFit:!0,tError:'<a href="%url%">The image</a> could not be loaded.'},proto:{initImage:function(){var e=h.st.image,i=".image";h.types.push("image"),w(r+i,function(){"image"===h.currItem.type&&e.cursor&&$(document.body).addClass(e.cursor)}),w(t+i,function(){e.cursor&&$(document.body).removeClass(e.cursor),m.off("resize"+l)}),w("Resize"+i,h.resizeImage),h.isLowIE&&w("AfterChange",h.resizeImage)},resizeImage:function(){var t=h.currItem;if(t&&t.img&&h.st.image.verticalFit){var e=0;h.isLowIE&&(e=parseInt(t.img.css("padding-top"),10)+parseInt(t.img.css("padding-bottom"),10)),t.img.css("max-height",h.wH-e)}},_onImageHasSize:function(t){t.img&&(t.hasSize=!0,F&&clearInterval(F),t.isCheckingImgSize=!1,S("ImageHasSize",t),t.imgHidden&&(h.content&&h.content.removeClass("mfp-loading"),t.imgHidden=!1))},findImageSize:function(t){var e=0,i=t.img[0],n=function(o){F&&clearInterval(F),F=setInterval(function(){return i.naturalWidth>0?void h._onImageHasSize(t):(e>200&&clearInterval(F),e++,void(3===e?n(10):40===e?n(50):100===e&&n(500)))},o)};n(1)},getImage:function(t,e){var i=0,n=function(){t&&(t.img[0].complete?(t.img.off(".mfploader"),t===h.currItem&&(h._onImageHasSize(t),h.updateStatus("ready")),t.hasSize=!0,t.loaded=!0,S("ImageLoadComplete")):(i++,i<200?setTimeout(n,100):o()))},o=function(){t&&(t.img.off(".mfploader"),t===h.currItem&&(h._onImageHasSize(t),h.updateStatus("error",r.tError.replace("%url%",t.src))),t.hasSize=!0,t.loaded=!0,t.loadError=!0)},r=h.st.image,a=e.find(".mfp-img");if(a.length){var s=document.createElement("img");s.className="mfp-img",t.el&&t.el.find("img").length&&(s.alt=t.el.find("img").attr("alt")),t.img=$(s).on("load.mfploader",n).on("error.mfploader",o),s.src=t.src,a.is("img")&&(t.img=t.img.clone()),s=t.img[0],s.naturalWidth>0?t.hasSize=!0:s.width||(t.hasSize=!1)}return h._parseMarkup(e,{title:R(t),img_replaceWith:t.img},t),h.resizeImage(),t.hasSize?(F&&clearInterval(F),t.loadError?(e.addClass("mfp-loading"),h.updateStatus("error",r.tError.replace("%url%",t.src))):(e.removeClass("mfp-loading"),h.updateStatus("ready")),e):(h.updateStatus("loading"),t.loading=!0,t.hasSize||(t.imgHidden=!0,e.addClass("mfp-loading"),h.findImageSize(t)),e)}}});var N,W=function(){return void 0===N&&(N=void 0!==document.createElement("p").style.MozTransform),N};$.magnificPopup.registerModule("zoom",{options:{enabled:!1,easing:"ease-in-out",duration:300,opener:function(t){return t.is("img")?t:t.find("img")}},proto:{initZoom:function(){var i=h.st.zoom,n=".zoom",o;if(i.enabled&&h.supportsTransition){var r=i.duration,a=function(t){var e=t.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),n="all "+i.duration/1e3+"s "+i.easing,o={position:"fixed",zIndex:9999,left:0,top:0,"-webkit-backface-visibility":"hidden"},r="transition";return o["-webkit-"+r]=o["-moz-"+r]=o["-o-"+r]=o[r]=n,e.css(o),e},s=function(){h.content.css("visibility","visible")},l,d;w("BuildControls"+n,function(){if(h._allowZoom()){if(clearTimeout(l),h.content.css("visibility","hidden"),o=h._getItemToZoom(),!o)return void s();d=a(o),d.css(h._getOffset()),h.wrap.append(d),l=setTimeout(function(){d.css(h._getOffset(!0)),l=setTimeout(function(){s(),setTimeout(function(){d.remove(),o=d=null,S("ZoomAnimationEnded")},16)},r)},16)}}),w(e+n,function(){if(h._allowZoom()){if(clearTimeout(l),h.st.removalDelay=r,!o){if(o=h._getItemToZoom(),!o)return;d=a(o)}d.css(h._getOffset(!0)),h.wrap.append(d),h.content.css("visibility","hidden"),setTimeout(function(){d.css(h._getOffset())},16)}}),w(t+n,function(){h._allowZoom()&&(s(),d&&d.remove(),o=null)})}},_allowZoom:function(){return"image"===h.currItem.type},_getItemToZoom:function(){return!!h.currItem.hasSize&&h.currItem.img},_getOffset:function(t){var e;e=t?h.currItem.img:h.st.zoom.opener(h.currItem.el||h.currItem);var i=e.offset(),n=parseInt(e.css("padding-top"),10),o=parseInt(e.css("padding-bottom"),10);i.top-=$(window).scrollTop()-n;var r={width:e.width(),height:(f?e.innerHeight():e[0].offsetHeight)-o-n};return W()?r["-moz-transform"]=r.transform="translate("+i.left+"px,"+i.top+"px)":(r.left=i.left,r.top=i.top),r}}});var B="iframe",j="//about:blank",V=function(t){if(h.currTemplate[B]){var e=h.currTemplate[B].find("iframe");e.length&&(t||(e[0].src=j),h.isIE8&&e.css("display",t?"block":"none"))}};$.magnificPopup.registerModule(B,{options:{markup:'<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',srcAction:"iframe_src",patterns:{youtube:{index:"youtube.com",id:"v=",src:"//www.youtube.com/embed/%id%?autoplay=1"},vimeo:{index:"vimeo.com/",id:"/",src:"//player.vimeo.com/video/%id%?autoplay=1"},gmaps:{index:"//maps.google.",src:"%id%&output=embed"}}},proto:{initIframe:function(){h.types.push(B),w("BeforeChange",function(t,e,i){e!==i&&(e===B?V():i===B&&V(!0))}),w(t+"."+B,function(){V()})},getIframe:function(t,e){var i=t.src,n=h.st.iframe;$.each(n.patterns,function(){if(i.indexOf(this.index)>-1)return this.id&&(i="string"==typeof this.id?i.substr(i.lastIndexOf(this.id)+this.id.length,i.length):this.id.call(this,i)),i=this.src.replace("%id%",i),!1});var o={};return n.srcAction&&(o[n.srcAction]=i),h._parseMarkup(e,o,t),h.updateStatus("ready"),e}}});var H=function(t){var e=h.items.length;return t>e-1?t-e:t<0?e+t:t},U=function(t,e,i){return t.replace(/%curr%/gi,e+1).replace(/%total%/gi,i)};$.magnificPopup.registerModule("gallery",{options:{enabled:!1,arrowMarkup:'<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',preload:[0,2],navigateByImgClick:!0,arrows:!0,tPrev:"Previous (Left arrow key)",tNext:"Next (Right arrow key)",tCounter:"%curr% of %total%"},proto:{initGallery:function(){var e=h.st.gallery,i=".mfp-gallery";return h.direction=!0,!(!e||!e.enabled)&&(b+=" mfp-gallery",w(r+i,function(){e.navigateByImgClick&&h.wrap.on("click"+i,".mfp-img",function(){if(h.items.length>1)return h.next(),!1}),v.on("keydown"+i,function(t){37===t.keyCode?h.prev():39===t.keyCode&&h.next()})}),w("UpdateStatus"+i,function(t,e){e.text&&(e.text=U(e.text,h.currItem.index,h.items.length))}),w(o+i,function(t,i,n,o){var r=h.items.length;n.counter=r>1?U(e.tCounter,o.index,r):""}),w("BuildControls"+i,function(){if(h.items.length>1&&e.arrows&&!h.arrowLeft){var t=e.arrowMarkup,i=h.arrowLeft=$(t.replace(/%title%/gi,e.tPrev).replace(/%dir%/gi,"left")).addClass(u),n=h.arrowRight=$(t.replace(/%title%/gi,e.tNext).replace(/%dir%/gi,"right")).addClass(u);i.click(function(){h.prev()}),n.click(function(){h.next()}),h.container.append(i.add(n))}}),w(a+i,function(){h._preloadTimeout&&clearTimeout(h._preloadTimeout),h._preloadTimeout=setTimeout(function(){h.preloadNearbyImages(),h._preloadTimeout=null},16)}),void w(t+i,function(){v.off(i),h.wrap.off("click"+i),h.arrowRight=h.arrowLeft=null}))},next:function(){h.direction=!0,h.index=H(h.index+1),h.updateItemHTML()},prev:function(){h.direction=!1,h.index=H(h.index-1),h.updateItemHTML()},goTo:function(t){h.direction=t>=h.index,h.index=t,h.updateItemHTML()},preloadNearbyImages:function(){var t=h.st.gallery.preload,e=Math.min(t[0],h.items.length),i=Math.min(t[1],h.items.length),n;for(n=1;n<=(h.direction?i:e);n++)h._preloadItem(h.index+n);for(n=1;n<=(h.direction?e:i);n++)h._preloadItem(h.index-n)},_preloadItem:function(t){if(t=H(t),!h.items[t].preloaded){var e=h.items[t];e.parsed||(e=h.parseEl(t)),S("LazyLoad",e),"image"===e.type&&(e.img=$('<img class="mfp-img" />').on("load.mfploader",function(){e.hasSize=!0}).on("error.mfploader",function(){e.hasSize=!0,e.loadError=!0,S("LazyLoadError",e)}).attr("src",e.src)),e.preloaded=!0}}}});var q="retina";$.magnificPopup.registerModule(q,{options:{replaceSrc:function(t){return t.src.replace(/\.\w+$/,function(t){return"@2x"+t})},ratio:1},proto:{initRetina:function(){if(window.devicePixelRatio>1){var t=h.st.retina,e=t.ratio;e=isNaN(e)?e():e,e>1&&(w("ImageHasSize."+q,function(t,i){i.img.css({"max-width":i.img[0].naturalWidth/e,width:"100%"})}),w("ElementParse."+q,function(i,n){n.src=t.replaceSrc(n,e)}))}}}}),T()}),function(t){"use strict";function e(t){return new RegExp("(^|\\s+)"+t+"(\\s+|$)")}function i(t,e){var i=n(t,e)?r:o;i(t,e)}var n,o,r;"classList"in document.documentElement?(n=function(t,e){return t.classList.contains(e)},o=function(t,e){t.classList.add(e)},r=function(t,e){t.classList.remove(e)}):(n=function(t,i){return e(i).test(t.className)},o=function(t,e){n(t,e)||(t.className=t.className+" "+e)},r=function(t,i){t.className=t.className.replace(e(i)," ")});var a={hasClass:n,addClass:o,removeClass:r,toggleClass:i,has:n,add:o,remove:r,toggle:i};"function"==typeof define&&define.amd?define(a):t.classie=a}(window),function(t){"use strict";function e(t,e){if(!t)return!1;for(var i=t.target||t.srcElement||t||!1;i&&i!=e;)i=i.parentNode||!1;return i!==!1}function i(t,e){for(var i in e)e.hasOwnProperty(i)&&(t[i]=e[i]);return t}function n(t,e){this.el=t,this.options=i({},this.options),i(this.options,e),this._init()}n.prototype.options={newTab:!0,stickyPlaceholder:!0,onChange:function(t){return!1}},n.prototype._init=function(){var t=this.el.options[this.el.selectedIndex];this.hasDefaultPlaceholder=t&&t.disabled,this.selectedOpt=t||this.el.querySelector("option"),this._createSelectEl(),this.selOpts=[].slice.call(this.selEl.querySelectorAll("li[data-option]")),this.selOptsCount=this.selOpts.length,this.current=this.selOpts.indexOf(this.selEl.querySelector("li.cs-selected"))||-1,this.selPlaceholder=this.selEl.querySelector("span.cs-placeholder"),this._initEvents()},n.prototype._createSelectEl=function(){var t=this,e="",i=function(t){var e="",i="",n="";!t.selectedOpt||this.foundSelected||this.hasDefaultPlaceholder||(i+="cs-selected ",this.foundSelected=!0),t.getAttribute("data-class")&&(i+=t.getAttribute("data-class")),t.getAttribute("data-link")&&(n="data-link="+t.getAttribute("data-link")),""!==i&&(e='class="'+i+'" ');var o="";return[].forEach.call(t.attributes,function(t){var e=t.name;e.indexOf("data-")+["data-option","data-value"].indexOf(e)==-1&&(o+=e+"='"+t.value+"' ")}),"<li "+e+n+o+' data-option data-value="'+t.value+'"><span>'+t.textContent+"</span></li>"};[].slice.call(this.el.children).forEach(function(t){if(!t.disabled){var n=t.tagName.toLowerCase();"option"===n?e+=i(t):"optgroup"===n&&(e+='<li class="cs-optgroup"><span>'+t.label+"</span><ul>",[].slice.call(t.children).forEach(function(t){e+=i(t)}),e+="</ul></li>")}});var n='<div class="cs-options"><ul>'+e+"</ul></div>";this.selEl=document.createElement("div"),this.selEl.className=this.el.className,this.selEl.tabIndex=this.el.tabIndex,this.selEl.innerHTML='<span class="cs-placeholder">'+this.selectedOpt.textContent+"</span>"+n,this.el.parentNode.appendChild(this.selEl),this.selEl.appendChild(this.el)},n.prototype._initEvents=function(){var t=this;this.selPlaceholder.addEventListener("click",function(){t._toggleSelect()}),this.selOpts.forEach(function(e,i){e.addEventListener("click",function(){t.current=i,t._changeOption(),t._toggleSelect()})}),document.addEventListener("click",function(i){var n=i.target;t._isOpen()&&n!==t.selEl&&!e(n,t.selEl)&&t._toggleSelect()}),this.selEl.addEventListener("keydown",function(e){var i=e.keyCode||e.which;switch(i){case 38:e.preventDefault(),t._navigateOpts("prev");break;case 40:e.preventDefault(),t._navigateOpts("next");break;case 32:e.preventDefault(),t._isOpen()&&"undefined"!=typeof t.preSelCurrent&&t.preSelCurrent!==-1&&t._changeOption(),t._toggleSelect();break;case 13:e.preventDefault(),t._isOpen()&&"undefined"!=typeof t.preSelCurrent&&t.preSelCurrent!==-1&&(t._changeOption(),t._toggleSelect());break;case 27:e.preventDefault(),t._isOpen()&&t._toggleSelect()}})},n.prototype._navigateOpts=function(t){this._isOpen()||this._toggleSelect();var e="undefined"!=typeof this.preSelCurrent&&this.preSelCurrent!==-1?this.preSelCurrent:this.current;("prev"===t&&e>0||"next"===t&&e<this.selOptsCount-1)&&(this.preSelCurrent="next"===t?e+1:e-1,this._removeFocus(),classie.add(this.selOpts[this.preSelCurrent],"cs-focus"))},n.prototype._toggleSelect=function(){this._removeFocus(),this._isOpen()?(this.current!==-1&&(this.selPlaceholder.textContent=this.selOpts[this.current].textContent),classie.remove(this.selEl,"cs-active")):(this.hasDefaultPlaceholder&&this.options.stickyPlaceholder&&(this.selPlaceholder.textContent=this.selectedOpt.textContent),classie.add(this.selEl,"cs-active"))},n.prototype._changeOption=function(){"undefined"!=typeof this.preSelCurrent&&this.preSelCurrent!==-1&&(this.current=this.preSelCurrent,this.preSelCurrent=-1);var e=this.selOpts[this.current];this.selPlaceholder.textContent=e.textContent,this.el.value=e.getAttribute("data-value");var i=this.selEl.querySelector("li.cs-selected");i&&classie.remove(i,"cs-selected"),classie.add(e,"cs-selected"),e.getAttribute("data-link")&&(this.options.newTab?t.open(e.getAttribute("data-link"),"_blank"):t.location=e.getAttribute("data-link")),this.options.onChange(this.el.value)},n.prototype._isOpen=function(t){return classie.has(this.selEl,"cs-active")},n.prototype._removeFocus=function(t){var e=this.selEl.querySelector("li.cs-focus");e&&classie.remove(e,"cs-focus")},t.SelectFx=n}(window),function(t,e){"function"==typeof define&&define.amd?define("ev-emitter/ev-emitter",e):"object"==typeof module&&module.exports?module.exports=e():t.EvEmitter=e()}("undefined"!=typeof window?window:this,function(){function t(){}var e=t.prototype;return e.on=function(t,e){if(t&&e){var i=this._events=this._events||{},n=i[t]=i[t]||[];return n.indexOf(e)==-1&&n.push(e),this}},e.once=function(t,e){if(t&&e){this.on(t,e);var i=this._onceEvents=this._onceEvents||{},n=i[t]=i[t]||{};return n[e]=!0,this}},e.off=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var n=i.indexOf(e);return n!=-1&&i.splice(n,1),this}},e.emitEvent=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var n=0,o=i[n];e=e||[];for(var r=this._onceEvents&&this._onceEvents[t];o;){var a=r&&r[o];a&&(this.off(t,o),delete r[o]),o.apply(this,e),n+=a?0:1,o=i[n]}return this}},t}),function(t,e){"use strict";"function"==typeof define&&define.amd?define(["ev-emitter/ev-emitter"],function(i){return e(t,i)}):"object"==typeof module&&module.exports?module.exports=e(t,require("ev-emitter")):t.imagesLoaded=e(t,t.EvEmitter)}(window,function t(e,i){function n(t,e){for(var i in e)t[i]=e[i];return t}function o(t){var e=[];if(Array.isArray(t))e=t;else if("number"==typeof t.length)for(var i=0;i<t.length;i++)e.push(t[i]);else e.push(t);return e}function r(t,e,i){return this instanceof r?("string"==typeof t&&(t=document.querySelectorAll(t)),this.elements=o(t),this.options=n({},this.options),"function"==typeof e?i=e:n(this.options,e),i&&this.on("always",i),this.getImages(),$&&(this.jqDeferred=new $.Deferred),void setTimeout(function(){this.check()}.bind(this))):new r(t,e,i)}function a(t){this.img=t}function s(t,e){this.url=t,this.element=e,this.img=new Image}var $=e.jQuery,l=e.console;r.prototype=Object.create(i.prototype),r.prototype.options={},r.prototype.getImages=function(){this.images=[],this.elements.forEach(this.addElementImages,this)},r.prototype.addElementImages=function(t){"IMG"==t.nodeName&&this.addImage(t),this.options.background===!0&&this.addElementBackgroundImages(t);var e=t.nodeType;if(e&&d[e]){for(var i=t.querySelectorAll("img"),n=0;n<i.length;n++){var o=i[n];this.addImage(o)}if("string"==typeof this.options.background){var r=t.querySelectorAll(this.options.background);for(n=0;n<r.length;n++){var a=r[n];this.addElementBackgroundImages(a)}}}};var d={1:!0,9:!0,11:!0};return r.prototype.addElementBackgroundImages=function(t){var e=getComputedStyle(t);if(e)for(var i=/url\((['"])?(.*?)\1\)/gi,n=i.exec(e.backgroundImage);null!==n;){var o=n&&n[2];o&&this.addBackground(o,t),n=i.exec(e.backgroundImage)}},r.prototype.addImage=function(t){var e=new a(t);this.images.push(e)},r.prototype.addBackground=function(t,e){var i=new s(t,e);this.images.push(i)},r.prototype.check=function(){function t(t,i,n){setTimeout(function(){e.progress(t,i,n)})}var e=this;return this.progressedCount=0,this.hasAnyBroken=!1,this.images.length?void this.images.forEach(function(e){e.once("progress",t),e.check()}):void this.complete()},r.prototype.progress=function(t,e,i){
this.progressedCount++,this.hasAnyBroken=this.hasAnyBroken||!t.isLoaded,this.emitEvent("progress",[this,t,e]),this.jqDeferred&&this.jqDeferred.notify&&this.jqDeferred.notify(this,t),this.progressedCount==this.images.length&&this.complete(),this.options.debug&&l&&l.log("progress: "+i,t,e)},r.prototype.complete=function(){var t=this.hasAnyBroken?"fail":"done";if(this.isComplete=!0,this.emitEvent(t,[this]),this.emitEvent("always",[this]),this.jqDeferred){var e=this.hasAnyBroken?"reject":"resolve";this.jqDeferred[e](this)}},a.prototype=Object.create(i.prototype),a.prototype.check=function(){var t=this.getIsImageComplete();return t?void this.confirm(0!==this.img.naturalWidth,"naturalWidth"):(this.proxyImage=new Image,this.proxyImage.addEventListener("load",this),this.proxyImage.addEventListener("error",this),this.img.addEventListener("load",this),this.img.addEventListener("error",this),void(this.proxyImage.src=this.img.src))},a.prototype.getIsImageComplete=function(){return this.img.complete&&void 0!==this.img.naturalWidth},a.prototype.confirm=function(t,e){this.isLoaded=t,this.emitEvent("progress",[this,this.img,e])},a.prototype.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},a.prototype.onload=function(){this.confirm(!0,"onload"),this.unbindEvents()},a.prototype.onerror=function(){this.confirm(!1,"onerror"),this.unbindEvents()},a.prototype.unbindEvents=function(){this.proxyImage.removeEventListener("load",this),this.proxyImage.removeEventListener("error",this),this.img.removeEventListener("load",this),this.img.removeEventListener("error",this)},s.prototype=Object.create(a.prototype),s.prototype.check=function(){this.img.addEventListener("load",this),this.img.addEventListener("error",this),this.img.src=this.url;var t=this.getIsImageComplete();t&&(this.confirm(0!==this.img.naturalWidth,"naturalWidth"),this.unbindEvents())},s.prototype.unbindEvents=function(){this.img.removeEventListener("load",this),this.img.removeEventListener("error",this)},s.prototype.confirm=function(t,e){this.isLoaded=t,this.emitEvent("progress",[this,this.element,e])},r.makeJQueryPlugin=function(t){t=t||e.jQuery,t&&($=t,$.fn.imagesLoaded=function(t,e){var i=new r(this,t,e);return i.jqDeferred.promise($(this))})},r.makeJQueryPlugin(),r}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("jquery-bridget/jquery-bridget",["jquery"],function(i){e(t,i)}):"object"==typeof module&&module.exports?module.exports=e(t,require("jquery")):t.jQueryBridget=e(t,t.jQuery)}(window,function t(e,i){"use strict";function n(t,n,$){function a(e,i,n){var o,r="$()."+t+'("'+i+'")';return e.each(function(e,a){var l=$.data(a,t);if(!l)return void s(t+" not initialized. Cannot call methods, i.e. "+r);var d=l[i];if(!d||"_"==i.charAt(0))return void s(r+" is not a valid method");var c=d.apply(l,n);o=void 0===o?c:o}),void 0!==o?o:e}function l(e,i){e.each(function(e,o){var r=$.data(o,t);r?(r.option(i),r._init()):(r=new n(o,i),$.data(o,t,r))})}$=$||i||e.jQuery,$&&(n.prototype.option||(n.prototype.option=function(t){$.isPlainObject(t)&&(this.options=$.extend(!0,this.options,t))}),$.fn[t]=function(t){if("string"==typeof t){var e=r.call(arguments,1);return a(this,t,e)}return l(this,t),this},o($))}function o($){!$||$&&$.bridget||($.bridget=n)}var r=Array.prototype.slice,a=e.console,s="undefined"==typeof a?function(){}:function(t){a.error(t)};return o(i||e.jQuery),n}),function(t,e){"function"==typeof define&&define.amd?define("ev-emitter/ev-emitter",e):"object"==typeof module&&module.exports?module.exports=e():t.EvEmitter=e()}("undefined"!=typeof window?window:this,function(){function t(){}var e=t.prototype;return e.on=function(t,e){if(t&&e){var i=this._events=this._events||{},n=i[t]=i[t]||[];return n.indexOf(e)==-1&&n.push(e),this}},e.once=function(t,e){if(t&&e){this.on(t,e);var i=this._onceEvents=this._onceEvents||{},n=i[t]=i[t]||{};return n[e]=!0,this}},e.off=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var n=i.indexOf(e);return n!=-1&&i.splice(n,1),this}},e.emitEvent=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var n=0,o=i[n];e=e||[];for(var r=this._onceEvents&&this._onceEvents[t];o;){var a=r&&r[o];a&&(this.off(t,o),delete r[o]),o.apply(this,e),n+=a?0:1,o=i[n]}return this}},t}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("get-size/get-size",[],function(){return e()}):"object"==typeof module&&module.exports?module.exports=e():t.getSize=e()}(window,function t(){"use strict";function e(t){var e=parseFloat(t),i=t.indexOf("%")==-1&&!isNaN(e);return i&&e}function i(){}function n(){for(var t={width:0,height:0,innerWidth:0,innerHeight:0,outerWidth:0,outerHeight:0},e=0;e<d;e++){var i=l[e];t[i]=0}return t}function o(t){var e=getComputedStyle(t);return e||s("Style returned "+e+". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"),e}function r(){if(!c){c=!0;var t=document.createElement("div");t.style.width="200px",t.style.padding="1px 2px 3px 4px",t.style.borderStyle="solid",t.style.borderWidth="1px 2px 3px 4px",t.style.boxSizing="border-box";var i=document.body||document.documentElement;i.appendChild(t);var n=o(t);a.isBoxSizeOuter=u=200==e(n.width),i.removeChild(t)}}function a(t){if(r(),"string"==typeof t&&(t=document.querySelector(t)),t&&"object"==typeof t&&t.nodeType){var i=o(t);if("none"==i.display)return n();var a={};a.width=t.offsetWidth,a.height=t.offsetHeight;for(var s=a.isBorderBox="border-box"==i.boxSizing,c=0;c<d;c++){var h=l[c],p=i[h],f=parseFloat(p);a[h]=isNaN(f)?0:f}var g=a.paddingLeft+a.paddingRight,m=a.paddingTop+a.paddingBottom,v=a.marginLeft+a.marginRight,y=a.marginTop+a.marginBottom,b=a.borderLeftWidth+a.borderRightWidth,x=a.borderTopWidth+a.borderBottomWidth,w=s&&u,k=e(i.width);k!==!1&&(a.width=k+(w?0:g+b));var S=e(i.height);return S!==!1&&(a.height=S+(w?0:m+x)),a.innerWidth=a.width-(g+b),a.innerHeight=a.height-(m+x),a.outerWidth=a.width+v,a.outerHeight=a.height+y,a}}var s="undefined"==typeof console?i:function(t){console.error(t)},l=["paddingLeft","paddingRight","paddingTop","paddingBottom","marginLeft","marginRight","marginTop","marginBottom","borderLeftWidth","borderRightWidth","borderTopWidth","borderBottomWidth"],d=l.length,c=!1,u;return a}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("desandro-matches-selector/matches-selector",e):"object"==typeof module&&module.exports?module.exports=e():t.matchesSelector=e()}(window,function t(){"use strict";var e=function(){var t=Element.prototype;if(t.matches)return"matches";if(t.matchesSelector)return"matchesSelector";for(var e=["webkit","moz","ms","o"],i=0;i<e.length;i++){var n=e[i],o=n+"MatchesSelector";if(t[o])return o}}();return function t(i,n){return i[e](n)}}),function(t,e){"function"==typeof define&&define.amd?define("fizzy-ui-utils/utils",["desandro-matches-selector/matches-selector"],function(i){return e(t,i)}):"object"==typeof module&&module.exports?module.exports=e(t,require("desandro-matches-selector")):t.fizzyUIUtils=e(t,t.matchesSelector)}(window,function t(e,i){var n={};n.extend=function(t,e){for(var i in e)t[i]=e[i];return t},n.modulo=function(t,e){return(t%e+e)%e},n.makeArray=function(t){var e=[];if(Array.isArray(t))e=t;else if(t&&"number"==typeof t.length)for(var i=0;i<t.length;i++)e.push(t[i]);else e.push(t);return e},n.removeFrom=function(t,e){var i=t.indexOf(e);i!=-1&&t.splice(i,1)},n.getParent=function(t,e){for(;t!=document.body;)if(t=t.parentNode,i(t,e))return t},n.getQueryElement=function(t){return"string"==typeof t?document.querySelector(t):t},n.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},n.filterFindElements=function(t,e){t=n.makeArray(t);var o=[];return t.forEach(function(t){if(t instanceof HTMLElement){if(!e)return void o.push(t);i(t,e)&&o.push(t);for(var n=t.querySelectorAll(e),r=0;r<n.length;r++)o.push(n[r])}}),o},n.debounceMethod=function(t,e,i){var n=t.prototype[e],o=e+"Timeout";t.prototype[e]=function(){var t=this[o];t&&clearTimeout(t);var e=arguments,r=this;this[o]=setTimeout(function(){n.apply(r,e),delete r[o]},i||100)}},n.docReady=function(t){var e=document.readyState;"complete"==e||"interactive"==e?t():document.addEventListener("DOMContentLoaded",t)},n.toDashed=function(t){return t.replace(/(.)([A-Z])/g,function(t,e,i){return e+"-"+i}).toLowerCase()};var o=e.console;return n.htmlInit=function(t,i){n.docReady(function(){var r=n.toDashed(i),a="data-"+r,s=document.querySelectorAll("["+a+"]"),l=document.querySelectorAll(".js-"+r),d=n.makeArray(s).concat(n.makeArray(l)),c=a+"-options",u=e.jQuery;d.forEach(function(e){var n=e.getAttribute(a)||e.getAttribute(c),r;try{r=n&&JSON.parse(n)}catch(t){return void(o&&o.error("Error parsing "+a+" on "+e.className+": "+t))}var s=new t(e,r);u&&u.data(e,i,s)})})},n}),function(t,e){"function"==typeof define&&define.amd?define("outlayer/item",["ev-emitter/ev-emitter","get-size/get-size"],e):"object"==typeof module&&module.exports?module.exports=e(require("ev-emitter"),require("get-size")):(t.Outlayer={},t.Outlayer.Item=e(t.EvEmitter,t.getSize))}(window,function t(e,i){"use strict";function n(t){for(var e in t)return!1;return e=null,!0}function o(t,e){t&&(this.element=t,this.layout=e,this.position={x:0,y:0},this._create())}function r(t){return t.replace(/([A-Z])/g,function(t){return"-"+t.toLowerCase()})}var a=document.documentElement.style,s="string"==typeof a.transition?"transition":"WebkitTransition",l="string"==typeof a.transform?"transform":"WebkitTransform",d={WebkitTransition:"webkitTransitionEnd",transition:"transitionend"}[s],c={transform:l,transition:s,transitionDuration:s+"Duration",transitionProperty:s+"Property",transitionDelay:s+"Delay"},u=o.prototype=Object.create(e.prototype);u.constructor=o,u._create=function(){this._transn={ingProperties:{},clean:{},onEnd:{}},this.css({position:"absolute"})},u.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},u.getSize=function(){this.size=i(this.element)},u.css=function(t){var e=this.element.style;for(var i in t){var n=c[i]||i;e[n]=t[i]}},u.getPosition=function(){var t=getComputedStyle(this.element),e=this.layout._getOption("originLeft"),i=this.layout._getOption("originTop"),n=t[e?"left":"right"],o=t[i?"top":"bottom"],r=this.layout.size,a=n.indexOf("%")!=-1?parseFloat(n)/100*r.width:parseInt(n,10),s=o.indexOf("%")!=-1?parseFloat(o)/100*r.height:parseInt(o,10);a=isNaN(a)?0:a,s=isNaN(s)?0:s,a-=e?r.paddingLeft:r.paddingRight,s-=i?r.paddingTop:r.paddingBottom,this.position.x=a,this.position.y=s},u.layoutPosition=function(){var t=this.layout.size,e={},i=this.layout._getOption("originLeft"),n=this.layout._getOption("originTop"),o=i?"paddingLeft":"paddingRight",r=i?"left":"right",a=i?"right":"left",s=this.position.x+t[o];e[r]=this.getXValue(s),e[a]="";var l=n?"paddingTop":"paddingBottom",d=n?"top":"bottom",c=n?"bottom":"top",u=this.position.y+t[l];e[d]=this.getYValue(u),e[c]="",this.css(e),this.emitEvent("layout",[this])},u.getXValue=function(t){var e=this.layout._getOption("horizontal");return this.layout.options.percentPosition&&!e?t/this.layout.size.width*100+"%":t+"px"},u.getYValue=function(t){var e=this.layout._getOption("horizontal");return this.layout.options.percentPosition&&e?t/this.layout.size.height*100+"%":t+"px"},u._transitionTo=function(t,e){this.getPosition();var i=this.position.x,n=this.position.y,o=parseInt(t,10),r=parseInt(e,10),a=o===this.position.x&&r===this.position.y;if(this.setPosition(t,e),a&&!this.isTransitioning)return void this.layoutPosition();var s=t-i,l=e-n,d={};d.transform=this.getTranslate(s,l),this.transition({to:d,onTransitionEnd:{transform:this.layoutPosition},isCleaning:!0})},u.getTranslate=function(t,e){var i=this.layout._getOption("originLeft"),n=this.layout._getOption("originTop");return t=i?t:-t,e=n?e:-e,"translate3d("+t+"px, "+e+"px, 0)"},u.goTo=function(t,e){this.setPosition(t,e),this.layoutPosition()},u.moveTo=u._transitionTo,u.setPosition=function(t,e){this.position.x=parseInt(t,10),this.position.y=parseInt(e,10)},u._nonTransition=function(t){this.css(t.to),t.isCleaning&&this._removeStyles(t.to);for(var e in t.onTransitionEnd)t.onTransitionEnd[e].call(this)},u.transition=function(t){if(!parseFloat(this.layout.options.transitionDuration))return void this._nonTransition(t);var e=this._transn;for(var i in t.onTransitionEnd)e.onEnd[i]=t.onTransitionEnd[i];for(i in t.to)e.ingProperties[i]=!0,t.isCleaning&&(e.clean[i]=!0);if(t.from){this.css(t.from);var n=this.element.offsetHeight;n=null}this.enableTransition(t.to),this.css(t.to),this.isTransitioning=!0};var h="opacity,"+r(l);u.enableTransition=function(){if(!this.isTransitioning){var t=this.layout.options.transitionDuration;t="number"==typeof t?t+"ms":t,this.css({transitionProperty:h,transitionDuration:t,transitionDelay:this.staggerDelay||0}),this.element.addEventListener(d,this,!1)}},u.onwebkitTransitionEnd=function(t){this.ontransitionend(t)},u.onotransitionend=function(t){this.ontransitionend(t)};var p={"-webkit-transform":"transform"};u.ontransitionend=function(t){if(t.target===this.element){var e=this._transn,i=p[t.propertyName]||t.propertyName;if(delete e.ingProperties[i],n(e.ingProperties)&&this.disableTransition(),i in e.clean&&(this.element.style[t.propertyName]="",delete e.clean[i]),i in e.onEnd){var o=e.onEnd[i];o.call(this),delete e.onEnd[i]}this.emitEvent("transitionEnd",[this])}},u.disableTransition=function(){this.removeTransitionStyles(),this.element.removeEventListener(d,this,!1),this.isTransitioning=!1},u._removeStyles=function(t){var e={};for(var i in t)e[i]="";this.css(e)};var f={transitionProperty:"",transitionDuration:"",transitionDelay:""};return u.removeTransitionStyles=function(){this.css(f)},u.stagger=function(t){t=isNaN(t)?0:t,this.staggerDelay=t+"ms"},u.removeElem=function(){this.element.parentNode.removeChild(this.element),this.css({display:""}),this.emitEvent("remove",[this])},u.remove=function(){return s&&parseFloat(this.layout.options.transitionDuration)?(this.once("transitionEnd",function(){this.removeElem()}),void this.hide()):void this.removeElem()},u.reveal=function(){delete this.isHidden,this.css({display:""});var t=this.layout.options,e={},i=this.getHideRevealTransitionEndProperty("visibleStyle");e[i]=this.onRevealTransitionEnd,this.transition({from:t.hiddenStyle,to:t.visibleStyle,isCleaning:!0,onTransitionEnd:e})},u.onRevealTransitionEnd=function(){this.isHidden||this.emitEvent("reveal")},u.getHideRevealTransitionEndProperty=function(t){var e=this.layout.options[t];if(e.opacity)return"opacity";for(var i in e)return i},u.hide=function(){this.isHidden=!0,this.css({display:""});var t=this.layout.options,e={},i=this.getHideRevealTransitionEndProperty("hiddenStyle");e[i]=this.onHideTransitionEnd,this.transition({from:t.visibleStyle,to:t.hiddenStyle,isCleaning:!0,onTransitionEnd:e})},u.onHideTransitionEnd=function(){this.isHidden&&(this.css({display:"none"}),this.emitEvent("hide"))},u.destroy=function(){this.css({position:"",left:"",right:"",top:"",bottom:"",transition:"",transform:""})},o}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("outlayer/outlayer",["ev-emitter/ev-emitter","get-size/get-size","fizzy-ui-utils/utils","./item"],function(i,n,o,r){return e(t,i,n,o,r)}):"object"==typeof module&&module.exports?module.exports=e(t,require("ev-emitter"),require("get-size"),require("fizzy-ui-utils"),require("./item")):t.Outlayer=e(t,t.EvEmitter,t.getSize,t.fizzyUIUtils,t.Outlayer.Item)}(window,function t(e,i,n,o,r){"use strict";function a(t,e){var i=o.getQueryElement(t);if(!i)return void(d&&d.error("Bad element for "+this.constructor.namespace+": "+(i||t)));this.element=i,c&&(this.$element=c(this.element)),this.options=o.extend({},this.constructor.defaults),this.option(e);var n=++h;this.element.outlayerGUID=n,p[n]=this,this._create();var r=this._getOption("initLayout");r&&this.layout()}function s(t){function e(){t.apply(this,arguments)}return e.prototype=Object.create(t.prototype),e.prototype.constructor=e,e}function l(t){if("number"==typeof t)return t;var e=t.match(/(^\d*\.?\d*)(\w*)/),i=e&&e[1],n=e&&e[2];if(!i.length)return 0;i=parseFloat(i);var o=g[n]||1;return i*o}var d=e.console,c=e.jQuery,u=function(){},h=0,p={};a.namespace="outlayer",a.Item=r,a.defaults={containerStyle:{position:"relative"},initLayout:!0,originLeft:!0,originTop:!0,resize:!0,resizeContainer:!0,transitionDuration:"0.4s",hiddenStyle:{opacity:0,transform:"scale(0.001)"},visibleStyle:{opacity:1,transform:"scale(1)"}};var f=a.prototype;o.extend(f,i.prototype),f.option=function(t){o.extend(this.options,t)},f._getOption=function(t){var e=this.constructor.compatOptions[t];return e&&void 0!==this.options[e]?this.options[e]:this.options[t]},a.compatOptions={initLayout:"isInitLayout",horizontal:"isHorizontal",layoutInstant:"isLayoutInstant",originLeft:"isOriginLeft",originTop:"isOriginTop",resize:"isResizeBound",resizeContainer:"isResizingContainer"},f._create=function(){this.reloadItems(),this.stamps=[],this.stamp(this.options.stamp),o.extend(this.element.style,this.options.containerStyle);var t=this._getOption("resize");t&&this.bindResize()},f.reloadItems=function(){this.items=this._itemize(this.element.children)},f._itemize=function(t){for(var e=this._filterFindItemElements(t),i=this.constructor.Item,n=[],o=0;o<e.length;o++){var r=e[o],a=new i(r,this);n.push(a)}return n},f._filterFindItemElements=function(t){return o.filterFindElements(t,this.options.itemSelector)},f.getItemElements=function(){return this.items.map(function(t){return t.element})},f.layout=function(){this._resetLayout(),this._manageStamps();var t=this._getOption("layoutInstant"),e=void 0!==t?t:!this._isLayoutInited;this.layoutItems(this.items,e),this._isLayoutInited=!0},f._init=f.layout,f._resetLayout=function(){this.getSize()},f.getSize=function(){this.size=n(this.element)},f._getMeasurement=function(t,e){var i=this.options[t],o;i?("string"==typeof i?o=this.element.querySelector(i):i instanceof HTMLElement&&(o=i),this[t]=o?n(o)[e]:i):this[t]=0},f.layoutItems=function(t,e){t=this._getItemsForLayout(t),this._layoutItems(t,e),this._postLayout()},f._getItemsForLayout=function(t){return t.filter(function(t){return!t.isIgnored})},f._layoutItems=function(t,e){if(this._emitCompleteOnItems("layout",t),t&&t.length){var i=[];t.forEach(function(t){var n=this._getItemLayoutPosition(t);n.item=t,n.isInstant=e||t.isLayoutInstant,i.push(n)},this),this._processLayoutQueue(i)}},f._getItemLayoutPosition=function(){return{x:0,y:0}},f._processLayoutQueue=function(t){this.updateStagger(),t.forEach(function(t,e){this._positionItem(t.item,t.x,t.y,t.isInstant,e)},this)},f.updateStagger=function(){var t=this.options.stagger;return null===t||void 0===t?void(this.stagger=0):(this.stagger=l(t),this.stagger)},f._positionItem=function(t,e,i,n,o){n?t.goTo(e,i):(t.stagger(o*this.stagger),t.moveTo(e,i))},f._postLayout=function(){this.resizeContainer()},f.resizeContainer=function(){var t=this._getOption("resizeContainer");if(t){var e=this._getContainerSize();e&&(this._setContainerMeasure(e.width,!0),this._setContainerMeasure(e.height,!1))}},f._getContainerSize=u,f._setContainerMeasure=function(t,e){if(void 0!==t){var i=this.size;i.isBorderBox&&(t+=e?i.paddingLeft+i.paddingRight+i.borderLeftWidth+i.borderRightWidth:i.paddingBottom+i.paddingTop+i.borderTopWidth+i.borderBottomWidth),t=Math.max(t,0),this.element.style[e?"width":"height"]=t+"px"}},f._emitCompleteOnItems=function(t,e){function i(){o.dispatchEvent(t+"Complete",null,[e])}function n(){a++,a==r&&i()}var o=this,r=e.length;if(!e||!r)return void i();var a=0;e.forEach(function(e){e.once(t,n)})},f.dispatchEvent=function(t,e,i){var n=e?[e].concat(i):i;if(this.emitEvent(t,n),c)if(this.$element=this.$element||c(this.element),e){var o=c.Event(e);o.type=t,this.$element.trigger(o,i)}else this.$element.trigger(t,i)},f.ignore=function(t){var e=this.getItem(t);e&&(e.isIgnored=!0)},f.unignore=function(t){var e=this.getItem(t);e&&delete e.isIgnored},f.stamp=function(t){t=this._find(t),t&&(this.stamps=this.stamps.concat(t),t.forEach(this.ignore,this))},f.unstamp=function(t){t=this._find(t),t&&t.forEach(function(t){o.removeFrom(this.stamps,t),this.unignore(t)},this)},f._find=function(t){if(t)return"string"==typeof t&&(t=this.element.querySelectorAll(t)),t=o.makeArray(t)},f._manageStamps=function(){this.stamps&&this.stamps.length&&(this._getBoundingRect(),this.stamps.forEach(this._manageStamp,this))},f._getBoundingRect=function(){var t=this.element.getBoundingClientRect(),e=this.size;this._boundingRect={left:t.left+e.paddingLeft+e.borderLeftWidth,top:t.top+e.paddingTop+e.borderTopWidth,right:t.right-(e.paddingRight+e.borderRightWidth),bottom:t.bottom-(e.paddingBottom+e.borderBottomWidth)}},f._manageStamp=u,f._getElementOffset=function(t){var e=t.getBoundingClientRect(),i=this._boundingRect,o=n(t),r={left:e.left-i.left-o.marginLeft,top:e.top-i.top-o.marginTop,right:i.right-e.right-o.marginRight,bottom:i.bottom-e.bottom-o.marginBottom};return r},f.handleEvent=o.handleEvent,f.bindResize=function(){e.addEventListener("resize",this),this.isResizeBound=!0},f.unbindResize=function(){e.removeEventListener("resize",this),this.isResizeBound=!1},f.onresize=function(){this.resize()},o.debounceMethod(a,"onresize",100),f.resize=function(){this.isResizeBound&&this.needsResizeLayout()&&this.layout()},f.needsResizeLayout=function(){var t=n(this.element),e=this.size&&t;return e&&t.innerWidth!==this.size.innerWidth},f.addItems=function(t){var e=this._itemize(t);return e.length&&(this.items=this.items.concat(e)),e},f.appended=function(t){var e=this.addItems(t);e.length&&(this.layoutItems(e,!0),this.reveal(e))},f.prepended=function(t){var e=this._itemize(t);if(e.length){var i=this.items.slice(0);this.items=e.concat(i),this._resetLayout(),this._manageStamps(),this.layoutItems(e,!0),this.reveal(e),this.layoutItems(i)}},f.reveal=function(t){if(this._emitCompleteOnItems("reveal",t),t&&t.length){var e=this.updateStagger();t.forEach(function(t,i){t.stagger(i*e),t.reveal()})}},f.hide=function(t){if(this._emitCompleteOnItems("hide",t),t&&t.length){var e=this.updateStagger();t.forEach(function(t,i){t.stagger(i*e),t.hide()})}},f.revealItemElements=function(t){var e=this.getItems(t);this.reveal(e)},f.hideItemElements=function(t){var e=this.getItems(t);this.hide(e)},f.getItem=function(t){for(var e=0;e<this.items.length;e++){var i=this.items[e];if(i.element==t)return i}},f.getItems=function(t){t=o.makeArray(t);var e=[];return t.forEach(function(t){var i=this.getItem(t);i&&e.push(i)},this),e},f.remove=function(t){var e=this.getItems(t);this._emitCompleteOnItems("remove",e),e&&e.length&&e.forEach(function(t){t.remove(),o.removeFrom(this.items,t)},this)},f.destroy=function(){var t=this.element.style;t.height="",t.position="",t.width="",this.items.forEach(function(t){t.destroy()}),this.unbindResize();var e=this.element.outlayerGUID;delete p[e],delete this.element.outlayerGUID,c&&c.removeData(this.element,this.constructor.namespace)},a.data=function(t){t=o.getQueryElement(t);var e=t&&t.outlayerGUID;return e&&p[e]},a.create=function(t,e){var i=s(a);return i.defaults=o.extend({},a.defaults),o.extend(i.defaults,e),i.compatOptions=o.extend({},a.compatOptions),i.namespace=t,i.data=a.data,i.Item=s(r),o.htmlInit(i,t),c&&c.bridget&&c.bridget(t,i),i};var g={ms:1,s:1e3};return a.Item=r,a}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/item",["outlayer/outlayer"],e):"object"==typeof module&&module.exports?module.exports=e(require("outlayer")):(t.Isotope=t.Isotope||{},t.Isotope.Item=e(t.Outlayer))}(window,function t(e){"use strict";function i(){e.Item.apply(this,arguments)}var n=i.prototype=Object.create(e.Item.prototype),o=n._create;n._create=function(){this.id=this.layout.itemGUID++,o.call(this),this.sortData={}},n.updateSortData=function(){if(!this.isIgnored){this.sortData.id=this.id,this.sortData["original-order"]=this.id,this.sortData.random=Math.random();var t=this.layout.options.getSortData,e=this.layout._sorters;for(var i in t){var n=e[i];this.sortData[i]=n(this.element,this)}}};var r=n.destroy;return n.destroy=function(){r.apply(this,arguments),this.css({display:""})},i}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-mode",["get-size/get-size","outlayer/outlayer"],e):"object"==typeof module&&module.exports?module.exports=e(require("get-size"),require("outlayer")):(t.Isotope=t.Isotope||{},t.Isotope.LayoutMode=e(t.getSize,t.Outlayer))}(window,function t(e,i){"use strict";function n(t){this.isotope=t,t&&(this.options=t.options[this.namespace],this.element=t.element,this.items=t.filteredItems,this.size=t.size)}var o=n.prototype,r=["_resetLayout","_getItemLayoutPosition","_manageStamp","_getContainerSize","_getElementOffset","needsResizeLayout","_getOption"];return r.forEach(function(t){o[t]=function(){return i.prototype[t].apply(this.isotope,arguments)}}),o.needsVerticalResizeLayout=function(){var t=e(this.isotope.element),i=this.isotope.size&&t;return i&&t.innerHeight!=this.isotope.size.innerHeight},o._getMeasurement=function(){this.isotope._getMeasurement.apply(this,arguments)},o.getColumnWidth=function(){this.getSegmentSize("column","Width")},o.getRowHeight=function(){this.getSegmentSize("row","Height")},o.getSegmentSize=function(t,e){var i=t+e,n="outer"+e;if(this._getMeasurement(i,n),!this[i]){var o=this.getFirstItemSize();this[i]=o&&o[n]||this.isotope.size["inner"+e]}},o.getFirstItemSize=function(){var t=this.isotope.filteredItems[0];return t&&t.element&&e(t.element)},o.layout=function(){this.isotope.layout.apply(this.isotope,arguments)},o.getSize=function(){this.isotope.getSize(),this.size=this.isotope.size},n.modes={},n.create=function(t,e){function i(){n.apply(this,arguments)}return i.prototype=Object.create(o),i.prototype.constructor=i,e&&(i.options=e),i.prototype.namespace=t,n.modes[t]=i,i},n}),function(t,e){"function"==typeof define&&define.amd?define("masonry/masonry",["outlayer/outlayer","get-size/get-size"],e):"object"==typeof module&&module.exports?module.exports=e(require("outlayer"),require("get-size")):t.Masonry=e(t.Outlayer,t.getSize)}(window,function t(e,i){var n=e.create("masonry");return n.compatOptions.fitWidth="isFitWidth",n.prototype._resetLayout=function(){this.getSize(),this._getMeasurement("columnWidth","outerWidth"),this._getMeasurement("gutter","outerWidth"),this.measureColumns(),this.colYs=[];for(var t=0;t<this.cols;t++)this.colYs.push(0);this.maxY=0},n.prototype.measureColumns=function(){if(this.getContainerWidth(),!this.columnWidth){var t=this.items[0],e=t&&t.element;this.columnWidth=e&&i(e).outerWidth||this.containerWidth}var n=this.columnWidth+=this.gutter,o=this.containerWidth+this.gutter,r=o/n,a=n-o%n,s=a&&a<1?"round":"floor";r=Math[s](r),this.cols=Math.max(r,1)},n.prototype.getContainerWidth=function(){var t=this._getOption("fitWidth"),e=t?this.element.parentNode:this.element,n=i(e);this.containerWidth=n&&n.innerWidth},n.prototype._getItemLayoutPosition=function(t){t.getSize();var e=t.size.outerWidth%this.columnWidth,i=e&&e<1?"round":"ceil",n=Math[i](t.size.outerWidth/this.columnWidth);n=Math.min(n,this.cols);for(var o=this._getColGroup(n),r=Math.min.apply(Math,o),a=o.indexOf(r),s={x:this.columnWidth*a,y:r},l=r+t.size.outerHeight,d=this.cols+1-o.length,c=0;c<d;c++)this.colYs[a+c]=l;return s},n.prototype._getColGroup=function(t){if(t<2)return this.colYs;for(var e=[],i=this.cols+1-t,n=0;n<i;n++){var o=this.colYs.slice(n,n+t);e[n]=Math.max.apply(Math,o)}return e},n.prototype._manageStamp=function(t){var e=i(t),n=this._getElementOffset(t),o=this._getOption("originLeft"),r=o?n.left:n.right,a=r+e.outerWidth,s=Math.floor(r/this.columnWidth);s=Math.max(0,s);var l=Math.floor(a/this.columnWidth);l-=a%this.columnWidth?0:1,l=Math.min(this.cols-1,l);for(var d=this._getOption("originTop"),c=(d?n.top:n.bottom)+e.outerHeight,u=s;u<=l;u++)this.colYs[u]=Math.max(c,this.colYs[u])},n.prototype._getContainerSize=function(){this.maxY=Math.max.apply(Math,this.colYs);var t={height:this.maxY};return this._getOption("fitWidth")&&(t.width=this._getContainerFitWidth()),t},n.prototype._getContainerFitWidth=function(){for(var t=0,e=this.cols;--e&&0===this.colYs[e];)t++;return(this.cols-t)*this.columnWidth-this.gutter},n.prototype.needsResizeLayout=function(){var t=this.containerWidth;return this.getContainerWidth(),t!=this.containerWidth},n}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-modes/masonry",["../layout-mode","masonry/masonry"],e):"object"==typeof module&&module.exports?module.exports=e(require("../layout-mode"),require("masonry-layout")):e(t.Isotope.LayoutMode,t.Masonry)}(window,function t(e,i){"use strict";var n=e.create("masonry"),o=n.prototype,r={_getElementOffset:!0,layout:!0,_getMeasurement:!0};for(var a in i.prototype)r[a]||(o[a]=i.prototype[a]);var s=o.measureColumns;o.measureColumns=function(){this.items=this.isotope.filteredItems,s.call(this)};var l=o._getOption;return o._getOption=function(t){return"fitWidth"==t?void 0!==this.options.isFitWidth?this.options.isFitWidth:this.options.fitWidth:l.apply(this.isotope,arguments)},n}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-modes/fit-rows",["../layout-mode"],e):"object"==typeof exports?module.exports=e(require("../layout-mode")):e(t.Isotope.LayoutMode)}(window,function t(e){"use strict";var i=e.create("fitRows"),n=i.prototype;return n._resetLayout=function(){this.x=0,this.y=0,this.maxY=0,this._getMeasurement("gutter","outerWidth")},n._getItemLayoutPosition=function(t){t.getSize();var e=t.size.outerWidth+this.gutter,i=this.isotope.size.innerWidth+this.gutter;0!==this.x&&e+this.x>i&&(this.x=0,this.y=this.maxY);var n={x:this.x,y:this.y};return this.maxY=Math.max(this.maxY,this.y+t.size.outerHeight),this.x+=e,n},n._getContainerSize=function(){return{height:this.maxY}},i}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-modes/vertical",["../layout-mode"],e):"object"==typeof module&&module.exports?module.exports=e(require("../layout-mode")):e(t.Isotope.LayoutMode)}(window,function t(e){"use strict";var i=e.create("vertical",{horizontalAlignment:0}),n=i.prototype;return n._resetLayout=function(){this.y=0},n._getItemLayoutPosition=function(t){t.getSize();var e=(this.isotope.size.innerWidth-t.size.outerWidth)*this.options.horizontalAlignment,i=this.y;return this.y+=t.size.outerHeight,{x:e,y:i}},n._getContainerSize=function(){return{height:this.y}},i}),function(t,e){"function"==typeof define&&define.amd?define(["outlayer/outlayer","get-size/get-size","desandro-matches-selector/matches-selector","fizzy-ui-utils/utils","isotope/js/item","isotope/js/layout-mode","isotope/js/layout-modes/masonry","isotope/js/layout-modes/fit-rows","isotope/js/layout-modes/vertical"],function(i,n,o,r,a,s){return e(t,i,n,o,r,a,s)}):"object"==typeof module&&module.exports?module.exports=e(t,require("outlayer"),require("get-size"),require("desandro-matches-selector"),require("fizzy-ui-utils"),require("isotope/js/item"),require("isotope/js/layout-mode"),require("isotope/js/layout-modes/masonry"),require("isotope/js/layout-modes/fit-rows"),require("isotope/js/layout-modes/vertical")):t.Isotope=e(t,t.Outlayer,t.getSize,t.matchesSelector,t.fizzyUIUtils,t.Isotope.Item,t.Isotope.LayoutMode)}(window,function t(e,i,n,o,r,a,s){function l(t,e){return function i(n,o){for(var r=0;r<t.length;r++){var a=t[r],s=n.sortData[a],l=o.sortData[a];if(s>l||s<l){var d=void 0!==e[a]?e[a]:e,c=d?1:-1;return(s>l?1:-1)*c}}return 0}}var d=e.jQuery,c=String.prototype.trim?function(t){return t.trim()}:function(t){return t.replace(/^\s+|\s+$/g,"")},u=i.create("isotope",{layoutMode:"masonry",isJQueryFiltering:!0,sortAscending:!0});u.Item=a,u.LayoutMode=s;var h=u.prototype;h._create=function(){this.itemGUID=0,this._sorters={},this._getSorters(),i.prototype._create.call(this),this.modes={},this.filteredItems=this.items,this.sortHistory=["original-order"];for(var t in s.modes)this._initLayoutMode(t)},h.reloadItems=function(){this.itemGUID=0,i.prototype.reloadItems.call(this)},h._itemize=function(){for(var t=i.prototype._itemize.apply(this,arguments),e=0;e<t.length;e++){
var n=t[e];n.id=this.itemGUID++}return this._updateItemsSortData(t),t},h._initLayoutMode=function(t){var e=s.modes[t],i=this.options[t]||{};this.options[t]=e.options?r.extend(e.options,i):i,this.modes[t]=new e(this)},h.layout=function(){return!this._isLayoutInited&&this._getOption("initLayout")?void this.arrange():void this._layout()},h._layout=function(){var t=this._getIsInstant();this._resetLayout(),this._manageStamps(),this.layoutItems(this.filteredItems,t),this._isLayoutInited=!0},h.arrange=function(t){this.option(t),this._getIsInstant();var e=this._filter(this.items);this.filteredItems=e.matches,this._bindArrangeComplete(),this._isInstant?this._noTransition(this._hideReveal,[e]):this._hideReveal(e),this._sort(),this._layout()},h._init=h.arrange,h._hideReveal=function(t){this.reveal(t.needReveal),this.hide(t.needHide)},h._getIsInstant=function(){var t=this._getOption("layoutInstant"),e=void 0!==t?t:!this._isLayoutInited;return this._isInstant=e,e},h._bindArrangeComplete=function(){function t(){e&&i&&n&&o.dispatchEvent("arrangeComplete",null,[o.filteredItems])}var e,i,n,o=this;this.once("layoutComplete",function(){e=!0,t()}),this.once("hideComplete",function(){i=!0,t()}),this.once("revealComplete",function(){n=!0,t()})},h._filter=function(t){var e=this.options.filter;e=e||"*";for(var i=[],n=[],o=[],r=this._getFilterTest(e),a=0;a<t.length;a++){var s=t[a];if(!s.isIgnored){var l=r(s);l&&i.push(s),l&&s.isHidden?n.push(s):l||s.isHidden||o.push(s)}}return{matches:i,needReveal:n,needHide:o}},h._getFilterTest=function(t){return d&&this.options.isJQueryFiltering?function(e){return d(e.element).is(t)}:"function"==typeof t?function(e){return t(e.element)}:function(e){return o(e.element,t)}},h.updateSortData=function(t){var e;t?(t=r.makeArray(t),e=this.getItems(t)):e=this.items,this._getSorters(),this._updateItemsSortData(e)},h._getSorters=function(){var t=this.options.getSortData;for(var e in t){var i=t[e];this._sorters[e]=p(i)}},h._updateItemsSortData=function(t){for(var e=t&&t.length,i=0;e&&i<e;i++){var n=t[i];n.updateSortData()}};var p=function(){function t(t){if("string"!=typeof t)return t;var i=c(t).split(" "),n=i[0],o=n.match(/^\[(.+)\]$/),r=o&&o[1],a=e(r,n),s=u.sortDataParsers[i[1]];return t=s?function(t){return t&&s(a(t))}:function(t){return t&&a(t)}}function e(t,e){return t?function e(i){return i.getAttribute(t)}:function t(i){var n=i.querySelector(e);return n&&n.textContent}}return t}();u.sortDataParsers={parseInt:function(t){return parseInt(t,10)},parseFloat:function(t){return parseFloat(t)}},h._sort=function(){var t=this.options.sortBy;if(t){var e=[].concat.apply(t,this.sortHistory),i=l(e,this.options.sortAscending);this.filteredItems.sort(i),t!=this.sortHistory[0]&&this.sortHistory.unshift(t)}},h._mode=function(){var t=this.options.layoutMode,e=this.modes[t];if(!e)throw new Error("No layout mode: "+t);return e.options=this.options[t],e},h._resetLayout=function(){i.prototype._resetLayout.call(this),this._mode()._resetLayout()},h._getItemLayoutPosition=function(t){return this._mode()._getItemLayoutPosition(t)},h._manageStamp=function(t){this._mode()._manageStamp(t)},h._getContainerSize=function(){return this._mode()._getContainerSize()},h.needsResizeLayout=function(){return this._mode().needsResizeLayout()},h.appended=function(t){var e=this.addItems(t);if(e.length){var i=this._filterRevealAdded(e);this.filteredItems=this.filteredItems.concat(i)}},h.prepended=function(t){var e=this._itemize(t);if(e.length){this._resetLayout(),this._manageStamps();var i=this._filterRevealAdded(e);this.layoutItems(this.filteredItems),this.filteredItems=i.concat(this.filteredItems),this.items=e.concat(this.items)}},h._filterRevealAdded=function(t){var e=this._filter(t);return this.hide(e.needHide),this.reveal(e.matches),this.layoutItems(e.matches,!0),e.matches},h.insert=function(t){var e=this.addItems(t);if(e.length){var i,n,o=e.length;for(i=0;i<o;i++)n=e[i],this.element.appendChild(n.element);var r=this._filter(e).matches;for(i=0;i<o;i++)e[i].isLayoutInstant=!0;for(this.arrange(),i=0;i<o;i++)delete e[i].isLayoutInstant;this.reveal(r)}};var f=h.remove;return h.remove=function(t){t=r.makeArray(t);var e=this.getItems(t);f.call(this,t);for(var i=e&&e.length,n=0;i&&n<i;n++){var o=e[n];r.removeFrom(this.filteredItems,o)}},h.shuffle=function(){for(var t=0;t<this.items.length;t++){var e=this.items[t];e.sortData.random=Math.random()}this.options.sortBy="random",this._sort(),this._layout()},h._noTransition=function(t,e){var i=this.options.transitionDuration;this.options.transitionDuration=0;var n=t.apply(this,e);return this.options.transitionDuration=i,n},h.getFilteredItemElements=function(){return this.filteredItems.map(function(t){return t.element})},u}),!function(t){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=t();else if("function"==typeof define&&define.amd)define([],t);else{var e;e="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this,e.Chart=t()}}(function(){return function t(e,i,n){function o(a,s){if(!i[a]){if(!e[a]){var l="function"==typeof require&&require;if(!s&&l)return l(a,!0);if(r)return r(a,!0);var d=new Error("Cannot find module '"+a+"'");throw d.code="MODULE_NOT_FOUND",d}var c=i[a]={exports:{}};e[a][0].call(c.exports,function(t){var i=e[a][1][t];return o(i?i:t)},c,c.exports,t,e,i,n)}return i[a].exports}for(var r="function"==typeof require&&require,a=0;a<n.length;a++)o(n[a]);return o}({1:[function(t,e,i){},{}],2:[function(t,e,i){function n(t){if(t){var e=/^#([a-fA-F0-9]{3})$/,i=/^#([a-fA-F0-9]{6})$/,n=/^rgba?\(\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/,o=/^rgba?\(\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/,r=/(\w+)/,a=[0,0,0],s=1,l=t.match(e);if(l){l=l[1];for(var d=0;d<a.length;d++)a[d]=parseInt(l[d]+l[d],16)}else if(l=t.match(i)){l=l[1];for(var d=0;d<a.length;d++)a[d]=parseInt(l.slice(2*d,2*d+2),16)}else if(l=t.match(n)){for(var d=0;d<a.length;d++)a[d]=parseInt(l[d+1]);s=parseFloat(l[4])}else if(l=t.match(o)){for(var d=0;d<a.length;d++)a[d]=Math.round(2.55*parseFloat(l[d+1]));s=parseFloat(l[4])}else if(l=t.match(r)){if("transparent"==l[1])return[0,0,0,0];if(a=x[l[1]],!a)return}for(var d=0;d<a.length;d++)a[d]=y(a[d],0,255);return s=s||0==s?y(s,0,1):1,a[3]=s,a}}function o(t){if(t){var e=/^hsla?\(\s*([+-]?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)/,i=t.match(e);if(i){var n=parseFloat(i[4]),o=y(parseInt(i[1]),0,360),r=y(parseFloat(i[2]),0,100),a=y(parseFloat(i[3]),0,100),s=y(isNaN(n)?1:n,0,1);return[o,r,a,s]}}}function r(t){if(t){var e=/^hwb\(\s*([+-]?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)/,i=t.match(e);if(i){var n=parseFloat(i[4]),o=y(parseInt(i[1]),0,360),r=y(parseFloat(i[2]),0,100),a=y(parseFloat(i[3]),0,100),s=y(isNaN(n)?1:n,0,1);return[o,r,a,s]}}}function a(t){var e=n(t);return e&&e.slice(0,3)}function s(t){var e=o(t);return e&&e.slice(0,3)}function l(t){var e=n(t);return e?e[3]:(e=o(t))?e[3]:(e=r(t))?e[3]:void 0}function d(t){return"#"+b(t[0])+b(t[1])+b(t[2])}function c(t,e){return 1>e||t[3]&&t[3]<1?u(t,e):"rgb("+t[0]+", "+t[1]+", "+t[2]+")"}function u(t,e){return void 0===e&&(e=void 0!==t[3]?t[3]:1),"rgba("+t[0]+", "+t[1]+", "+t[2]+", "+e+")"}function h(t,e){if(1>e||t[3]&&t[3]<1)return p(t,e);var i=Math.round(t[0]/255*100),n=Math.round(t[1]/255*100),o=Math.round(t[2]/255*100);return"rgb("+i+"%, "+n+"%, "+o+"%)"}function p(t,e){var i=Math.round(t[0]/255*100),n=Math.round(t[1]/255*100),o=Math.round(t[2]/255*100);return"rgba("+i+"%, "+n+"%, "+o+"%, "+(e||t[3]||1)+")"}function f(t,e){return 1>e||t[3]&&t[3]<1?g(t,e):"hsl("+t[0]+", "+t[1]+"%, "+t[2]+"%)"}function g(t,e){return void 0===e&&(e=void 0!==t[3]?t[3]:1),"hsla("+t[0]+", "+t[1]+"%, "+t[2]+"%, "+e+")"}function m(t,e){return void 0===e&&(e=void 0!==t[3]?t[3]:1),"hwb("+t[0]+", "+t[1]+"%, "+t[2]+"%"+(void 0!==e&&1!==e?", "+e:"")+")"}function v(t){return w[t.slice(0,3)]}function y(t,e,i){return Math.min(Math.max(e,t),i)}function b(t){var e=t.toString(16).toUpperCase();return e.length<2?"0"+e:e}var x=t(6);e.exports={getRgba:n,getHsla:o,getRgb:a,getHsl:s,getHwb:r,getAlpha:l,hexString:d,rgbString:c,rgbaString:u,percentString:h,percentaString:p,hslString:f,hslaString:g,hwbString:m,keyword:v};var w={};for(var k in x)w[x[k]]=k},{6:6}],3:[function(t,e,i){var n=t(5),o=t(2),r=function(t){if(t instanceof r)return t;if(!(this instanceof r))return new r(t);this.values={rgb:[0,0,0],hsl:[0,0,0],hsv:[0,0,0],hwb:[0,0,0],cmyk:[0,0,0,0],alpha:1};var e;if("string"==typeof t)if(e=o.getRgba(t))this.setValues("rgb",e);else if(e=o.getHsla(t))this.setValues("hsl",e);else{if(!(e=o.getHwb(t)))throw new Error('Unable to parse color from string "'+t+'"');this.setValues("hwb",e)}else if("object"==typeof t)if(e=t,void 0!==e.r||void 0!==e.red)this.setValues("rgb",e);else if(void 0!==e.l||void 0!==e.lightness)this.setValues("hsl",e);else if(void 0!==e.v||void 0!==e.value)this.setValues("hsv",e);else if(void 0!==e.w||void 0!==e.whiteness)this.setValues("hwb",e);else{if(void 0===e.c&&void 0===e.cyan)throw new Error("Unable to parse color from object "+JSON.stringify(t));this.setValues("cmyk",e)}};r.prototype={rgb:function(){return this.setSpace("rgb",arguments)},hsl:function(){return this.setSpace("hsl",arguments)},hsv:function(){return this.setSpace("hsv",arguments)},hwb:function(){return this.setSpace("hwb",arguments)},cmyk:function(){return this.setSpace("cmyk",arguments)},rgbArray:function(){return this.values.rgb},hslArray:function(){return this.values.hsl},hsvArray:function(){return this.values.hsv},hwbArray:function(){var t=this.values;return 1!==t.alpha?t.hwb.concat([t.alpha]):t.hwb},cmykArray:function(){return this.values.cmyk},rgbaArray:function(){var t=this.values;return t.rgb.concat([t.alpha])},hslaArray:function(){var t=this.values;return t.hsl.concat([t.alpha])},alpha:function(t){return void 0===t?this.values.alpha:(this.setValues("alpha",t),this)},red:function(t){return this.setChannel("rgb",0,t)},green:function(t){return this.setChannel("rgb",1,t)},blue:function(t){return this.setChannel("rgb",2,t)},hue:function(t){return t&&(t%=360,t=0>t?360+t:t),this.setChannel("hsl",0,t)},saturation:function(t){return this.setChannel("hsl",1,t)},lightness:function(t){return this.setChannel("hsl",2,t)},saturationv:function(t){return this.setChannel("hsv",1,t)},whiteness:function(t){return this.setChannel("hwb",1,t)},blackness:function(t){return this.setChannel("hwb",2,t)},value:function(t){return this.setChannel("hsv",2,t)},cyan:function(t){return this.setChannel("cmyk",0,t)},magenta:function(t){return this.setChannel("cmyk",1,t)},yellow:function(t){return this.setChannel("cmyk",2,t)},black:function(t){return this.setChannel("cmyk",3,t)},hexString:function(){return o.hexString(this.values.rgb)},rgbString:function(){return o.rgbString(this.values.rgb,this.values.alpha)},rgbaString:function(){return o.rgbaString(this.values.rgb,this.values.alpha)},percentString:function(){return o.percentString(this.values.rgb,this.values.alpha)},hslString:function(){return o.hslString(this.values.hsl,this.values.alpha)},hslaString:function(){return o.hslaString(this.values.hsl,this.values.alpha)},hwbString:function(){return o.hwbString(this.values.hwb,this.values.alpha)},keyword:function(){return o.keyword(this.values.rgb,this.values.alpha)},rgbNumber:function(){var t=this.values.rgb;return t[0]<<16|t[1]<<8|t[2]},luminosity:function(){for(var t=this.values.rgb,e=[],i=0;i<t.length;i++){var n=t[i]/255;e[i]=.03928>=n?n/12.92:Math.pow((n+.055)/1.055,2.4)}return.2126*e[0]+.7152*e[1]+.0722*e[2]},contrast:function(t){var e=this.luminosity(),i=t.luminosity();return e>i?(e+.05)/(i+.05):(i+.05)/(e+.05)},level:function(t){var e=this.contrast(t);return e>=7.1?"AAA":e>=4.5?"AA":""},dark:function(){var t=this.values.rgb,e=(299*t[0]+587*t[1]+114*t[2])/1e3;return 128>e},light:function(){return!this.dark()},negate:function(){for(var t=[],e=0;3>e;e++)t[e]=255-this.values.rgb[e];return this.setValues("rgb",t),this},lighten:function(t){var e=this.values.hsl;return e[2]+=e[2]*t,this.setValues("hsl",e),this},darken:function(t){var e=this.values.hsl;return e[2]-=e[2]*t,this.setValues("hsl",e),this},saturate:function(t){var e=this.values.hsl;return e[1]+=e[1]*t,this.setValues("hsl",e),this},desaturate:function(t){var e=this.values.hsl;return e[1]-=e[1]*t,this.setValues("hsl",e),this},whiten:function(t){var e=this.values.hwb;return e[1]+=e[1]*t,this.setValues("hwb",e),this},blacken:function(t){var e=this.values.hwb;return e[2]+=e[2]*t,this.setValues("hwb",e),this},greyscale:function(){var t=this.values.rgb,e=.3*t[0]+.59*t[1]+.11*t[2];return this.setValues("rgb",[e,e,e]),this},clearer:function(t){var e=this.values.alpha;return this.setValues("alpha",e-e*t),this},opaquer:function(t){var e=this.values.alpha;return this.setValues("alpha",e+e*t),this},rotate:function(t){var e=this.values.hsl,i=(e[0]+t)%360;return e[0]=0>i?360+i:i,this.setValues("hsl",e),this},mix:function(t,e){var i=this,n=t,o=void 0===e?.5:e,r=2*o-1,a=i.alpha()-n.alpha(),s=((r*a===-1?r:(r+a)/(1+r*a))+1)/2,l=1-s;return this.rgb(s*i.red()+l*n.red(),s*i.green()+l*n.green(),s*i.blue()+l*n.blue()).alpha(i.alpha()*o+n.alpha()*(1-o))},toJSON:function(){return this.rgb()},clone:function(){var t,e,i=new r,n=this.values,o=i.values;for(var a in n)n.hasOwnProperty(a)&&(t=n[a],e={}.toString.call(t),"[object Array]"===e?o[a]=t.slice(0):"[object Number]"===e?o[a]=t:console.error("unexpected color value:",t));return i}},r.prototype.spaces={rgb:["red","green","blue"],hsl:["hue","saturation","lightness"],hsv:["hue","saturation","value"],hwb:["hue","whiteness","blackness"],cmyk:["cyan","magenta","yellow","black"]},r.prototype.maxes={rgb:[255,255,255],hsl:[360,100,100],hsv:[360,100,100],hwb:[360,100,100],cmyk:[100,100,100,100]},r.prototype.getValues=function(t){for(var e=this.values,i={},n=0;n<t.length;n++)i[t.charAt(n)]=e[t][n];return 1!==e.alpha&&(i.a=e.alpha),i},r.prototype.setValues=function(t,e){var i,o=this.values,r=this.spaces,a=this.maxes,s=1;if("alpha"===t)s=e;else if(e.length)o[t]=e.slice(0,t.length),s=e[t.length];else if(void 0!==e[t.charAt(0)]){for(i=0;i<t.length;i++)o[t][i]=e[t.charAt(i)];s=e.a}else if(void 0!==e[r[t][0]]){var l=r[t];for(i=0;i<t.length;i++)o[t][i]=e[l[i]];s=e.alpha}if(o.alpha=Math.max(0,Math.min(1,void 0===s?o.alpha:s)),"alpha"===t)return!1;var d;for(i=0;i<t.length;i++)d=Math.max(0,Math.min(a[t][i],o[t][i])),o[t][i]=Math.round(d);for(var c in r)c!==t&&(o[c]=n[t][c](o[t]));return!0},r.prototype.setSpace=function(t,e){var i=e[0];return void 0===i?this.getValues(t):("number"==typeof i&&(i=Array.prototype.slice.call(e)),this.setValues(t,i),this)},r.prototype.setChannel=function(t,e,i){var n=this.values[t];return void 0===i?n[e]:i===n[e]?this:(n[e]=i,this.setValues(t,n),this)},"undefined"!=typeof window&&(window.Color=r),e.exports=r},{2:2,5:5}],4:[function(t,e,i){function n(t){var e,i,n,o=t[0]/255,r=t[1]/255,a=t[2]/255,s=Math.min(o,r,a),l=Math.max(o,r,a),d=l-s;return l==s?e=0:o==l?e=(r-a)/d:r==l?e=2+(a-o)/d:a==l&&(e=4+(o-r)/d),e=Math.min(60*e,360),0>e&&(e+=360),n=(s+l)/2,i=l==s?0:.5>=n?d/(l+s):d/(2-l-s),[e,100*i,100*n]}function o(t){var e,i,n,o=t[0],r=t[1],a=t[2],s=Math.min(o,r,a),l=Math.max(o,r,a),d=l-s;return i=0==l?0:d/l*1e3/10,l==s?e=0:o==l?e=(r-a)/d:r==l?e=2+(a-o)/d:a==l&&(e=4+(o-r)/d),e=Math.min(60*e,360),0>e&&(e+=360),n=l/255*1e3/10,[e,i,n]}function a(t){var e=t[0],i=t[1],o=t[2],r=n(t)[0],a=1/255*Math.min(e,Math.min(i,o)),o=1-1/255*Math.max(e,Math.max(i,o));return[r,100*a,100*o]}function s(t){var e,i,n,o,r=t[0]/255,a=t[1]/255,s=t[2]/255;return o=Math.min(1-r,1-a,1-s),e=(1-r-o)/(1-o)||0,i=(1-a-o)/(1-o)||0,n=(1-s-o)/(1-o)||0,[100*e,100*i,100*n,100*o]}function l(t){return Z[JSON.stringify(t)]}function d(t){var e=t[0]/255,i=t[1]/255,n=t[2]/255;e=e>.04045?Math.pow((e+.055)/1.055,2.4):e/12.92,i=i>.04045?Math.pow((i+.055)/1.055,2.4):i/12.92,n=n>.04045?Math.pow((n+.055)/1.055,2.4):n/12.92;var o=.4124*e+.3576*i+.1805*n,r=.2126*e+.7152*i+.0722*n,a=.0193*e+.1192*i+.9505*n;return[100*o,100*r,100*a]}function c(t){var e,i,n,o=d(t),r=o[0],a=o[1],s=o[2];return r/=95.047,a/=100,s/=108.883,r=r>.008856?Math.pow(r,1/3):7.787*r+16/116,a=a>.008856?Math.pow(a,1/3):7.787*a+16/116,s=s>.008856?Math.pow(s,1/3):7.787*s+16/116,e=116*a-16,i=500*(r-a),n=200*(a-s),[e,i,n]}function u(t){return N(c(t))}function h(t){var e,i,n,o,r,a=t[0]/360,s=t[1]/100,l=t[2]/100;if(0==s)return r=255*l,[r,r,r];i=.5>l?l*(1+s):l+s-l*s,e=2*l-i,o=[0,0,0];for(var d=0;3>d;d++)n=a+1/3*-(d-1),0>n&&n++,n>1&&n--,r=1>6*n?e+6*(i-e)*n:1>2*n?i:2>3*n?e+(i-e)*(2/3-n)*6:e,o[d]=255*r;return o}function p(t){var e,i,n=t[0],o=t[1]/100,r=t[2]/100;return 0===r?[0,0,0]:(r*=2,o*=1>=r?r:2-r,i=(r+o)/2,e=2*o/(r+o),[n,100*e,100*i])}function f(t){return a(h(t))}function m(t){return s(h(t))}function v(t){return l(h(t))}function y(t){var e=t[0]/60,i=t[1]/100,n=t[2]/100,o=Math.floor(e)%6,r=e-Math.floor(e),a=255*n*(1-i),s=255*n*(1-i*r),l=255*n*(1-i*(1-r)),n=255*n;switch(o){case 0:return[n,l,a];case 1:return[s,n,a];case 2:return[a,n,l];case 3:return[a,s,n];case 4:return[l,a,n];case 5:return[n,a,s]}}function x(t){var e,i,n=t[0],o=t[1]/100,r=t[2]/100;return i=(2-o)*r,e=o*r,e/=1>=i?i:2-i,e=e||0,i/=2,[n,100*e,100*i]}function w(t){return a(y(t))}function k(t){return s(y(t))}function S(t){return l(y(t))}function C(t){var e,i,n,o,a=t[0]/360,s=t[1]/100,l=t[2]/100,d=s+l;switch(d>1&&(s/=d,l/=d),e=Math.floor(6*a),i=1-l,n=6*a-e,0!=(1&e)&&(n=1-n),o=s+n*(i-s),e){default:case 6:case 0:r=i,g=o,b=s;break;case 1:r=o,g=i,b=s;break;case 2:r=s,g=i,b=o;break;case 3:r=s,g=o,b=i;break;case 4:r=o,g=s,b=i;break;case 5:r=i,g=s,b=o}return[255*r,255*g,255*b]}function T(t){return n(C(t))}function I(t){return o(C(t))}function M(t){return s(C(t))}function A(t){return l(C(t))}function _(t){var e,i,n,o=t[0]/100,r=t[1]/100,a=t[2]/100,s=t[3]/100;return e=1-Math.min(1,o*(1-s)+s),i=1-Math.min(1,r*(1-s)+s),n=1-Math.min(1,a*(1-s)+s),[255*e,255*i,255*n]}function D(t){return n(_(t))}function P(t){return o(_(t))}function E(t){return a(_(t))}function O(t){return l(_(t))}function L(t){var e,i,n,o=t[0]/100,r=t[1]/100,a=t[2]/100;return e=3.2406*o+-1.5372*r+a*-.4986,i=o*-.9689+1.8758*r+.0415*a,n=.0557*o+r*-.204+1.057*a,e=e>.0031308?1.055*Math.pow(e,1/2.4)-.055:e=12.92*e,i=i>.0031308?1.055*Math.pow(i,1/2.4)-.055:i=12.92*i,n=n>.0031308?1.055*Math.pow(n,1/2.4)-.055:n=12.92*n,e=Math.min(Math.max(0,e),1),i=Math.min(Math.max(0,i),1),n=Math.min(Math.max(0,n),1),[255*e,255*i,255*n]}function z(t){var e,i,n,o=t[0],r=t[1],a=t[2];return o/=95.047,r/=100,a/=108.883,o=o>.008856?Math.pow(o,1/3):7.787*o+16/116,r=r>.008856?Math.pow(r,1/3):7.787*r+16/116,a=a>.008856?Math.pow(a,1/3):7.787*a+16/116,e=116*r-16,i=500*(o-r),n=200*(r-a),[e,i,n]}function F(t){return N(z(t))}function R(t){var e,i,n,o,r=t[0],a=t[1],s=t[2];return 8>=r?(i=100*r/903.3,o=7.787*(i/100)+16/116):(i=100*Math.pow((r+16)/116,3),o=Math.pow(i/100,1/3)),e=.008856>=e/95.047?e=95.047*(a/500+o-16/116)/7.787:95.047*Math.pow(a/500+o,3),n=.008859>=n/108.883?n=108.883*(o-s/200-16/116)/7.787:108.883*Math.pow(o-s/200,3),[e,i,n]}function N(t){var e,i,n,o=t[0],r=t[1],a=t[2];return e=Math.atan2(a,r),i=360*e/2/Math.PI,0>i&&(i+=360),n=Math.sqrt(r*r+a*a),[o,n,i]}function W(t){return L(R(t))}function B(t){var e,i,n,o=t[0],r=t[1],a=t[2];return n=a/360*2*Math.PI,e=r*Math.cos(n),i=r*Math.sin(n),[o,e,i]}function j(t){return R(B(t))}function V(t){return W(B(t))}function H(t){return K[t]}function U(t){return n(H(t))}function q(t){return o(H(t))}function Y(t){return a(H(t))}function Q(t){return s(H(t))}function X(t){return c(H(t))}function G(t){return d(H(t))}e.exports={rgb2hsl:n,rgb2hsv:o,rgb2hwb:a,rgb2cmyk:s,rgb2keyword:l,rgb2xyz:d,rgb2lab:c,rgb2lch:u,hsl2rgb:h,hsl2hsv:p,hsl2hwb:f,hsl2cmyk:m,hsl2keyword:v,hsv2rgb:y,hsv2hsl:x,hsv2hwb:w,hsv2cmyk:k,hsv2keyword:S,hwb2rgb:C,hwb2hsl:T,hwb2hsv:I,hwb2cmyk:M,hwb2keyword:A,cmyk2rgb:_,cmyk2hsl:D,cmyk2hsv:P,cmyk2hwb:E,cmyk2keyword:O,keyword2rgb:H,keyword2hsl:U,keyword2hsv:q,keyword2hwb:Y,keyword2cmyk:Q,keyword2lab:X,keyword2xyz:G,xyz2rgb:L,xyz2lab:z,xyz2lch:F,lab2xyz:R,lab2rgb:W,lab2lch:N,lch2lab:B,lch2xyz:j,lch2rgb:V};var K={aliceblue:[240,248,255],antiquewhite:[250,235,215],aqua:[0,255,255],aquamarine:[127,255,212],azure:[240,255,255],beige:[245,245,220],bisque:[255,228,196],black:[0,0,0],blanchedalmond:[255,235,205],blue:[0,0,255],blueviolet:[138,43,226],brown:[165,42,42],burlywood:[222,184,135],cadetblue:[95,158,160],chartreuse:[127,255,0],chocolate:[210,105,30],coral:[255,127,80],cornflowerblue:[100,149,237],cornsilk:[255,248,220],crimson:[220,20,60],cyan:[0,255,255],darkblue:[0,0,139],darkcyan:[0,139,139],darkgoldenrod:[184,134,11],darkgray:[169,169,169],darkgreen:[0,100,0],darkgrey:[169,169,169],darkkhaki:[189,183,107],darkmagenta:[139,0,139],darkolivegreen:[85,107,47],darkorange:[255,140,0],darkorchid:[153,50,204],darkred:[139,0,0],darksalmon:[233,150,122],darkseagreen:[143,188,143],darkslateblue:[72,61,139],darkslategray:[47,79,79],darkslategrey:[47,79,79],darkturquoise:[0,206,209],darkviolet:[148,0,211],deeppink:[255,20,147],deepskyblue:[0,191,255],dimgray:[105,105,105],dimgrey:[105,105,105],dodgerblue:[30,144,255],firebrick:[178,34,34],floralwhite:[255,250,240],forestgreen:[34,139,34],fuchsia:[255,0,255],gainsboro:[220,220,220],ghostwhite:[248,248,255],gold:[255,215,0],goldenrod:[218,165,32],gray:[128,128,128],green:[0,128,0],greenyellow:[173,255,47],grey:[128,128,128],honeydew:[240,255,240],hotpink:[255,105,180],indianred:[205,92,92],indigo:[75,0,130],ivory:[255,255,240],khaki:[240,230,140],lavender:[230,230,250],lavenderblush:[255,240,245],lawngreen:[124,252,0],lemonchiffon:[255,250,205],lightblue:[173,216,230],lightcoral:[240,128,128],lightcyan:[224,255,255],lightgoldenrodyellow:[250,250,210],lightgray:[211,211,211],lightgreen:[144,238,144],lightgrey:[211,211,211],lightpink:[255,182,193],lightsalmon:[255,160,122],lightseagreen:[32,178,170],lightskyblue:[135,206,250],lightslategray:[119,136,153],lightslategrey:[119,136,153],lightsteelblue:[176,196,222],lightyellow:[255,255,224],lime:[0,255,0],limegreen:[50,205,50],linen:[250,240,230],magenta:[255,0,255],maroon:[128,0,0],mediumaquamarine:[102,205,170],mediumblue:[0,0,205],mediumorchid:[186,85,211],mediumpurple:[147,112,219],mediumseagreen:[60,179,113],mediumslateblue:[123,104,238],mediumspringgreen:[0,250,154],mediumturquoise:[72,209,204],mediumvioletred:[199,21,133],midnightblue:[25,25,112],mintcream:[245,255,250],mistyrose:[255,228,225],moccasin:[255,228,181],navajowhite:[255,222,173],navy:[0,0,128],oldlace:[253,245,230],olive:[128,128,0],olivedrab:[107,142,35],orange:[255,165,0],orangered:[255,69,0],orchid:[218,112,214],palegoldenrod:[238,232,170],palegreen:[152,251,152],paleturquoise:[175,238,238],palevioletred:[219,112,147],papayawhip:[255,239,213],peachpuff:[255,218,185],peru:[205,133,63],pink:[255,192,203],plum:[221,160,221],powderblue:[176,224,230],purple:[128,0,128],rebeccapurple:[102,51,153],red:[255,0,0],rosybrown:[188,143,143],royalblue:[65,105,225],saddlebrown:[139,69,19],salmon:[250,128,114],sandybrown:[244,164,96],seagreen:[46,139,87],seashell:[255,245,238],sienna:[160,82,45],silver:[192,192,192],skyblue:[135,206,235],slateblue:[106,90,205],slategray:[112,128,144],slategrey:[112,128,144],snow:[255,250,250],springgreen:[0,255,127],steelblue:[70,130,180],tan:[210,180,140],teal:[0,128,128],thistle:[216,191,216],tomato:[255,99,71],turquoise:[64,224,208],violet:[238,130,238],wheat:[245,222,179],white:[255,255,255],whitesmoke:[245,245,245],yellow:[255,255,0],yellowgreen:[154,205,50]},Z={};for(var J in K)Z[JSON.stringify(K[J])]=J},{}],5:[function(t,e,i){var n=t(4),o=function(){return new d};for(var r in n){o[r+"Raw"]=function(t){return function(e){return"number"==typeof e&&(e=Array.prototype.slice.call(arguments)),n[t](e)}}(r);var a=/(\w+)2(\w+)/.exec(r),s=a[1],l=a[2];o[s]=o[s]||{},o[s][l]=o[r]=function(t){return function(e){"number"==typeof e&&(e=Array.prototype.slice.call(arguments));var i=n[t](e);if("string"==typeof i||void 0===i)return i;for(var o=0;o<i.length;o++)i[o]=Math.round(i[o]);return i}}(r)}var d=function(){this.convs={}};d.prototype.routeSpace=function(t,e){var i=e[0];return void 0===i?this.getValues(t):("number"==typeof i&&(i=Array.prototype.slice.call(e)),this.setValues(t,i))},d.prototype.setValues=function(t,e){return this.space=t,this.convs={},this.convs[t]=e,this},d.prototype.getValues=function(t){var e=this.convs[t];if(!e){var i=this.space,n=this.convs[i];e=o[i][t](n),this.convs[t]=e}return e},["rgb","hsl","hsv","cmyk","keyword"].forEach(function(t){d.prototype[t]=function(e){return this.routeSpace(t,arguments)}}),e.exports=o},{4:4}],6:[function(t,e,i){e.exports={aliceblue:[240,248,255],antiquewhite:[250,235,215],aqua:[0,255,255],aquamarine:[127,255,212],azure:[240,255,255],beige:[245,245,220],bisque:[255,228,196],black:[0,0,0],blanchedalmond:[255,235,205],blue:[0,0,255],blueviolet:[138,43,226],brown:[165,42,42],burlywood:[222,184,135],cadetblue:[95,158,160],chartreuse:[127,255,0],chocolate:[210,105,30],coral:[255,127,80],cornflowerblue:[100,149,237],cornsilk:[255,248,220],crimson:[220,20,60],cyan:[0,255,255],darkblue:[0,0,139],darkcyan:[0,139,139],darkgoldenrod:[184,134,11],darkgray:[169,169,169],darkgreen:[0,100,0],darkgrey:[169,169,169],darkkhaki:[189,183,107],darkmagenta:[139,0,139],darkolivegreen:[85,107,47],darkorange:[255,140,0],darkorchid:[153,50,204],darkred:[139,0,0],darksalmon:[233,150,122],darkseagreen:[143,188,143],darkslateblue:[72,61,139],darkslategray:[47,79,79],darkslategrey:[47,79,79],darkturquoise:[0,206,209],darkviolet:[148,0,211],deeppink:[255,20,147],deepskyblue:[0,191,255],dimgray:[105,105,105],dimgrey:[105,105,105],dodgerblue:[30,144,255],firebrick:[178,34,34],floralwhite:[255,250,240],forestgreen:[34,139,34],fuchsia:[255,0,255],gainsboro:[220,220,220],ghostwhite:[248,248,255],gold:[255,215,0],goldenrod:[218,165,32],gray:[128,128,128],green:[0,128,0],greenyellow:[173,255,47],grey:[128,128,128],honeydew:[240,255,240],hotpink:[255,105,180],indianred:[205,92,92],indigo:[75,0,130],ivory:[255,255,240],khaki:[240,230,140],lavender:[230,230,250],lavenderblush:[255,240,245],lawngreen:[124,252,0],lemonchiffon:[255,250,205],lightblue:[173,216,230],lightcoral:[240,128,128],lightcyan:[224,255,255],lightgoldenrodyellow:[250,250,210],lightgray:[211,211,211],lightgreen:[144,238,144],lightgrey:[211,211,211],lightpink:[255,182,193],lightsalmon:[255,160,122],lightseagreen:[32,178,170],lightskyblue:[135,206,250],lightslategray:[119,136,153],lightslategrey:[119,136,153],lightsteelblue:[176,196,222],lightyellow:[255,255,224],lime:[0,255,0],limegreen:[50,205,50],linen:[250,240,230],magenta:[255,0,255],maroon:[128,0,0],mediumaquamarine:[102,205,170],mediumblue:[0,0,205],mediumorchid:[186,85,211],mediumpurple:[147,112,219],mediumseagreen:[60,179,113],mediumslateblue:[123,104,238],mediumspringgreen:[0,250,154],mediumturquoise:[72,209,204],mediumvioletred:[199,21,133],midnightblue:[25,25,112],mintcream:[245,255,250],mistyrose:[255,228,225],moccasin:[255,228,181],navajowhite:[255,222,173],navy:[0,0,128],oldlace:[253,245,230],olive:[128,128,0],olivedrab:[107,142,35],orange:[255,165,0],orangered:[255,69,0],orchid:[218,112,214],palegoldenrod:[238,232,170],palegreen:[152,251,152],paleturquoise:[175,238,238],palevioletred:[219,112,147],papayawhip:[255,239,213],peachpuff:[255,218,185],peru:[205,133,63],pink:[255,192,203],plum:[221,160,221],powderblue:[176,224,230],purple:[128,0,128],rebeccapurple:[102,51,153],red:[255,0,0],rosybrown:[188,143,143],royalblue:[65,105,225],saddlebrown:[139,69,19],salmon:[250,128,114],sandybrown:[244,164,96],seagreen:[46,139,87],seashell:[255,245,238],sienna:[160,82,45],silver:[192,192,192],skyblue:[135,206,235],slateblue:[106,90,205],slategray:[112,128,144],slategrey:[112,128,144],snow:[255,250,250],springgreen:[0,255,127],steelblue:[70,130,180],tan:[210,180,140],teal:[0,128,128],thistle:[216,191,216],tomato:[255,99,71],turquoise:[64,224,208],violet:[238,130,238],wheat:[245,222,179],white:[255,255,255],whitesmoke:[245,245,245],yellow:[255,255,0],yellowgreen:[154,205,50]}},{}],7:[function(t,e,i){var n=t(28)();t(26)(n),t(22)(n),t(25)(n),t(21)(n),t(23)(n),t(24)(n),t(29)(n),t(33)(n),t(31)(n),t(34)(n),t(32)(n),t(35)(n),t(30)(n),t(27)(n),t(36)(n),t(37)(n),t(38)(n),t(39)(n),t(40)(n),t(43)(n),t(41)(n),t(42)(n),t(44)(n),t(45)(n),t(46)(n),t(15)(n),t(16)(n),t(17)(n),t(18)(n),t(19)(n),t(20)(n),t(8)(n),t(9)(n),t(10)(n),t(11)(n),t(12)(n),t(13)(n),t(14)(n),window.Chart=e.exports=n},{10:10,11:11,12:12,13:13,14:14,15:15,16:16,17:17,18:18,19:19,20:20,21:21,22:22,23:23,24:24,25:25,26:26,27:27,28:28,29:29,30:30,31:31,32:32,33:33,34:34,35:35,36:36,37:37,38:38,39:39,40:40,41:41,42:42,43:43,44:44,45:45,46:46,8:8,9:9}],8:[function(t,e,i){"use strict";e.exports=function(t){t.Bar=function(e,i){return i.type="bar",new t(e,i)}}},{}],9:[function(t,e,i){"use strict";e.exports=function(t){t.Bubble=function(e,i){return i.type="bubble",new t(e,i)}}},{}],10:[function(t,e,i){"use strict";e.exports=function(t){t.Doughnut=function(e,i){return i.type="doughnut",new t(e,i)}}},{}],11:[function(t,e,i){"use strict";e.exports=function(t){t.Line=function(e,i){return i.type="line",new t(e,i)}}},{}],12:[function(t,e,i){"use strict";e.exports=function(t){t.PolarArea=function(e,i){return i.type="polarArea",new t(e,i)}}},{}],13:[function(t,e,i){"use strict";e.exports=function(t){t.Radar=function(e,i){return i.type="radar",new t(e,i)}}},{}],14:[function(t,e,i){"use strict";e.exports=function(t){var e={hover:{mode:"single"},scales:{xAxes:[{type:"linear",position:"bottom",id:"x-axis-1"}],yAxes:[{type:"linear",position:"left",id:"y-axis-1"}]},tooltips:{callbacks:{title:function(){return""},label:function(t){return"("+t.xLabel+", "+t.yLabel+")"}}}};t.defaults.scatter=e,t.controllers.scatter=t.controllers.line,t.Scatter=function(e,i){return i.type="scatter",new t(e,i)}}},{}],15:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers;t.defaults.bar={hover:{mode:"label"},scales:{xAxes:[{type:"category",categoryPercentage:.8,barPercentage:.9,gridLines:{offsetGridLines:!0}}],yAxes:[{type:"linear"}]}},t.controllers.bar=t.DatasetController.extend({dataElementType:t.elements.Rectangle,initialize:function(e,i){t.DatasetController.prototype.initialize.call(this,e,i),this.getMeta().bar=!0},getBarCount:function(){var t=this,i=0;return e.each(t.chart.data.datasets,function(e,n){var o=t.chart.getDatasetMeta(n);o.bar&&t.chart.isDatasetVisible(n)&&++i},t),i},update:function(t){var i=this;e.each(i.getMeta().data,function(e,n){i.updateElement(e,n,t)},i)},updateElement:function(t,i,n){var o=this,r=o.getMeta(),a=o.getScaleForId(r.xAxisID),s=o.getScaleForId(r.yAxisID),l=s.getBasePixel(),d=o.chart.options.elements.rectangle,c=t.custom||{},u=o.getDataset();t._xScale=a,t._yScale=s,t._datasetIndex=o.index,t._index=i;var h=o.getRuler(i);t._model={x:o.calculateBarX(i,o.index,h),y:n?l:o.calculateBarY(i,o.index),label:o.chart.data.labels[i],datasetLabel:u.label,base:n?l:o.calculateBarBase(o.index,i),width:o.calculateBarWidth(h),backgroundColor:c.backgroundColor?c.backgroundColor:e.getValueAtIndexOrDefault(u.backgroundColor,i,d.backgroundColor),borderSkipped:c.borderSkipped?c.borderSkipped:d.borderSkipped,borderColor:c.borderColor?c.borderColor:e.getValueAtIndexOrDefault(u.borderColor,i,d.borderColor),borderWidth:c.borderWidth?c.borderWidth:e.getValueAtIndexOrDefault(u.borderWidth,i,d.borderWidth)},t.pivot()},calculateBarBase:function(t,e){var i=this,n=i.getMeta(),o=i.getScaleForId(n.yAxisID),r=0;if(o.options.stacked){for(var a=i.chart,s=a.data.datasets,l=Number(s[t].data[e]),d=0;t>d;d++){var c=s[d],u=a.getDatasetMeta(d);if(u.bar&&u.yAxisID===o.id&&a.isDatasetVisible(d)){var h=Number(c.data[e]);r+=0>l?Math.min(h,0):Math.max(h,0)}}return o.getPixelForValue(r)}return o.getBasePixel()},getRuler:function(t){var e,i=this,n=i.getMeta(),o=i.getScaleForId(n.xAxisID),r=i.getBarCount();e="category"===o.options.type?o.getPixelForTick(t+1)-o.getPixelForTick(t):o.width/o.ticks.length;var a=e*o.options.categoryPercentage,s=(e-e*o.options.categoryPercentage)/2,l=a/r;
if(o.ticks.length!==i.chart.data.labels.length){var d=o.ticks.length/i.chart.data.labels.length;l*=d}var c=l*o.options.barPercentage,u=l-l*o.options.barPercentage;return{datasetCount:r,tickWidth:e,categoryWidth:a,categorySpacing:s,fullBarWidth:l,barWidth:c,barSpacing:u}},calculateBarWidth:function(t){var e=this.getScaleForId(this.getMeta().xAxisID);return e.options.barThickness?e.options.barThickness:e.options.stacked?t.categoryWidth:t.barWidth},getBarIndex:function(t){var e,i,n=0;for(i=0;t>i;++i)e=this.chart.getDatasetMeta(i),e.bar&&this.chart.isDatasetVisible(i)&&++n;return n},calculateBarX:function(t,e,i){var n=this,o=n.getMeta(),r=n.getScaleForId(o.xAxisID),a=n.getBarIndex(e),s=r.getPixelForValue(null,t,e,n.chart.isCombo);return s-=n.chart.isCombo?i.tickWidth/2:0,r.options.stacked?s+i.categoryWidth/2+i.categorySpacing:s+i.barWidth/2+i.categorySpacing+i.barWidth*a+i.barSpacing/2+i.barSpacing*a},calculateBarY:function(t,e){var i=this,n=i.getMeta(),o=i.getScaleForId(n.yAxisID),r=Number(i.getDataset().data[t]);if(o.options.stacked){for(var a=0,s=0,l=0;e>l;l++){var d=i.chart.data.datasets[l],c=i.chart.getDatasetMeta(l);if(c.bar&&c.yAxisID===o.id&&i.chart.isDatasetVisible(l)){var u=Number(d.data[t]);0>u?s+=u||0:a+=u||0}}return 0>r?o.getPixelForValue(s+r):o.getPixelForValue(a+r)}return o.getPixelForValue(r)},draw:function(t){var e,i,n=this,o=t||1,r=n.getMeta().data,a=n.getDataset();for(e=0,i=r.length;i>e;++e){var s=a.data[e];null===s||void 0===s||isNaN(s)||r[e].transition(o).draw()}},setHoverStyle:function(t){var i=this.chart.data.datasets[t._datasetIndex],n=t._index,o=t.custom||{},r=t._model;r.backgroundColor=o.hoverBackgroundColor?o.hoverBackgroundColor:e.getValueAtIndexOrDefault(i.hoverBackgroundColor,n,e.getHoverColor(r.backgroundColor)),r.borderColor=o.hoverBorderColor?o.hoverBorderColor:e.getValueAtIndexOrDefault(i.hoverBorderColor,n,e.getHoverColor(r.borderColor)),r.borderWidth=o.hoverBorderWidth?o.hoverBorderWidth:e.getValueAtIndexOrDefault(i.hoverBorderWidth,n,r.borderWidth)},removeHoverStyle:function(t){var i=this.chart.data.datasets[t._datasetIndex],n=t._index,o=t.custom||{},r=t._model,a=this.chart.options.elements.rectangle;r.backgroundColor=o.backgroundColor?o.backgroundColor:e.getValueAtIndexOrDefault(i.backgroundColor,n,a.backgroundColor),r.borderColor=o.borderColor?o.borderColor:e.getValueAtIndexOrDefault(i.borderColor,n,a.borderColor),r.borderWidth=o.borderWidth?o.borderWidth:e.getValueAtIndexOrDefault(i.borderWidth,n,a.borderWidth)}}),t.defaults.horizontalBar={hover:{mode:"label"},scales:{xAxes:[{type:"linear",position:"bottom"}],yAxes:[{position:"left",type:"category",categoryPercentage:.8,barPercentage:.9,gridLines:{offsetGridLines:!0}}]},elements:{rectangle:{borderSkipped:"left"}},tooltips:{callbacks:{title:function(t,e){var i="";return t.length>0&&(t[0].yLabel?i=t[0].yLabel:e.labels.length>0&&t[0].index<e.labels.length&&(i=e.labels[t[0].index])),i},label:function(t,e){var i=e.datasets[t.datasetIndex].label||"";return i+": "+t.xLabel}}}},t.controllers.horizontalBar=t.controllers.bar.extend({updateElement:function(t,i,n){var o=this,r=o.getMeta(),a=o.getScaleForId(r.xAxisID),s=o.getScaleForId(r.yAxisID),l=a.getBasePixel(),d=t.custom||{},c=o.getDataset(),u=o.chart.options.elements.rectangle;t._xScale=a,t._yScale=s,t._datasetIndex=o.index,t._index=i;var h=o.getRuler(i);t._model={x:n?l:o.calculateBarX(i,o.index),y:o.calculateBarY(i,o.index,h),label:o.chart.data.labels[i],datasetLabel:c.label,base:n?l:o.calculateBarBase(o.index,i),height:o.calculateBarHeight(h),backgroundColor:d.backgroundColor?d.backgroundColor:e.getValueAtIndexOrDefault(c.backgroundColor,i,u.backgroundColor),borderSkipped:d.borderSkipped?d.borderSkipped:u.borderSkipped,borderColor:d.borderColor?d.borderColor:e.getValueAtIndexOrDefault(c.borderColor,i,u.borderColor),borderWidth:d.borderWidth?d.borderWidth:e.getValueAtIndexOrDefault(c.borderWidth,i,u.borderWidth)},t.draw=function(){function t(t){return l[(c+t)%4]}var e=this._chart.ctx,i=this._view,n=i.height/2,o=i.y-n,r=i.y+n,a=i.base-(i.base-i.x),s=i.borderWidth/2;i.borderWidth&&(o+=s,r-=s,a+=s),e.beginPath(),e.fillStyle=i.backgroundColor,e.strokeStyle=i.borderColor,e.lineWidth=i.borderWidth;var l=[[i.base,r],[i.base,o],[a,o],[a,r]],d=["bottom","left","top","right"],c=d.indexOf(i.borderSkipped,0);-1===c&&(c=0),e.moveTo.apply(e,t(0));for(var u=1;4>u;u++)e.lineTo.apply(e,t(u));e.fill(),i.borderWidth&&e.stroke()},t.pivot()},calculateBarBase:function(t,e){var i=this,n=i.getMeta(),o=i.getScaleForId(n.xAxisID),r=0;if(o.options.stacked){for(var a=i.chart,s=a.data.datasets,l=Number(s[t].data[e]),d=0;t>d;d++){var c=s[d],u=a.getDatasetMeta(d);if(u.bar&&u.xAxisID===o.id&&a.isDatasetVisible(d)){var h=Number(c.data[e]);r+=0>l?Math.min(h,0):Math.max(h,0)}}return o.getPixelForValue(r)}return o.getBasePixel()},getRuler:function(t){var e,i=this,n=i.getMeta(),o=i.getScaleForId(n.yAxisID),r=i.getBarCount();e="category"===o.options.type?o.getPixelForTick(t+1)-o.getPixelForTick(t):o.width/o.ticks.length;var a=e*o.options.categoryPercentage,s=(e-e*o.options.categoryPercentage)/2,l=a/r;if(o.ticks.length!==i.chart.data.labels.length){var d=o.ticks.length/i.chart.data.labels.length;l*=d}var c=l*o.options.barPercentage,u=l-l*o.options.barPercentage;return{datasetCount:r,tickHeight:e,categoryHeight:a,categorySpacing:s,fullBarHeight:l,barHeight:c,barSpacing:u}},calculateBarHeight:function(t){var e=this,i=e.getScaleForId(e.getMeta().yAxisID);return i.options.barThickness?i.options.barThickness:i.options.stacked?t.categoryHeight:t.barHeight},calculateBarX:function(t,e){var i=this,n=i.getMeta(),o=i.getScaleForId(n.xAxisID),r=Number(i.getDataset().data[t]);if(o.options.stacked){for(var a=0,s=0,l=0;e>l;l++){var d=i.chart.data.datasets[l],c=i.chart.getDatasetMeta(l);if(c.bar&&c.xAxisID===o.id&&i.chart.isDatasetVisible(l)){var u=Number(d.data[t]);0>u?s+=u||0:a+=u||0}}return 0>r?o.getPixelForValue(s+r):o.getPixelForValue(a+r)}return o.getPixelForValue(r)},calculateBarY:function(t,e,i){var n=this,o=n.getMeta(),r=n.getScaleForId(o.yAxisID),a=n.getBarIndex(e),s=r.getPixelForValue(null,t,e,n.chart.isCombo);return s-=n.chart.isCombo?i.tickHeight/2:0,r.options.stacked?s+i.categoryHeight/2+i.categorySpacing:s+i.barHeight/2+i.categorySpacing+i.barHeight*a+i.barSpacing/2+i.barSpacing*a}})}},{}],16:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers;t.defaults.bubble={hover:{mode:"single"},scales:{xAxes:[{type:"linear",position:"bottom",id:"x-axis-0"}],yAxes:[{type:"linear",position:"left",id:"y-axis-0"}]},tooltips:{callbacks:{title:function(){return""},label:function(t,e){var i=e.datasets[t.datasetIndex].label||"",n=e.datasets[t.datasetIndex].data[t.index];return i+": ("+t.xLabel+", "+t.yLabel+", "+n.r+")"}}}},t.controllers.bubble=t.DatasetController.extend({dataElementType:t.elements.Point,update:function(t){var i=this,n=i.getMeta(),o=n.data;e.each(o,function(e,n){i.updateElement(e,n,t)})},updateElement:function(i,n,o){var r=this,a=r.getMeta(),s=r.getScaleForId(a.xAxisID),l=r.getScaleForId(a.yAxisID),d=i.custom||{},c=r.getDataset(),u=c.data[n],h=r.chart.options.elements.point,p=r.index;e.extend(i,{_xScale:s,_yScale:l,_datasetIndex:p,_index:n,_model:{x:o?s.getPixelForDecimal(.5):s.getPixelForValue("object"==typeof u?u:NaN,n,p,r.chart.isCombo),y:o?l.getBasePixel():l.getPixelForValue(u,n,p),radius:o?0:d.radius?d.radius:r.getRadius(u),hitRadius:d.hitRadius?d.hitRadius:e.getValueAtIndexOrDefault(c.hitRadius,n,h.hitRadius)}}),t.DatasetController.prototype.removeHoverStyle.call(r,i,h);var f=i._model;f.skip=d.skip?d.skip:isNaN(f.x)||isNaN(f.y),i.pivot()},getRadius:function(t){return t.r||this.chart.options.elements.point.radius},setHoverStyle:function(i){var n=this;t.DatasetController.prototype.setHoverStyle.call(n,i);var o=n.chart.data.datasets[i._datasetIndex],r=i._index,a=i.custom||{},s=i._model;s.radius=a.hoverRadius?a.hoverRadius:e.getValueAtIndexOrDefault(o.hoverRadius,r,n.chart.options.elements.point.hoverRadius)+n.getRadius(o.data[r])},removeHoverStyle:function(e){var i=this;t.DatasetController.prototype.removeHoverStyle.call(i,e,i.chart.options.elements.point);var n=i.chart.data.datasets[e._datasetIndex].data[e._index],o=e.custom||{},r=e._model;r.radius=o.radius?o.radius:i.getRadius(n)}})}},{}],17:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers,i=t.defaults;i.doughnut={animation:{animateRotate:!0,animateScale:!1},aspectRatio:1,hover:{mode:"single"},legendCallback:function(t){var e=[];e.push('<ul class="'+t.id+'-legend">');var i=t.data,n=i.datasets,o=i.labels;if(n.length)for(var r=0;r<n[0].data.length;++r)e.push('<li><span style="background-color:'+n[0].backgroundColor[r]+'"></span>'),o[r]&&e.push(o[r]),e.push("</li>");return e.push("</ul>"),e.join("")},legend:{labels:{generateLabels:function(t){var i=t.data;return i.labels.length&&i.datasets.length?i.labels.map(function(n,o){var r=t.getDatasetMeta(0),a=i.datasets[0],s=r.data[o],l=s&&s.custom||{},d=e.getValueAtIndexOrDefault,c=t.options.elements.arc,u=l.backgroundColor?l.backgroundColor:d(a.backgroundColor,o,c.backgroundColor),h=l.borderColor?l.borderColor:d(a.borderColor,o,c.borderColor),p=l.borderWidth?l.borderWidth:d(a.borderWidth,o,c.borderWidth);return{text:n,fillStyle:u,strokeStyle:h,lineWidth:p,hidden:isNaN(a.data[o])||r.data[o].hidden,index:o}}):[]}},onClick:function(t,e){var i,n,o,r=e.index,a=this.chart;for(i=0,n=(a.data.datasets||[]).length;n>i;++i)o=a.getDatasetMeta(i),o.data[r]&&(o.data[r].hidden=!o.data[r].hidden);a.update()}},cutoutPercentage:50,rotation:Math.PI*-.5,circumference:2*Math.PI,tooltips:{callbacks:{title:function(){return""},label:function(t,i){var n=i.labels[t.index],o=": "+i.datasets[t.datasetIndex].data[t.index];return e.isArray(n)?(n=n.slice(),n[0]+=o):n+=o,n}}}},i.pie=e.clone(i.doughnut),e.extend(i.pie,{cutoutPercentage:0}),t.controllers.doughnut=t.controllers.pie=t.DatasetController.extend({dataElementType:t.elements.Arc,linkScales:e.noop,getRingIndex:function(t){for(var e=0,i=0;t>i;++i)this.chart.isDatasetVisible(i)&&++e;return e},update:function(t){var i=this,n=i.chart,o=n.chartArea,r=n.options,a=r.elements.arc,s=o.right-o.left-a.borderWidth,l=o.bottom-o.top-a.borderWidth,d=Math.min(s,l),c={x:0,y:0},u=i.getMeta(),h=r.cutoutPercentage,p=r.circumference;if(p<2*Math.PI){var f=r.rotation%(2*Math.PI);f+=2*Math.PI*(f>=Math.PI?-1:f<-Math.PI?1:0);var g=f+p,m={x:Math.cos(f),y:Math.sin(f)},v={x:Math.cos(g),y:Math.sin(g)},y=0>=f&&g>=0||f<=2*Math.PI&&2*Math.PI<=g,b=f<=.5*Math.PI&&.5*Math.PI<=g||f<=2.5*Math.PI&&2.5*Math.PI<=g,x=f<=-Math.PI&&-Math.PI<=g||f<=Math.PI&&Math.PI<=g,w=f<=.5*-Math.PI&&.5*-Math.PI<=g||f<=1.5*Math.PI&&1.5*Math.PI<=g,k=h/100,S={x:x?-1:Math.min(m.x*(m.x<0?1:k),v.x*(v.x<0?1:k)),y:w?-1:Math.min(m.y*(m.y<0?1:k),v.y*(v.y<0?1:k))},C={x:y?1:Math.max(m.x*(m.x>0?1:k),v.x*(v.x>0?1:k)),y:b?1:Math.max(m.y*(m.y>0?1:k),v.y*(v.y>0?1:k))},T={width:.5*(C.x-S.x),height:.5*(C.y-S.y)};d=Math.min(s/T.width,l/T.height),c={x:(C.x+S.x)*-.5,y:(C.y+S.y)*-.5}}n.borderWidth=i.getMaxBorderWidth(u.data),n.outerRadius=Math.max((d-n.borderWidth)/2,0),n.innerRadius=Math.max(h?n.outerRadius/100*h:1,0),n.radiusLength=(n.outerRadius-n.innerRadius)/n.getVisibleDatasetCount(),n.offsetX=c.x*n.outerRadius,n.offsetY=c.y*n.outerRadius,u.total=i.calculateTotal(),i.outerRadius=n.outerRadius-n.radiusLength*i.getRingIndex(i.index),i.innerRadius=i.outerRadius-n.radiusLength,e.each(u.data,function(e,n){i.updateElement(e,n,t)})},updateElement:function(t,i,n){var o=this,r=o.chart,a=r.chartArea,s=r.options,l=s.animation,d=(a.left+a.right)/2,c=(a.top+a.bottom)/2,u=s.rotation,h=s.rotation,p=o.getDataset(),f=n&&l.animateRotate?0:t.hidden?0:o.calculateCircumference(p.data[i])*(s.circumference/(2*Math.PI)),g=n&&l.animateScale?0:o.innerRadius,m=n&&l.animateScale?0:o.outerRadius,v=e.getValueAtIndexOrDefault;e.extend(t,{_datasetIndex:o.index,_index:i,_model:{x:d+r.offsetX,y:c+r.offsetY,startAngle:u,endAngle:h,circumference:f,outerRadius:m,innerRadius:g,label:v(p.label,i,r.data.labels[i])}});var y=t._model;this.removeHoverStyle(t),n&&l.animateRotate||(0===i?y.startAngle=s.rotation:y.startAngle=o.getMeta().data[i-1]._model.endAngle,y.endAngle=y.startAngle+y.circumference),t.pivot()},removeHoverStyle:function(e){t.DatasetController.prototype.removeHoverStyle.call(this,e,this.chart.options.elements.arc)},calculateTotal:function(){var t,i=this.getDataset(),n=this.getMeta(),o=0;return e.each(n.data,function(e,n){t=i.data[n],isNaN(t)||e.hidden||(o+=Math.abs(t))}),o},calculateCircumference:function(t){var e=this.getMeta().total;return e>0&&!isNaN(t)?2*Math.PI*(t/e):0},getMaxBorderWidth:function(t){for(var e,i,n=0,o=this.index,r=t.length,a=0;r>a;a++)e=t[a]._model?t[a]._model.borderWidth:0,i=t[a]._chart?t[a]._chart.config.data.datasets[o].hoverBorderWidth:0,n=e>n?e:n,n=i>n?i:n;return n}})}},{}],18:[function(t,e,i){"use strict";e.exports=function(t){function e(t,e){return i.getValueOrDefault(t.showLine,e.showLines)}var i=t.helpers;t.defaults.line={showLines:!0,spanGaps:!1,hover:{mode:"label"},scales:{xAxes:[{type:"category",id:"x-axis-0"}],yAxes:[{type:"linear",id:"y-axis-0"}]}},t.controllers.line=t.DatasetController.extend({datasetElementType:t.elements.Line,dataElementType:t.elements.Point,update:function(t){var n,o,r,a=this,s=a.getMeta(),l=s.dataset,d=s.data||[],c=a.chart.options,u=c.elements.line,h=a.getScaleForId(s.yAxisID),p=a.getDataset(),f=e(p,c);for(f&&(r=l.custom||{},void 0!==p.tension&&void 0===p.lineTension&&(p.lineTension=p.tension),l._scale=h,l._datasetIndex=a.index,l._children=d,l._model={spanGaps:p.spanGaps?p.spanGaps:c.spanGaps,tension:r.tension?r.tension:i.getValueOrDefault(p.lineTension,u.tension),backgroundColor:r.backgroundColor?r.backgroundColor:p.backgroundColor||u.backgroundColor,borderWidth:r.borderWidth?r.borderWidth:p.borderWidth||u.borderWidth,borderColor:r.borderColor?r.borderColor:p.borderColor||u.borderColor,borderCapStyle:r.borderCapStyle?r.borderCapStyle:p.borderCapStyle||u.borderCapStyle,borderDash:r.borderDash?r.borderDash:p.borderDash||u.borderDash,borderDashOffset:r.borderDashOffset?r.borderDashOffset:p.borderDashOffset||u.borderDashOffset,borderJoinStyle:r.borderJoinStyle?r.borderJoinStyle:p.borderJoinStyle||u.borderJoinStyle,fill:r.fill?r.fill:void 0!==p.fill?p.fill:u.fill,steppedLine:r.steppedLine?r.steppedLine:i.getValueOrDefault(p.steppedLine,u.stepped),cubicInterpolationMode:r.cubicInterpolationMode?r.cubicInterpolationMode:i.getValueOrDefault(p.cubicInterpolationMode,u.cubicInterpolationMode),scaleTop:h.top,scaleBottom:h.bottom,scaleZero:h.getBasePixel()},l.pivot()),n=0,o=d.length;o>n;++n)a.updateElement(d[n],n,t);for(f&&0!==l._model.tension&&a.updateBezierControlPoints(),n=0,o=d.length;o>n;++n)d[n].pivot()},getPointBackgroundColor:function(t,e){var n=this.chart.options.elements.point.backgroundColor,o=this.getDataset(),r=t.custom||{};return r.backgroundColor?n=r.backgroundColor:o.pointBackgroundColor?n=i.getValueAtIndexOrDefault(o.pointBackgroundColor,e,n):o.backgroundColor&&(n=o.backgroundColor),n},getPointBorderColor:function(t,e){var n=this.chart.options.elements.point.borderColor,o=this.getDataset(),r=t.custom||{};return r.borderColor?n=r.borderColor:o.pointBorderColor?n=i.getValueAtIndexOrDefault(o.pointBorderColor,e,n):o.borderColor&&(n=o.borderColor),n},getPointBorderWidth:function(t,e){var n=this.chart.options.elements.point.borderWidth,o=this.getDataset(),r=t.custom||{};return r.borderWidth?n=r.borderWidth:o.pointBorderWidth?n=i.getValueAtIndexOrDefault(o.pointBorderWidth,e,n):o.borderWidth&&(n=o.borderWidth),n},updateElement:function(t,e,n){var o,r,a=this,s=a.getMeta(),l=t.custom||{},d=a.getDataset(),c=a.index,u=d.data[e],h=a.getScaleForId(s.yAxisID),p=a.getScaleForId(s.xAxisID),f=a.chart.options.elements.point,g=a.chart.data.labels||[],m=1===g.length||1===d.data.length||a.chart.isCombo;void 0!==d.radius&&void 0===d.pointRadius&&(d.pointRadius=d.radius),void 0!==d.hitRadius&&void 0===d.pointHitRadius&&(d.pointHitRadius=d.hitRadius),o=p.getPixelForValue("object"==typeof u?u:NaN,e,c,m),r=n?h.getBasePixel():a.calculatePointY(u,e,c),t._xScale=p,t._yScale=h,t._datasetIndex=c,t._index=e,t._model={x:o,y:r,skip:l.skip||isNaN(o)||isNaN(r),radius:l.radius||i.getValueAtIndexOrDefault(d.pointRadius,e,f.radius),pointStyle:l.pointStyle||i.getValueAtIndexOrDefault(d.pointStyle,e,f.pointStyle),backgroundColor:a.getPointBackgroundColor(t,e),borderColor:a.getPointBorderColor(t,e),borderWidth:a.getPointBorderWidth(t,e),tension:s.dataset._model?s.dataset._model.tension:0,steppedLine:!!s.dataset._model&&s.dataset._model.steppedLine,hitRadius:l.hitRadius||i.getValueAtIndexOrDefault(d.pointHitRadius,e,f.hitRadius)}},calculatePointY:function(t,e,i){var n,o,r,a=this,s=a.chart,l=a.getMeta(),d=a.getScaleForId(l.yAxisID),c=0,u=0;if(d.options.stacked){for(n=0;i>n;n++)if(o=s.data.datasets[n],r=s.getDatasetMeta(n),"line"===r.type&&r.yAxisID===d.id&&s.isDatasetVisible(n)){var h=Number(d.getRightValue(o.data[e]));0>h?u+=h||0:c+=h||0}var p=Number(d.getRightValue(t));return 0>p?d.getPixelForValue(u+p):d.getPixelForValue(c+p)}return d.getPixelForValue(t)},updateBezierControlPoints:function(){function t(t,e,i){return Math.max(Math.min(t,i),e)}var e,n,o,r,a,s=this,l=s.getMeta(),d=s.chart.chartArea,c=l.data||[];if(l.dataset._model.spanGaps&&(c=c.filter(function(t){return!t._model.skip})),"monotone"===l.dataset._model.cubicInterpolationMode)i.splineCurveMonotone(c);else for(e=0,n=c.length;n>e;++e)o=c[e],r=o._model,a=i.splineCurve(i.previousItem(c,e)._model,r,i.nextItem(c,e)._model,l.dataset._model.tension),r.controlPointPreviousX=a.previous.x,r.controlPointPreviousY=a.previous.y,r.controlPointNextX=a.next.x,r.controlPointNextY=a.next.y;if(s.chart.options.elements.line.capBezierPoints)for(e=0,n=c.length;n>e;++e)r=c[e]._model,r.controlPointPreviousX=t(r.controlPointPreviousX,d.left,d.right),r.controlPointPreviousY=t(r.controlPointPreviousY,d.top,d.bottom),r.controlPointNextX=t(r.controlPointNextX,d.left,d.right),r.controlPointNextY=t(r.controlPointNextY,d.top,d.bottom)},draw:function(t){var i,n,o=this,r=o.getMeta(),a=r.data||[],s=t||1;for(i=0,n=a.length;n>i;++i)a[i].transition(s);for(e(o.getDataset(),o.chart.options)&&r.dataset.transition(s).draw(),i=0,n=a.length;n>i;++i)a[i].draw()},setHoverStyle:function(t){var e=this.chart.data.datasets[t._datasetIndex],n=t._index,o=t.custom||{},r=t._model;r.radius=o.hoverRadius||i.getValueAtIndexOrDefault(e.pointHoverRadius,n,this.chart.options.elements.point.hoverRadius),r.backgroundColor=o.hoverBackgroundColor||i.getValueAtIndexOrDefault(e.pointHoverBackgroundColor,n,i.getHoverColor(r.backgroundColor)),r.borderColor=o.hoverBorderColor||i.getValueAtIndexOrDefault(e.pointHoverBorderColor,n,i.getHoverColor(r.borderColor)),r.borderWidth=o.hoverBorderWidth||i.getValueAtIndexOrDefault(e.pointHoverBorderWidth,n,r.borderWidth)},removeHoverStyle:function(t){var e=this,n=e.chart.data.datasets[t._datasetIndex],o=t._index,r=t.custom||{},a=t._model;void 0!==n.radius&&void 0===n.pointRadius&&(n.pointRadius=n.radius),a.radius=r.radius||i.getValueAtIndexOrDefault(n.pointRadius,o,e.chart.options.elements.point.radius),a.backgroundColor=e.getPointBackgroundColor(t,o),a.borderColor=e.getPointBorderColor(t,o),a.borderWidth=e.getPointBorderWidth(t,o)}})}},{}],19:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers;t.defaults.polarArea={scale:{type:"radialLinear",lineArc:!0,ticks:{beginAtZero:!0}},animation:{animateRotate:!0,animateScale:!0},startAngle:-.5*Math.PI,aspectRatio:1,legendCallback:function(t){var e=[];e.push('<ul class="'+t.id+'-legend">');var i=t.data,n=i.datasets,o=i.labels;if(n.length)for(var r=0;r<n[0].data.length;++r)e.push('<li><span style="background-color:'+n[0].backgroundColor[r]+'"></span>'),o[r]&&e.push(o[r]),e.push("</li>");return e.push("</ul>"),e.join("")},legend:{labels:{generateLabels:function(t){var i=t.data;return i.labels.length&&i.datasets.length?i.labels.map(function(n,o){var r=t.getDatasetMeta(0),a=i.datasets[0],s=r.data[o],l=s.custom||{},d=e.getValueAtIndexOrDefault,c=t.options.elements.arc,u=l.backgroundColor?l.backgroundColor:d(a.backgroundColor,o,c.backgroundColor),h=l.borderColor?l.borderColor:d(a.borderColor,o,c.borderColor),p=l.borderWidth?l.borderWidth:d(a.borderWidth,o,c.borderWidth);return{text:n,fillStyle:u,strokeStyle:h,lineWidth:p,hidden:isNaN(a.data[o])||r.data[o].hidden,index:o}}):[]}},onClick:function(t,e){var i,n,o,r=e.index,a=this.chart;for(i=0,n=(a.data.datasets||[]).length;n>i;++i)o=a.getDatasetMeta(i),o.data[r].hidden=!o.data[r].hidden;a.update()}},tooltips:{callbacks:{title:function(){return""},label:function(t,e){return e.labels[t.index]+": "+t.yLabel}}}},t.controllers.polarArea=t.DatasetController.extend({dataElementType:t.elements.Arc,linkScales:e.noop,update:function(t){var i=this,n=i.chart,o=n.chartArea,r=i.getMeta(),a=n.options,s=a.elements.arc,l=Math.min(o.right-o.left,o.bottom-o.top);n.outerRadius=Math.max((l-s.borderWidth/2)/2,0),n.innerRadius=Math.max(a.cutoutPercentage?n.outerRadius/100*a.cutoutPercentage:1,0),n.radiusLength=(n.outerRadius-n.innerRadius)/n.getVisibleDatasetCount(),i.outerRadius=n.outerRadius-n.radiusLength*i.index,i.innerRadius=i.outerRadius-n.radiusLength,r.count=i.countVisibleElements(),e.each(r.data,function(e,n){i.updateElement(e,n,t)})},updateElement:function(t,i,n){for(var o=this,r=o.chart,a=o.getDataset(),s=r.options,l=s.animation,d=r.scale,c=e.getValueAtIndexOrDefault,u=r.data.labels,h=o.calculateCircumference(a.data[i]),p=d.xCenter,f=d.yCenter,g=0,m=o.getMeta(),v=0;i>v;++v)isNaN(a.data[v])||m.data[v].hidden||++g;var y=s.startAngle,b=t.hidden?0:d.getDistanceFromCenterForValue(a.data[i]),x=y+h*g,w=x+(t.hidden?0:h),k=l.animateScale?0:d.getDistanceFromCenterForValue(a.data[i]);e.extend(t,{_datasetIndex:o.index,_index:i,_scale:d,_model:{x:p,y:f,innerRadius:0,outerRadius:n?k:b,startAngle:n&&l.animateRotate?y:x,endAngle:n&&l.animateRotate?y:w,label:c(u,i,u[i])}}),o.removeHoverStyle(t),t.pivot()},removeHoverStyle:function(e){t.DatasetController.prototype.removeHoverStyle.call(this,e,this.chart.options.elements.arc)},countVisibleElements:function(){var t=this.getDataset(),i=this.getMeta(),n=0;return e.each(i.data,function(e,i){isNaN(t.data[i])||e.hidden||n++}),n},calculateCircumference:function(t){var e=this.getMeta().count;return e>0&&!isNaN(t)?2*Math.PI/e:0}})}},{}],20:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers;t.defaults.radar={aspectRatio:1,scale:{type:"radialLinear"},elements:{line:{tension:0}}},t.controllers.radar=t.DatasetController.extend({datasetElementType:t.elements.Line,dataElementType:t.elements.Point,linkScales:e.noop,update:function(t){var i=this,n=i.getMeta(),o=n.dataset,r=n.data,a=o.custom||{},s=i.getDataset(),l=i.chart.options.elements.line,d=i.chart.scale;void 0!==s.tension&&void 0===s.lineTension&&(s.lineTension=s.tension),e.extend(n.dataset,{_datasetIndex:i.index,_children:r,_loop:!0,_model:{tension:a.tension?a.tension:e.getValueOrDefault(s.lineTension,l.tension),backgroundColor:a.backgroundColor?a.backgroundColor:s.backgroundColor||l.backgroundColor,borderWidth:a.borderWidth?a.borderWidth:s.borderWidth||l.borderWidth,borderColor:a.borderColor?a.borderColor:s.borderColor||l.borderColor,fill:a.fill?a.fill:void 0!==s.fill?s.fill:l.fill,borderCapStyle:a.borderCapStyle?a.borderCapStyle:s.borderCapStyle||l.borderCapStyle,borderDash:a.borderDash?a.borderDash:s.borderDash||l.borderDash,borderDashOffset:a.borderDashOffset?a.borderDashOffset:s.borderDashOffset||l.borderDashOffset,borderJoinStyle:a.borderJoinStyle?a.borderJoinStyle:s.borderJoinStyle||l.borderJoinStyle,scaleTop:d.top,scaleBottom:d.bottom,scaleZero:d.getBasePosition()}}),n.dataset.pivot(),e.each(r,function(e,n){i.updateElement(e,n,t)},i),i.updateBezierControlPoints()},updateElement:function(t,i,n){var o=this,r=t.custom||{},a=o.getDataset(),s=o.chart.scale,l=o.chart.options.elements.point,d=s.getPointPositionForValue(i,a.data[i]);e.extend(t,{_datasetIndex:o.index,_index:i,_scale:s,_model:{x:n?s.xCenter:d.x,y:n?s.yCenter:d.y,tension:r.tension?r.tension:e.getValueOrDefault(a.tension,o.chart.options.elements.line.tension),radius:r.radius?r.radius:e.getValueAtIndexOrDefault(a.pointRadius,i,l.radius),backgroundColor:r.backgroundColor?r.backgroundColor:e.getValueAtIndexOrDefault(a.pointBackgroundColor,i,l.backgroundColor),borderColor:r.borderColor?r.borderColor:e.getValueAtIndexOrDefault(a.pointBorderColor,i,l.borderColor),borderWidth:r.borderWidth?r.borderWidth:e.getValueAtIndexOrDefault(a.pointBorderWidth,i,l.borderWidth),pointStyle:r.pointStyle?r.pointStyle:e.getValueAtIndexOrDefault(a.pointStyle,i,l.pointStyle),hitRadius:r.hitRadius?r.hitRadius:e.getValueAtIndexOrDefault(a.hitRadius,i,l.hitRadius)}}),t._model.skip=r.skip?r.skip:isNaN(t._model.x)||isNaN(t._model.y)},updateBezierControlPoints:function(){var t=this.chart.chartArea,i=this.getMeta();e.each(i.data,function(n,o){var r=n._model,a=e.splineCurve(e.previousItem(i.data,o,!0)._model,r,e.nextItem(i.data,o,!0)._model,r.tension);r.controlPointPreviousX=Math.max(Math.min(a.previous.x,t.right),t.left),r.controlPointPreviousY=Math.max(Math.min(a.previous.y,t.bottom),t.top),r.controlPointNextX=Math.max(Math.min(a.next.x,t.right),t.left),r.controlPointNextY=Math.max(Math.min(a.next.y,t.bottom),t.top),n.pivot()})},draw:function(t){var i=this.getMeta(),n=t||1;e.each(i.data,function(t){t.transition(n)}),i.dataset.transition(n).draw(),e.each(i.data,function(t){t.draw()})},setHoverStyle:function(t){var i=this.chart.data.datasets[t._datasetIndex],n=t.custom||{},o=t._index,r=t._model;r.radius=n.hoverRadius?n.hoverRadius:e.getValueAtIndexOrDefault(i.pointHoverRadius,o,this.chart.options.elements.point.hoverRadius),r.backgroundColor=n.hoverBackgroundColor?n.hoverBackgroundColor:e.getValueAtIndexOrDefault(i.pointHoverBackgroundColor,o,e.getHoverColor(r.backgroundColor)),r.borderColor=n.hoverBorderColor?n.hoverBorderColor:e.getValueAtIndexOrDefault(i.pointHoverBorderColor,o,e.getHoverColor(r.borderColor)),r.borderWidth=n.hoverBorderWidth?n.hoverBorderWidth:e.getValueAtIndexOrDefault(i.pointHoverBorderWidth,o,r.borderWidth)},removeHoverStyle:function(t){var i=this.chart.data.datasets[t._datasetIndex],n=t.custom||{},o=t._index,r=t._model,a=this.chart.options.elements.point;r.radius=n.radius?n.radius:e.getValueAtIndexOrDefault(i.radius,o,a.radius),r.backgroundColor=n.backgroundColor?n.backgroundColor:e.getValueAtIndexOrDefault(i.pointBackgroundColor,o,a.backgroundColor),r.borderColor=n.borderColor?n.borderColor:e.getValueAtIndexOrDefault(i.pointBorderColor,o,a.borderColor),r.borderWidth=n.borderWidth?n.borderWidth:e.getValueAtIndexOrDefault(i.pointBorderWidth,o,a.borderWidth)}})}},{}],21:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers;t.defaults.global.animation={duration:1e3,easing:"easeOutQuart",onProgress:e.noop,onComplete:e.noop},t.Animation=t.Element.extend({currentStep:null,numSteps:60,easing:"",render:null,onAnimationProgress:null,onAnimationComplete:null}),t.animationService={frameDuration:17,animations:[],dropFrames:0,request:null,addAnimation:function(t,e,i,n){var o=this;n||(t.animating=!0);for(var r=0;r<o.animations.length;++r)if(o.animations[r].chartInstance===t)return void(o.animations[r].animationObject=e);o.animations.push({chartInstance:t,animationObject:e}),1===o.animations.length&&o.requestAnimationFrame()},cancelAnimation:function(t){var i=e.findIndex(this.animations,function(e){return e.chartInstance===t});-1!==i&&(this.animations.splice(i,1),t.animating=!1)},requestAnimationFrame:function(){var t=this;null===t.request&&(t.request=e.requestAnimFrame.call(window,function(){t.request=null,t.startDigest()}))},startDigest:function(){var t=this,e=Date.now(),i=0;t.dropFrames>1&&(i=Math.floor(t.dropFrames),t.dropFrames=t.dropFrames%1);for(var n=0;n<t.animations.length;)null===t.animations[n].animationObject.currentStep&&(t.animations[n].animationObject.currentStep=0),t.animations[n].animationObject.currentStep+=1+i,t.animations[n].animationObject.currentStep>t.animations[n].animationObject.numSteps&&(t.animations[n].animationObject.currentStep=t.animations[n].animationObject.numSteps),t.animations[n].animationObject.render(t.animations[n].chartInstance,t.animations[n].animationObject),t.animations[n].animationObject.onAnimationProgress&&t.animations[n].animationObject.onAnimationProgress.call&&t.animations[n].animationObject.onAnimationProgress.call(t.animations[n].chartInstance,t.animations[n]),t.animations[n].animationObject.currentStep===t.animations[n].animationObject.numSteps?(t.animations[n].animationObject.onAnimationComplete&&t.animations[n].animationObject.onAnimationComplete.call&&t.animations[n].animationObject.onAnimationComplete.call(t.animations[n].chartInstance,t.animations[n]),t.animations[n].chartInstance.animating=!1,t.animations.splice(n,1)):++n;var o=Date.now(),r=(o-e)/t.frameDuration;t.dropFrames+=r,t.animations.length>0&&t.requestAnimationFrame()}}}},{}],22:[function(t,e,i){"use strict";e.exports=function(t){var e=t.canvasHelpers={};e.drawPoint=function(t,e,i,n,o){var r,a,s,l,d,c;if("object"==typeof e&&(r=e.toString(),"[object HTMLImageElement]"===r||"[object HTMLCanvasElement]"===r))return void t.drawImage(e,n-e.width/2,o-e.height/2);if(!(isNaN(i)||0>=i)){switch(e){default:t.beginPath(),t.arc(n,o,i,0,2*Math.PI),t.closePath(),t.fill();break;case"triangle":t.beginPath(),a=3*i/Math.sqrt(3),d=a*Math.sqrt(3)/2,t.moveTo(n-a/2,o+d/3),t.lineTo(n+a/2,o+d/3),t.lineTo(n,o-2*d/3),t.closePath(),t.fill();break;case"rect":c=1/Math.SQRT2*i,t.beginPath(),t.fillRect(n-c,o-c,2*c,2*c),t.strokeRect(n-c,o-c,2*c,2*c);break;case"rectRot":c=1/Math.SQRT2*i,t.beginPath(),t.moveTo(n-c,o),t.lineTo(n,o+c),t.lineTo(n+c,o),t.lineTo(n,o-c),t.closePath(),t.fill();break;case"cross":t.beginPath(),t.moveTo(n,o+i),t.lineTo(n,o-i),t.moveTo(n-i,o),t.lineTo(n+i,o),t.closePath();break;case"crossRot":t.beginPath(),s=Math.cos(Math.PI/4)*i,l=Math.sin(Math.PI/4)*i,t.moveTo(n-s,o-l),t.lineTo(n+s,o+l),t.moveTo(n-s,o+l),t.lineTo(n+s,o-l),t.closePath();break;case"star":t.beginPath(),t.moveTo(n,o+i),t.lineTo(n,o-i),t.moveTo(n-i,o),t.lineTo(n+i,o),s=Math.cos(Math.PI/4)*i,l=Math.sin(Math.PI/4)*i,t.moveTo(n-s,o-l),t.lineTo(n+s,o+l),t.moveTo(n-s,o+l),t.lineTo(n+s,o-l),t.closePath();break;case"line":t.beginPath(),t.moveTo(n-i,o),t.lineTo(n+i,o),t.closePath();break;case"dash":t.beginPath(),t.moveTo(n,o),t.lineTo(n+i,o),t.closePath()}t.stroke()}}}},{}],23:[function(t,e,i){"use strict";e.exports=function(t){function e(t,e){var i=a.getStyle(t,e),n=i&&i.match(/(\d+)px/);return n?Number(n[1]):void 0}function i(t,i){var n=t.style,o=t.getAttribute("height"),r=t.getAttribute("width");if(t._chartjs={initial:{height:o,width:r,style:{display:n.display,height:n.height,width:n.width}}},n.display=n.display||"block",null===r||""===r){var a=e(t,"width");void 0!==a&&(t.width=a)}if(null===o||""===o)if(""===t.style.height)t.height=t.width/(i.options.aspectRatio||2);else{var s=e(t,"height");void 0!==a&&(t.height=s)}return t}function n(t){if(t._chartjs){var e=t._chartjs.initial;["height","width"].forEach(function(i){var n=e[i];void 0===n||null===n?t.removeAttribute(i):t.setAttribute(i,n)}),a.each(e.style||{},function(e,i){t.style[i]=e}),t.width=t.width,delete t._chartjs}}function o(t,e){if("string"==typeof t?t=document.getElementById(t):t.length&&(t=t[0]),t&&t.canvas&&(t=t.canvas),t instanceof HTMLCanvasElement){var n=t.getContext&&t.getContext("2d");if(n instanceof CanvasRenderingContext2D)return i(t,e),n}return null}function r(e){e=e||{};var i=e.data=e.data||{};return i.datasets=i.datasets||[],i.labels=i.labels||[],e.options=a.configMerge(t.defaults.global,t.defaults[e.type],e.options||{}),e}var a=t.helpers;t.types={},t.instances={},t.controllers={},t.Controller=function(e,i,n){var s=this;i=r(i);var l=o(e,i),d=l&&l.canvas,c=d&&d.height,u=d&&d.width;return n.ctx=l,n.canvas=d,n.config=i,n.width=u,n.height=c,n.aspectRatio=c?u/c:null,s.id=a.uid(),s.chart=n,s.config=i,s.options=i.options,s._bufferedRender=!1,t.instances[s.id]=s,Object.defineProperty(s,"data",{get:function(){return s.config.data}}),l&&d?(a.retinaScale(n),
s.options.responsive&&(a.addResizeListener(d.parentNode,function(){s.resize()}),s.resize(!0)),s.initialize(),s):(console.error("Failed to create chart: can't acquire context from the given item"),s)},a.extend(t.Controller.prototype,{initialize:function(){var e=this;return t.plugins.notify("beforeInit",[e]),e.bindEvents(),e.ensureScalesHaveIDs(),e.buildOrUpdateControllers(),e.buildScales(),e.updateLayout(),e.resetElements(),e.initToolTip(),e.update(),t.plugins.notify("afterInit",[e]),e},clear:function(){return a.clear(this.chart),this},stop:function(){return t.animationService.cancelAnimation(this),this},resize:function(e){var i=this,n=i.chart,o=i.options,r=n.canvas,s=o.maintainAspectRatio&&n.aspectRatio||null,l=Math.floor(a.getMaximumWidth(r)),d=Math.floor(s?l/s:a.getMaximumHeight(r));if(n.width!==l||n.height!==d){r.width=n.width=l,r.height=n.height=d,r.style.width=l+"px",r.style.height=d+"px",a.retinaScale(n);var c={width:l,height:d};t.plugins.notify("resize",[i,c]),i.options.onResize&&i.options.onResize(i,c),e||(i.stop(),i.update(i.options.responsiveAnimationDuration))}},ensureScalesHaveIDs:function(){var t=this.options,e=t.scales||{},i=t.scale;a.each(e.xAxes,function(t,e){t.id=t.id||"x-axis-"+e}),a.each(e.yAxes,function(t,e){t.id=t.id||"y-axis-"+e}),i&&(i.id=i.id||"scale")},buildScales:function(){var e=this,i=e.options,n=e.scales={},o=[];i.scales&&(o=o.concat((i.scales.xAxes||[]).map(function(t){return{options:t,dtype:"category"}}),(i.scales.yAxes||[]).map(function(t){return{options:t,dtype:"linear"}}))),i.scale&&o.push({options:i.scale,dtype:"radialLinear",isDefault:!0}),a.each(o,function(i){var o=i.options,r=a.getValueOrDefault(o.type,i.dtype),s=t.scaleService.getScaleConstructor(r);if(s){var l=new s({id:o.id,options:o,ctx:e.chart.ctx,chart:e});n[l.id]=l,i.isDefault&&(e.scale=l)}}),t.scaleService.addScalesToLayout(this)},updateLayout:function(){t.layoutService.update(this,this.chart.width,this.chart.height)},buildOrUpdateControllers:function(){var e=this,i=[],n=[];if(a.each(e.data.datasets,function(o,r){var a=e.getDatasetMeta(r);a.type||(a.type=o.type||e.config.type),i.push(a.type),a.controller?a.controller.updateIndex(r):(a.controller=new t.controllers[a.type](e,r),n.push(a.controller))},e),i.length>1)for(var o=1;o<i.length;o++)if(i[o]!==i[o-1]){e.isCombo=!0;break}return n},resetElements:function(){var t=this;a.each(t.data.datasets,function(e,i){t.getDatasetMeta(i).controller.reset()},t)},reset:function(){this.resetElements(),this.tooltip.initialize()},update:function(e,i){var n=this;t.plugins.notify("beforeUpdate",[n]),n.tooltip._data=n.data;var o=n.buildOrUpdateControllers();a.each(n.data.datasets,function(t,e){n.getDatasetMeta(e).controller.buildOrUpdateElements()},n),t.layoutService.update(n,n.chart.width,n.chart.height),t.plugins.notify("afterScaleUpdate",[n]),a.each(o,function(t){t.reset()}),n.updateDatasets(),t.plugins.notify("afterUpdate",[n]),n._bufferedRender?n._bufferedRequest={lazy:i,duration:e}:n.render(e,i)},updateDatasets:function(){var e,i,n=this;if(t.plugins.notify("beforeDatasetsUpdate",[n])){for(e=0,i=n.data.datasets.length;i>e;++e)n.getDatasetMeta(e).controller.update();t.plugins.notify("afterDatasetsUpdate",[n])}},render:function(e,i){var n=this;t.plugins.notify("beforeRender",[n]);var o=n.options.animation;if(o&&("undefined"!=typeof e&&0!==e||"undefined"==typeof e&&0!==o.duration)){var r=new t.Animation;r.numSteps=(e||o.duration)/16.66,r.easing=o.easing,r.render=function(t,e){var i=a.easingEffects[e.easing],n=e.currentStep/e.numSteps,o=i(n);t.draw(o,n,e.currentStep)},r.onAnimationProgress=o.onProgress,r.onAnimationComplete=o.onComplete,t.animationService.addAnimation(n,r,e,i)}else n.draw(),o&&o.onComplete&&o.onComplete.call&&o.onComplete.call(n);return n},draw:function(e){var i=this,n=e||1;i.clear(),t.plugins.notify("beforeDraw",[i,n]),a.each(i.boxes,function(t){t.draw(i.chartArea)},i),i.scale&&i.scale.draw(),t.plugins.notify("beforeDatasetsDraw",[i,n]),a.each(i.data.datasets,function(t,n){i.isDatasetVisible(n)&&i.getDatasetMeta(n).controller.draw(e)},i,!0),t.plugins.notify("afterDatasetsDraw",[i,n]),i.tooltip.transition(n).draw(),t.plugins.notify("afterDraw",[i,n])},getElementAtEvent:function(e){return t.Interaction.modes.single(this,e)},getElementsAtEvent:function(e){return t.Interaction.modes.label(this,e,{intersect:!0})},getElementsAtXAxis:function(e){return t.Interaction.modes["x-axis"](this,e,{intersect:!0})},getElementsAtEventForMode:function(e,i,n){var o=t.Interaction.modes[i];return"function"==typeof o?o(this,e,n):[]},getDatasetAtEvent:function(e){return t.Interaction.modes.dataset(this,e)},getDatasetMeta:function(t){var e=this,i=e.data.datasets[t];i._meta||(i._meta={});var n=i._meta[e.id];return n||(n=i._meta[e.id]={type:null,data:[],dataset:null,controller:null,hidden:null,xAxisID:null,yAxisID:null}),n},getVisibleDatasetCount:function(){for(var t=0,e=0,i=this.data.datasets.length;i>e;++e)this.isDatasetVisible(e)&&t++;return t},isDatasetVisible:function(t){var e=this.getDatasetMeta(t);return"boolean"==typeof e.hidden?!e.hidden:!this.data.datasets[t].hidden},generateLegend:function(){return this.options.legendCallback(this)},destroy:function(){var e,i,o,r=this,s=r.chart.canvas;for(r.stop(),i=0,o=r.data.datasets.length;o>i;++i)e=r.getDatasetMeta(i),e.controller&&(e.controller.destroy(),e.controller=null);s&&(a.unbindEvents(r,r.events),a.removeResizeListener(s.parentNode),a.clear(r.chart),n(s),r.chart.canvas=null,r.chart.ctx=null),t.plugins.notify("destroy",[r]),delete t.instances[r.id]},toBase64Image:function(){return this.chart.canvas.toDataURL.apply(this.chart.canvas,arguments)},initToolTip:function(){var e=this;e.tooltip=new t.Tooltip({_chart:e.chart,_chartInstance:e,_data:e.data,_options:e.options.tooltips},e),e.tooltip.initialize()},bindEvents:function(){var t=this;a.bindEvents(t,t.options.events,function(e){t.eventHandler(e)})},updateHoverStyle:function(t,e,i){var n,o,r,a=i?"setHoverStyle":"removeHoverStyle";for(o=0,r=t.length;r>o;++o)n=t[o],n&&this.getDatasetMeta(n._datasetIndex).controller[a](n)},eventHandler:function(t){var e=this,i=e.legend,n=e.tooltip,o=e.options.hover;e._bufferedRender=!0,e._bufferedRequest=null;var r=e.handleEvent(t);r|=i&&i.handleEvent(t),r|=n&&n.handleEvent(t);var a=e._bufferedRequest;return a?e.render(a.duration,a.lazy):r&&!e.animating&&(e.stop(),e.render(o.animationDuration,!0)),e._bufferedRender=!1,e._bufferedRequest=null,e},handleEvent:function(t){var e=this,i=e.options||{},n=i.hover,o=!1;return e.lastActive=e.lastActive||[],"mouseout"===t.type?e.active=[]:e.active=e.getElementsAtEventForMode(t,n.mode,n),n.onHover&&n.onHover.call(e,e.active),("mouseup"===t.type||"click"===t.type)&&i.onClick&&i.onClick.call(e,t,e.active),e.lastActive.length&&e.updateHoverStyle(e.lastActive,n.mode,!1),e.active.length&&n.mode&&e.updateHoverStyle(e.active,n.mode,!0),o=!a.arrayEquals(e.active,e.lastActive),e.lastActive=e.active,o}})}},{}],24:[function(t,e,i){"use strict";e.exports=function(t){function e(t,e){return t._chartjs?void t._chartjs.listeners.push(e):(Object.defineProperty(t,"_chartjs",{configurable:!0,enumerable:!1,value:{listeners:[e]}}),void o.forEach(function(e){var i="onData"+e.charAt(0).toUpperCase()+e.slice(1),o=t[e];Object.defineProperty(t,e,{configurable:!0,enumerable:!1,value:function(){var e=Array.prototype.slice.call(arguments),r=o.apply(this,e);return n.each(t._chartjs.listeners,function(t){"function"==typeof t[i]&&t[i].apply(t,e)}),r}})}))}function i(t,e){var i=t._chartjs;if(i){var n=i.listeners,r=n.indexOf(e);-1!==r&&n.splice(r,1),n.length>0||(o.forEach(function(e){delete t[e]}),delete t._chartjs)}}var n=t.helpers,o=["push","pop","shift","splice","unshift"];t.DatasetController=function(t,e){this.initialize(t,e)},n.extend(t.DatasetController.prototype,{datasetElementType:null,dataElementType:null,initialize:function(t,e){var i=this;i.chart=t,i.index=e,i.linkScales(),i.addElements()},updateIndex:function(t){this.index=t},linkScales:function(){var t=this,e=t.getMeta(),i=t.getDataset();null===e.xAxisID&&(e.xAxisID=i.xAxisID||t.chart.options.scales.xAxes[0].id),null===e.yAxisID&&(e.yAxisID=i.yAxisID||t.chart.options.scales.yAxes[0].id)},getDataset:function(){return this.chart.data.datasets[this.index]},getMeta:function(){return this.chart.getDatasetMeta(this.index)},getScaleForId:function(t){return this.chart.scales[t]},reset:function(){this.update(!0)},destroy:function(){this._data&&i(this._data,this)},createMetaDataset:function(){var t=this,e=t.datasetElementType;return e&&new e({_chart:t.chart.chart,_datasetIndex:t.index})},createMetaData:function(t){var e=this,i=e.dataElementType;return i&&new i({_chart:e.chart.chart,_datasetIndex:e.index,_index:t})},addElements:function(){var t,e,i=this,n=i.getMeta(),o=i.getDataset().data||[],r=n.data;for(t=0,e=o.length;e>t;++t)r[t]=r[t]||i.createMetaData(t);n.dataset=n.dataset||i.createMetaDataset()},addElementAndReset:function(t){var e=this.createMetaData(t);this.getMeta().data.splice(t,0,e),this.updateElement(e,t,!0)},buildOrUpdateElements:function(){var t=this,n=t.getDataset(),o=n.data||(n.data=[]);t._data!==o&&(t._data&&i(t._data,t),e(o,t),t._data=o),t.resyncElements()},update:n.noop,draw:function(t){var e,i,n=t||1,o=this.getMeta().data;for(e=0,i=o.length;i>e;++e)o[e].transition(n).draw()},removeHoverStyle:function(t,e){var i=this.chart.data.datasets[t._datasetIndex],o=t._index,r=t.custom||{},a=n.getValueAtIndexOrDefault,s=t._model;s.backgroundColor=r.backgroundColor?r.backgroundColor:a(i.backgroundColor,o,e.backgroundColor),s.borderColor=r.borderColor?r.borderColor:a(i.borderColor,o,e.borderColor),s.borderWidth=r.borderWidth?r.borderWidth:a(i.borderWidth,o,e.borderWidth)},setHoverStyle:function(t){var e=this.chart.data.datasets[t._datasetIndex],i=t._index,o=t.custom||{},r=n.getValueAtIndexOrDefault,a=n.getHoverColor,s=t._model;s.backgroundColor=o.hoverBackgroundColor?o.hoverBackgroundColor:r(e.hoverBackgroundColor,i,a(s.backgroundColor)),s.borderColor=o.hoverBorderColor?o.hoverBorderColor:r(e.hoverBorderColor,i,a(s.borderColor)),s.borderWidth=o.hoverBorderWidth?o.hoverBorderWidth:r(e.hoverBorderWidth,i,s.borderWidth)},resyncElements:function(){var t=this,e=t.getMeta(),i=t.getDataset().data,n=e.data.length,o=i.length;n>o?e.data.splice(o,n-o):o>n&&t.insertElements(n,o-n)},insertElements:function(t,e){for(var i=0;e>i;++i)this.addElementAndReset(t+i)},onDataPush:function(){this.insertElements(this.getDataset().data.length-1,arguments.length)},onDataPop:function(){this.getMeta().data.pop()},onDataShift:function(){this.getMeta().data.shift()},onDataSplice:function(t,e){this.getMeta().data.splice(t,e),this.insertElements(t,arguments.length-2)},onDataUnshift:function(){this.insertElements(0,arguments.length)}}),t.DatasetController.extend=n.inherits}},{}],25:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers;t.elements={},t.Element=function(t){e.extend(this,t),this.initialize.apply(this,arguments)},e.extend(t.Element.prototype,{initialize:function(){this.hidden=!1},pivot:function(){var t=this;return t._view||(t._view=e.clone(t._model)),t._start=e.clone(t._view),t},transition:function(t){var i=this;return i._view||(i._view=e.clone(i._model)),1===t?(i._view=i._model,i._start=null,i):(i._start||i.pivot(),e.each(i._model,function(n,o){if("_"===o[0]);else if(i._view.hasOwnProperty(o))if(n===i._view[o]);else if("string"==typeof n)try{var r=e.color(i._model[o]).mix(e.color(i._start[o]),t);i._view[o]=r.rgbString()}catch(t){i._view[o]=n}else if("number"==typeof n){var a=void 0!==i._start[o]&&isNaN(i._start[o])===!1?i._start[o]:0;i._view[o]=(i._model[o]-a)*t+a}else i._view[o]=n;else"number"!=typeof n||isNaN(i._view[o])?i._view[o]=n:i._view[o]=n*t},i),i)},tooltipPosition:function(){return{x:this._model.x,y:this._model.y}},hasValue:function(){return e.isNumber(this._model.x)&&e.isNumber(this._model.y)}}),t.Element.extend=e.inherits}},{}],26:[function(t,e,i){"use strict";var n=t(3);e.exports=function(t){function e(t,e,i){var n;return"string"==typeof t?(n=parseInt(t,10),-1!==t.indexOf("%")&&(n=n/100*e.parentNode[i])):n=t,n}function i(t){return void 0!==t&&null!==t&&"none"!==t}function o(t,n,o){var r=document.defaultView,a=t.parentNode,s=r.getComputedStyle(t)[n],l=r.getComputedStyle(a)[n],d=i(s),c=i(l),u=Number.POSITIVE_INFINITY;return d||c?Math.min(d?e(s,t,o):u,c?e(l,a,o):u):"none"}var r=t.helpers={};r.each=function(t,e,i,n){var o,a;if(r.isArray(t))if(a=t.length,n)for(o=a-1;o>=0;o--)e.call(i,t[o],o);else for(o=0;a>o;o++)e.call(i,t[o],o);else if("object"==typeof t){var s=Object.keys(t);for(a=s.length,o=0;a>o;o++)e.call(i,t[s[o]],s[o])}},r.clone=function(t){var e={};return r.each(t,function(t,i){r.isArray(t)?e[i]=t.slice(0):"object"==typeof t&&null!==t?e[i]=r.clone(t):e[i]=t}),e},r.extend=function(t){for(var e=function(e,i){t[i]=e},i=1,n=arguments.length;n>i;i++)r.each(arguments[i],e);return t},r.configMerge=function(e){var i=r.clone(e);return r.each(Array.prototype.slice.call(arguments,1),function(e){r.each(e,function(e,n){var o=i.hasOwnProperty(n),a=o?i[n]:{};"scales"===n?i[n]=r.scaleMerge(a,e):"scale"===n?i[n]=r.configMerge(a,t.scaleService.getScaleDefaults(e.type),e):!o||"object"!=typeof a||r.isArray(a)||null===a||"object"!=typeof e||r.isArray(e)?i[n]=e:i[n]=r.configMerge(a,e)})}),i},r.scaleMerge=function(e,i){var n=r.clone(e);return r.each(i,function(e,i){"xAxes"===i||"yAxes"===i?n.hasOwnProperty(i)?r.each(e,function(e,o){var a=r.getValueOrDefault(e.type,"xAxes"===i?"category":"linear"),s=t.scaleService.getScaleDefaults(a);o>=n[i].length||!n[i][o].type?n[i].push(r.configMerge(s,e)):e.type&&e.type!==n[i][o].type?n[i][o]=r.configMerge(n[i][o],s,e):n[i][o]=r.configMerge(n[i][o],e)}):(n[i]=[],r.each(e,function(e){var o=r.getValueOrDefault(e.type,"xAxes"===i?"category":"linear");n[i].push(r.configMerge(t.scaleService.getScaleDefaults(o),e))})):n.hasOwnProperty(i)&&"object"==typeof n[i]&&null!==n[i]&&"object"==typeof e?n[i]=r.configMerge(n[i],e):n[i]=e}),n},r.getValueAtIndexOrDefault=function(t,e,i){return void 0===t||null===t?i:r.isArray(t)?e<t.length?t[e]:i:t},r.getValueOrDefault=function(t,e){return void 0===t?e:t},r.indexOf=Array.prototype.indexOf?function(t,e){return t.indexOf(e)}:function(t,e){for(var i=0,n=t.length;n>i;++i)if(t[i]===e)return i;return-1},r.where=function(t,e){if(r.isArray(t)&&Array.prototype.filter)return t.filter(e);var i=[];return r.each(t,function(t){e(t)&&i.push(t)}),i},r.findIndex=Array.prototype.findIndex?function(t,e,i){return t.findIndex(e,i)}:function(t,e,i){i=void 0===i?t:i;for(var n=0,o=t.length;o>n;++n)if(e.call(i,t[n],n,t))return n;return-1},r.findNextWhere=function(t,e,i){(void 0===i||null===i)&&(i=-1);for(var n=i+1;n<t.length;n++){var o=t[n];if(e(o))return o}},r.findPreviousWhere=function(t,e,i){(void 0===i||null===i)&&(i=t.length);for(var n=i-1;n>=0;n--){var o=t[n];if(e(o))return o}},r.inherits=function(t){var e=this,i=t&&t.hasOwnProperty("constructor")?t.constructor:function(){return e.apply(this,arguments)},n=function(){this.constructor=i};return n.prototype=e.prototype,i.prototype=new n,i.extend=r.inherits,t&&r.extend(i.prototype,t),i.__super__=e.prototype,i},r.noop=function(){},r.uid=function(){var t=0;return function(){return t++}}(),r.isNumber=function(t){return!isNaN(parseFloat(t))&&isFinite(t)},r.almostEquals=function(t,e,i){return Math.abs(t-e)<i},r.max=function(t){return t.reduce(function(t,e){return isNaN(e)?t:Math.max(t,e)},Number.NEGATIVE_INFINITY)},r.min=function(t){return t.reduce(function(t,e){return isNaN(e)?t:Math.min(t,e)},Number.POSITIVE_INFINITY)},r.sign=Math.sign?function(t){return Math.sign(t)}:function(t){return t=+t,0===t||isNaN(t)?t:t>0?1:-1},r.log10=Math.log10?function(t){return Math.log10(t)}:function(t){return Math.log(t)/Math.LN10},r.toRadians=function(t){return t*(Math.PI/180)},r.toDegrees=function(t){return t*(180/Math.PI)},r.getAngleFromPoint=function(t,e){var i=e.x-t.x,n=e.y-t.y,o=Math.sqrt(i*i+n*n),r=Math.atan2(n,i);return r<-.5*Math.PI&&(r+=2*Math.PI),{angle:r,distance:o}},r.distanceBetweenPoints=function(t,e){return Math.sqrt(Math.pow(e.x-t.x,2)+Math.pow(e.y-t.y,2))},r.aliasPixel=function(t){return t%2===0?0:.5},r.splineCurve=function(t,e,i,n){var o=t.skip?e:t,r=e,a=i.skip?e:i,s=Math.sqrt(Math.pow(r.x-o.x,2)+Math.pow(r.y-o.y,2)),l=Math.sqrt(Math.pow(a.x-r.x,2)+Math.pow(a.y-r.y,2)),d=s/(s+l),c=l/(s+l);d=isNaN(d)?0:d,c=isNaN(c)?0:c;var u=n*d,h=n*c;return{previous:{x:r.x-u*(a.x-o.x),y:r.y-u*(a.y-o.y)},next:{x:r.x+h*(a.x-o.x),y:r.y+h*(a.y-o.y)}}},r.EPSILON=Number.EPSILON||1e-14,r.splineCurveMonotone=function(t){var e,i,n,o,a=(t||[]).map(function(t){return{model:t._model,deltaK:0,mK:0}}),s=a.length;for(e=0;s>e;++e)n=a[e],n.model.skip||(i=e>0?a[e-1]:null,o=s-1>e?a[e+1]:null,o&&!o.model.skip&&(n.deltaK=(o.model.y-n.model.y)/(o.model.x-n.model.x)),!i||i.model.skip?n.mK=n.deltaK:!o||o.model.skip?n.mK=i.deltaK:this.sign(i.deltaK)!==this.sign(n.deltaK)?n.mK=0:n.mK=(i.deltaK+n.deltaK)/2);var l,d,c,u;for(e=0;s-1>e;++e)n=a[e],o=a[e+1],n.model.skip||o.model.skip||(r.almostEquals(n.deltaK,0,this.EPSILON)?n.mK=o.mK=0:(l=n.mK/n.deltaK,d=o.mK/n.deltaK,u=Math.pow(l,2)+Math.pow(d,2),9>=u||(c=3/Math.sqrt(u),n.mK=l*c*n.deltaK,o.mK=d*c*n.deltaK)));var h;for(e=0;s>e;++e)n=a[e],n.model.skip||(i=e>0?a[e-1]:null,o=s-1>e?a[e+1]:null,i&&!i.model.skip&&(h=(n.model.x-i.model.x)/3,n.model.controlPointPreviousX=n.model.x-h,n.model.controlPointPreviousY=n.model.y-h*n.mK),o&&!o.model.skip&&(h=(o.model.x-n.model.x)/3,n.model.controlPointNextX=n.model.x+h,n.model.controlPointNextY=n.model.y+h*n.mK))},r.nextItem=function(t,e,i){return i?e>=t.length-1?t[0]:t[e+1]:e>=t.length-1?t[t.length-1]:t[e+1]},r.previousItem=function(t,e,i){return i?0>=e?t[t.length-1]:t[e-1]:0>=e?t[0]:t[e-1]},r.niceNum=function(t,e){var i,n=Math.floor(r.log10(t)),o=t/Math.pow(10,n);return i=e?1.5>o?1:3>o?2:7>o?5:10:1>=o?1:2>=o?2:5>=o?5:10,i*Math.pow(10,n)};var a=r.easingEffects={linear:function(t){return t},easeInQuad:function(t){return t*t},easeOutQuad:function(t){return-1*t*(t-2)},easeInOutQuad:function(t){return(t/=.5)<1?.5*t*t:-.5*(--t*(t-2)-1)},easeInCubic:function(t){return t*t*t},easeOutCubic:function(t){return 1*((t=t/1-1)*t*t+1)},easeInOutCubic:function(t){return(t/=.5)<1?.5*t*t*t:.5*((t-=2)*t*t+2)},easeInQuart:function(t){return t*t*t*t},easeOutQuart:function(t){return-1*((t=t/1-1)*t*t*t-1)},easeInOutQuart:function(t){return(t/=.5)<1?.5*t*t*t*t:-.5*((t-=2)*t*t*t-2)},easeInQuint:function(t){return 1*(t/=1)*t*t*t*t},easeOutQuint:function(t){return 1*((t=t/1-1)*t*t*t*t+1)},easeInOutQuint:function(t){return(t/=.5)<1?.5*t*t*t*t*t:.5*((t-=2)*t*t*t*t+2)},easeInSine:function(t){return-1*Math.cos(t/1*(Math.PI/2))+1},easeOutSine:function(t){return 1*Math.sin(t/1*(Math.PI/2))},easeInOutSine:function(t){return-.5*(Math.cos(Math.PI*t/1)-1)},easeInExpo:function(t){return 0===t?1:1*Math.pow(2,10*(t/1-1))},easeOutExpo:function(t){return 1===t?1:1*(-Math.pow(2,-10*t/1)+1)},easeInOutExpo:function(t){return 0===t?0:1===t?1:(t/=.5)<1?.5*Math.pow(2,10*(t-1)):.5*(-Math.pow(2,-10*--t)+2)},easeInCirc:function(t){return t>=1?t:-1*(Math.sqrt(1-(t/=1)*t)-1)},easeOutCirc:function(t){return 1*Math.sqrt(1-(t=t/1-1)*t)},easeInOutCirc:function(t){return(t/=.5)<1?-.5*(Math.sqrt(1-t*t)-1):.5*(Math.sqrt(1-(t-=2)*t)+1)},easeInElastic:function(t){var e=1.70158,i=0,n=1;return 0===t?0:1===(t/=1)?1:(i||(i=.3),n<Math.abs(1)?(n=1,e=i/4):e=i/(2*Math.PI)*Math.asin(1/n),-(n*Math.pow(2,10*(t-=1))*Math.sin((1*t-e)*(2*Math.PI)/i)))},easeOutElastic:function(t){var e=1.70158,i=0,n=1;return 0===t?0:1===(t/=1)?1:(i||(i=.3),n<Math.abs(1)?(n=1,e=i/4):e=i/(2*Math.PI)*Math.asin(1/n),n*Math.pow(2,-10*t)*Math.sin((1*t-e)*(2*Math.PI)/i)+1)},easeInOutElastic:function(t){var e=1.70158,i=0,n=1;return 0===t?0:2===(t/=.5)?1:(i||(i=1*(.3*1.5)),n<Math.abs(1)?(n=1,e=i/4):e=i/(2*Math.PI)*Math.asin(1/n),1>t?-.5*(n*Math.pow(2,10*(t-=1))*Math.sin((1*t-e)*(2*Math.PI)/i)):n*Math.pow(2,-10*(t-=1))*Math.sin((1*t-e)*(2*Math.PI)/i)*.5+1)},easeInBack:function(t){var e=1.70158;return 1*(t/=1)*t*((e+1)*t-e)},easeOutBack:function(t){var e=1.70158;return 1*((t=t/1-1)*t*((e+1)*t+e)+1)},easeInOutBack:function(t){var e=1.70158;return(t/=.5)<1?.5*(t*t*(((e*=1.525)+1)*t-e)):.5*((t-=2)*t*(((e*=1.525)+1)*t+e)+2)},easeInBounce:function(t){return 1-a.easeOutBounce(1-t)},easeOutBounce:function(t){return(t/=1)<1/2.75?1*(7.5625*t*t):2/2.75>t?1*(7.5625*(t-=1.5/2.75)*t+.75):2.5/2.75>t?1*(7.5625*(t-=2.25/2.75)*t+.9375):1*(7.5625*(t-=2.625/2.75)*t+.984375)},easeInOutBounce:function(t){return.5>t?.5*a.easeInBounce(2*t):.5*a.easeOutBounce(2*t-1)+.5}};r.requestAnimFrame=function(){return window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function(t){return window.setTimeout(t,1e3/60)}}(),r.cancelAnimFrame=function(){return window.cancelAnimationFrame||window.webkitCancelAnimationFrame||window.mozCancelAnimationFrame||window.oCancelAnimationFrame||window.msCancelAnimationFrame||function(t){return window.clearTimeout(t,1e3/60)}}(),r.getRelativePosition=function(t,e){var i,n,o=t.originalEvent||t,a=t.currentTarget||t.srcElement,s=a.getBoundingClientRect(),l=o.touches;l&&l.length>0?(i=l[0].clientX,n=l[0].clientY):(i=o.clientX,n=o.clientY);var d=parseFloat(r.getStyle(a,"padding-left")),c=parseFloat(r.getStyle(a,"padding-top")),u=parseFloat(r.getStyle(a,"padding-right")),h=parseFloat(r.getStyle(a,"padding-bottom")),p=s.right-s.left-d-u,f=s.bottom-s.top-c-h;return i=Math.round((i-s.left-d)/p*a.width/e.currentDevicePixelRatio),n=Math.round((n-s.top-c)/f*a.height/e.currentDevicePixelRatio),{x:i,y:n}},r.addEvent=function(t,e,i){t.addEventListener?t.addEventListener(e,i):t.attachEvent?t.attachEvent("on"+e,i):t["on"+e]=i},r.removeEvent=function(t,e,i){t.removeEventListener?t.removeEventListener(e,i,!1):t.detachEvent?t.detachEvent("on"+e,i):t["on"+e]=r.noop},r.bindEvents=function(t,e,i){var n=t.events=t.events||{};r.each(e,function(e){n[e]=function(){i.apply(t,arguments)},r.addEvent(t.chart.canvas,e,n[e])})},r.unbindEvents=function(t,e){var i=t.chart.canvas;r.each(e,function(t,e){r.removeEvent(i,e,t)})},r.getConstraintWidth=function(t){return o(t,"max-width","clientWidth")},r.getConstraintHeight=function(t){return o(t,"max-height","clientHeight")},r.getMaximumWidth=function(t){var e=t.parentNode,i=parseInt(r.getStyle(e,"padding-left"),10),n=parseInt(r.getStyle(e,"padding-right"),10),o=e.clientWidth-i-n,a=r.getConstraintWidth(t);return isNaN(a)?o:Math.min(o,a)},r.getMaximumHeight=function(t){var e=t.parentNode,i=parseInt(r.getStyle(e,"padding-top"),10),n=parseInt(r.getStyle(e,"padding-bottom"),10),o=e.clientHeight-i-n,a=r.getConstraintHeight(t);return isNaN(a)?o:Math.min(o,a)},r.getStyle=function(t,e){return t.currentStyle?t.currentStyle[e]:document.defaultView.getComputedStyle(t,null).getPropertyValue(e)},r.retinaScale=function(t){var e=t.currentDevicePixelRatio=window.devicePixelRatio||1;if(1!==e){var i=t.canvas,n=t.height,o=t.width;i.height=n*e,i.width=o*e,t.ctx.scale(e,e),i.style.height=n+"px",i.style.width=o+"px"}},r.clear=function(t){t.ctx.clearRect(0,0,t.width,t.height)},r.fontString=function(t,e,i){return e+" "+t+"px "+i},r.longestText=function(t,e,i,n){n=n||{};var o=n.data=n.data||{},a=n.garbageCollect=n.garbageCollect||[];n.font!==e&&(o=n.data={},a=n.garbageCollect=[],n.font=e),t.font=e;var s=0;r.each(i,function(e){void 0!==e&&null!==e&&r.isArray(e)!==!0?s=r.measureText(t,o,a,s,e):r.isArray(e)&&r.each(e,function(e){void 0===e||null===e||r.isArray(e)||(s=r.measureText(t,o,a,s,e))})});var l=a.length/2;if(l>i.length){for(var d=0;l>d;d++)delete o[a[d]];a.splice(0,l)}return s},r.measureText=function(t,e,i,n,o){var r=e[o];return r||(r=e[o]=t.measureText(o).width,i.push(o)),r>n&&(n=r),n},r.numberOfLabelLines=function(t){var e=1;return r.each(t,function(t){r.isArray(t)&&t.length>e&&(e=t.length)}),e},r.drawRoundedRectangle=function(t,e,i,n,o,r){t.beginPath(),t.moveTo(e+r,i),t.lineTo(e+n-r,i),t.quadraticCurveTo(e+n,i,e+n,i+r),t.lineTo(e+n,i+o-r),t.quadraticCurveTo(e+n,i+o,e+n-r,i+o),t.lineTo(e+r,i+o),t.quadraticCurveTo(e,i+o,e,i+o-r),t.lineTo(e,i+r),t.quadraticCurveTo(e,i,e+r,i),t.closePath()},r.color=function(e){return n?n(e instanceof CanvasGradient?t.defaults.global.defaultColor:e):(console.error("Color.js not found!"),e)},r.addResizeListener=function(t,e){var i=document.createElement("iframe");i.className="chartjs-hidden-iframe",i.style.cssText="display:block;overflow:hidden;border:0;margin:0;top:0;left:0;bottom:0;right:0;height:100%;width:100%;position:absolute;pointer-events:none;z-index:-1;",i.tabIndex=-1;var n=t._chartjs={resizer:i,ticking:!1},o=function(){n.ticking||(n.ticking=!0,r.requestAnimFrame.call(window,function(){return n.resizer?(n.ticking=!1,e()):void 0}))};r.addEvent(i,"load",function(){r.addEvent(i.contentWindow||i,"resize",o),o()}),t.insertBefore(i,t.firstChild)},r.removeResizeListener=function(t){if(t&&t._chartjs){var e=t._chartjs.resizer;e&&(e.parentNode.removeChild(e),t._chartjs.resizer=null),delete t._chartjs}},r.isArray=Array.isArray?function(t){return Array.isArray(t)}:function(t){return"[object Array]"===Object.prototype.toString.call(t)},r.arrayEquals=function(t,e){var i,n,o,a;if(!t||!e||t.length!==e.length)return!1;for(i=0,n=t.length;n>i;++i)if(o=t[i],a=e[i],o instanceof Array&&a instanceof Array){if(!r.arrayEquals(o,a))return!1}else if(o!==a)return!1;return!0},r.callCallback=function(t,e,i){t&&"function"==typeof t.call&&t.apply(i,e)},r.getHoverColor=function(t){return t instanceof CanvasPattern?t:r.color(t).saturate(.5).darken(.1).rgbString()}}},{3:3}],27:[function(t,e,i){"use strict";e.exports=function(t){function e(t,e){var i,n,o,r,a,s=t.data.datasets;for(n=0,r=s.length;r>n;++n)if(t.isDatasetVisible(n))for(i=t.getDatasetMeta(n),o=0,a=i.data.length;a>o;++o){var l=i.data[o];l._view.skip||e(l)}}function i(t,i){var n=[];return e(t,function(t){t.inRange(i.x,i.y)&&n.push(t)}),n}function n(t,i,n,o){var a=Number.POSITIVE_INFINITY,s=[];return o||(o=r.distanceBetweenPoints),e(t,function(t){if(!n||t.inRange(i.x,i.y)){var e=t.getCenterPoint(),r=o(i,e);a>r?(s=[t],a=r):r===a&&s.push(t)}}),s}function o(t,e,o){var a=r.getRelativePosition(e,t.chart),s=function(t,e){return Math.abs(t.x-e.x)},l=o.intersect?i(t,a):n(t,a,!1,s),d=[];return l.length?(t.data.datasets.forEach(function(e,i){if(t.isDatasetVisible(i)){var n=t.getDatasetMeta(i),o=n.data[l[0]._index];o&&!o._view.skip&&d.push(o)}}),d):[]}var r=t.helpers;t.Interaction={modes:{single:function(t,i){var n=r.getRelativePosition(i,t.chart),o=[];return e(t,function(t){return t.inRange(n.x,n.y)?(o.push(t),o):void 0}),o.slice(0,1)},label:o,index:o,dataset:function(t,e,o){var a=r.getRelativePosition(e,t.chart),s=o.intersect?i(t,a):n(t,a,!1);return s.length>0&&(s=t.getDatasetMeta(s[0]._datasetIndex).data),s},"x-axis":function(t,e){return o(t,e,!0)},point:function(t,e){var n=r.getRelativePosition(e,t.chart);return i(t,n)},nearest:function(t,e,i){var o=r.getRelativePosition(e,t.chart),a=n(t,o,i.intersect);return a.length>1&&a.sort(function(t,e){var i=t.getArea(),n=e.getArea(),o=i-n;return 0===o&&(o=t._datasetIndex-e._datasetIndex),o}),a.slice(0,1)},x:function(t,i,n){var o=r.getRelativePosition(i,t.chart),a=[],s=!1;return e(t,function(t){t.inXRange(o.x)&&a.push(t),t.inRange(o.x,o.y)&&(s=!0)}),n.intersect&&!s&&(a=[]),a},y:function(t,i,n){var o=r.getRelativePosition(i,t.chart),a=[],s=!1;return e(t,function(t){t.inYRange(o.y)&&a.push(t),t.inRange(o.x,o.y)&&(s=!0)}),n.intersect&&!s&&(a=[]),a}}}}},{}],28:[function(t,e,i){"use strict";e.exports=function(){var t=function(e,i){return this.controller=new t.Controller(e,i,this),this.controller};return t.defaults={global:{responsive:!0,responsiveAnimationDuration:0,maintainAspectRatio:!0,events:["mousemove","mouseout","click","touchstart","touchmove"],hover:{onHover:null,mode:"nearest",intersect:!0,animationDuration:400},onClick:null,defaultColor:"rgba(0,0,0,0.1)",defaultFontColor:"#666",defaultFontFamily:"'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",defaultFontSize:12,defaultFontStyle:"normal",showLines:!0,elements:{},legendCallback:function(t){var e=[];e.push('<ul class="'+t.id+'-legend">');for(var i=0;i<t.data.datasets.length;i++)e.push('<li><span style="background-color:'+t.data.datasets[i].backgroundColor+'"></span>'),t.data.datasets[i].label&&e.push(t.data.datasets[i].label),e.push("</li>");return e.push("</ul>"),e.join("")}}},t.Chart=t,t}},{}],29:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers;t.layoutService={defaults:{},addBox:function(t,e){t.boxes||(t.boxes=[]),t.boxes.push(e)},removeBox:function(t,e){t.boxes&&t.boxes.splice(t.boxes.indexOf(e),1)},update:function(t,i,n){function o(t){var e,i=t.isHorizontal();i?(e=t.update(t.options.fullWidth?b:T,C),I-=e.height):(e=t.update(S,k),T-=e.width),M.push({horizontal:i,minSize:e,box:t})}function r(t){var i=e.findNextWhere(M,function(e){return e.box===t});if(i)if(t.isHorizontal()){var n={left:A,right:_,top:0,bottom:0};t.update(t.options.fullWidth?b:T,x/2,n)}else t.update(i.minSize.width,I)}function a(t){var i=e.findNextWhere(M,function(e){return e.box===t}),n={left:0,right:0,top:D,bottom:P};i&&t.update(i.minSize.width,I,n)}function s(t){t.isHorizontal()?(t.left=t.options.fullWidth?c:A,t.right=t.options.fullWidth?i-u:A+T,t.top=z,t.bottom=z+t.height,z=t.bottom):(t.left=L,t.right=L+t.width,t.top=D,t.bottom=D+I,L=t.right)}if(t){var l=t.options.layout,d=l?l.padding:null,c=0,u=0,h=0,p=0;isNaN(d)?(c=d.left||0,u=d.right||0,h=d.top||0,p=d.bottom||0):(c=d,u=d,h=d,p=d);var f=e.where(t.boxes,function(t){return"left"===t.options.position}),g=e.where(t.boxes,function(t){return"right"===t.options.position}),m=e.where(t.boxes,function(t){return"top"===t.options.position}),v=e.where(t.boxes,function(t){return"bottom"===t.options.position}),y=e.where(t.boxes,function(t){return"chartArea"===t.options.position});m.sort(function(t,e){return(e.options.fullWidth?1:0)-(t.options.fullWidth?1:0)}),v.sort(function(t,e){return(t.options.fullWidth?1:0)-(e.options.fullWidth?1:0)});var b=i-c-u,x=n-h-p,w=b/2,k=x/2,S=(i-w)/(f.length+g.length),C=(n-k)/(m.length+v.length),T=b,I=x,M=[];e.each(f.concat(g,m,v),o);var A=c,_=u,D=h,P=p;e.each(f.concat(g),r),e.each(f,function(t){A+=t.width}),e.each(g,function(t){_+=t.width}),e.each(m.concat(v),r),e.each(m,function(t){D+=t.height}),e.each(v,function(t){P+=t.height}),e.each(f.concat(g),a),A=c,_=u,D=h,P=p,e.each(f,function(t){A+=t.width}),e.each(g,function(t){_+=t.width}),e.each(m,function(t){D+=t.height}),e.each(v,function(t){P+=t.height});var E=n-D-P,O=i-A-_;(O!==T||E!==I)&&(e.each(f,function(t){t.height=E}),e.each(g,function(t){t.height=E}),e.each(m,function(t){t.options.fullWidth||(t.width=O)}),e.each(v,function(t){t.options.fullWidth||(t.width=O)}),I=E,T=O);var L=c,z=h;e.each(f.concat(m),s),L+=T,z+=I,e.each(g,s),e.each(v,s),t.chartArea={left:A,top:D,right:A+T,bottom:D+I},e.each(y,function(e){e.left=t.chartArea.left,e.top=t.chartArea.top,e.right=t.chartArea.right,e.bottom=t.chartArea.bottom,e.update(T,I)})}}}}},{}],30:[function(t,e,i){"use strict";e.exports=function(t){function e(t,e){return t.usePointStyle?e*Math.SQRT2:t.boxWidth}var i=t.helpers,n=i.noop;t.defaults.global.legend={display:!0,position:"top",fullWidth:!0,reverse:!1,onClick:function(t,e){var i=e.datasetIndex,n=this.chart,o=n.getDatasetMeta(i);o.hidden=null===o.hidden?!n.data.datasets[i].hidden:null,n.update()},onHover:null,labels:{boxWidth:40,padding:10,generateLabels:function(t){var e=t.data;return i.isArray(e.datasets)?e.datasets.map(function(e,n){return{text:e.label,fillStyle:i.isArray(e.backgroundColor)?e.backgroundColor[0]:e.backgroundColor,hidden:!t.isDatasetVisible(n),lineCap:e.borderCapStyle,lineDash:e.borderDash,lineDashOffset:e.borderDashOffset,lineJoin:e.borderJoinStyle,lineWidth:e.borderWidth,strokeStyle:e.borderColor,pointStyle:e.pointStyle,datasetIndex:n}},this):[]}}},t.Legend=t.Element.extend({initialize:function(t){i.extend(this,t),this.legendHitBoxes=[],this.doughnutMode=!1},beforeUpdate:n,update:function(t,e,i){var n=this;return n.beforeUpdate(),n.maxWidth=t,n.maxHeight=e,n.margins=i,n.beforeSetDimensions(),
n.setDimensions(),n.afterSetDimensions(),n.beforeBuildLabels(),n.buildLabels(),n.afterBuildLabels(),n.beforeFit(),n.fit(),n.afterFit(),n.afterUpdate(),n.minSize},afterUpdate:n,beforeSetDimensions:n,setDimensions:function(){var t=this;t.isHorizontal()?(t.width=t.maxWidth,t.left=0,t.right=t.width):(t.height=t.maxHeight,t.top=0,t.bottom=t.height),t.paddingLeft=0,t.paddingTop=0,t.paddingRight=0,t.paddingBottom=0,t.minSize={width:0,height:0}},afterSetDimensions:n,beforeBuildLabels:n,buildLabels:function(){var t=this;t.legendItems=t.options.labels.generateLabels.call(t,t.chart),t.options.reverse&&t.legendItems.reverse()},afterBuildLabels:n,beforeFit:n,fit:function(){var n=this,o=n.options,r=o.labels,a=o.display,s=n.ctx,l=t.defaults.global,d=i.getValueOrDefault,c=d(r.fontSize,l.defaultFontSize),u=d(r.fontStyle,l.defaultFontStyle),h=d(r.fontFamily,l.defaultFontFamily),p=i.fontString(c,u,h),f=n.legendHitBoxes=[],g=n.minSize,m=n.isHorizontal();if(m?(g.width=n.maxWidth,g.height=a?10:0):(g.width=a?10:0,g.height=n.maxHeight),a)if(s.font=p,m){var v=n.lineWidths=[0],y=n.legendItems.length?c+r.padding:0;s.textAlign="left",s.textBaseline="top",i.each(n.legendItems,function(t,i){var o=e(r,c),a=o+c/2+s.measureText(t.text).width;v[v.length-1]+a+r.padding>=n.width&&(y+=c+r.padding,v[v.length]=n.left),f[i]={left:0,top:0,width:a,height:c},v[v.length-1]+=a+r.padding}),g.height+=y}else{var b=r.padding,x=n.columnWidths=[],w=r.padding,k=0,S=0,C=c+b;i.each(n.legendItems,function(t,i){var n=e(r,c),o=n+c/2+s.measureText(t.text).width;S+C>g.height&&(w+=k+r.padding,x.push(k),k=0,S=0),k=Math.max(k,o),S+=C,f[i]={left:0,top:0,width:o,height:c}}),w+=k,x.push(k),g.width+=w}n.width=g.width,n.height=g.height},afterFit:n,isHorizontal:function(){return"top"===this.options.position||"bottom"===this.options.position},draw:function(){var n=this,o=n.options,r=o.labels,a=t.defaults.global,s=a.elements.line,l=n.width,d=n.lineWidths;if(o.display){var c,u=n.ctx,h=i.getValueOrDefault,p=h(r.fontColor,a.defaultFontColor),f=h(r.fontSize,a.defaultFontSize),g=h(r.fontStyle,a.defaultFontStyle),m=h(r.fontFamily,a.defaultFontFamily),v=i.fontString(f,g,m);u.textAlign="left",u.textBaseline="top",u.lineWidth=.5,u.strokeStyle=p,u.fillStyle=p,u.font=v;var y=e(r,f),b=n.legendHitBoxes,x=function(e,i,n){if(!(isNaN(y)||0>=y)){u.save(),u.fillStyle=h(n.fillStyle,a.defaultColor),u.lineCap=h(n.lineCap,s.borderCapStyle),u.lineDashOffset=h(n.lineDashOffset,s.borderDashOffset),u.lineJoin=h(n.lineJoin,s.borderJoinStyle),u.lineWidth=h(n.lineWidth,s.borderWidth),u.strokeStyle=h(n.strokeStyle,a.defaultColor);var r=0===h(n.lineWidth,s.borderWidth);if(u.setLineDash&&u.setLineDash(h(n.lineDash,s.borderDash)),o.labels&&o.labels.usePointStyle){var l=f*Math.SQRT2/2,d=l/Math.SQRT2,c=e+d,p=i+d;t.canvasHelpers.drawPoint(u,n.pointStyle,l,c,p)}else r||u.strokeRect(e,i,y,f),u.fillRect(e,i,y,f);u.restore()}},w=function(t,e,i,n){u.fillText(i.text,y+f/2+t,e),i.hidden&&(u.beginPath(),u.lineWidth=2,u.moveTo(y+f/2+t,e+f/2),u.lineTo(y+f/2+t+n,e+f/2),u.stroke())},k=n.isHorizontal();c=k?{x:n.left+(l-d[0])/2,y:n.top+r.padding,line:0}:{x:n.left+r.padding,y:n.top+r.padding,line:0};var S=f+r.padding;i.each(n.legendItems,function(t,e){var i=u.measureText(t.text).width,o=y+f/2+i,a=c.x,s=c.y;k?a+o>=l&&(s=c.y+=S,c.line++,a=c.x=n.left+(l-d[c.line])/2):s+S>n.bottom&&(a=c.x=a+n.columnWidths[c.line]+r.padding,s=c.y=n.top,c.line++),x(a,s,t),b[e].left=a,b[e].top=s,w(a,s,t,i),k?c.x+=o+r.padding:c.y+=S})}},handleEvent:function(t){var e=this,n=e.options,o="mouseup"===t.type?"click":t.type,r=!1;if("mousemove"===o){if(!n.onHover)return}else{if("click"!==o)return;if(!n.onClick)return}var a=i.getRelativePosition(t,e.chart.chart),s=a.x,l=a.y;if(s>=e.left&&s<=e.right&&l>=e.top&&l<=e.bottom)for(var d=e.legendHitBoxes,c=0;c<d.length;++c){var u=d[c];if(s>=u.left&&s<=u.left+u.width&&l>=u.top&&l<=u.top+u.height){if("click"===o){n.onClick.call(e,t,e.legendItems[c]),r=!0;break}if("mousemove"===o){n.onHover.call(e,t,e.legendItems[c]),r=!0;break}}}return r}}),t.plugins.register({beforeInit:function(e){var i=e.options,n=i.legend;n&&(e.legend=new t.Legend({ctx:e.chart.ctx,options:n,chart:e}),t.layoutService.addBox(e,e.legend))}})}},{}],31:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers.noop;t.plugins={_plugins:[],register:function(t){var e=this._plugins;[].concat(t).forEach(function(t){-1===e.indexOf(t)&&e.push(t)})},unregister:function(t){var e=this._plugins;[].concat(t).forEach(function(t){var i=e.indexOf(t);-1!==i&&e.splice(i,1)})},clear:function(){this._plugins=[]},count:function(){return this._plugins.length},getAll:function(){return this._plugins},notify:function(t,e){var i,n,o=this._plugins,r=o.length;for(i=0;r>i;++i)if(n=o[i],"function"==typeof n[t]&&n[t].apply(n,e||[])===!1)return!1;return!0}},t.PluginBase=t.Element.extend({beforeInit:e,afterInit:e,beforeUpdate:e,afterUpdate:e,beforeDraw:e,afterDraw:e,destroy:e}),t.pluginService=t.plugins}},{}],32:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers;t.defaults.scale={display:!0,position:"left",gridLines:{display:!0,color:"rgba(0, 0, 0, 0.1)",lineWidth:1,drawBorder:!0,drawOnChartArea:!0,drawTicks:!0,tickMarkLength:10,zeroLineWidth:1,zeroLineColor:"rgba(0,0,0,0.25)",offsetGridLines:!1,borderDash:[],borderDashOffset:0},scaleLabel:{labelString:"",display:!1},ticks:{beginAtZero:!1,minRotation:0,maxRotation:50,mirror:!1,padding:10,reverse:!1,display:!0,autoSkip:!0,autoSkipPadding:0,labelOffset:0,callback:t.Ticks.formatters.values}},t.Scale=t.Element.extend({beforeUpdate:function(){e.callCallback(this.options.beforeUpdate,[this])},update:function(t,i,n){var o=this;return o.beforeUpdate(),o.maxWidth=t,o.maxHeight=i,o.margins=e.extend({left:0,right:0,top:0,bottom:0},n),o.beforeSetDimensions(),o.setDimensions(),o.afterSetDimensions(),o.beforeDataLimits(),o.determineDataLimits(),o.afterDataLimits(),o.beforeBuildTicks(),o.buildTicks(),o.afterBuildTicks(),o.beforeTickToLabelConversion(),o.convertTicksToLabels(),o.afterTickToLabelConversion(),o.beforeCalculateTickRotation(),o.calculateTickRotation(),o.afterCalculateTickRotation(),o.beforeFit(),o.fit(),o.afterFit(),o.afterUpdate(),o.minSize},afterUpdate:function(){e.callCallback(this.options.afterUpdate,[this])},beforeSetDimensions:function(){e.callCallback(this.options.beforeSetDimensions,[this])},setDimensions:function(){var t=this;t.isHorizontal()?(t.width=t.maxWidth,t.left=0,t.right=t.width):(t.height=t.maxHeight,t.top=0,t.bottom=t.height),t.paddingLeft=0,t.paddingTop=0,t.paddingRight=0,t.paddingBottom=0},afterSetDimensions:function(){e.callCallback(this.options.afterSetDimensions,[this])},beforeDataLimits:function(){e.callCallback(this.options.beforeDataLimits,[this])},determineDataLimits:e.noop,afterDataLimits:function(){e.callCallback(this.options.afterDataLimits,[this])},beforeBuildTicks:function(){e.callCallback(this.options.beforeBuildTicks,[this])},buildTicks:e.noop,afterBuildTicks:function(){e.callCallback(this.options.afterBuildTicks,[this])},beforeTickToLabelConversion:function(){e.callCallback(this.options.beforeTickToLabelConversion,[this])},convertTicksToLabels:function(){var t=this,e=t.options.ticks;t.ticks=t.ticks.map(e.userCallback||e.callback)},afterTickToLabelConversion:function(){e.callCallback(this.options.afterTickToLabelConversion,[this])},beforeCalculateTickRotation:function(){e.callCallback(this.options.beforeCalculateTickRotation,[this])},calculateTickRotation:function(){var i=this,n=i.ctx,o=t.defaults.global,r=i.options.ticks,a=e.getValueOrDefault(r.fontSize,o.defaultFontSize),s=e.getValueOrDefault(r.fontStyle,o.defaultFontStyle),l=e.getValueOrDefault(r.fontFamily,o.defaultFontFamily),d=e.fontString(a,s,l);n.font=d;var c,u=n.measureText(i.ticks[0]).width,h=n.measureText(i.ticks[i.ticks.length-1]).width;if(i.labelRotation=r.minRotation||0,i.paddingRight=0,i.paddingLeft=0,i.options.display&&i.isHorizontal()){i.paddingRight=h/2+3,i.paddingLeft=u/2+3,i.longestTextCache||(i.longestTextCache={});for(var p,f,g=e.longestText(n,d,i.ticks,i.longestTextCache),m=g,v=i.getPixelForTick(1)-i.getPixelForTick(0)-6;m>v&&i.labelRotation<r.maxRotation;){if(p=Math.cos(e.toRadians(i.labelRotation)),f=Math.sin(e.toRadians(i.labelRotation)),c=p*u,c+a/2>i.yLabelWidth&&(i.paddingLeft=c+a/2),i.paddingRight=a/2,f*g>i.maxHeight){i.labelRotation--;break}i.labelRotation++,m=p*g}}i.margins&&(i.paddingLeft=Math.max(i.paddingLeft-i.margins.left,0),i.paddingRight=Math.max(i.paddingRight-i.margins.right,0))},afterCalculateTickRotation:function(){e.callCallback(this.options.afterCalculateTickRotation,[this])},beforeFit:function(){e.callCallback(this.options.beforeFit,[this])},fit:function(){var i=this,n=i.minSize={width:0,height:0},o=i.options,r=t.defaults.global,a=o.ticks,s=o.scaleLabel,l=o.gridLines,d=o.display,c=i.isHorizontal(),u=e.getValueOrDefault(a.fontSize,r.defaultFontSize),h=e.getValueOrDefault(a.fontStyle,r.defaultFontStyle),p=e.getValueOrDefault(a.fontFamily,r.defaultFontFamily),f=e.fontString(u,h,p),g=e.getValueOrDefault(s.fontSize,r.defaultFontSize),m=o.gridLines.tickMarkLength;if(c?n.width=i.isFullWidth()?i.maxWidth-i.margins.left-i.margins.right:i.maxWidth:n.width=d&&l.drawTicks?m:0,c?n.height=d&&l.drawTicks?m:0:n.height=i.maxHeight,s.display&&d&&(c?n.height+=1.5*g:n.width+=1.5*g),a.display&&d){i.longestTextCache||(i.longestTextCache={});var v=e.longestText(i.ctx,f,i.ticks,i.longestTextCache),y=e.numberOfLabelLines(i.ticks),b=.5*u;if(c){i.longestLabelWidth=v;var x=Math.sin(e.toRadians(i.labelRotation))*i.longestLabelWidth+u*y+b*y;n.height=Math.min(i.maxHeight,n.height+x),i.ctx.font=f;var w=i.ctx.measureText(i.ticks[0]).width,k=i.ctx.measureText(i.ticks[i.ticks.length-1]).width,S=Math.cos(e.toRadians(i.labelRotation)),C=Math.sin(e.toRadians(i.labelRotation));i.paddingLeft=0!==i.labelRotation?S*w+3:w/2+3,i.paddingRight=0!==i.labelRotation?C*(u/2)+3:k/2+3}else{var T=i.maxWidth-n.width,I=a.mirror;I?v=0:v+=i.options.ticks.padding,T>v?n.width+=v:n.width=i.maxWidth,i.paddingTop=u/2,i.paddingBottom=u/2}}i.margins&&(i.paddingLeft=Math.max(i.paddingLeft-i.margins.left,0),i.paddingTop=Math.max(i.paddingTop-i.margins.top,0),i.paddingRight=Math.max(i.paddingRight-i.margins.right,0),i.paddingBottom=Math.max(i.paddingBottom-i.margins.bottom,0)),i.width=n.width,i.height=n.height},afterFit:function(){e.callCallback(this.options.afterFit,[this])},isHorizontal:function(){return"top"===this.options.position||"bottom"===this.options.position},isFullWidth:function(){return this.options.fullWidth},getRightValue:function(t){return null===t||"undefined"==typeof t?NaN:"number"!=typeof t||isFinite(t)?"object"==typeof t?t instanceof Date||t.isValid?t:this.getRightValue(this.isHorizontal()?t.x:t.y):t:NaN},getLabelForIndex:e.noop,getPixelForValue:e.noop,getValueForPixel:e.noop,getPixelForTick:function(t,e){var i=this;if(i.isHorizontal()){var n=i.width-(i.paddingLeft+i.paddingRight),o=n/Math.max(i.ticks.length-(i.options.gridLines.offsetGridLines?0:1),1),r=o*t+i.paddingLeft;e&&(r+=o/2);var a=i.left+Math.round(r);return a+=i.isFullWidth()?i.margins.left:0}var s=i.height-(i.paddingTop+i.paddingBottom);return i.top+t*(s/(i.ticks.length-1))},getPixelForDecimal:function(t){var e=this;if(e.isHorizontal()){var i=e.width-(e.paddingLeft+e.paddingRight),n=i*t+e.paddingLeft,o=e.left+Math.round(n);return o+=e.isFullWidth()?e.margins.left:0}return e.top+t*e.height},getBasePixel:function(){var t=this,e=t.min,i=t.max;return t.getPixelForValue(t.beginAtZero?0:0>e&&0>i?i:e>0&&i>0?e:0)},draw:function(i){var n=this,o=n.options;if(o.display){var r,a,s=n.ctx,l=t.defaults.global,d=o.ticks,c=o.gridLines,u=o.scaleLabel,h=0!==n.labelRotation,p=d.autoSkip,f=n.isHorizontal();d.maxTicksLimit&&(a=d.maxTicksLimit);var g=e.getValueOrDefault(d.fontColor,l.defaultFontColor),m=e.getValueOrDefault(d.fontSize,l.defaultFontSize),v=e.getValueOrDefault(d.fontStyle,l.defaultFontStyle),y=e.getValueOrDefault(d.fontFamily,l.defaultFontFamily),b=e.fontString(m,v,y),x=c.tickMarkLength,w=e.getValueOrDefault(c.borderDash,l.borderDash),k=e.getValueOrDefault(c.borderDashOffset,l.borderDashOffset),S=e.getValueOrDefault(u.fontColor,l.defaultFontColor),C=e.getValueOrDefault(u.fontSize,l.defaultFontSize),T=e.getValueOrDefault(u.fontStyle,l.defaultFontStyle),I=e.getValueOrDefault(u.fontFamily,l.defaultFontFamily),M=e.fontString(C,T,I),A=e.toRadians(n.labelRotation),_=Math.cos(A),D=n.longestLabelWidth*_;s.fillStyle=g;var P=[];if(f){if(r=!1,h&&(D/=2),(D+d.autoSkipPadding)*n.ticks.length>n.width-(n.paddingLeft+n.paddingRight)&&(r=1+Math.floor((D+d.autoSkipPadding)*n.ticks.length/(n.width-(n.paddingLeft+n.paddingRight)))),a&&n.ticks.length>a)for(;!r||n.ticks.length/(r||1)>a;)r||(r=1),r+=1;p||(r=!1)}var E="right"===o.position?n.left:n.right-x,O="right"===o.position?n.left+x:n.right,L="bottom"===o.position?n.top:n.bottom-x,z="bottom"===o.position?n.top+x:n.bottom;if(e.each(n.ticks,function(t,a){if(void 0!==t&&null!==t){var s=n.ticks.length===a+1,l=r>1&&a%r>0||a%r===0&&a+r>=n.ticks.length;if((!l||s)&&void 0!==t&&null!==t){var u,p;a===("undefined"!=typeof n.zeroLineIndex?n.zeroLineIndex:0)?(u=c.zeroLineWidth,p=c.zeroLineColor):(u=e.getValueAtIndexOrDefault(c.lineWidth,a),p=e.getValueAtIndexOrDefault(c.color,a));var g,m,v,y,b,S,C,T,I,M,_="middle",D="middle";if(f){h||(D="top"===o.position?"bottom":"top"),_=h?"right":"center";var F=n.getPixelForTick(a)+e.aliasPixel(u);I=n.getPixelForTick(a,c.offsetGridLines)+d.labelOffset,M=h?n.top+12:"top"===o.position?n.bottom-x:n.top+x,g=v=b=C=F,m=L,y=z,S=i.top,T=i.bottom}else{"left"===o.position?d.mirror?(I=n.right+d.padding,_="left"):(I=n.right-d.padding,_="right"):d.mirror?(I=n.left-d.padding,_="right"):(I=n.left+d.padding,_="left");var R=n.getPixelForTick(a);R+=e.aliasPixel(u),M=n.getPixelForTick(a,c.offsetGridLines),g=E,v=O,b=i.left,C=i.right,m=y=S=T=R}P.push({tx1:g,ty1:m,tx2:v,ty2:y,x1:b,y1:S,x2:C,y2:T,labelX:I,labelY:M,glWidth:u,glColor:p,glBorderDash:w,glBorderDashOffset:k,rotation:-1*A,label:t,textBaseline:D,textAlign:_})}}}),e.each(P,function(t){if(c.display&&(s.save(),s.lineWidth=t.glWidth,s.strokeStyle=t.glColor,s.setLineDash&&(s.setLineDash(t.glBorderDash),s.lineDashOffset=t.glBorderDashOffset),s.beginPath(),c.drawTicks&&(s.moveTo(t.tx1,t.ty1),s.lineTo(t.tx2,t.ty2)),c.drawOnChartArea&&(s.moveTo(t.x1,t.y1),s.lineTo(t.x2,t.y2)),s.stroke(),s.restore()),d.display){s.save(),s.translate(t.labelX,t.labelY),s.rotate(t.rotation),s.font=b,s.textBaseline=t.textBaseline,s.textAlign=t.textAlign;var i=t.label;if(e.isArray(i))for(var n=0,o=-(i.length-1)*m*.75;n<i.length;++n)s.fillText(""+i[n],0,o),o+=1.5*m;else s.fillText(i,0,0);s.restore()}}),u.display){var F,R,N=0;if(f)F=n.left+(n.right-n.left)/2,R="bottom"===o.position?n.bottom-C/2:n.top+C/2;else{var W="left"===o.position;F=W?n.left+C/2:n.right-C/2,R=n.top+(n.bottom-n.top)/2,N=W?-.5*Math.PI:.5*Math.PI}s.save(),s.translate(F,R),s.rotate(N),s.textAlign="center",s.textBaseline="middle",s.fillStyle=S,s.font=M,s.fillText(u.labelString,0,0),s.restore()}if(c.drawBorder){s.lineWidth=e.getValueAtIndexOrDefault(c.lineWidth,0),s.strokeStyle=e.getValueAtIndexOrDefault(c.color,0);var B=n.left,j=n.right,V=n.top,H=n.bottom,U=e.aliasPixel(s.lineWidth);f?(V=H="top"===o.position?n.bottom:n.top,V+=U,H+=U):(B=j="left"===o.position?n.right:n.left,B+=U,j+=U),s.beginPath(),s.moveTo(B,V),s.lineTo(j,H),s.stroke()}}}})}},{}],33:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers;t.scaleService={constructors:{},defaults:{},registerScaleType:function(t,i,n){this.constructors[t]=i,this.defaults[t]=e.clone(n)},getScaleConstructor:function(t){return this.constructors.hasOwnProperty(t)?this.constructors[t]:void 0},getScaleDefaults:function(i){return this.defaults.hasOwnProperty(i)?e.scaleMerge(t.defaults.scale,this.defaults[i]):{}},updateScaleDefaults:function(t,i){var n=this.defaults;n.hasOwnProperty(t)&&(n[t]=e.extend(n[t],i))},addScalesToLayout:function(i){e.each(i.scales,function(e){t.layoutService.addBox(i,e)})}}}},{}],34:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers;t.Ticks={generators:{linear:function(t,i){var n,o=[];if(t.stepSize&&t.stepSize>0)n=t.stepSize;else{var r=e.niceNum(i.max-i.min,!1);n=e.niceNum(r/(t.maxTicks-1),!0)}var a=Math.floor(i.min/n)*n,s=Math.ceil(i.max/n)*n;if(t.min&&t.max&&t.stepSize){var l=(t.max-t.min)%t.stepSize===0;l&&(a=t.min,s=t.max)}var d=(s-a)/n;d=e.almostEquals(d,Math.round(d),n/1e3)?Math.round(d):Math.ceil(d),o.push(void 0!==t.min?t.min:a);for(var c=1;d>c;++c)o.push(a+c*n);return o.push(void 0!==t.max?t.max:s),o},logarithmic:function(t,i){for(var n=[],o=e.getValueOrDefault,r=o(t.min,Math.pow(10,Math.floor(e.log10(i.min))));r<i.max;){n.push(r);var a,s;0===r?(a=Math.floor(e.log10(i.minNotZero)),s=Math.round(i.minNotZero/Math.pow(10,a))):(a=Math.floor(e.log10(r)),s=Math.floor(r/Math.pow(10,a))+1),10===s&&(s=1,++a),r=s*Math.pow(10,a)}var l=o(t.max,r);return n.push(l),n}},formatters:{values:function(t){return e.isArray(t)?t:""+t},linear:function(t,i,n){var o=n.length>3?n[2]-n[1]:n[1]-n[0];Math.abs(o)>1&&t!==Math.floor(t)&&(o=t-Math.floor(t));var r=e.log10(Math.abs(o)),a="";if(0!==t){var s=-1*Math.floor(r);s=Math.max(Math.min(s,20),0),a=t.toFixed(s)}else a="0";return a},logarithmic:function(t,i,n){var o=t/Math.pow(10,Math.floor(e.log10(t)));return 0===t?"0":1===o||2===o||5===o||0===i||i===n.length-1?t.toExponential():""}}}}},{}],35:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers;t.defaults.global.title={display:!1,position:"top",fullWidth:!0,fontStyle:"bold",padding:10,text:""};var i=e.noop;t.Title=t.Element.extend({initialize:function(i){var n=this;e.extend(n,i),n.options=e.configMerge(t.defaults.global.title,i.options),n.legendHitBoxes=[]},beforeUpdate:function(){var i=this.chart.options;i&&i.title&&(this.options=e.configMerge(t.defaults.global.title,i.title))},update:function(t,e,i){var n=this;return n.beforeUpdate(),n.maxWidth=t,n.maxHeight=e,n.margins=i,n.beforeSetDimensions(),n.setDimensions(),n.afterSetDimensions(),n.beforeBuildLabels(),n.buildLabels(),n.afterBuildLabels(),n.beforeFit(),n.fit(),n.afterFit(),n.afterUpdate(),n.minSize},afterUpdate:i,beforeSetDimensions:i,setDimensions:function(){var t=this;t.isHorizontal()?(t.width=t.maxWidth,t.left=0,t.right=t.width):(t.height=t.maxHeight,t.top=0,t.bottom=t.height),t.paddingLeft=0,t.paddingTop=0,t.paddingRight=0,t.paddingBottom=0,t.minSize={width:0,height:0}},afterSetDimensions:i,beforeBuildLabels:i,buildLabels:i,afterBuildLabels:i,beforeFit:i,fit:function(){var i=this,n=e.getValueOrDefault,o=i.options,r=t.defaults.global,a=o.display,s=n(o.fontSize,r.defaultFontSize),l=i.minSize;i.isHorizontal()?(l.width=i.maxWidth,l.height=a?s+2*o.padding:0):(l.width=a?s+2*o.padding:0,l.height=i.maxHeight),i.width=l.width,i.height=l.height},afterFit:i,isHorizontal:function(){var t=this.options.position;return"top"===t||"bottom"===t},draw:function(){var i=this,n=i.ctx,o=e.getValueOrDefault,r=i.options,a=t.defaults.global;if(r.display){var s,l,d,c=o(r.fontSize,a.defaultFontSize),u=o(r.fontStyle,a.defaultFontStyle),h=o(r.fontFamily,a.defaultFontFamily),p=e.fontString(c,u,h),f=0,g=i.top,m=i.left,v=i.bottom,y=i.right;n.fillStyle=o(r.fontColor,a.defaultFontColor),n.font=p,i.isHorizontal()?(s=m+(y-m)/2,l=g+(v-g)/2,d=y-m):(s="left"===r.position?m+c/2:y-c/2,l=g+(v-g)/2,d=v-g,f=Math.PI*("left"===r.position?-.5:.5)),n.save(),n.translate(s,l),n.rotate(f),n.textAlign="center",n.textBaseline="middle",n.fillText(r.text,0,0,d),n.restore()}}}),t.plugins.register({beforeInit:function(e){var i=e.options,n=i.title;n&&(e.titleBlock=new t.Title({ctx:e.chart.ctx,options:n,chart:e}),t.layoutService.addBox(e,e.titleBlock))}})}},{}],36:[function(t,e,i){"use strict";e.exports=function(t){function e(t,e){var i=l.color(t);return i.alpha(e*i.alpha()).rgbaString()}function i(t,e){return e&&(l.isArray(e)?Array.prototype.push.apply(t,e):t.push(e)),t}function n(t){var e=t._xScale,i=t._yScale||t._scale,n=t._index,o=t._datasetIndex;return{xLabel:e?e.getLabelForIndex(n,o):"",yLabel:i?i.getLabelForIndex(n,o):"",index:n,datasetIndex:o,x:t._model.x,y:t._model.y}}function o(e){var i=t.defaults.global,n=l.getValueOrDefault;return{xPadding:e.xPadding,yPadding:e.yPadding,xAlign:e.xAlign,yAlign:e.yAlign,bodyFontColor:e.bodyFontColor,_bodyFontFamily:n(e.bodyFontFamily,i.defaultFontFamily),_bodyFontStyle:n(e.bodyFontStyle,i.defaultFontStyle),_bodyAlign:e.bodyAlign,bodyFontSize:n(e.bodyFontSize,i.defaultFontSize),bodySpacing:e.bodySpacing,titleFontColor:e.titleFontColor,_titleFontFamily:n(e.titleFontFamily,i.defaultFontFamily),_titleFontStyle:n(e.titleFontStyle,i.defaultFontStyle),titleFontSize:n(e.titleFontSize,i.defaultFontSize),_titleAlign:e.titleAlign,titleSpacing:e.titleSpacing,titleMarginBottom:e.titleMarginBottom,footerFontColor:e.footerFontColor,_footerFontFamily:n(e.footerFontFamily,i.defaultFontFamily),_footerFontStyle:n(e.footerFontStyle,i.defaultFontStyle),footerFontSize:n(e.footerFontSize,i.defaultFontSize),_footerAlign:e.footerAlign,footerSpacing:e.footerSpacing,footerMarginTop:e.footerMarginTop,caretSize:e.caretSize,cornerRadius:e.cornerRadius,backgroundColor:e.backgroundColor,opacity:0,legendColorBackground:e.multiKeyBackground,displayColors:e.displayColors}}function r(t,e){var i=t._chart.ctx,n=2*e.yPadding,o=0,r=e.body,a=r.reduce(function(t,e){return t+e.before.length+e.lines.length+e.after.length},0);a+=e.beforeBody.length+e.afterBody.length;var s=e.title.length,d=e.footer.length,c=e.titleFontSize,u=e.bodyFontSize,h=e.footerFontSize;n+=s*c,n+=s?(s-1)*e.titleSpacing:0,n+=s?e.titleMarginBottom:0,n+=a*u,n+=a?(a-1)*e.bodySpacing:0,n+=d?e.footerMarginTop:0,n+=d*h,n+=d?(d-1)*e.footerSpacing:0;var p=0,f=function(t){o=Math.max(o,i.measureText(t).width+p)};return i.font=l.fontString(c,e._titleFontStyle,e._titleFontFamily),l.each(e.title,f),i.font=l.fontString(u,e._bodyFontStyle,e._bodyFontFamily),l.each(e.beforeBody.concat(e.afterBody),f),p=e.displayColors?u+2:0,l.each(r,function(t){l.each(t.before,f),l.each(t.lines,f),l.each(t.after,f)}),p=0,i.font=l.fontString(h,e._footerFontStyle,e._footerFontFamily),l.each(e.footer,f),o+=2*e.xPadding,{width:o,height:n}}function a(t,e){var i=t._model,n=t._chart,o=t._chartInstance.chartArea,r="center",a="center";i.y<e.height?a="top":i.y>n.height-e.height&&(a="bottom");var s,l,d,c,u,h=(o.left+o.right)/2,p=(o.top+o.bottom)/2;"center"===a?(s=function(t){return h>=t},l=function(t){return t>h}):(s=function(t){return t<=e.width/2},l=function(t){return t>=n.width-e.width/2}),d=function(t){return t+e.width>n.width},c=function(t){return t-e.width<0},u=function(t){return p>=t?"top":"bottom"},s(i.x)?(r="left",d(i.x)&&(r="center",a=u(i.y))):l(i.x)&&(r="right",c(i.x)&&(r="center",a=u(i.y)));var f=t._options;return{xAlign:f.xAlign?f.xAlign:r,yAlign:f.yAlign?f.yAlign:a}}function s(t,e,i){var n=t.x,o=t.y,r=t.caretSize,a=t.caretPadding,s=t.cornerRadius,l=i.xAlign,d=i.yAlign,c=r+a,u=s+a;return"right"===l?n-=e.width:"center"===l&&(n-=e.width/2),"top"===d?o+=c:o-="bottom"===d?e.height+c:e.height/2,"center"===d?"left"===l?n+=c:"right"===l&&(n-=c):"left"===l?n-=u:"right"===l&&(n+=u),{x:n,y:o}}var l=t.helpers;t.defaults.global.tooltips={enabled:!0,custom:null,mode:"nearest",position:"average",intersect:!0,backgroundColor:"rgba(0,0,0,0.8)",titleFontStyle:"bold",titleSpacing:2,titleMarginBottom:6,titleFontColor:"#fff",titleAlign:"left",bodySpacing:2,bodyFontColor:"#fff",bodyAlign:"left",footerFontStyle:"bold",footerSpacing:2,footerMarginTop:6,footerFontColor:"#fff",footerAlign:"left",yPadding:6,xPadding:6,caretSize:5,cornerRadius:6,multiKeyBackground:"#fff",displayColors:!0,callbacks:{beforeTitle:l.noop,title:function(t,e){var i="",n=e.labels,o=n?n.length:0;if(t.length>0){var r=t[0];r.xLabel?i=r.xLabel:o>0&&r.index<o&&(i=n[r.index])}return i},afterTitle:l.noop,beforeBody:l.noop,beforeLabel:l.noop,label:function(t,e){var i=e.datasets[t.datasetIndex].label||"";return i+": "+t.yLabel},labelColor:function(t,e){var i=e.getDatasetMeta(t.datasetIndex),n=i.data[t.index],o=n._view;return{borderColor:o.borderColor,backgroundColor:o.backgroundColor}},afterLabel:l.noop,afterBody:l.noop,beforeFooter:l.noop,footer:l.noop,afterFooter:l.noop}},t.Tooltip=t.Element.extend({initialize:function(){this._model=o(this._options)},getTitle:function(){var t=this,e=t._options,n=e.callbacks,o=n.beforeTitle.apply(t,arguments),r=n.title.apply(t,arguments),a=n.afterTitle.apply(t,arguments),s=[];return s=i(s,o),s=i(s,r),s=i(s,a)},getBeforeBody:function(){var t=this._options.callbacks.beforeBody.apply(this,arguments);return l.isArray(t)?t:void 0!==t?[t]:[]},getBody:function(t,e){var n=this,o=n._options.callbacks,r=[];return l.each(t,function(t){var a={before:[],lines:[],after:[]};i(a.before,o.beforeLabel.call(n,t,e)),i(a.lines,o.label.call(n,t,e)),i(a.after,o.afterLabel.call(n,t,e)),r.push(a)}),r},getAfterBody:function(){var t=this._options.callbacks.afterBody.apply(this,arguments);return l.isArray(t)?t:void 0!==t?[t]:[]},getFooter:function(){var t=this,e=t._options.callbacks,n=e.beforeFooter.apply(t,arguments),o=e.footer.apply(t,arguments),r=e.afterFooter.apply(t,arguments),a=[];return a=i(a,n),a=i(a,o),a=i(a,r)},update:function(e){var i,d,c=this,u=c._options,h=c._model,p=c._model=o(u),f=c._active,g=c._data,m=c._chartInstance,v={xAlign:h.xAlign,yAlign:h.yAlign},y={x:h.x,y:h.y},b={width:h.width,height:h.height},x={x:h.caretX,y:h.caretY};if(f.length){p.opacity=1;var w=[];x=t.Tooltip.positioners[u.position](f,c._eventPosition);var k=[];for(i=0,d=f.length;d>i;++i)k.push(n(f[i]));u.filter&&(k=k.filter(function(t){return u.filter(t,g)})),u.itemSort&&(k=k.sort(function(t,e){return u.itemSort(t,e,g)})),l.each(k,function(t){w.push(u.callbacks.labelColor.call(c,t,m))}),p.title=c.getTitle(k,g),p.beforeBody=c.getBeforeBody(k,g),p.body=c.getBody(k,g),p.afterBody=c.getAfterBody(k,g),p.footer=c.getFooter(k,g),p.x=Math.round(x.x),p.y=Math.round(x.y),p.caretPadding=l.getValueOrDefault(x.padding,2),p.labelColors=w,p.dataPoints=k,b=r(this,p),v=a(this,b),y=s(p,b,v)}else p.opacity=0;return p.xAlign=v.xAlign,p.yAlign=v.yAlign,p.x=y.x,p.y=y.y,p.width=b.width,p.height=b.height,p.caretX=x.x,p.caretY=x.y,c._model=p,e&&u.custom&&u.custom.call(c,p),c},drawCaret:function(t,i,n){var o,r,a,s,l,d,c=this._view,u=this._chart.ctx,h=c.caretSize,p=c.cornerRadius,f=c.xAlign,g=c.yAlign,m=t.x,v=t.y,y=i.width,b=i.height;"center"===g?("left"===f?(o=m,r=o-h,a=o):(o=m+y,r=o+h,a=o),l=v+b/2,s=l-h,d=l+h):("left"===f?(o=m+p,r=o+h,a=r+h):"right"===f?(o=m+y-p,r=o-h,a=r-h):(r=m+y/2,o=r-h,a=r+h),"top"===g?(s=v,l=s-h,d=s):(s=v+b,l=s+h,d=s)),u.fillStyle=e(c.backgroundColor,n),u.beginPath(),u.moveTo(o,s),u.lineTo(r,l),u.lineTo(a,d),u.closePath(),u.fill()},drawTitle:function(t,i,n,o){var r=i.title;if(r.length){n.textAlign=i._titleAlign,n.textBaseline="top";var a=i.titleFontSize,s=i.titleSpacing;n.fillStyle=e(i.titleFontColor,o),n.font=l.fontString(a,i._titleFontStyle,i._titleFontFamily);var d,c;for(d=0,c=r.length;c>d;++d)n.fillText(r[d],t.x,t.y),t.y+=a+s,d+1===r.length&&(t.y+=i.titleMarginBottom-s)}},drawBody:function(t,i,n,o){var r=i.bodyFontSize,a=i.bodySpacing,s=i.body;n.textAlign=i._bodyAlign,n.textBaseline="top";var d=e(i.bodyFontColor,o);n.fillStyle=d,n.font=l.fontString(r,i._bodyFontStyle,i._bodyFontFamily);var c=0,u=function(e){n.fillText(e,t.x+c,t.y),t.y+=r+a};l.each(i.beforeBody,u);var h=i.displayColors;c=h?r+2:0,l.each(s,function(a,s){l.each(a.before,u),l.each(a.lines,function(a){h&&(n.fillStyle=e(i.legendColorBackground,o),n.fillRect(t.x,t.y,r,r),n.strokeStyle=e(i.labelColors[s].borderColor,o),n.strokeRect(t.x,t.y,r,r),n.fillStyle=e(i.labelColors[s].backgroundColor,o),n.fillRect(t.x+1,t.y+1,r-2,r-2),n.fillStyle=d),u(a)}),l.each(a.after,u)}),c=0,l.each(i.afterBody,u),t.y-=a},drawFooter:function(t,i,n,o){var r=i.footer;r.length&&(t.y+=i.footerMarginTop,n.textAlign=i._footerAlign,n.textBaseline="top",n.fillStyle=e(i.footerFontColor,o),n.font=l.fontString(i.footerFontSize,i._footerFontStyle,i._footerFontFamily),l.each(r,function(e){n.fillText(e,t.x,t.y),t.y+=i.footerFontSize+i.footerSpacing}))},drawBackground:function(t,i,n,o,r){n.fillStyle=e(i.backgroundColor,r),l.drawRoundedRectangle(n,t.x,t.y,o.width,o.height,i.cornerRadius),n.fill()},draw:function(){var t=this._chart.ctx,e=this._view;if(0!==e.opacity){var i={width:e.width,height:e.height},n={x:e.x,y:e.y},o=Math.abs(e.opacity<.001)?0:e.opacity;this._options.enabled&&(this.drawBackground(n,e,t,i,o),this.drawCaret(n,i,o),n.x+=e.xPadding,n.y+=e.yPadding,this.drawTitle(n,e,t,o),this.drawBody(n,e,t,o),this.drawFooter(n,e,t,o))}},handleEvent:function(t){var e=this,i=e._options,n=!1;if(e._lastActive=e._lastActive||[],"mouseout"===t.type?e._active=[]:e._active=e._chartInstance.getElementsAtEventForMode(t,i.mode,i),n=!l.arrayEquals(e._active,e._lastActive),e._lastActive=e._active,i.enabled||i.custom){e._eventPosition=l.getRelativePosition(t,e._chart);var o=e._model;e.update(!0),e.pivot(),n|=o.x!==e._model.x||o.y!==e._model.y}return n}}),t.Tooltip.positioners={average:function(t){if(!t.length)return!1;var e,i,n=0,o=0,r=0;for(e=0,i=t.length;i>e;++e){var a=t[e];if(a&&a.hasValue()){var s=a.tooltipPosition();n+=s.x,o+=s.y,++r}}return{x:Math.round(n/r),y:Math.round(o/r)}},nearest:function(t,e){var i,n,o,r=e.x,a=e.y,s=Number.POSITIVE_INFINITY;for(n=0,o=t.length;o>n;++n){var d=t[n];if(d&&d.hasValue()){var c=d.getCenterPoint(),u=l.distanceBetweenPoints(e,c);s>u&&(s=u,i=d)}}if(i){var h=i.tooltipPosition();r=h.x,a=h.y}return{x:r,y:a}}}}},{}],37:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers,i=t.defaults.global;i.elements.arc={backgroundColor:i.defaultColor,borderColor:"#fff",borderWidth:2},t.elements.Arc=t.Element.extend({inLabelRange:function(t){var e=this._view;return!!e&&Math.pow(t-e.x,2)<Math.pow(e.radius+e.hoverRadius,2)},inRange:function(t,i){var n=this._view;if(n){for(var o=e.getAngleFromPoint(n,{x:t,y:i}),r=o.angle,a=o.distance,s=n.startAngle,l=n.endAngle;s>l;)l+=2*Math.PI;for(;r>l;)r-=2*Math.PI;for(;s>r;)r+=2*Math.PI;var d=r>=s&&l>=r,c=a>=n.innerRadius&&a<=n.outerRadius;return d&&c}return!1},getCenterPoint:function(){var t=this._view,e=(t.startAngle+t.endAngle)/2,i=(t.innerRadius+t.outerRadius)/2;return{x:t.x+Math.cos(e)*i,y:t.y+Math.sin(e)*i}},getArea:function(){var t=this._view;return Math.PI*((t.endAngle-t.startAngle)/(2*Math.PI))*(Math.pow(t.outerRadius,2)-Math.pow(t.innerRadius,2))},tooltipPosition:function(){var t=this._view,e=t.startAngle+(t.endAngle-t.startAngle)/2,i=(t.outerRadius-t.innerRadius)/2+t.innerRadius;return{x:t.x+Math.cos(e)*i,y:t.y+Math.sin(e)*i}},draw:function(){var t=this._chart.ctx,e=this._view,i=e.startAngle,n=e.endAngle;t.beginPath(),t.arc(e.x,e.y,e.outerRadius,i,n),t.arc(e.x,e.y,e.innerRadius,n,i,!0),t.closePath(),t.strokeStyle=e.borderColor,t.lineWidth=e.borderWidth,t.fillStyle=e.backgroundColor,t.fill(),t.lineJoin="bevel",e.borderWidth&&t.stroke()}})}},{}],38:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers,i=t.defaults.global;t.defaults.global.elements.line={tension:.4,backgroundColor:i.defaultColor,borderWidth:3,borderColor:i.defaultColor,borderCapStyle:"butt",borderDash:[],borderDashOffset:0,borderJoinStyle:"miter",capBezierPoints:!0,fill:!0},t.elements.Line=t.Element.extend({draw:function(){function t(t,e){var i=e._view;e._view.steppedLine===!0?(l.lineTo(i.x,t._view.y),l.lineTo(i.x,i.y)):0===e._view.tension?l.lineTo(i.x,i.y):l.bezierCurveTo(t._view.controlPointNextX,t._view.controlPointNextY,i.controlPointPreviousX,i.controlPointPreviousY,i.x,i.y)}var n=this,o=n._view,r=o.spanGaps,a=o.scaleZero,s=n._loop;s||("top"===o.fill?a=o.scaleTop:"bottom"===o.fill&&(a=o.scaleBottom));var l=n._chart.ctx;l.save();var d=n._children.slice(),c=-1;s&&d.length&&d.push(d[0]);var u,h,p,f;if(d.length&&o.fill){for(l.beginPath(),u=0;u<d.length;++u)h=d[u],p=e.previousItem(d,u),f=h._view,0===u?(s?l.moveTo(a.x,a.y):l.moveTo(f.x,a),f.skip||(c=u,l.lineTo(f.x,f.y))):(p=-1===c?p:d[c],f.skip?r||c!==u-1||(s?l.lineTo(a.x,a.y):l.lineTo(p._view.x,a)):(c!==u-1?r&&-1!==c?t(p,h):s?l.lineTo(f.x,f.y):(l.lineTo(f.x,a),l.lineTo(f.x,f.y)):t(p,h),c=u));s||-1===c||l.lineTo(d[c]._view.x,a),l.fillStyle=o.backgroundColor||i.defaultColor,
l.closePath(),l.fill()}var g=i.elements.line;for(l.lineCap=o.borderCapStyle||g.borderCapStyle,l.setLineDash&&l.setLineDash(o.borderDash||g.borderDash),l.lineDashOffset=o.borderDashOffset||g.borderDashOffset,l.lineJoin=o.borderJoinStyle||g.borderJoinStyle,l.lineWidth=o.borderWidth||g.borderWidth,l.strokeStyle=o.borderColor||i.defaultColor,l.beginPath(),c=-1,u=0;u<d.length;++u)h=d[u],p=e.previousItem(d,u),f=h._view,0===u?f.skip||(l.moveTo(f.x,f.y),c=u):(p=-1===c?p:d[c],f.skip||(c!==u-1&&!r||-1===c?l.moveTo(f.x,f.y):t(p,h),c=u));l.stroke(),l.restore()}})}},{}],39:[function(t,e,i){"use strict";e.exports=function(t){function e(t){var e=this._view;return!!e&&Math.pow(t-e.x,2)<Math.pow(e.radius+e.hitRadius,2)}function i(t){var e=this._view;return!!e&&Math.pow(t-e.y,2)<Math.pow(e.radius+e.hitRadius,2)}var n=t.helpers,o=t.defaults.global,r=o.defaultColor;o.elements.point={radius:3,pointStyle:"circle",backgroundColor:r,borderWidth:1,borderColor:r,hitRadius:1,hoverRadius:4,hoverBorderWidth:1},t.elements.Point=t.Element.extend({inRange:function(t,e){var i=this._view;return!!i&&Math.pow(t-i.x,2)+Math.pow(e-i.y,2)<Math.pow(i.hitRadius+i.radius,2)},inLabelRange:e,inXRange:e,inYRange:i,getCenterPoint:function(){var t=this._view;return{x:t.x,y:t.y}},getArea:function(){return Math.PI*Math.pow(this._view.radius,2)},tooltipPosition:function(){var t=this._view;return{x:t.x,y:t.y,padding:t.radius+t.borderWidth}},draw:function(){var e=this._view,i=this._chart.ctx,a=e.pointStyle,s=e.radius,l=e.x,d=e.y;e.skip||(i.strokeStyle=e.borderColor||r,i.lineWidth=n.getValueOrDefault(e.borderWidth,o.elements.point.borderWidth),i.fillStyle=e.backgroundColor||r,t.canvasHelpers.drawPoint(i,a,s,l,d))}})}},{}],40:[function(t,e,i){"use strict";e.exports=function(t){function e(t){return void 0!==t._view.width}function i(t){var i,n,o,r,a=t._view;if(e(t)){var s=a.width/2;i=a.x-s,n=a.x+s,o=Math.min(a.y,a.base),r=Math.max(a.y,a.base)}else{var l=a.height/2;i=Math.min(a.x,a.base),n=Math.max(a.x,a.base),o=a.y-l,r=a.y+l}return{left:i,top:o,right:n,bottom:r}}var n=t.defaults.global;n.elements.rectangle={backgroundColor:n.defaultColor,borderWidth:0,borderColor:n.defaultColor,borderSkipped:"bottom"},t.elements.Rectangle=t.Element.extend({draw:function(){function t(t){return l[(c+t)%4]}var e=this._chart.ctx,i=this._view,n=i.width/2,o=i.x-n,r=i.x+n,a=i.base-(i.base-i.y),s=i.borderWidth/2;i.borderWidth&&(o+=s,r-=s,a+=s),e.beginPath(),e.fillStyle=i.backgroundColor,e.strokeStyle=i.borderColor,e.lineWidth=i.borderWidth;var l=[[o,i.base],[o,a],[r,a],[r,i.base]],d=["bottom","left","top","right"],c=d.indexOf(i.borderSkipped,0);-1===c&&(c=0);var u=t(0);e.moveTo(u[0],u[1]);for(var h=1;4>h;h++)u=t(h),e.lineTo(u[0],u[1]);e.fill(),i.borderWidth&&e.stroke()},height:function(){var t=this._view;return t.base-t.y},inRange:function(t,e){var n=!1;if(this._view){var o=i(this);n=t>=o.left&&t<=o.right&&e>=o.top&&e<=o.bottom}return n},inLabelRange:function(t,n){var o=this;if(!o._view)return!1;var r=!1,a=i(o);return r=e(o)?t>=a.left&&t<=a.right:n>=a.top&&n<=a.bottom},inXRange:function(t){var e=i(this);return t>=e.left&&t<=e.right},inYRange:function(t){var e=i(this);return t>=e.top&&t<=e.bottom},getCenterPoint:function(){var t,i,n=this._view;return e(this)?(t=n.x,i=(n.y+n.base)/2):(t=(n.x+n.base)/2,i=n.y),{x:t,y:i}},getArea:function(){var t=this._view;return t.width*Math.abs(t.y-t.base)},tooltipPosition:function(){var t=this._view;return{x:t.x,y:t.y}}})}},{}],41:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers,i={position:"bottom"},n=t.Scale.extend({getLabels:function(){var t=this.chart.data;return(this.isHorizontal()?t.xLabels:t.yLabels)||t.labels},determineDataLimits:function(){var t=this,i=t.getLabels();t.minIndex=0,t.maxIndex=i.length-1;var n;void 0!==t.options.ticks.min&&(n=e.indexOf(i,t.options.ticks.min),t.minIndex=-1!==n?n:t.minIndex),void 0!==t.options.ticks.max&&(n=e.indexOf(i,t.options.ticks.max),t.maxIndex=-1!==n?n:t.maxIndex),t.min=i[t.minIndex],t.max=i[t.maxIndex]},buildTicks:function(){var t=this,e=t.getLabels();t.ticks=0===t.minIndex&&t.maxIndex===e.length-1?e:e.slice(t.minIndex,t.maxIndex+1)},getLabelForIndex:function(t,e){var i=this,n=i.chart.data,o=i.isHorizontal();return n.xLabels&&o||n.yLabels&&!o?i.getRightValue(n.datasets[e].data[t]):i.ticks[t]},getPixelForValue:function(t,e,i,n){var o=this,r=Math.max(o.maxIndex+1-o.minIndex-(o.options.gridLines.offsetGridLines?0:1),1);if(void 0!==t&&isNaN(e)){var a=o.getLabels(),s=a.indexOf(t);e=-1!==s?s:e}if(o.isHorizontal()){var l=o.width-(o.paddingLeft+o.paddingRight),d=l/r,c=d*(e-o.minIndex)+o.paddingLeft;return(o.options.gridLines.offsetGridLines&&n||o.maxIndex===o.minIndex&&n)&&(c+=d/2),o.left+Math.round(c)}var u=o.height-(o.paddingTop+o.paddingBottom),h=u/r,p=h*(e-o.minIndex)+o.paddingTop;return o.options.gridLines.offsetGridLines&&n&&(p+=h/2),o.top+Math.round(p)},getPixelForTick:function(t,e){return this.getPixelForValue(this.ticks[t],t+this.minIndex,null,e)},getValueForPixel:function(t){var e,i=this,n=Math.max(i.ticks.length-(i.options.gridLines.offsetGridLines?0:1),1),o=i.isHorizontal(),r=o?i.width-(i.paddingLeft+i.paddingRight):i.height-(i.paddingTop+i.paddingBottom),a=r/n;return t-=o?i.left:i.top,i.options.gridLines.offsetGridLines&&(t-=a/2),t-=o?i.paddingLeft:i.paddingTop,e=0>=t?0:Math.round(t/a)},getBasePixel:function(){return this.bottom}});t.scaleService.registerScaleType("category",n,i)}},{}],42:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers,i={position:"left",ticks:{callback:t.Ticks.formatters.linear}},n=t.LinearScaleBase.extend({determineDataLimits:function(){function t(t){return s?t.xAxisID===i.id:t.yAxisID===i.id}var i=this,n=i.options,o=i.chart,r=o.data,a=r.datasets,s=i.isHorizontal();if(i.min=null,i.max=null,n.stacked){var l={};e.each(a,function(r,a){var s=o.getDatasetMeta(a);void 0===l[s.type]&&(l[s.type]={positiveValues:[],negativeValues:[]});var d=l[s.type].positiveValues,c=l[s.type].negativeValues;o.isDatasetVisible(a)&&t(s)&&e.each(r.data,function(t,e){var o=+i.getRightValue(t);isNaN(o)||s.data[e].hidden||(d[e]=d[e]||0,c[e]=c[e]||0,n.relativePoints?d[e]=100:0>o?c[e]+=o:d[e]+=o)})}),e.each(l,function(t){var n=t.positiveValues.concat(t.negativeValues),o=e.min(n),r=e.max(n);i.min=null===i.min?o:Math.min(i.min,o),i.max=null===i.max?r:Math.max(i.max,r)})}else e.each(a,function(n,r){var a=o.getDatasetMeta(r);o.isDatasetVisible(r)&&t(a)&&e.each(n.data,function(t,e){var n=+i.getRightValue(t);isNaN(n)||a.data[e].hidden||(null===i.min?i.min=n:n<i.min&&(i.min=n),null===i.max?i.max=n:n>i.max&&(i.max=n))})});this.handleTickRangeOptions()},getTickLimit:function(){var i,n=this,o=n.options.ticks;if(n.isHorizontal())i=Math.min(o.maxTicksLimit?o.maxTicksLimit:11,Math.ceil(n.width/50));else{var r=e.getValueOrDefault(o.fontSize,t.defaults.global.defaultFontSize);i=Math.min(o.maxTicksLimit?o.maxTicksLimit:11,Math.ceil(n.height/(2*r)))}return i},handleDirectionalChanges:function(){this.isHorizontal()||this.ticks.reverse()},getLabelForIndex:function(t,e){return+this.getRightValue(this.chart.data.datasets[e].data[t])},getPixelForValue:function(t){var e,i,n=this,o=n.paddingLeft,r=n.paddingBottom,a=n.start,s=+n.getRightValue(t),l=n.end-a;return n.isHorizontal()?(i=n.width-(o+n.paddingRight),e=n.left+i/l*(s-a),Math.round(e+o)):(i=n.height-(n.paddingTop+r),e=n.bottom-r-i/l*(s-a),Math.round(e))},getValueForPixel:function(t){var e=this,i=e.isHorizontal(),n=e.paddingLeft,o=e.paddingBottom,r=i?e.width-(n+e.paddingRight):e.height-(e.paddingTop+o),a=(i?t-e.left-n:e.bottom-o-t)/r;return e.start+(e.end-e.start)*a},getPixelForTick:function(t){return this.getPixelForValue(this.ticksAsNumbers[t])}});t.scaleService.registerScaleType("linear",n,i)}},{}],43:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers,i=e.noop;t.LinearScaleBase=t.Scale.extend({handleTickRangeOptions:function(){var t=this,i=t.options,n=i.ticks;if(n.beginAtZero){var o=e.sign(t.min),r=e.sign(t.max);0>o&&0>r?t.max=0:o>0&&r>0&&(t.min=0)}void 0!==n.min?t.min=n.min:void 0!==n.suggestedMin&&(t.min=Math.min(t.min,n.suggestedMin)),void 0!==n.max?t.max=n.max:void 0!==n.suggestedMax&&(t.max=Math.max(t.max,n.suggestedMax)),t.min===t.max&&(t.max++,n.beginAtZero||t.min--)},getTickLimit:i,handleDirectionalChanges:i,buildTicks:function(){var i=this,n=i.options,o=n.ticks,r=i.getTickLimit();r=Math.max(2,r);var a={maxTicks:r,min:o.min,max:o.max,stepSize:e.getValueOrDefault(o.fixedStepSize,o.stepSize)},s=i.ticks=t.Ticks.generators.linear(a,i);i.handleDirectionalChanges(),i.max=e.max(s),i.min=e.min(s),o.reverse?(s.reverse(),i.start=i.max,i.end=i.min):(i.start=i.min,i.end=i.max)},convertTicksToLabels:function(){var e=this;e.ticksAsNumbers=e.ticks.slice(),e.zeroLineIndex=e.ticks.indexOf(0),t.Scale.prototype.convertTicksToLabels.call(e)}})}},{}],44:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers,i={position:"left",ticks:{callback:t.Ticks.formatters.logarithmic}},n=t.Scale.extend({determineDataLimits:function(){function t(t){return d?t.xAxisID===i.id:t.yAxisID===i.id}var i=this,n=i.options,o=n.ticks,r=i.chart,a=r.data,s=a.datasets,l=e.getValueOrDefault,d=i.isHorizontal();if(i.min=null,i.max=null,i.minNotZero=null,n.stacked){var c={};e.each(s,function(o,a){var s=r.getDatasetMeta(a);r.isDatasetVisible(a)&&t(s)&&(void 0===c[s.type]&&(c[s.type]=[]),e.each(o.data,function(t,e){var o=c[s.type],r=+i.getRightValue(t);isNaN(r)||s.data[e].hidden||(o[e]=o[e]||0,n.relativePoints?o[e]=100:o[e]+=r)}))}),e.each(c,function(t){var n=e.min(t),o=e.max(t);i.min=null===i.min?n:Math.min(i.min,n),i.max=null===i.max?o:Math.max(i.max,o)})}else e.each(s,function(n,o){var a=r.getDatasetMeta(o);r.isDatasetVisible(o)&&t(a)&&e.each(n.data,function(t,e){var n=+i.getRightValue(t);isNaN(n)||a.data[e].hidden||(null===i.min?i.min=n:n<i.min&&(i.min=n),null===i.max?i.max=n:n>i.max&&(i.max=n),0!==n&&(null===i.minNotZero||n<i.minNotZero)&&(i.minNotZero=n))})});i.min=l(o.min,i.min),i.max=l(o.max,i.max),i.min===i.max&&(0!==i.min&&null!==i.min?(i.min=Math.pow(10,Math.floor(e.log10(i.min))-1),i.max=Math.pow(10,Math.floor(e.log10(i.max))+1)):(i.min=1,i.max=10))},buildTicks:function(){var i=this,n=i.options,o=n.ticks,r={min:o.min,max:o.max},a=i.ticks=t.Ticks.generators.logarithmic(r,i);i.isHorizontal()||a.reverse(),i.max=e.max(a),i.min=e.min(a),o.reverse?(a.reverse(),i.start=i.max,i.end=i.min):(i.start=i.min,i.end=i.max)},convertTicksToLabels:function(){this.tickValues=this.ticks.slice(),t.Scale.prototype.convertTicksToLabels.call(this)},getLabelForIndex:function(t,e){return+this.getRightValue(this.chart.data.datasets[e].data[t])},getPixelForTick:function(t){return this.getPixelForValue(this.tickValues[t])},getPixelForValue:function(t){var i,n,o,r=this,a=r.start,s=+r.getRightValue(t),l=r.paddingTop,d=r.paddingBottom,c=r.paddingLeft,u=r.options,h=u.ticks;return r.isHorizontal()?(o=e.log10(r.end)-e.log10(a),0===s?n=r.left+c:(i=r.width-(c+r.paddingRight),n=r.left+i/o*(e.log10(s)-e.log10(a)),n+=c)):(i=r.height-(l+d),0!==a||h.reverse?0===r.end&&h.reverse?(o=e.log10(r.start)-e.log10(r.minNotZero),n=s===r.end?r.top+l:s===r.minNotZero?r.top+l+.02*i:r.top+l+.02*i+.98*i/o*(e.log10(s)-e.log10(r.minNotZero))):(o=e.log10(r.end)-e.log10(a),i=r.height-(l+d),n=r.bottom-d-i/o*(e.log10(s)-e.log10(a))):(o=e.log10(r.end)-e.log10(r.minNotZero),n=s===a?r.bottom-d:s===r.minNotZero?r.bottom-d-.02*i:r.bottom-d-.02*i-.98*i/o*(e.log10(s)-e.log10(r.minNotZero)))),n},getValueForPixel:function(t){var i,n,o=this,r=e.log10(o.end)-e.log10(o.start);return o.isHorizontal()?(n=o.width-(o.paddingLeft+o.paddingRight),i=o.start*Math.pow(10,(t-o.left-o.paddingLeft)*r/n)):(n=o.height-(o.paddingTop+o.paddingBottom),i=Math.pow(10,(o.bottom-o.paddingBottom-t)*r/n)/o.start),i}});t.scaleService.registerScaleType("logarithmic",n,i)}},{}],45:[function(t,e,i){"use strict";e.exports=function(t){var e=t.helpers,i=t.defaults.global,n={display:!0,animate:!0,lineArc:!1,position:"chartArea",angleLines:{display:!0,color:"rgba(0, 0, 0, 0.1)",lineWidth:1},ticks:{showLabelBackdrop:!0,backdropColor:"rgba(255,255,255,0.75)",backdropPaddingY:2,backdropPaddingX:2,callback:t.Ticks.formatters.linear},pointLabels:{fontSize:10,callback:function(t){return t}}},o=t.LinearScaleBase.extend({getValueCount:function(){return this.chart.data.labels.length},setDimensions:function(){var t=this,n=t.options,o=n.ticks;t.width=t.maxWidth,t.height=t.maxHeight,t.xCenter=Math.round(t.width/2),t.yCenter=Math.round(t.height/2);var r=e.min([t.height,t.width]),a=e.getValueOrDefault(o.fontSize,i.defaultFontSize);t.drawingArea=n.display?r/2-(a/2+o.backdropPaddingY):r/2},determineDataLimits:function(){var t=this,i=t.chart;t.min=null,t.max=null,e.each(i.data.datasets,function(n,o){if(i.isDatasetVisible(o)){var r=i.getDatasetMeta(o);e.each(n.data,function(e,i){var n=+t.getRightValue(e);isNaN(n)||r.data[i].hidden||(null===t.min?t.min=n:n<t.min&&(t.min=n),null===t.max?t.max=n:n>t.max&&(t.max=n))})}}),t.handleTickRangeOptions()},getTickLimit:function(){var t=this.options.ticks,n=e.getValueOrDefault(t.fontSize,i.defaultFontSize);return Math.min(t.maxTicksLimit?t.maxTicksLimit:11,Math.ceil(this.drawingArea/(1.5*n)))},convertTicksToLabels:function(){var e=this;t.LinearScaleBase.prototype.convertTicksToLabels.call(e),e.pointLabels=e.chart.data.labels.map(e.options.pointLabels.callback,e)},getLabelForIndex:function(t,e){return+this.getRightValue(this.chart.data.datasets[e].data[t])},fit:function(){var t,n,o,r,a,s,l,d,c,u,h,p,f=this.options.pointLabels,g=e.getValueOrDefault(f.fontSize,i.defaultFontSize),m=e.getValueOrDefault(f.fontStyle,i.defaultFontStyle),v=e.getValueOrDefault(f.fontFamily,i.defaultFontFamily),y=e.fontString(g,m,v),b=e.min([this.height/2-g-5,this.width/2]),x=this.width,w=0;for(this.ctx.font=y,n=0;n<this.getValueCount();n++){t=this.getPointPosition(n,b),o=this.ctx.measureText(this.pointLabels[n]?this.pointLabels[n]:"").width+5;var k=this.getIndexAngle(n)+Math.PI/2,S=360*k/(2*Math.PI)%360;0===S||180===S?(r=o/2,t.x+r>x&&(x=t.x+r,a=n),t.x-r<w&&(w=t.x-r,l=n)):180>S?t.x+o>x&&(x=t.x+o,a=n):t.x-o<w&&(w=t.x-o,l=n)}c=w,u=Math.ceil(x-this.width),s=this.getIndexAngle(a),d=this.getIndexAngle(l),h=u/Math.sin(s+Math.PI/2),p=c/Math.sin(d+Math.PI/2),h=e.isNumber(h)?h:0,p=e.isNumber(p)?p:0,this.drawingArea=Math.round(b-(p+h)/2),this.setCenterPoint(p,h)},setCenterPoint:function(t,e){var i=this,n=i.width-e-i.drawingArea,o=t+i.drawingArea;i.xCenter=Math.round((o+n)/2+i.left),i.yCenter=Math.round(i.height/2+i.top)},getIndexAngle:function(t){var e=2*Math.PI/this.getValueCount(),i=this.chart.options&&this.chart.options.startAngle?this.chart.options.startAngle:0,n=i*Math.PI*2/360;return t*e-Math.PI/2+n},getDistanceFromCenterForValue:function(t){var e=this;if(null===t)return 0;var i=e.drawingArea/(e.max-e.min);return e.options.reverse?(e.max-t)*i:(t-e.min)*i},getPointPosition:function(t,e){var i=this,n=i.getIndexAngle(t);return{x:Math.round(Math.cos(n)*e)+i.xCenter,y:Math.round(Math.sin(n)*e)+i.yCenter}},getPointPositionForValue:function(t,e){return this.getPointPosition(t,this.getDistanceFromCenterForValue(e))},getBasePosition:function(){var t=this,e=t.min,i=t.max;return t.getPointPositionForValue(0,t.beginAtZero?0:0>e&&0>i?i:e>0&&i>0?e:0)},draw:function(){var t=this,n=t.options,o=n.gridLines,r=n.ticks,a=n.angleLines,s=n.pointLabels,l=e.getValueOrDefault;if(n.display){var d=t.ctx,c=l(r.fontSize,i.defaultFontSize),u=l(r.fontStyle,i.defaultFontStyle),h=l(r.fontFamily,i.defaultFontFamily),p=e.fontString(c,u,h);if(e.each(t.ticks,function(a,s){if(s>0||n.reverse){var u=t.getDistanceFromCenterForValue(t.ticksAsNumbers[s]),h=t.yCenter-u;if(o.display&&0!==s)if(d.strokeStyle=e.getValueAtIndexOrDefault(o.color,s-1),d.lineWidth=e.getValueAtIndexOrDefault(o.lineWidth,s-1),n.lineArc)d.beginPath(),d.arc(t.xCenter,t.yCenter,u,0,2*Math.PI),d.closePath(),d.stroke();else{d.beginPath();for(var f=0;f<t.getValueCount();f++){var g=t.getPointPosition(f,u);0===f?d.moveTo(g.x,g.y):d.lineTo(g.x,g.y)}d.closePath(),d.stroke()}if(r.display){var m=l(r.fontColor,i.defaultFontColor);if(d.font=p,r.showLabelBackdrop){var v=d.measureText(a).width;d.fillStyle=r.backdropColor,d.fillRect(t.xCenter-v/2-r.backdropPaddingX,h-c/2-r.backdropPaddingY,v+2*r.backdropPaddingX,c+2*r.backdropPaddingY)}d.textAlign="center",d.textBaseline="middle",d.fillStyle=m,d.fillText(a,t.xCenter,h)}}}),!n.lineArc){d.lineWidth=a.lineWidth,d.strokeStyle=a.color;for(var f=t.getDistanceFromCenterForValue(n.reverse?t.min:t.max),g=l(s.fontSize,i.defaultFontSize),m=l(s.fontStyle,i.defaultFontStyle),v=l(s.fontFamily,i.defaultFontFamily),y=e.fontString(g,m,v),b=t.getValueCount()-1;b>=0;b--){if(a.display){var x=t.getPointPosition(b,f);d.beginPath(),d.moveTo(t.xCenter,t.yCenter),d.lineTo(x.x,x.y),d.stroke(),d.closePath()}var w=t.getPointPosition(b,f+5),k=l(s.fontColor,i.defaultFontColor);d.font=y,d.fillStyle=k;var S=t.pointLabels,C=this.getIndexAngle(b)+Math.PI/2,T=360*C/(2*Math.PI)%360;0===T||180===T?d.textAlign="center":180>T?d.textAlign="left":d.textAlign="right",90===T||270===T?d.textBaseline="middle":T>270||90>T?d.textBaseline="bottom":d.textBaseline="top",d.fillText(S[b]?S[b]:"",w.x,w.y)}}}}});t.scaleService.registerScaleType("radialLinear",o,n)}},{}],46:[function(t,e,i){"use strict";var n=t(1);n="function"==typeof n?n:window.moment,e.exports=function(t){var e=t.helpers,i={units:[{name:"millisecond",steps:[1,2,5,10,20,50,100,250,500]},{name:"second",steps:[1,2,5,10,30]},{name:"minute",steps:[1,2,5,10,30]},{name:"hour",steps:[1,2,3,6,12]},{name:"day",steps:[1,2,5]},{name:"week",maxStep:4},{name:"month",maxStep:3},{name:"quarter",maxStep:4},{name:"year",maxStep:!1}]},o={position:"bottom",time:{parser:!1,format:!1,unit:!1,round:!1,displayFormat:!1,isoWeekday:!1,minUnit:"millisecond",displayFormats:{millisecond:"h:mm:ss.SSS a",second:"h:mm:ss a",minute:"h:mm:ss a",hour:"MMM D, hA",day:"ll",week:"ll",month:"MMM YYYY",quarter:"[Q]Q - YYYY",year:"YYYY"}},ticks:{autoSkip:!1}},r=t.Scale.extend({initialize:function(){if(!n)throw new Error("Chart.js - Moment.js could not be found! You must include it before Chart.js to use the time scale. Download at https://momentjs.com");t.Scale.prototype.initialize.call(this)},getLabelMoment:function(t,e){return null===t||null===e?null:"undefined"!=typeof this.labelMoments[t]?this.labelMoments[t][e]:null},getLabelDiff:function(t,e){var i=this;return null===t||null===e?null:(void 0===i.labelDiffs&&i.buildLabelDiffs(),"undefined"!=typeof i.labelDiffs[t]?i.labelDiffs[t][e]:null)},getMomentStartOf:function(t){var e=this;return"week"===e.options.time.unit&&e.options.time.isoWeekday!==!1?t.clone().startOf("isoWeek").isoWeekday(e.options.time.isoWeekday):t.clone().startOf(e.tickUnit)},determineDataLimits:function(){var t=this;t.labelMoments=[];var i=[];t.chart.data.labels&&t.chart.data.labels.length>0?(e.each(t.chart.data.labels,function(e){var n=t.parseTime(e);n.isValid()&&(t.options.time.round&&n.startOf(t.options.time.round),i.push(n))},t),t.firstTick=n.min.call(t,i),t.lastTick=n.max.call(t,i)):(t.firstTick=null,t.lastTick=null),e.each(t.chart.data.datasets,function(o,r){var a=[],s=t.chart.isDatasetVisible(r);"object"==typeof o.data[0]&&null!==o.data[0]?e.each(o.data,function(e){var i=t.parseTime(t.getRightValue(e));i.isValid()&&(t.options.time.round&&i.startOf(t.options.time.round),a.push(i),s&&(t.firstTick=null!==t.firstTick?n.min(t.firstTick,i):i,t.lastTick=null!==t.lastTick?n.max(t.lastTick,i):i))},t):a=i,t.labelMoments.push(a)},t),t.options.time.min&&(t.firstTick=t.parseTime(t.options.time.min)),t.options.time.max&&(t.lastTick=t.parseTime(t.options.time.max)),t.firstTick=(t.firstTick||n()).clone(),t.lastTick=(t.lastTick||n()).clone()},buildLabelDiffs:function(){var t=this;t.labelDiffs=[];var i=[];t.chart.data.labels&&t.chart.data.labels.length>0&&e.each(t.chart.data.labels,function(e){var n=t.parseTime(e);n.isValid()&&(t.options.time.round&&n.startOf(t.options.time.round),i.push(n.diff(t.firstTick,t.tickUnit,!0)))},t),e.each(t.chart.data.datasets,function(n){var o=[];"object"==typeof n.data[0]&&null!==n.data[0]?e.each(n.data,function(e){var i=t.parseTime(t.getRightValue(e));i.isValid()&&(t.options.time.round&&i.startOf(t.options.time.round),o.push(i.diff(t.firstTick,t.tickUnit,!0)))},t):o=i,t.labelDiffs.push(o)},t)},buildTicks:function(){var n=this;n.ctx.save();var o=e.getValueOrDefault(n.options.ticks.fontSize,t.defaults.global.defaultFontSize),r=e.getValueOrDefault(n.options.ticks.fontStyle,t.defaults.global.defaultFontStyle),a=e.getValueOrDefault(n.options.ticks.fontFamily,t.defaults.global.defaultFontFamily),s=e.fontString(o,r,a);if(n.ctx.font=s,n.ticks=[],n.unitScale=1,n.scaleSizeInUnits=0,n.options.time.unit)n.tickUnit=n.options.time.unit||"day",n.displayFormat=n.options.time.displayFormats[n.tickUnit],n.scaleSizeInUnits=n.lastTick.diff(n.firstTick,n.tickUnit,!0),n.unitScale=e.getValueOrDefault(n.options.time.unitStepSize,1);else{var l=n.isHorizontal()?n.width-(n.paddingLeft+n.paddingRight):n.height-(n.paddingTop+n.paddingBottom),d=n.tickFormatFunction(n.firstTick,0,[]),c=n.ctx.measureText(d).width,u=Math.cos(e.toRadians(n.options.ticks.maxRotation)),h=Math.sin(e.toRadians(n.options.ticks.maxRotation));c=c*u+o*h;var p=l/c;n.tickUnit=n.options.time.minUnit,n.scaleSizeInUnits=n.lastTick.diff(n.firstTick,n.tickUnit,!0),n.displayFormat=n.options.time.displayFormats[n.tickUnit];for(var f=0,g=i.units[f];f<i.units.length;){if(n.unitScale=1,e.isArray(g.steps)&&Math.ceil(n.scaleSizeInUnits/p)<e.max(g.steps)){for(var m=0;m<g.steps.length;++m)if(g.steps[m]>=Math.ceil(n.scaleSizeInUnits/p)){n.unitScale=e.getValueOrDefault(n.options.time.unitStepSize,g.steps[m]);break}break}if(g.maxStep===!1||Math.ceil(n.scaleSizeInUnits/p)<g.maxStep){n.unitScale=e.getValueOrDefault(n.options.time.unitStepSize,Math.ceil(n.scaleSizeInUnits/p));break}++f,g=i.units[f],n.tickUnit=g.name;var v=n.firstTick.diff(n.getMomentStartOf(n.firstTick),n.tickUnit,!0),y=n.getMomentStartOf(n.lastTick.clone().add(1,n.tickUnit)).diff(n.lastTick,n.tickUnit,!0);n.scaleSizeInUnits=n.lastTick.diff(n.firstTick,n.tickUnit,!0)+v+y,n.displayFormat=n.options.time.displayFormats[g.name]}}var b;if(n.options.time.min?b=n.getMomentStartOf(n.firstTick):(n.firstTick=n.getMomentStartOf(n.firstTick),b=n.firstTick),!n.options.time.max){var x=n.getMomentStartOf(n.lastTick),w=x.diff(n.lastTick,n.tickUnit,!0);0>w?n.lastTick=n.getMomentStartOf(n.lastTick.add(1,n.tickUnit)):w>=0&&(n.lastTick=x),n.scaleSizeInUnits=n.lastTick.diff(n.firstTick,n.tickUnit,!0)}n.options.time.displayFormat&&(n.displayFormat=n.options.time.displayFormat),n.ticks.push(n.firstTick.clone());for(var k=1;k<=n.scaleSizeInUnits;++k){var S=b.clone().add(k,n.tickUnit);if(n.options.time.max&&S.diff(n.lastTick,n.tickUnit,!0)>=0)break;k%n.unitScale===0&&n.ticks.push(S)}var C=n.ticks[n.ticks.length-1].diff(n.lastTick,n.tickUnit);(0!==C||0===n.scaleSizeInUnits)&&(n.options.time.max?(n.ticks.push(n.lastTick.clone()),n.scaleSizeInUnits=n.lastTick.diff(n.ticks[0],n.tickUnit,!0)):(n.ticks.push(n.lastTick.clone()),n.scaleSizeInUnits=n.lastTick.diff(n.firstTick,n.tickUnit,!0))),n.ctx.restore(),n.labelDiffs=void 0},getLabelForIndex:function(t,e){var i=this,n=i.chart.data.labels&&t<i.chart.data.labels.length?i.chart.data.labels[t]:"";return"object"==typeof i.chart.data.datasets[e].data[0]&&(n=i.getRightValue(i.chart.data.datasets[e].data[t])),i.options.time.tooltipFormat&&(n=i.parseTime(n).format(i.options.time.tooltipFormat)),n},tickFormatFunction:function(t,i,n){var o=t.format(this.displayFormat),r=this.options.ticks,a=e.getValueOrDefault(r.callback,r.userCallback);return a?a(o,i,n):o},convertTicksToLabels:function(){var t=this;t.tickMoments=t.ticks,t.ticks=t.ticks.map(t.tickFormatFunction,t)},getPixelForValue:function(t,e,i){var n=this,o=null;if(void 0!==e&&void 0!==i&&(o=n.getLabelDiff(i,e)),null===o&&(t&&t.isValid||(t=n.parseTime(n.getRightValue(t))),t&&t.isValid&&t.isValid()&&(o=t.diff(n.firstTick,n.tickUnit,!0))),null!==o){var r=0!==o?o/n.scaleSizeInUnits:o;if(n.isHorizontal()){var a=n.width-(n.paddingLeft+n.paddingRight),s=a*r+n.paddingLeft;return n.left+Math.round(s)}var l=n.height-(n.paddingTop+n.paddingBottom),d=l*r+n.paddingTop;return n.top+Math.round(d)}},getPixelForTick:function(t){return this.getPixelForValue(this.tickMoments[t],null,null)},getValueForPixel:function(t){var e=this,i=e.isHorizontal()?e.width-(e.paddingLeft+e.paddingRight):e.height-(e.paddingTop+e.paddingBottom),o=(t-(e.isHorizontal()?e.left+e.paddingLeft:e.top+e.paddingTop))/i;return o*=e.scaleSizeInUnits,e.firstTick.clone().add(n.duration(o,e.tickUnit).asSeconds(),"seconds")},parseTime:function(t){var e=this;return"string"==typeof e.options.time.parser?n(t,e.options.time.parser):"function"==typeof e.options.time.parser?e.options.time.parser(t):"function"==typeof t.getMonth||"number"==typeof t?n(t):t.isValid&&t.isValid()?t:"string"!=typeof e.options.time.format&&e.options.time.format.call?(console.warn("options.time.format is deprecated and replaced by options.time.parser. See http://nnnick.github.io/Chart.js/docs-v2/#scales-time-scale"),e.options.time.format(t)):n(t,e.options.time.format)}});t.scaleService.registerScaleType("time",r,o)}},{1:1}]},{},[7])(7)}),!function(t){"function"==typeof define&&define.amd?define([],t):"object"==typeof exports?module.exports=t():window.noUiSlider=t()}(function(){"use strict";function t(t,e){var i=document.createElement("div");return d(i,e),t.appendChild(i),i}function e(t){return t.filter(function(t){return!this[t]&&(this[t]=!0)},{})}function i(t,e){return Math.round(t/e)*e}function n(t,e){var i=t.getBoundingClientRect(),n=t.ownerDocument,o=n.documentElement,r=h();return/webkit.*Chrome.*Mobile/i.test(navigator.userAgent)&&(r.x=0),e?i.top+r.y-o.clientTop:i.left+r.x-o.clientLeft}function o(t){return"number"==typeof t&&!isNaN(t)&&isFinite(t)}function r(t,e,i){i>0&&(d(t,e),setTimeout(function(){c(t,e)},i))}function a(t){return Math.max(Math.min(t,100),0)}function s(t){return Array.isArray(t)?t:[t]}function l(t){t=String(t);var e=t.split(".");return e.length>1?e[1].length:0}function d(t,e){t.classList?t.classList.add(e):t.className+=" "+e}function c(t,e){t.classList?t.classList.remove(e):t.className=t.className.replace(new RegExp("(^|\\b)"+e.split(" ").join("|")+"(\\b|$)","gi")," ")}function u(t,e){return t.classList?t.classList.contains(e):new RegExp("\\b"+e+"\\b").test(t.className)}function h(){var t=void 0!==window.pageXOffset,e="CSS1Compat"===(document.compatMode||""),i=t?window.pageXOffset:e?document.documentElement.scrollLeft:document.body.scrollLeft,n=t?window.pageYOffset:e?document.documentElement.scrollTop:document.body.scrollTop;return{x:i,y:n}}function p(){return window.navigator.pointerEnabled?{start:"pointerdown",move:"pointermove",end:"pointerup"}:window.navigator.msPointerEnabled?{start:"MSPointerDown",move:"MSPointerMove",end:"MSPointerUp"}:{start:"mousedown touchstart",move:"mousemove touchmove",end:"mouseup touchend"}}function f(t,e){return 100/(e-t)}function g(t,e){return 100*e/(t[1]-t[0])}function m(t,e){return g(t,t[0]<0?e+Math.abs(t[0]):e-t[0])}function v(t,e){return e*(t[1]-t[0])/100+t[0]}function y(t,e){for(var i=1;t>=e[i];)i+=1;return i}function b(t,e,i){if(i>=t.slice(-1)[0])return 100;var n,o,r,a,s=y(i,t);return n=t[s-1],o=t[s],r=e[s-1],a=e[s],r+m([n,o],i)/f(r,a)}function x(t,e,i){if(i>=100)return t.slice(-1)[0];var n,o,r,a,s=y(i,e);return n=t[s-1],o=t[s],r=e[s-1],a=e[s],v([n,o],(i-r)*f(r,a))}function w(t,e,n,o){if(100===o)return o;var r,a,s=y(o,t);return n?(r=t[s-1],a=t[s],o-r>(a-r)/2?a:r):e[s-1]?t[s-1]+i(o-t[s-1],e[s-1]):o}function k(t,e,i){var n;if("number"==typeof e&&(e=[e]),"[object Array]"!==Object.prototype.toString.call(e))throw new Error("noUiSlider: 'range' contains invalid value.");if(n="min"===t?0:"max"===t?100:parseFloat(t),!o(n)||!o(e[0]))throw new Error("noUiSlider: 'range' value isn't numeric.");i.xPct.push(n),i.xVal.push(e[0]),n?i.xSteps.push(!isNaN(e[1])&&e[1]):isNaN(e[1])||(i.xSteps[0]=e[1]),i.xHighestCompleteStep.push(0)}function S(t,e,i){if(!e)return!0;i.xSteps[t]=g([i.xVal[t],i.xVal[t+1]],e)/f(i.xPct[t],i.xPct[t+1]);var n=(i.xVal[t+1]-i.xVal[t])/i.xNumSteps[t],o=Math.ceil(Number(n.toFixed(3))-1),r=i.xVal[t]+i.xNumSteps[t]*o;i.xHighestCompleteStep[t]=r}function C(t,e,i,n){this.xPct=[],this.xVal=[],this.xSteps=[n||!1],this.xNumSteps=[!1],this.xHighestCompleteStep=[],this.snap=e,this.direction=i;var o,r=[];for(o in t)t.hasOwnProperty(o)&&r.push([t[o],o]);for(r.length&&"object"==typeof r[0][0]?r.sort(function(t,e){return t[0][0]-e[0][0]}):r.sort(function(t,e){return t[0]-e[0]}),o=0;o<r.length;o++)k(r[o][1],r[o][0],this);for(this.xNumSteps=this.xSteps.slice(0),o=0;o<this.xNumSteps.length;o++)S(o,this.xNumSteps[o],this)}function T(t,e){if(!o(e))throw new Error("noUiSlider: 'step' is not numeric.");t.singleStep=e}function I(t,e){if("object"!=typeof e||Array.isArray(e))throw new Error("noUiSlider: 'range' is not an object.");if(void 0===e.min||void 0===e.max)throw new Error("noUiSlider: Missing 'min' or 'max' in 'range'.");if(e.min===e.max)throw new Error("noUiSlider: 'range' 'min' and 'max' cannot be equal.");t.spectrum=new C(e,t.snap,t.dir,t.singleStep)}function M(t,e){if(e=s(e),!Array.isArray(e)||!e.length)throw new Error("noUiSlider: 'start' option is incorrect.");t.handles=e.length,t.start=e}function A(t,e){if(t.snap=e,"boolean"!=typeof e)throw new Error("noUiSlider: 'snap' option must be a boolean.")}function _(t,e){if(t.animate=e,"boolean"!=typeof e)throw new Error("noUiSlider: 'animate' option must be a boolean.")}function D(t,e){if(t.animationDuration=e,"number"!=typeof e)throw new Error("noUiSlider: 'animationDuration' option must be a number.")}function P(t,e){var i,n=[!1];if(e===!0||e===!1){for(i=1;i<t.handles;i++)n.push(e);n.push(!1)}else{if(!Array.isArray(e)||!e.length||e.length!==t.handles+1)throw new Error("noUiSlider: 'connect' option doesn't match handle count.");n=e}t.connect=n}function E(t,e){switch(e){case"horizontal":t.ort=0;break;case"vertical":t.ort=1;break;default:throw new Error("noUiSlider: 'orientation' option is invalid.")}}function O(t,e){if(!o(e))throw new Error("noUiSlider: 'margin' option must be numeric.");if(0!==e&&(t.margin=t.spectrum.getMargin(e),!t.margin))throw new Error("noUiSlider: 'margin' option is only supported on linear sliders.")}function L(t,e){if(!o(e))throw new Error("noUiSlider: 'limit' option must be numeric.");if(t.limit=t.spectrum.getMargin(e),!t.limit||t.handles<2)throw new Error("noUiSlider: 'limit' option is only supported on linear sliders with 2 or more handles.")}function z(t,e){switch(e){case"ltr":t.dir=0;break;case"rtl":t.dir=1;break;default:throw new Error("noUiSlider: 'direction' option was not recognized.")}}function F(t,e){if("string"!=typeof e)throw new Error("noUiSlider: 'behaviour' must be a string containing options.");var i=e.indexOf("tap")>=0,n=e.indexOf("drag")>=0,o=e.indexOf("fixed")>=0,r=e.indexOf("snap")>=0,a=e.indexOf("hover")>=0;if(o){if(2!==t.handles)throw new Error("noUiSlider: 'fixed' behaviour must be used with 2 handles");O(t,t.start[1]-t.start[0])}t.events={tap:i||r,drag:n,fixed:o,snap:r,hover:a}}function R(t,e){if(e!==!1)if(e===!0){t.tooltips=[];for(var i=0;i<t.handles;i++)t.tooltips.push(!0)}else{if(t.tooltips=s(e),t.tooltips.length!==t.handles)throw new Error("noUiSlider: must pass a formatter for all handles.");t.tooltips.forEach(function(t){if("boolean"!=typeof t&&("object"!=typeof t||"function"!=typeof t.to))throw new Error("noUiSlider: 'tooltips' must be passed a formatter or 'false'.")})}}function N(t,e){if(t.format=e,"function"==typeof e.to&&"function"==typeof e.from)return!0;throw new Error("noUiSlider: 'format' requires 'to' and 'from' methods.")}function W(t,e){if(void 0!==e&&"string"!=typeof e&&e!==!1)throw new Error("noUiSlider: 'cssPrefix' must be a string or `false`.");t.cssPrefix=e}function B(t,e){if(void 0!==e&&"object"!=typeof e)throw new Error("noUiSlider: 'cssClasses' must be an object.");if("string"==typeof t.cssPrefix){t.cssClasses={};for(var i in e)e.hasOwnProperty(i)&&(t.cssClasses[i]=t.cssPrefix+e[i])}else t.cssClasses=e}function j(t,e){
if(e!==!0&&e!==!1)throw new Error("noUiSlider: 'useRequestAnimationFrame' option should be true (default) or false.");t.useRequestAnimationFrame=e}function V(t){var e,i={margin:0,limit:0,animate:!0,animationDuration:300,format:q};e={step:{r:!1,t:T},start:{r:!0,t:M},connect:{r:!0,t:P},direction:{r:!0,t:z},snap:{r:!1,t:A},animate:{r:!1,t:_},animationDuration:{r:!1,t:D},range:{r:!0,t:I},orientation:{r:!1,t:E},margin:{r:!1,t:O},limit:{r:!1,t:L},behaviour:{r:!0,t:F},format:{r:!1,t:N},tooltips:{r:!1,t:R},cssPrefix:{r:!1,t:W},cssClasses:{r:!1,t:B},useRequestAnimationFrame:{r:!1,t:j}};var n={connect:!1,direction:"ltr",behaviour:"tap",orientation:"horizontal",cssPrefix:"noUi-",cssClasses:{target:"target",base:"base",origin:"origin",handle:"handle",horizontal:"horizontal",vertical:"vertical",background:"background",connect:"connect",ltr:"ltr",rtl:"rtl",draggable:"draggable",drag:"state-drag",tap:"state-tap",active:"active",tooltip:"tooltip",pips:"pips",pipsHorizontal:"pips-horizontal",pipsVertical:"pips-vertical",marker:"marker",markerHorizontal:"marker-horizontal",markerVertical:"marker-vertical",markerNormal:"marker-normal",markerLarge:"marker-large",markerSub:"marker-sub",value:"value",valueHorizontal:"value-horizontal",valueVertical:"value-vertical",valueNormal:"value-normal",valueLarge:"value-large",valueSub:"value-sub"},useRequestAnimationFrame:!0};Object.keys(e).forEach(function(o){if(void 0===t[o]&&void 0===n[o]){if(e[o].r)throw new Error("noUiSlider: '"+o+"' is required.");return!0}e[o].t(i,void 0===t[o]?n[o]:t[o])}),i.pips=t.pips;var o=[["left","top"],["right","bottom"]];return i.style=o[i.dir][i.ort],i.styleOposite=o[i.dir?0:1][i.ort],i}function H(i,o,l){function f(e,i){var n=t(e,o.cssClasses.origin),r=t(n,o.cssClasses.handle);return r.setAttribute("data-handle",i),n}function g(e,i){return!!i&&t(e,o.cssClasses.connect)}function m(t,e){et=[],it=[],it.push(g(e,t[0]));for(var i=0;i<o.handles;i++)et.push(f(e,i)),st[i]=i,it.push(g(e,t[i+1]))}function v(e){d(e,o.cssClasses.target),0===o.dir?d(e,o.cssClasses.ltr):d(e,o.cssClasses.rtl),0===o.ort?d(e,o.cssClasses.horizontal):d(e,o.cssClasses.vertical),tt=t(e,o.cssClasses.base)}function y(e,i){return!!o.tooltips[i]&&t(e.firstChild,o.cssClasses.tooltip)}function b(){var t=et.map(y);Z("update",function(e,i,n){if(t[i]){var r=e[i];o.tooltips[i]!==!0&&(r=o.tooltips[i].to(n[i])),t[i].innerHTML=r}})}function x(t,e,i){if("range"===t||"steps"===t)return lt.xVal;if("count"===t){var n,o=100/(e-1),r=0;for(e=[];(n=r++*o)<=100;)e.push(n);t="positions"}return"positions"===t?e.map(function(t){return lt.fromStepping(i?lt.getStep(t):t)}):"values"===t?i?e.map(function(t){return lt.fromStepping(lt.getStep(lt.toStepping(t)))}):e:void 0}function w(t,i,n){function o(t,e){return(t+e).toFixed(7)/1}var r={},a=lt.xVal[0],s=lt.xVal[lt.xVal.length-1],l=!1,d=!1,c=0;return n=e(n.slice().sort(function(t,e){return t-e})),n[0]!==a&&(n.unshift(a),l=!0),n[n.length-1]!==s&&(n.push(s),d=!0),n.forEach(function(e,a){var s,u,h,p,f,g,m,v,y,b,x=e,w=n[a+1];if("steps"===i&&(s=lt.xNumSteps[a]),s||(s=w-x),x!==!1&&void 0!==w)for(s=Math.max(s,1e-7),u=x;u<=w;u=o(u,s)){for(p=lt.toStepping(u),f=p-c,v=f/t,y=Math.round(v),b=f/y,h=1;h<=y;h+=1)g=c+h*b,r[g.toFixed(5)]=["x",0];m=n.indexOf(u)>-1?1:"steps"===i?2:0,!a&&l&&(m=0),u===w&&d||(r[p.toFixed(5)]=[u,m]),c=p}}),r}function k(t,e,i){function n(t,e){var i=e===o.cssClasses.value,n=i?h:p,r=i?c:u;return e+" "+n[o.ort]+" "+r[t]}function r(t,e,i){return'class="'+n(i[1],e)+'" style="'+o.style+": "+t+'%"'}function a(t,n){n[1]=n[1]&&e?e(n[0],n[1]):n[1],l+="<div "+r(t,o.cssClasses.marker,n)+"></div>",n[1]&&(l+="<div "+r(t,o.cssClasses.value,n)+">"+i.to(n[0])+"</div>")}var s=document.createElement("div"),l="",c=[o.cssClasses.valueNormal,o.cssClasses.valueLarge,o.cssClasses.valueSub],u=[o.cssClasses.markerNormal,o.cssClasses.markerLarge,o.cssClasses.markerSub],h=[o.cssClasses.valueHorizontal,o.cssClasses.valueVertical],p=[o.cssClasses.markerHorizontal,o.cssClasses.markerVertical];return d(s,o.cssClasses.pips),d(s,0===o.ort?o.cssClasses.pipsHorizontal:o.cssClasses.pipsVertical),Object.keys(t).forEach(function(e){a(e,t[e])}),s.innerHTML=l,s}function S(t){var e=t.mode,i=t.density||1,n=t.filter||!1,o=t.values||!1,r=t.stepped||!1,a=x(e,o,r),s=w(i,e,a),l=t.format||{to:Math.round};return rt.appendChild(k(s,n,l))}function C(){var t=tt.getBoundingClientRect(),e="offset"+["Width","Height"][o.ort];return 0===o.ort?t.width||tt[e]:t.height||tt[e]}function T(t,e,i,n){var r=function(e){return!rt.hasAttribute("disabled")&&!u(rt,o.cssClasses.tap)&&(e=I(e,n.pageOffset),!(t===ot.start&&void 0!==e.buttons&&e.buttons>1)&&(!n.hover||!e.buttons)&&(e.calcPoint=e.points[o.ort],void i(e,n)))},a=[];return t.split(" ").forEach(function(t){e.addEventListener(t,r,!1),a.push([t,r])}),a}function I(t,e){t.preventDefault();var i,n,o=0===t.type.indexOf("touch"),r=0===t.type.indexOf("mouse"),a=0===t.type.indexOf("pointer"),s=t;if(0===t.type.indexOf("MSPointer")&&(a=!0),o){if(s.touches.length>1)return!1;i=t.changedTouches[0].pageX,n=t.changedTouches[0].pageY}return e=e||h(),(r||a)&&(i=t.clientX+e.x,n=t.clientY+e.y),s.pageOffset=e,s.points=[i,n],s.cursor=r||a,s}function M(t){var e=t-n(tt,o.ort),i=100*e/C();return o.dir?100-i:i}function A(t){var e=100,i=!1;return et.forEach(function(n,o){if(!n.hasAttribute("disabled")){var r=Math.abs(at[o]-t);r<e&&(i=o,e=r)}}),i}function _(t,e,i,n){var o=i.slice(),r=[!t,t],a=[t,!t];n=n.slice(),t&&n.reverse(),n.length>1?n.forEach(function(t,i){var n=N(o,t,o[t]+e,r[i],a[i]);n===!1?e=0:(e=n-o[t],o[t]=n)}):r=a=[!0];var s=!1;n.forEach(function(t,n){s=H(t,i[t]+e,r[n],a[n])||s}),s&&n.forEach(function(t){D("update",t),D("slide",t)})}function D(t,e,i){Object.keys(ct).forEach(function(n){var r=n.split(".")[0];t===r&&ct[n].forEach(function(t){t.call(nt,dt.map(o.format.to),e,dt.slice(),i||!1,at.slice())})})}function P(t,e){"mouseout"===t.type&&"HTML"===t.target.nodeName&&null===t.relatedTarget&&O(t,e)}function E(t,e){if(navigator.appVersion.indexOf("MSIE 9")===-1&&0===t.buttons&&0!==e.buttonsProperty)return O(t,e);var i=(o.dir?-1:1)*(t.calcPoint-e.startCalcPoint),n=100*i/e.baseSize;_(i>0,n,e.locations,e.handleNumbers)}function O(t,e){var i=tt.querySelector("."+o.cssClasses.active);null!==i&&c(i,o.cssClasses.active),t.cursor&&(document.body.style.cursor="",document.body.removeEventListener("selectstart",document.body.noUiListener)),document.documentElement.noUiListeners.forEach(function(t){document.documentElement.removeEventListener(t[0],t[1])}),c(rt,o.cssClasses.drag),j(),e.handleNumbers.forEach(function(t){D("set",t),D("change",t),D("end",t)})}function L(t,e){if(1===e.handleNumbers.length){var i=et[e.handleNumbers[0]];if(i.hasAttribute("disabled"))return!1;d(i.children[0],o.cssClasses.active)}t.preventDefault(),t.stopPropagation();var n=T(ot.move,document.documentElement,E,{startCalcPoint:t.calcPoint,baseSize:C(),pageOffset:t.pageOffset,handleNumbers:e.handleNumbers,buttonsProperty:t.buttons,locations:at.slice()}),r=T(ot.end,document.documentElement,O,{handleNumbers:e.handleNumbers}),a=T("mouseout",document.documentElement,P,{handleNumbers:e.handleNumbers});if(document.documentElement.noUiListeners=n.concat(r,a),t.cursor){document.body.style.cursor=getComputedStyle(t.target).cursor,et.length>1&&d(rt,o.cssClasses.drag);var s=function(){return!1};document.body.noUiListener=s,document.body.addEventListener("selectstart",s,!1)}e.handleNumbers.forEach(function(t){D("start",t)})}function z(t){t.stopPropagation();var e=M(t.calcPoint),i=A(e);return i!==!1&&(o.events.snap||r(rt,o.cssClasses.tap,o.animationDuration),H(i,e,!0,!0),j(),D("slide",i,!0),D("set",i,!0),D("change",i,!0),D("update",i,!0),void(o.events.snap&&L(t,{handleNumbers:[i]})))}function F(t){var e=M(t.calcPoint),i=lt.getStep(e),n=lt.fromStepping(i);Object.keys(ct).forEach(function(t){"hover"===t.split(".")[0]&&ct[t].forEach(function(t){t.call(nt,n)})})}function R(t){t.fixed||et.forEach(function(t,e){T(ot.start,t.children[0],L,{handleNumbers:[e]})}),t.tap&&T(ot.start,tt,z,{}),t.hover&&T(ot.move,tt,F,{hover:!0}),t.drag&&it.forEach(function(e,i){if(e!==!1&&0!==i&&i!==it.length-1){var n=et[i-1],r=et[i],a=[e];d(e,o.cssClasses.draggable),t.fixed&&(a.push(n.children[0]),a.push(r.children[0])),a.forEach(function(t){T(ot.start,t,L,{handles:[n,r],handleNumbers:[i-1,i]})})}})}function N(t,e,i,n,r){return et.length>1&&(n&&e>0&&(i=Math.max(i,t[e-1]+o.margin)),r&&e<et.length-1&&(i=Math.min(i,t[e+1]-o.margin))),et.length>1&&o.limit&&(n&&e>0&&(i=Math.min(i,t[e-1]+o.limit)),r&&e<et.length-1&&(i=Math.max(i,t[e+1]-o.limit))),i=lt.getStep(i),i=a(i),i!==t[e]&&i}function W(t){return t+"%"}function B(t,e){at[t]=e,dt[t]=lt.fromStepping(e);var i=function(){et[t].style[o.style]=W(e),U(t),U(t+1)};window.requestAnimationFrame&&o.useRequestAnimationFrame?window.requestAnimationFrame(i):i()}function j(){st.forEach(function(t){var e=at[t]>50?-1:1,i=3+(et.length+e*t);et[t].childNodes[0].style.zIndex=i})}function H(t,e,i,n){return e=N(at,t,e,i,n),e!==!1&&(B(t,e),!0)}function U(t){if(it[t]){var e=0,i=100;0!==t&&(e=at[t-1]),t!==it.length-1&&(i=at[t]),it[t].style[o.style]=W(e),it[t].style[o.styleOposite]=W(100-i)}}function q(t,e){null!==t&&t!==!1&&("number"==typeof t&&(t=String(t)),t=o.format.from(t),t===!1||isNaN(t)||H(e,lt.toStepping(t),!1,!1))}function Y(t,e){var i=s(t),n=void 0===at[0];e=void 0===e||!!e,i.forEach(q),o.animate&&!n&&r(rt,o.cssClasses.tap,o.animationDuration),st.forEach(function(t){H(t,at[t],!0,!1)}),j(),st.forEach(function(t){D("update",t),null!==i[t]&&e&&D("set",t)})}function Q(t){Y(o.start,t)}function X(){var t=dt.map(o.format.to);return 1===t.length?t[0]:t}function G(){for(var t in o.cssClasses)o.cssClasses.hasOwnProperty(t)&&c(rt,o.cssClasses[t]);for(;rt.firstChild;)rt.removeChild(rt.firstChild);delete rt.noUiSlider}function K(){return at.map(function(t,e){var i=lt.getNearbySteps(t),n=dt[e],o=i.thisStep.step,r=null;o!==!1&&n+o>i.stepAfter.startValue&&(o=i.stepAfter.startValue-n),r=n>i.thisStep.startValue?i.thisStep.step:i.stepBefore.step!==!1&&n-i.stepBefore.highestStep,100===t?o=null:0===t&&(r=null);var a=lt.countStepDecimals();return null!==o&&o!==!1&&(o=Number(o.toFixed(a))),null!==r&&r!==!1&&(r=Number(r.toFixed(a))),[r,o]})}function Z(t,e){ct[t]=ct[t]||[],ct[t].push(e),"update"===t.split(".")[0]&&et.forEach(function(t,e){D("update",e)})}function $(t){var e=t&&t.split(".")[0],i=e&&t.substring(e.length);Object.keys(ct).forEach(function(t){var n=t.split(".")[0],o=t.substring(n.length);e&&e!==n||i&&i!==o||delete ct[t]})}function J(t,e){var i=X(),n=["margin","limit","range","animate","snap","step","format"];n.forEach(function(e){void 0!==t[e]&&(l[e]=t[e])});var r=V(l);n.forEach(function(e){void 0!==t[e]&&(o[e]=r[e])}),r.spectrum.direction=lt.direction,lt=r.spectrum,o.margin=r.margin,o.limit=r.limit,at=[],Y(t.start||i,e)}var tt,et,it,nt,ot=p(),rt=i,at=[],st=[],lt=o.spectrum,dt=[],ct={};if(rt.noUiSlider)throw new Error("Slider was already initialized.");return v(rt),m(o.connect,tt),nt={destroy:G,steps:K,on:Z,off:$,get:X,set:Y,reset:Q,__moveHandles:function(t,e,i){_(t,e,at,i)},options:l,updateOptions:J,target:rt,pips:S},R(o.events),Y(o.start),o.pips&&S(o.pips),o.tooltips&&b(),nt}function U(t,e){if(!t.nodeName)throw new Error("noUiSlider.create requires a single element.");var i=V(e,t),n=H(t,i,e);return t.noUiSlider=n,n}C.prototype.getMargin=function(t){var e=this.xNumSteps[0];if(e&&t%e)throw new Error("noUiSlider: 'limit' and 'margin' must be divisible by step.");return 2===this.xPct.length&&g(this.xVal,t)},C.prototype.toStepping=function(t){return t=b(this.xVal,this.xPct,t)},C.prototype.fromStepping=function(t){return x(this.xVal,this.xPct,t)},C.prototype.getStep=function(t){return t=w(this.xPct,this.xSteps,this.snap,t)},C.prototype.getNearbySteps=function(t){var e=y(t,this.xPct);return{stepBefore:{startValue:this.xVal[e-2],step:this.xNumSteps[e-2],highestStep:this.xHighestCompleteStep[e-2]},thisStep:{startValue:this.xVal[e-1],step:this.xNumSteps[e-1],highestStep:this.xHighestCompleteStep[e-1]},stepAfter:{startValue:this.xVal[e-0],step:this.xNumSteps[e-0],highestStep:this.xHighestCompleteStep[e-0]}}},C.prototype.countStepDecimals=function(){var t=this.xNumSteps.map(l);return Math.max.apply(null,t)},C.prototype.convert=function(t){return this.getStep(this.toStepping(t))};var q={to:function(t){return void 0!==t&&t.toFixed(2)},from:Number};return{create:U}});
jQuery(function($){var displaylimit=2;var screenname="Dan Fisher";var showdirecttweets=false;var showretweets=true;var showtweetlinks=true;var showprofilepic=true;var showtweetactions=true;var showretweetindicator=false;var headerHTML='';var loadingHTML='';loadingHTML+='<div class="twitter-feed__preloader">Please wait...</div>';$('.twitter-feed').html(headerHTML+loadingHTML);$.getJSON('php/get-tweets.php',function(feeds){var feedHTML='';var displayCounter=1;for(var i=0;i<feeds.length;i++){var tweetscreenname=feeds[i].user.name;var tweetusername=feeds[i].user.screen_name;var profileimage=feeds[i].user.profile_image_url_https;var status=feeds[i].text;var isaretweet=false;var isdirect=false;var tweetid=feeds[i].id_str;if(typeof feeds[i].retweeted_status!='undefined'){profileimage=feeds[i].retweeted_status.user.profile_image_url_https;tweetscreenname=feeds[i].retweeted_status.user.name;tweetusername=feeds[i].retweeted_status.user.screen_name;tweetid=feeds[i].retweeted_status.id_str
isaretweet=true;};if(feeds[i].text.substr(0,1)=="@"){isdirect=true;}if(((showretweets==true)||((isaretweet==false)&&(showretweets==false)))&&((showdirecttweets==true)||((showdirecttweets==false)&&(isdirect==false)))){if((feeds[i].text.length>1)&&(displayCounter<=displaylimit)){if(showtweetlinks==true){status=addlinks(status);}if(displayCounter==1){feedHTML+=headerHTML;}feedHTML+='<li class="twitter-feed__item">';feedHTML+='<header class="twitter-feed__header">';feedHTML+='<figure class="twitter-feed__thumb">';feedHTML+='<a href="https://twitter.com/'+tweetusername+'" target="_blank"><img src="'+profileimage+'"images/twitter-feed-icon.png" width="40" height="40" alt="Avatar" /></a>';feedHTML+='</figure>';feedHTML+='<div class="twitter-feed__info">';feedHTML+='<h5 class="twitter-feed__name">'+tweetscreenname+'</h5>';feedHTML+='<h6 class="twitter-feed__username"><a href="https://twitter.com/'+tweetusername+'" target="_blank">@'+tweetusername+'</a></h6>';feedHTML+='</div>';feedHTML+='</header>';feedHTML+='<div class="twitter-feed__body">'+status+'</div>';feedHTML+='<footer class="twitter-feed__footer">';feedHTML+='<div class="twitter-feed__timestamp">';feedHTML+='<a href="https://twitter.com/'+tweetusername+'/status/'+tweetid+'">'+relative_time(feeds[i].created_at)+'</a>';feedHTML+='</div>';if(showtweetactions==true){feedHTML+='<div class="twitter-feed__actions">';feedHTML+='<a href="https://twitter.com/intent/tweet?in_reply_to='+tweetid+'" class="twitter-feed__reply"></a>';feedHTML+='<a href="https://twitter.com/intent/retweet?tweet_id='+tweetid+'" class="twitter-feed__retweet"></a>';feedHTML+='<a href="https://twitter.com/intent/favorite?tweet_id='+tweetid+'" class="twitter-feed__favorite"></a>';feedHTML+='</div>';}feedHTML+='</footer>';feedHTML+='</li>';displayCounter++;}}}$('.twitter-feed').html(feedHTML);if(showtweetactions==true){$('.twitter-feed__actions a').click(function(){var url=$(this).attr('href');window.open(url,'tweet action window','width=580,height=500');return false;});}});function addlinks(data){data=data.replace(/((https?|s?ftp|ssh)\:\/\/[^"\s\<\>]*[^.,;'">\:\s\<\>\)\]\!])/g,function(url){return'<a href="'+url+'" >'+url+'</a>';});data=data.replace(/\B@([_a-z0-9]+)/ig,function(reply){return'<a href="http://twitter.com/'+reply.substring(1)+'" style="font-weight:lighter;" >'+reply.charAt(0)+reply.substring(1)+'</a>';});return data;}function relative_time(time_value){var values=time_value.split(" ");time_value=values[1]+" "+values[2]+", "+values[5]+" "+values[3];var parsed_date=Date.parse(time_value);var relative_to=(arguments.length>1)?arguments[1]:new Date();var delta=parseInt((relative_to.getTime()-parsed_date)/1000);delta=delta+(relative_to.getTimezoneOffset()*60);if(delta<60){return'less than a minute ago';}else if(delta<120){return'about a minute ago';}else if(delta<(60*60)){return(parseInt(delta/60)).toString()+' minutes ago';}else if(delta<(120*60)){return'about an hour ago';}else if(delta<(24*60*60)){return'about '+(parseInt(delta/3600)).toString()+' hours ago';}else if(delta<(48*60*60)){return'1 day ago';}else{return(parseInt(delta/86400)).toString()+' days ago';}}});
;(function($){"use strict";$('body').jpreLoader({showSplash:false,loaderVPos:"50%",});$.fn.exists=function(){return this.length>0;};if(window.getComputedStyle){var $template_var=window.getComputedStyle(document.body,':after').getPropertyValue('content');}var $color_primary='#ffdc11',$main_nav=$('.main-nav'),$circular_bar=$('.circular__bar'),$gmap=$('.gm-map'),$mp_single=$('.mp_single-img'),$mp_gallery=$('.mp_gallery'),$mp_iframe=$('.mp_iframe'),$post_fitRows=$('.post-grid--fitRows'),$post_masonry=$('.post-grid--masonry'),$team_album=$('.album'),$slick_featured_slider=$('.posts--slider-featured'),$slick_featured_carousel=$('.featured-carousel'),$slick_video_carousel=$('.video-carousel'),$slick_team_roster=$('.team-roster--slider'),$slick_awards=$('.awards--slider'),$slick_player_info=$('.player-info'),$slick_product=$('.product__slider'),$slick_product_soccer=$('.product__slider-soccer'),$slick_team_roster_card=$('.team-roster--card-slider'),$slick_hero_slider=$('.hero-slider'),$chart_games_history=$('#games-history'),$chart_games_history_soccer=$('#games-history-soccer'),$chart_points_history=$('#points-history'),$chart_points_history_soccer=$('#points-history-soccer'),$chart_player_stats=$('#player-stats'),$content_filter=$('.content-filter'),$range_slider=$('#slider-range');if($template_var=='soccer'){$color_primary='#1892ed';}var Core={initialize:function(){this.SvgPolyfill();this.headerNav();this.countDown();this.circularBar();this.MagnificPopup();this.isotope();this.SlickCarousel();this.ContentFilter();this.ChartJs();this.RangeSlider();this.miscScripts();},SvgPolyfill:function(){svg4everybody();},headerNav:function(){if($main_nav.exists()){var $top_nav=$('.nav-account'),$top_nav_li=$('.nav-account > li'),$social=$('.social-links--main-nav'),$info_nav_li=$('.info-block--header > li'),$wrapper=$('.site-wrapper'),$nav_list=$('.main-nav__list'),$nav_list_li=$('.main-nav__list > li'),$toggle_btn=$('#header-mobile__toggle'),$pushy_btn=$('.pushy-panel__toggle');var $header_search_form=$('.header-search-form').clone();$('#header-mobile').append($header_search_form);var $shop_cart=$('.info-block__item--shopping-cart > .info-block__link-wrapper').clone();$shop_cart.appendTo($nav_list).wrap('<li class="main-nav__item--shopping-cart"></li>');if($top_nav.exists()){var children=$top_nav.children().clone();$nav_list.append(children);}var $logo_mobile=$('.header-mobile__logo').clone();$nav_list.prepend($logo_mobile);$logo_mobile.prepend('<span class="main-nav__back"></span>');var header_info1=$('.info-block__item--contact-primary').clone();var header_info2=$('.info-block__item--contact-secondary').clone();$nav_list.append(header_info1).append(header_info2);if($social.exists()){var social_li=$social.children().clone();var social_li_new=social_li.contents().unwrap();social_li_new.appendTo($nav_list).wrapAll('<li class="main-nav__item--social-links"></li>');}$top_nav_li.has('ul').addClass('has-children');$info_nav_li.has('ul').addClass('has-children');$toggle_btn.on('click',function(){$wrapper.toggleClass('site-wrapper--has-overlay');});$('.site-overlay, .main-nav__back').on('click',function(){$wrapper.toggleClass('site-wrapper--has-overlay');});$pushy_btn.on('click',function(e){e.preventDefault();$wrapper.toggleClass('site-wrapper--has-overlay-pushy');});$('.site-overlay, .pushy-panel__back-btn').on('click',function(e){e.preventDefault();$wrapper.removeClass('site-wrapper--has-overlay-pushy site-wrapper--has-overlay');});$nav_list_li.has('.main-nav__sub').addClass('has-children').prepend('<span class="main-nav__toggle"></span>');$nav_list_li.has('.main-nav__megamenu').addClass('has-children').prepend('<span class="main-nav__toggle"></span>');$('.main-nav__toggle').on('click',function(){$(this).toggleClass('main-nav__toggle--rotate').parent().siblings().children().removeClass('main-nav__toggle--rotate');$(".main-nav__sub, .main-nav__megamenu").not($(this).siblings('.main-nav__sub, .main-nav__megamenu')).slideUp('normal');$(this).siblings('.main-nav__sub').slideToggle('normal');$(this).siblings('.main-nav__megamenu').slideToggle('normal');});$('.main-nav__list > li > ul > li').has('.main-nav__sub-2').addClass('has-children').prepend('<span class="main-nav__toggle-2"></span>');$('.main-nav__list > li > ul > li > ul > li').has('.main-nav__sub-3').addClass('has-children').prepend('<span class="main-nav__toggle-2"></span>');$('.main-nav__toggle-2').on('click',function(){$(this).toggleClass('main-nav__toggle--rotate');$(this).siblings('.main-nav__sub-2').slideToggle('normal');$(this).siblings('.main-nav__sub-3').slideToggle('normal');});$('#header-mobile__search-icon').on('click',function(){$(this).toggleClass('header-mobile__search-icon--close');$('.header-mobile').toggleClass('header-mobile--expanded');});}},countDown:function(){var countdown=$('.countdown-counter');var count_time=countdown.data('date');countdown.countdown({date:count_time,render:function(data){$(this.el).html("<div class='countdown-counter__item countdown-counter__item--days'>"+this.leadingZeros(data.days,2)+" <span class='countdown-counter__label'>days</span></div><div class='countdown-counter__item countdown-counter__item--hours'>"+this.leadingZeros(data.hours,2)+" <span class='countdown-counter__label'>hours</span></div><div class='countdown-counter__item countdown-counter__item--mins'>"+this.leadingZeros(data.min,2)+" <span class='countdown-counter__label'>mins</span></div><div class='countdown-counter__item countdown-counter__item--secs'>"+this.leadingZeros(data.sec,2)+" <span class='countdown-counter__label'>secs</span></div>");}});},circularBar:function(){if($circular_bar.exists()){$circular_bar.easyPieChart({barColor:$color_primary,trackColor:'#ecf0f6',lineCap:'square',lineWidth:8,size:90,scaleLength:0});}},MagnificPopup:function(){if($mp_single.exists()){$('.mp_single-img').magnificPopup({type:'image',removalDelay:300,gallery:{enabled:false},mainClass:'mfp-fade',autoFocusLast:false,});};if($mp_gallery.exists()){$('.mp_gallery').magnificPopup({type:'image',removalDelay:300,gallery:{enabled:true},mainClass:'mfp-fade',autoFocusLast:false,});};if($mp_iframe.exists()){$('.mp_iframe').magnificPopup({type:'iframe',removalDelay:300,mainClass:'mfp-fade',autoFocusLast:false,patterns:{youtube:{index:'youtube.com/',id:'v=',src:'//www.youtube.com/embed/%id%?autoplay=1'},vimeo:{index:'vimeo.com/',id:'/',src:'//player.vimeo.com/video/%id%?autoplay=1'},gmaps:{index:'//maps.google.',src:'%id%&output=embed'}},srcAction:'iframe_src',});};},isotope:function(){if($post_fitRows.exists()){var $grid=$post_fitRows.imagesLoaded(function(){$grid.isotope({itemSelector:'.post-grid__item',layoutMode:'fitRows'});});}if($post_masonry.exists()){var $masonry=$post_masonry.imagesLoaded(function(){$masonry.isotope({itemSelector:'.post-grid__item',layoutMode:'masonry',percentPosition:true,masonry:{columnWidth:'.post-grid__item'}});});}if($team_album.exists()){var $masonry=$team_album.imagesLoaded(function(){$masonry.isotope({itemSelector:'.album__item',layoutMode:'masonry',percentPosition:true,masonry:{columnWidth:'.album__item'}});});}},SlickCarousel:function(){if($slick_featured_carousel.exists()){$slick_featured_carousel.slick({slidesToShow:3,slidesToScroll:1,autoplay:true,autoplaySpeed:5000,centerMode:true,centerPadding:0,responsive:[{breakpoint:992,settings:{arrows:false,centerMode:true,centerPadding:0,slidesToShow:3}},{breakpoint:768,settings:{arrows:false,centerMode:true,centerPadding:0,slidesToShow:2}},{breakpoint:480,settings:{arrows:false,centerMode:true,centerPadding:0,slidesToShow:1,dots:true}}]});};if($slick_video_carousel.exists()){$slick_video_carousel.slick({slidesToShow:4,slidesToScroll:1,infinite:true,autoplay:false,autoplaySpeed:5000,responsive:[{breakpoint:992,settings:{arrows:false,slidesToShow:3,infinite:true}},{breakpoint:768,settings:{arrows:false,slidesToShow:2,infinite:false}},{breakpoint:480,settings:{arrows:false,slidesToShow:1,infinite:false}}]});var filtered=false;$('.category-filter--carousel .category-filter__link').on('click',function(e){var category=$(this).data('category');$slick_video_carousel.slick('slickUnfilter');$slick_video_carousel.slick('slickFilter','.'+category);$('.category-filter--carousel .category-filter__link--active').removeClass('category-filter__link--active');$(this).addClass('category-filter__link--active');e.preventDefault();});$('.category-filter--carousel .category-filter__link--reset').on('click',function(e){$slick_video_carousel.slick('slickUnfilter');$('.category-filter--carousel .category-filter__link').removeClass('category-filter__link--active');$(this).addClass('category-filter__link--active');filtered=false;e.preventDefault();});};if($slick_featured_slider.exists()){$slick_featured_slider.slick({slidesToShow:1,slidesToScroll:1,autoplay:true,autoplaySpeed:5000,responsive:[{breakpoint:768,settings:{arrows:false}}]});var filtered=false;$('.category-filter--featured .category-filter__link').on('click',function(e){var category=$(this).data('category');$slick_featured_slider.slick('slickUnfilter');$slick_featured_slider.slick('slickFilter','.'+category);$('.category-filter--featured .category-filter__link--active').removeClass('category-filter__link--active');$(this).addClass('category-filter__link--active');e.preventDefault();});$('.category-filter--featured .category-filter__link--reset').on('click',function(e){$slick_featured_slider.slick('slickUnfilter');$('.category-filter--featured .category-filter__link').removeClass('category-filter__link--active');$(this).addClass('category-filter__link--active');filtered=false;e.preventDefault();});};if($slick_team_roster.exists()){$slick_team_roster.slick({centerMode:true,centerPadding:0,slidesToShow:3,autoplay:true,autoplaySpeed:3000,responsive:[{breakpoint:768,settings:{arrows:false,centerMode:true,centerPadding:0,slidesToShow:3}},{breakpoint:480,settings:{arrows:false,centerMode:true,centerPadding:0,slidesToShow:1}}]});};if($slick_awards.exists()){$slick_awards.slick({slidesToShow:1,arrows:true,dots:true,responsive:[{breakpoint:768,settings:{arrows:false}}]});};if($slick_player_info.exists()){$(window).on('load',function(){$slick_player_info.slick({slidesToShow:3,arrows:false,dots:false,infinite:false,variableWidth:true,responsive:[{breakpoint:992,settings:{slidesToShow:1,dots:true,variableWidth:false}}]});});};if($slick_product.exists()){$slick_product.slick({slidesToShow:1,arrows:false,dots:true,});};if($slick_product_soccer.exists()){$slick_product_soccer.slick({slidesToShow:1,slidesToScroll:1,arrows:false,asNavFor:'.product__slider-thumbs'});$('.product__slider-thumbs').slick({slidesToShow:3,slidesToScroll:1,asNavFor:$slick_product_soccer,focusOnSelect:true,vertical:true,});};if($slick_team_roster_card.exists()){$slick_team_roster_card.slick({slidesToShow:1,arrows:true,dots:false,responsive:[{breakpoint:992,settings:{arrows:false,}}]});};if($slick_hero_slider.exists()){$slick_hero_slider.slick({slidesToShow:1,slidesToScroll:1,arrows:false,fade:true,autoplay:true,autoplaySpeed:8000,asNavFor:'.hero-slider-thumbs',responsive:[{breakpoint:992,settings:{fade:false,}}]});$('.hero-slider-thumbs').slick({slidesToShow:3,slidesToScroll:1,asNavFor:$slick_hero_slider,focusOnSelect:true,});};},ContentFilter:function(){if($content_filter.exists()){$('.content-filter__toggle').on('click',function(e){e.preventDefault();$(this).toggleClass('content-filter__toggle--active');$('.content-filter__list').toggleClass('content-filter__list--expanded');});}},ChartJs:function(){if($chart_games_history.exists()){var data={type:'bar',data:{labels:["2010","2011","2012","2013","2014","2015"],datasets:[{label:'WON',data:[70,67,78,87,69,76],backgroundColor:"#ffdc11",},{label:'LOST',data:[40,45,36,28,43,35],backgroundColor:"#ff8429"}]},options:{legend:{display:false,labels:{boxWidth:8,fontSize:9,fontColor:'#31404b',fontFamily:'Montserrat, sans-serif',padding:20,}},tooltips:{backgroundColor:"rgba(49,64,75,0.8)",titleFontSize:0,titleSpacing:0,titleMarginBottom:0,bodyFontFamily:'Montserrat, sans-serif',bodyFontSize:9,bodySpacing:0,cornerRadius:2,xPadding:10,displayColors:false,},scales:{xAxes:[{barThickness:14,gridLines:{display:false,color:"rgba(255,255,255,0)",},ticks:{fontColor:'#9a9da2',fontFamily:'Montserrat, sans-serif',fontSize:10,},}],yAxes:[{gridLines:{display:false,color:"rgba(255,255,255,0)",},ticks:{beginAtZero:true,fontColor:'#9a9da2',fontFamily:'Montserrat, sans-serif',fontSize:10,padding:20}}]}},};var ctx=$chart_games_history;var gamesHistory=new Chart(ctx,data);document.getElementById('gamesHistoryLegend').innerHTML=gamesHistory.generateLegend();}if($chart_games_history_soccer.exists()){var data={type:'bar',data:{labels:["2010","2011","2012","2013","2014","2015"],datasets:[{label:'WON',data:[40,45,36,28,42,35],backgroundColor:"#c2ff1f",},{label:'LOST',data:[70,67,75,86,68,76],backgroundColor:"#38a9ff",}]},options:{legend:{display:false,labels:{boxWidth:8,fontSize:9,fontColor:'#31404b',fontFamily:'Montserrat, sans-serif',padding:20,}},tooltips:{backgroundColor:"rgba(49,64,75,0.8)",titleFontSize:0,titleSpacing:0,titleMarginBottom:0,bodyFontFamily:'Montserrat, sans-serif',bodyFontSize:9,bodySpacing:0,cornerRadius:2,xPadding:10,displayColors:false,},scales:{xAxes:[{stacked:true,barThickness:34,gridLines:{display:false,color:"rgba(255,255,255,0)",},ticks:{fontColor:'#9a9da2',fontFamily:'Montserrat, sans-serif',fontSize:10,},}],yAxes:[{stacked:true,gridLines:{display:false,color:"rgba(255,255,255,0)",},ticks:{fontColor:'#9a9da2',fontFamily:'Montserrat, sans-serif',fontSize:10,padding:20,}}]}},};var ctx=$chart_games_history_soccer;var gamesHistory=new Chart(ctx,data);document.getElementById('gamesHistoryLegendSoccer').innerHTML=gamesHistory.generateLegend();}if($chart_points_history.exists()){var data={type:'line',data:{labels:["Aug 8","Aug 15","Aug 21","Aug 28","Sep 4","Sep 19","Sep 26","Oct 3","Oct 10","Oct 16","Oct 23","Oct 30"],datasets:[{label:'POINTS',fill:false,lineTension:0,backgroundColor:"#ffdc11",borderWidth:2,borderColor:"#ffdc11",borderCapStyle:'butt',borderDashOffset:0.0,borderJoinStyle:'bevel',pointRadius:0,pointBorderWidth:0,pointHoverRadius:5,pointHoverBackgroundColor:"#fff",pointHoverBorderColor:"#ffcc00",pointHoverBorderWidth:5,pointHitRadius:10,data:[104,70,115,105,45,94,84,100,82,125,78,86,110],spanGaps:false,}]},options:{legend:{display:false,labels:{boxWidth:8,fontSize:9,fontColor:'#31404b',fontFamily:'Montserrat, sans-serif',padding:20,}},tooltips:{backgroundColor:"rgba(49,64,75,0.8)",titleFontSize:0,titleSpacing:0,titleMarginBottom:0,bodyFontFamily:'Montserrat, sans-serif',bodyFontSize:9,bodySpacing:0,cornerRadius:2,xPadding:10,displayColors:false,},scales:{xAxes:[{gridLines:{color:"#e4e7ed",},ticks:{fontColor:'#9a9da2',fontFamily:'Montserrat, sans-serif',fontSize:10,},}],yAxes:[{gridLines:{display:false,color:"rgba(255,255,255,0)",},ticks:{beginAtZero:true,fontColor:'#9a9da2',fontFamily:'Montserrat, sans-serif',fontSize:10,padding:20}}]}},};var ctx=$chart_points_history;var gamesHistory=new Chart(ctx,data);}if($chart_points_history_soccer.exists()){var data={type:'line',data:{labels:["Aug 8","Aug 15","Aug 21","Aug 28","Sep 4","Sep 19","Sep 26","Oct 3","Oct 10","Oct 16","Oct 23","Oct 30"],datasets:[{label:'PREV RECORD',fill:true,lineTension:0.5,backgroundColor:"rgba(194,255,31,0.8)",borderWidth:2,borderColor:"#c2ff1f",borderCapStyle:'butt',borderDashOffset:0.0,borderJoinStyle:'bevel',pointRadius:0,pointBorderWidth:0,pointHoverRadius:5,pointHoverBackgroundColor:"#fff",pointHoverBorderColor:"#c2ff1f",pointHoverBorderWidth:5,pointHitRadius:10,data:[52,71,40,53,62,40,44,58,38,64,46,70,75],spanGaps:false,},{label:'THIS YEAR',fill:true,lineTension:0.5,backgroundColor:"rgba(56,169,255,0.8)",borderWidth:2,borderColor:"#38a9ff",borderCapStyle:'butt',borderDashOffset:0.0,borderJoinStyle:'bevel',pointRadius:0,pointBorderWidth:0,pointHoverRadius:5,pointHoverBackgroundColor:"#fff",pointHoverBorderColor:"#38a9ff",pointHoverBorderWidth:5,pointHitRadius:10,data:[71,52,87,53,17,62,56,37,91,48,75,64,78],spanGaps:false,}]},options:{legend:{display:false,labels:{boxWidth:8,fontSize:9,fontColor:'#31404b',fontFamily:'Montserrat, sans-serif',padding:20,}},tooltips:{backgroundColor:"rgba(49,64,75,0.8)",titleFontSize:0,titleSpacing:0,titleMarginBottom:0,bodyFontFamily:'Montserrat, sans-serif',bodyFontSize:9,bodySpacing:0,cornerRadius:2,xPadding:10,displayColors:false,},scales:{xAxes:[{gridLines:{color:"#e4e7ed",},ticks:{fontColor:'#9a9da2',fontFamily:'Montserrat, sans-serif',fontSize:10,},}],yAxes:[{gridLines:{display:false,color:"rgba(255,255,255,0)",},ticks:{beginAtZero:true,fontColor:'#9a9da2',fontFamily:'Montserrat, sans-serif',fontSize:10,padding:20}}]}},};var ctx=$chart_points_history_soccer;var gamesHistory=new Chart(ctx,data);document.getElementById('gamesPoinstsLegendSoccer').innerHTML=gamesHistory.generateLegend();}if($chart_player_stats.exists()){var radar_data={type:'radar',data:{labels:["OFF","AST","3PT","2PT","DEF"],datasets:[{data:[72,25,40,72,50],backgroundColor:"rgba(255,220,17,0.8)",borderColor:"#ffdc11",pointBorderColor:"rgba(255,255,255,0)",pointBackgroundColor:"rgba(255,255,255,0)",pointBorderWidth:0}]},options:{legend:{display:false,},tooltips:{backgroundColor:"rgba(49,64,75,0.8)",titleFontSize:10,titleSpacing:2,titleMarginBottom:4,bodyFontFamily:'Montserrat, sans-serif',bodyFontSize:9,bodySpacing:0,cornerRadius:2,xPadding:10,displayColors:false,},scale:{angleLines:{color:"rgba(255,255,255,0.025)",},pointLabels:{fontColor:"#9a9da2",fontFamily:'Montserrat, sans-serif',},ticks:{beginAtZero:true,display:false,},gridLines:{color:"rgba(255,255,255,0.05)",lineWidth:2,},labels:{display:false}}},};var ctx=$chart_player_stats;var playerInfo=new Chart(ctx,radar_data);}},RangeSlider:function(){if($range_slider.exists()){var rangeSlider=document.getElementById('slider-range');noUiSlider.create(rangeSlider,{start:[0,350],connect:true,range:{'min':[0],'max':[450]}});var snapValues=[document.getElementById('slider-range-value-min'),document.getElementById('slider-range-value-max')];rangeSlider.noUiSlider.on('update',function(values,handle){snapValues[handle].innerHTML=values[handle];});}},miscScripts:function(){$('[data-toggle="tooltip"]').tooltip();[].slice.call(document.querySelectorAll('select.cs-select')).forEach(function(el){new SelectFx(el);});},};$(document).on('ready',function(){Core.initialize();});})(jQuery);
// Add your custom JS code here

//# sourceMappingURL=site.js.map
