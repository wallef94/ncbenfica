<?php

namespace NCBenfica\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'name',
    	'avatar',
    	'initials',
        'slug'
    ];

    public function tournament()
    {
        return $this->belongsToMany(Tournament::class);
    }

    public function player()
    {
        return $this->hasMany(Player::class, 'team_id', 'id');
    }
}
