<?php

namespace NCBenfica\Models;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $fillable = [
    	'name'
    ];

    public function tournament()
    {
        return $this->hasMany(Tournament::class, 'division_id', 'id');
    }
}
