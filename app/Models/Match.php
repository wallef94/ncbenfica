<?php

namespace NCBenfica\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Match extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'name',
    	'home_score',
    	'away_score',
    	'home_team_id',
    	'away_team_id',
    	'tournament_id'
    ];
}
