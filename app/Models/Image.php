<?php

namespace NCBenfica\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
    	'name',
    	'label',
    	'path',
        'dimensions',
    	'content_id'
    ];

    public function content()
    {
        return $this->belongsTo(Content::class, 'content_id');
    }
}
