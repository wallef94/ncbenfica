<?php

namespace NCBenfica\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'title',
    	'slug',
        'banner',
    	'summary',
    	'body',
    	'category_id'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function image()
    {
        return $this->hasMany(Image::class, 'content_id', 'id');
    }

    public function file()
    {
        return $this->hasMany(File::class, 'content_id', 'id');
    }
}
