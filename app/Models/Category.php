<?php

namespace NCBenfica\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'name',
    	'slug',
        'category_id'
    ];

    public function fatherCategory()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function childCategory()
    {
        return $this->hasMany(Category::class, 'category_id');
    }

    public function content()
    {
        return $this->hasMany(Content::class, 'category_id', 'id');
    }
}
