<?php

namespace NCBenfica\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Player extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'name',
    	'slug',
        'avatar',
    	'date_of_birth',
    	'first_year_korfball',
    	'first_year_ncb',
    	'previous_clubs',
    	'national_teams',
    	'shirt',
    	'gender',
    	'team_id'
    ];

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function setDateOfBirthAttribute($date)
    {
        return $this->attributes['date_of_birth'] = $date ? Carbon::createFromFormat('d/m/Y', $date) : null;
    }

    public function getDateOfBirthAttribute($date)
    {
        return !empty($date) ? Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y') : null;
    }

    public function setFirstYearKorfballAttribute($date)
    {
        return $this->attributes['first_year_korfball'] = $date ? Carbon::createFromFormat('d/m/Y', $date) : null;
    }

    public function getFirstYearKorfballAttribute($date)
    {
        return !empty($date) ? Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y') : null;
    }

    public function setFirstYearNcbAttribute($date)
    {
        return $this->attributes['first_year_ncb'] = $date ? Carbon::createFromFormat('d/m/Y', $date) : null;
    }

    public function getFirstYearNcbAttribute($date)
    {
        return !empty($date) ? Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y') : null;
    }
}
