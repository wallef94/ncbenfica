<?php

namespace NCBenfica\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coach extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'name',
    	'slug',
    	'date_of_birth',
    	'first_year_korfball',
    	'first_year_ncb',
    	'previous_clubs',
    	'national_teams',
        'team_id'
    ];
}
