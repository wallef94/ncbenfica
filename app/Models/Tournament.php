<?php

namespace NCBenfica\Models;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    protected $fillable = [
    	'name',
    	'slug',
    	'division_id'
    ];

    public function team()
    {
        return $this->belongsToMany(Team::class)->withPivot('points', 'victories', 'defeats', 'ties', 'goals_scored', 'goals_against');
    }

    public function addTeam(Team $team)
    {
        if (is_string($team)) {
            return $this->team()->save(
                Team::whereName($team)->firstOrFail()
            );
        }

        return $this->team()->save(
            Team::whereName($team->name)->firstOrFail()
        );
    }

    public function deleteTeam(Team $team)
    {
        if (is_string($team)) {
            return $this->team()->detach(
                Team::whereName($team)->firstOrFail()
            );
        }

        return $this->team()->detach($team);
    }

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id');
    }
}
