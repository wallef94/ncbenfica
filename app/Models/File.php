<?php

namespace NCBenfica\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
    	'name',
    	'label',
    	'path',
    	'content_id'
    ];

    public function content()
    {
        return $this->belongsTo(Content::class, 'content_id');
    }
}
