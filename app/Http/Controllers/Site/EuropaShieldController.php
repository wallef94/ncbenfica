<?php

namespace NCBenfica\Http\Controllers\Site;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use NCBenfica\Http\Requests;
use NCBenfica\Http\Controllers\Controller;
use NCBenfica\Models\Category;
use NCBenfica\Models\Content;
use NCBenfica\Models\Image;
use NCBenfica\Models\Team;

class EuropaShieldController extends Controller
{
    public function home()
    {
        $categories = Category::where('category_id', '=', 1)->get();

        $news = Content::whereHas('category', function ($query) {
            $query->where('category_id', '=', 1);
        })->latest()->take(10)->get();

        $group_a = DB::table('team_tournament')
            ->join('teams', 'teams.id', '=', 'team_tournament.team_id')
            ->orderBy('points', 'desc')
            ->where('tournament_id', '=', 6)
            ->get();

        $group_b = DB::table('team_tournament')
            ->join('teams', 'teams.id', '=', 'team_tournament.team_id')
            ->orderBy('points', 'desc')
            ->where('tournament_id', '=', 4)
            ->get();

        return view('site.europashield.home.main', compact('categories', 'group_a', 'group_b', 'news'));
    }

    public function contentList($category_id, $category)
    {
        $categories = Category::where('category_id', '=', 1)->get();
        $category = Category::findOrFail($category_id);

        $news = Content::where('category_id', '=', $category_id)->latest()->simplePaginate(8);

        $group_a = DB::table('team_tournament')
            ->join('teams', 'teams.id', '=', 'team_tournament.team_id')
            ->orderBy('points', 'desc')
            ->where('tournament_id', '=', 6)
            ->get();

        $group_b = DB::table('team_tournament')
            ->join('teams', 'teams.id', '=', 'team_tournament.team_id')
            ->orderBy('points', 'desc')
            ->where('tournament_id', '=', 4)
            ->get();

        return view('site.europashield.news.main', compact('categories', 'category', 'group_a', 'group_b', 'news'));
    }

    public function show($category_id, $category, $slug)
    {
        $categories = Category::where('category_id', '=', 1)->get();
        $category = Category::findOrFail($category_id);

        $news = Content::where('slug', '=', $slug)->latest()->first();
        $gallery = Image::where('content_id', '=', $news->id)->latest()->get();

        $group_a = DB::table('team_tournament')
            ->join('teams', 'teams.id', '=', 'team_tournament.team_id')
            ->orderBy('points', 'desc')
            ->where('tournament_id', '=', 6)
            ->get();

        $group_b = DB::table('team_tournament')
            ->join('teams', 'teams.id', '=', 'team_tournament.team_id')
            ->orderBy('points', 'desc')
            ->where('tournament_id', '=', 4)
            ->get();

        return view('site.europashield.news.show', compact('categories', 'category', 'group_a', 'group_b', 'news', 'gallery'));
    }

    public function schedule()
    {
        $categories = Category::where('category_id', '=', 1)->get();

        $group_a = DB::table('team_tournament')
            ->join('teams', 'teams.id', '=', 'team_tournament.team_id')
            ->orderBy('points', 'desc')
            ->where('tournament_id', '=', 6)
            ->get();

        $group_b = DB::table('team_tournament')
            ->join('teams', 'teams.id', '=', 'team_tournament.team_id')
            ->orderBy('points', 'desc')
            ->where('tournament_id', '=', 4)
            ->get();

        return view('site.europashield.schedule.main', compact('categories', 'group_a', 'group_b'));
    }

    public function standings()
    {
        $categories = Category::where('category_id', '=', 1)->get();

        $group_a = DB::table('team_tournament')
            ->join('teams', 'teams.id', '=', 'team_tournament.team_id')
            ->orderBy('points', 'desc')
            ->where('tournament_id', '=', 6)
            ->get();

        $group_b = DB::table('team_tournament')
            ->join('teams', 'teams.id', '=', 'team_tournament.team_id')
            ->orderBy('points', 'desc')
            ->where('tournament_id', '=', 4)
            ->get();

        return view('site.europashield.standings.main', compact('categories', 'group_a', 'group_b'));
    }
}
