<?php

namespace NCBenfica\Http\Controllers\Site;

use Illuminate\Http\Request;

use NCBenfica\Http\Requests;
use NCBenfica\Http\Controllers\Controller;

class SiteController extends Controller
{
    public function home()
    {
        return view('site.home.main');
    }
}
