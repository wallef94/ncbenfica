<?php

namespace NCBenfica\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use NCBenfica\Http\Requests;
use NCBenfica\Http\Controllers\Controller;
use NCBenfica\Models\Division;
use NCBenfica\Models\Team;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Equipas';
        $page_description = 'Lista de todas os equipas';
        $breadcrumb = array(['name' => 'Equipas']);

        $teams = Team::latest()->paginate(15);

        return view('admin.team.index', compact('teams', 'page_title', 'page_description', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Equipas';
        $page_description = 'Preencha o formulário abaixo para criar uma nova equipa';
        $breadcrumb = array(['name' => 'Equipas', 'link' => route('admin.team.index')], ['name' => 'Novo']);

        return view('admin.team.create', compact('divisions', 'page_title', 'page_description', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['slug'] = Str::slug($request['name']);

        $team = Team::create($request->all());

        return redirect()->route('admin.team.edit', $team->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = Team::findOrFail($id);

        $page_title = 'Equipa';
        $page_description = "Editar equipa: {$team->name}";
        $breadcrumb = array(['name' => 'Equipas', 'link' => route('admin.team.index')], ['name' => 'Editar']);

        return view('admin.team.edit', compact('team', 'divisions', 'page_title', 'page_description', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request['slug'] = Str::slug($request['name']);

        $team = Team::findOrFail($id);

        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $new_name = "{$team->slug}.{$avatar->getClientOriginalExtension()}";

            $image = Image::make($avatar)->resize(500, 500);
            $image->resize(500, 500, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path("uploads/cms/team/{$team->id}/avatar/{$new_name}"));

            $team->avatar = $new_name;
            $team->save();

            return redirect()->back();
        }

        $team->update($request->all());

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Team::find($id)->delete();

        return redirect()->back();
    }

    public function updateAvatar(Request $request, $id)
    {
        $team = Team::findOrFail($id);

        $image = $this->upload($request->file('avatar'), $team->id);

        $team->update($image);

        return redirect()->back();
    }

    public function upload($file, $team_id)
    {
        $image_path = public_path("uploads/cms/team/{$team_id}/avatar");

        if (!is_dir($image_path . "/thumbs")) {
            mkdir($image_path . "/thumbs", 0777, true);
        }

        $team = Team::findOrFail($team_id);

        $new_name = "{$team->slug}.{$file->getClientOriginalExtension()}";

        $image = Image::make($file->getRealPath());

        $image->resize(1024, 1024, function ($constraint) {
            $constraint->aspectRatio();
        })->save("{$image_path}/{$new_name}");

        $image->resize(300, 300, function ($constraint) {
            $constraint->aspectRatio();
        })->save("{$image_path}/thumbs/{$new_name}");

        return ['avatar' => $new_name];
    }
}
