<?php

namespace NCBenfica\Http\Controllers\Admin;

use Illuminate\Http\Request;

use NCBenfica\Http\Requests;
use NCBenfica\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function dashboard()
    {
        $page_title = 'Dashboard';
        $page_description = 'Núcleo de Corfebol de Benfica';

        return view('admin.dashboard', compact('page_title', 'page_description'));
    }
}
