<?php

namespace NCBenfica\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Intervention\Image\Facades\Image;
use NCBenfica\Http\Requests;
use NCBenfica\Http\Controllers\Controller;
use NCBenfica\Models\Content;

class BannerController extends Controller
{
    public function index($content_id)
    {
        $page_title = 'Banner do Conteúdo';
        $page_description = 'Insira o seu banner';
        $breadcrumb = array(['name' => 'Conteúdos', 'link' =>  route('admin.content.index')], ['name' => 'Banner do Conteúdo']);

        $content = Content::findOrFail($content_id);

        $banner = !empty($content->banner) ? $content->banner : null;

        return view('admin.content.banner.index', compact('content', 'banner', 'page_title', 'page_description', 'breadcrumb'));
    }

    public function store(Request $request, $content_id)
    {
        $image = $this->upload($request->file('banner'), $content_id);

        Content::findOrFail($content_id)->update($image);

        return redirect()->back();
    }

    public function destroy($content_id)
    {
        $delete = ['banner' => null];

        $content = Content::findOrFail($content_id);

        if (file_exists(public_path("uploads/cms/content/{$content->id}/banner/{$content->banner}"))) {
            unlink(public_path("uploads/cms/content/{$content->id}/banner/{$content->banner}"));
        }

        if (file_exists(public_path("uploads/cms/content/{$content->id}/banner/thumbs/{$content->banner}"))) {
            unlink(public_path("uploads/cms/content/{$content->id}/banner/thumbs/{$content->banner}"));
        }

        rmdir(public_path("uploads/cms/content/{$content->id}/banner/thumbs"));
        rmdir(public_path("uploads/cms/content/{$content->id}/banner"));

        $content->update($delete);

        return redirect()->back();
    }

    public function upload($file, $content_id)
    {
        $image_path = public_path("uploads/cms/content/{$content_id}/banner");

        if (!is_dir($image_path . "/thumbs")) {
            mkdir($image_path . "/thumbs", 0777, true);
        }
        $content = Content::findOrFail($content_id);

        $new_name = "{$content->slug}.{$file->getClientOriginalExtension()}";

        $image = Image::make($file);

        $image->resize(1024, 1024, function ($constraint) {
            $constraint->aspectRatio();
        })->save("{$image_path}/{$new_name}");

        $image->resize(300, 300, function ($constraint) {
            $constraint->aspectRatio();
        })->save("{$image_path}/thumbs/{$new_name}");

        return ['banner' => $new_name];
    }
}
