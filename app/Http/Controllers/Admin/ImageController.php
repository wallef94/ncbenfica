<?php

namespace NCBenfica\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Response;
use NCBenfica\Http\Requests;
use NCBenfica\Http\Controllers\Controller;
use NCBenfica\Models\Content;
use NCBenfica\Models\Image;
use Intervention\Image\Facades\Image as Intervention;

class ImageController extends Controller
{
    private $path = "uploads/cms/content/{content_id}/gallery/";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($content_id)
    {
        $page_title = 'Galeria do conteúdo';
        $page_description = 'Veja abaixo toda a galeria';
        $breadcrumb = array(['name' => 'Conteúdos', 'link' => route('admin.content.index')], ['name' => 'Galeria de Imagens']);

        $gallery = Image::with('content')->where('content_id', '=', $content_id)->latest()->paginate(10);

        return view('admin.content.gallery.index', compact('gallery', 'page_title', 'page_description', 'breadcrumb', 'content_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $content_id)
    {
        $images = $this->upload($request->file('image'), $content_id);

        foreach ($images as $image)
        {
            $gallery = new Image();
            $gallery->name = $image['name'];
            $gallery->label = $image['name'];
            $gallery->path = $image['path'];
            $gallery->dimensions = $image['dimensions'];
            $gallery->content_id = $content_id;
            $gallery->save();
        }

        return Response::json('success', 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $content_id, $id) {
        Image::findOrFail($id)->update($request->all());

        return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($content_id, $id)
    {
        $gallery = Image::find($id);

        $gallery->delete();

        $this->unlinkImage($gallery, $content_id);

        return redirect()->back();
    }

    public function upload($files, $content_id)
    {
        $content = Content::findOrFail($content_id);

        $path = str_replace("{content_id}", $content_id, $this->path);
        $images = [];

        if (!is_dir($path . "/thumbs")) {
            mkdir($path . "/thumbs", 0777, true);
        }

        foreach ($files as $file) {
            $hash = substr(md5(uniqid(time(), true)), 0, 6);
            $filename = "{$content->slug}-{$hash}.{$file->getClientOriginalExtension()}";

            $image = Intervention::make($file);

            $image->resize(1024, 1024, function ($constraint) {
                $constraint->aspectRatio();
            })->save("{$path}/{$filename}", 70);

            $temp = ['name' => $filename, 'path' => "{$path}{$filename}", 'dimensions' => "{$image->width()}x{$image->height()}"];
            array_push($images, $temp);

            $image->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save("{$path}/thumbs/{$filename}", 70);
        }

        return $images;
    }

    public function unlinkImage(Image $image, $content_id)
    {
        $path = str_replace("{content_id}", $content_id, $this->path);

        if (file_exists(public_path("{$path}{$image->name}")))
            unlink(public_path("{$path}{$image->name}"));

        if (file_exists(public_path("{$path}thumbs/{$image->name}")))
            unlink(public_path("{$path}thumbs/{$image->name}"));
    }
}
