<?php

namespace NCBenfica\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Str;
use NCBenfica\Http\Requests;
use NCBenfica\Http\Controllers\Controller;
use NCBenfica\Models\Player;
use NCBenfica\Models\Team;

class PlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Jogadores';
        $page_description = 'Lista de todos os jogadores';
        $breadcrumb = array(['name' => 'Jogadores']);

        $players = Player::with('team')->latest()->paginate(15);

        return view('admin.player.index', compact('players', 'page_title', 'page_description', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Jogadores';
        $page_description = 'Preencha o formulário abaixo para criar um novo jogador';
        $breadcrumb = array(['name' => 'Jogadores', 'link' => route('admin.player.index')], ['name' => 'Novo']);

        $teams = Team::lists('name', 'id');

        return view('admin.player.create', compact('teams', 'page_title', 'page_description', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['team_id'] = (empty($request['team_id']) ? null : $request['team_id']);
        $request['slug'] = Str::slug($request['name']);

        $player = Player::create($request->all());

        return redirect()->route('admin.player.edit', $player->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $player = Player::findOrFail($id);
        $teams = Team::lists('name', 'id');

        $page_title = 'Conteúdo';
        $page_description = "Editar jogador: {$player->title}";
        $breadcrumb = array(['name' => 'Jogadores', 'link' => route('admin.player.index')], ['name' => 'Editar']);

        return view('admin.player.edit', compact('player', 'teams', 'page_title', 'page_description', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request['team_id'] = (empty($request['team_id']) ? null : $request['team_id']);
        $request['slug'] = Str::slug($request['name']);

        Player::findOrFail($id)->update($request->all());

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Player::find($id)->delete();

        return redirect()->back();
    }
}
