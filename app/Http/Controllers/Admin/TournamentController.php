<?php

namespace NCBenfica\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use NCBenfica\Http\Requests;
use NCBenfica\Http\Controllers\Controller;
use NCBenfica\Models\Division;
use NCBenfica\Models\Team;
use NCBenfica\Models\Tournament;

class TournamentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Campeonatos e Torneios';
        $page_description = 'Lista de todos os campeonatos e torneios';
        $breadcrumb = array(['name' => 'Campeonatos e Torneios']);

        $tournaments = Tournament::with('division')->latest()->paginate(15);

        return view('admin.tournament.index', compact('tournaments', 'page_title', 'page_description', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Campeonatos e Torneios';
        $page_description = 'Preencha o formulário abaixo para criar um novo torneio';
        $breadcrumb = array(['name' => 'Campeonatos e Torneios', 'link' => route('admin.tournament.index')], ['name' => 'Novo']);

        $divisions = Division::lists('name', 'id');

        return view('admin.tournament.create', compact('divisions', 'page_title', 'page_description', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['slug'] = Str::slug($request['name']);

        Tournament::create($request->all());

        return redirect()->route('admin.tournament.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tournament = Tournament::findOrFail($id);

        $page_title = 'Campeonatos e Torneios';
        $page_description = "Editar torneio: {$tournament->name}";
        $breadcrumb = array(['name' => 'Campeonatos e Torneios', 'link' => route('admin.tournament.index')], ['name' => 'Editar']);

        $divisions = Division::lists('name', 'id');

        return view('admin.tournament.edit', compact('tournament', 'divisions', 'page_title', 'page_description', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request['slug'] = Str::slug($request['name']);

        Tournament::findOrFail($id)->update($request->all());

        return redirect()->route('admin.tournament.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tournament::find($id)->delete();

        return redirect()->back();
    }

    public function teamList($id)
    {
        $tournament = Tournament::find($id);

        $page_title = 'Equipas do torneio';
        $page_description = "Gerenciar as equipas do torneio: {$tournament->name}";
        $breadcrumb = array(['name' => 'Campeonatos e Torneios', 'link' => route('admin.tournament.index')], ['name' => 'Equipas']);

        $teams = $this->getNotRelatedTeams($tournament->id);

        return view('admin.tournament.team', compact('tournament', 'teams', 'page_title', 'page_description', 'breadcrumb', 'standings'));
    }

    public function storeTeam(Request $request, $id)
    {
        $tournament = Tournament::find($id);
        $team = Team::findOrFail($request->all()['team_id']);
        $tournament->addTeam($team);

        return redirect()->back();
    }

    public function destroyTeam($id, $team_id)
    {
        $tournament = Tournament::find($id);
        $team = Team::findOrFail($team_id);
        $tournament->deleteTeam($team);

        return redirect()->back();
    }

    private function getNotRelatedTeams($tournament_id)
    {
        $team_tournament = DB::table('team_tournament')->where('tournament_id', '=', $tournament_id)->lists('team_id');

        $teams = Team::lists('name', 'id')->except($team_tournament);

        return $teams;
    }
}
