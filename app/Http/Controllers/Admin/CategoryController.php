<?php

namespace NCBenfica\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Str;
use NCBenfica\Http\Requests;
use NCBenfica\Http\Controllers\Controller;
use NCBenfica\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Categorias';
        $page_description = 'Lista de todas as categorias';
        $breadcrumb = array(['name' => 'Categorias']);

        $categories = Category::with('fatherCategory')->latest()->paginate(15);

        return view('admin.category.index', compact('page_title', 'page_description', 'breadcrumb', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Categorias';
        $page_description = 'Preencha o formulário abaixo para criar uma nova categoria';
        $breadcrumb = array(['name' => 'Categorias', 'link' => route('admin.category.index')], ['name' => 'Novo']);

        $categories = Category::lists('name', 'id');

        return view('admin.category.create', compact('page_title', 'page_description', 'breadcrumb', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['category_id'] = (empty($request['category_id']) ? null : $request['category_id']);
        $request['slug'] = Str::slug($request['name']);

        Category::create($request->all());

        return redirect()->route('admin.category.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        $categories = Category::where('id', '!=', $id)->get()->lists('name', 'id');

        $page_title = 'Categorias';
        $page_description = "Editar categoria: {$category->name}";
        $breadcrumb = array(['name' => 'Categorias', 'link' => route('admin.category.index')], ['name' => 'Editar']);

        return view('admin.category.edit', compact('category', 'categories', 'page_title', 'page_description', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request['category_id'] = (empty($request['category_id']) ? null : $request['category_id']);
        $request['slug'] = Str::slug($request['name']);

        Category::findOrFail($id)->update($request->all());

        return redirect()->route('admin.category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::find($id)->delete();

        return redirect()->back();
    }
}
