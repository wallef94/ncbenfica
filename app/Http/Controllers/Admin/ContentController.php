<?php

namespace NCBenfica\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Str;
use NCBenfica\Http\Requests;
use NCBenfica\Http\Controllers\Controller;
use NCBenfica\Models\Category;
use NCBenfica\Models\Content;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Conteúdos';
        $page_description = 'Lista de todos os conteúdos';
        $breadcrumb = array(['name' => 'Conteúdos']);

        $contents = Content::with('category')->latest()->paginate(15);

        return view('admin.content.index', compact('contents', 'page_title', 'page_description', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Conteúdos';
        $page_description = 'Preencha o formulário abaixo para criar um novo conteúdo';
        $breadcrumb = array(['name' => 'Conteúdos', 'link' => route('admin.content.index')], ['name' => 'Novo']);

        $categories = Category::lists('name', 'id');

        return view('admin.content.create', compact('categories', 'page_title', 'page_description', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['category_id'] = (empty($request['category_id']) ? null : $request['category_id']);
        $request['slug'] = Str::slug($request['title']);

        $content = Content::create($request->all());

        return redirect()->route('admin.content.edit', $content->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $content = Content::findOrFail($id);
        $categories = Category::lists('name', 'id');

        $page_title = 'Conteúdo';
        $page_description = "Editar conteúdo: {$content->title}";
        $breadcrumb = array(['name' => 'Conteúdos', 'link' => route('admin.content.index')], ['name' => 'Editar']);

        return view('admin.content.edit', compact('content', 'categories', 'page_title', 'page_description', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request['category_id'] = (empty($request['category_id']) ? null : $request['category_id']);
        $request['slug'] = Str::slug($request['title']);

        Content::findOrFail($id)->update($request->all());

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Content::find($id)->delete();

        return redirect()->back();
    }
}
