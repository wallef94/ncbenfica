<?php

//Route::auth();
//Authentication Routes...
Route::get('login', 'Auth\AuthController@showLoginForm');
Route::post('login', 'Auth\AuthController@login');
Route::get('logout', 'Auth\AuthController@logout');

//Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');

// Admin route group.
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {
    Route::get('', ['as' => 'admin.dashboard', 'uses' => 'Admin\AdminController@dashboard']);

    // Category CRUD.
    Route::resource('category', 'Admin\CategoryController', [
        'except' => ['show', 'destroy'],
        'parameters' => ['category' => 'id']
    ]);
    Route::get('category/{id}', ['as' => 'admin.category.destroy', 'uses' => 'Admin\CategoryController@destroy']);

    // Content CRUD.
    Route::resource('content', 'Admin\ContentController', [
        'except' => ['show', 'destroy'],
        'parameters' => ['content' => 'id']
    ]);
    Route::get('content/{id}', ['as' => 'admin.content.destroy', 'uses' => 'Admin\ContentController@destroy']);

    // Gallery CRUD
    Route::group(['prefix' => 'content/{content_id}/gallery', 'as' => 'admin.content.gallery.'], function () {
        Route::get('', ['as' => 'index', 'uses' => 'Admin\ImageController@index']);
        Route::post('store', ['as' => 'store', 'uses' => 'Admin\ImageController@store']);
        Route::post('update/{id}', ['as' => 'update', 'uses' => 'Admin\ImageController@update']);
        Route::get('destroy/{id}', ['as' => 'destroy', 'uses' => 'Admin\ImageController@destroy']);
    });

    // Banner CRUD
    Route::group(['prefix' => 'content/{content_id}/banner', 'as' => 'admin.content.banner.'], function () {
        Route::get('', ['as' => 'index', 'uses' => 'Admin\BannerController@index']);
        Route::post('store', ['as' => 'store', 'uses' => 'Admin\BannerController@store']);
        Route::get('destroy', ['as' => 'destroy', 'uses' => 'Admin\BannerController@destroy']);
    });

    // Team CRUD.
    Route::resource('team', 'Admin\TeamController', [
        'except' => ['show', 'destroy'],
        'parameters' => ['team' => 'id']
    ]);
    Route::get('team/{id}', ['as' => 'admin.team.destroy', 'uses' => 'Admin\TeamController@destroy']);
    Route::post('updateAvatar/{id}', ['as' => 'admin.team.updateAvatar', 'uses' => 'Admin\TeamController@updateAvatar']);

    // Tournament CRUD
    Route::resource('tournament', 'Admin\TournamentController', [
        'except' => ['show', 'destroy'],
        'parameters' => ['tournament' => 'id']
    ]);
    Route::get('tournament/{id}', ['as' => 'admin.tournament.destroy', 'uses' => 'Admin\TournamentController@destroy']);
    Route::get('tournament/{id}/team', ['as' => 'admin.tournament.team', 'uses' => 'Admin\TournamentController@teamList']);
    Route::post('tournament/{id}/team/store', ['as' => 'admin.tournament.team.store', 'uses' => 'Admin\TournamentController@storeTeam']);
    Route::get('tournament/{id}/team/destroy/{team_id}', ['as' => 'admin.tournament.team.destroy', 'uses' => 'Admin\TournamentController@destroyTeam']);

    // Team CRUD.
    Route::resource('player', 'Admin\PlayerController', [
        'except' => ['show', 'destroy'],
        'parameters' => ['player' => 'id']
    ]);
    Route::get('player/{id}', ['as' => 'admin.player.destroy', 'uses' => 'Admin\PlayerController@destroy']);
});

// Site route group.
Route::group(['prefix' => '/', 'as' => 'site.', 'middleware' => 'web'], function() {
    Route::get('', ['as' => 'home', 'uses' => 'Site\SiteController@home']);

    Route::group(['prefix' => 'europashield2017', 'as' => 'europashield.'], function () {
        Route::get('', ['as' => 'home','uses' => 'Site\EuropaShieldController@home']);
        Route::get('content/{category_id}/{category}', ['as' => 'content.list', 'uses' => 'Site\EuropaShieldController@contentList']);
        Route::get('content/{category_id}/{category}/{slug}', ['as' => 'content.show', 'uses' => 'Site\EuropaShieldController@show']);
        Route::get('schedule', ['as' => 'schedule', 'uses' => 'Site\EuropaShieldController@schedule']);
        Route::get('standings', ['as' => 'standings', 'uses' => 'Site\EuropaShieldController@standings']);
    });
});