var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    // Admin CSS
    mix.styles([
        '../../../node_modules/admin-lte/bootstrap/css/bootstrap.css',
        '../../../node_modules/font-awesome/css/font-awesome.css',
        '../../../node_modules/admin-lte/plugins/pace/pace.css',
        '../../../node_modules/admin-lte/plugins/iCheck/square/red.css',
        '../../../node_modules/admin-lte/plugins/select2/select2.css',
        '../../../node_modules/photoswipe/dist/photoswipe.css',
        '../../../node_modules/photoswipe/dist/default-skin/default-skin.css',
        '../../../node_modules/dropzone/dist/dropzone.css',
        '../../../node_modules/admin-lte/dist/css/AdminLTE.css',
        '../../../node_modules/admin-lte/dist/css/skins/skin-red.css'
    ],  'public/css/app.css');

    // Admin Scripts
    mix.scripts([
        '../../../node_modules/jquery/dist/jquery.js',
        '../../../node_modules/admin-lte/bootstrap/js/bootstrap.js',
        '../../../node_modules/admin-lte/dist/js/app.js',
        '../../../node_modules/admin-lte/plugins/pace/pace.js',
        '../../../node_modules/admin-lte/plugins/iCheck/icheck.min.js',
        '../../../node_modules/admin-lte/plugins/select2/select2.full.js',
        '../../../node_modules/admin-lte/plugins/jQueryUI/jquery-ui.js',
        '../../../node_modules/admin-lte/plugins/input-mask/jquery.inputmask.js',
        '../../../node_modules/bootstrap-validator/dist/validator.js',
        '../../../node_modules/photoswipe/dist/photoswipe.js',
        '../../../node_modules/photoswipe/dist/photoswipe-ui-default.js',
        '../../../node_modules/dropzone/dist/dropzone.js'

    ],  'public/js/app.js');

    // Site CSS
    mix.styles([
        'preloader.css',
        '../../../node_modules/admin-lte/bootstrap/css/bootstrap.css',
        '../../../node_modules/font-awesome/css/font-awesome.css',
        '../../../node_modules/simple-line-icons/css/simple-line-icons.css',
        '../../../node_modules/magnific-popup/dist/magnific-popup.css',
        '../../../node_modules/slick-carousel/slick/slick.css',
        'content.css',
        'components.css',
        'style.css',
        'custom.css'
    ], 'public/css/site.css');

    // Site Scripts
    mix.scripts([
        '../../../node_modules/jquery/dist/jquery.js',
        'core-min.js',
        'jquery.twitter.js',
        'init.js',
        'custom.js'
    ], 'public/js/site.js');

    mix.copy([
        'node_modules/font-awesome/fonts',
        'node_modules/admin-lte/bootstrap/fonts',
        'node_modules/simple-line-icons/fonts',
        'node_modules/slick-carousel/slick/fonts'
    ],  'public/fonts');

    mix.copy('node_modules/admin-lte/dist/img', 'public/img');
    mix.copy('node_modules/admin-lte/plugins/iCheck/square/red.png', 'public/css');
    mix.copy('node_modules/photoswipe/dist/default-skin/default-skin.png', 'public/css');
});
